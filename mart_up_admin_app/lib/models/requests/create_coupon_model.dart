// To parse this JSON data, do
//
//     final couponModel = couponModelFromJson(jsonString);

import 'dart:convert';

List<CouponModel> couponModelFromJson(String str) => List<CouponModel>.from(json.decode(str).map((x) => CouponModel.fromJson(x)));

String couponModelToJson(List<CouponModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CouponModel {
    CouponModel({
        this.id,
        this.code,
        this.amount,
        this.discountType,
        this.description,
        this.dateExpires,
        this.usageLimit,
        this.usageLimitPerUser,
        this.limitUsageToXItems,
        this.minimumAmount,
        this.maximumAmount,
    });

    int? id;
    String? code;
    String? amount;
    String? discountType;
    String? description;
    String? dateExpires;
    int? usageLimit;
    int? usageLimitPerUser;
    int? limitUsageToXItems;
    String? minimumAmount;
    String? maximumAmount;

    factory CouponModel.fromJson(Map<String, dynamic> json) => CouponModel(
        id: json["id"],
        code: json["code"],
        amount: json["amount"],
        discountType: json["discount_type"],
        description: json["description"],
        dateExpires: json["date_expires"],
        usageLimit: json["usage_limit"],
        usageLimitPerUser: json["usage_limit_per_user"],
        limitUsageToXItems: json["limit_usage_to_x_items"],
        minimumAmount: json["minimum_amount"],
        maximumAmount: json["maximum_amount"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "amount": amount,
        "discount_type": discountType,
        "description": description,
        "date_expires": dateExpires,
        "usage_limit": usageLimit,
        "usage_limit_per_user": usageLimitPerUser,
        "limit_usage_to_x_items": limitUsageToXItems,
        "minimum_amount": minimumAmount,
        "maximum_amount": maximumAmount,
    };
}
