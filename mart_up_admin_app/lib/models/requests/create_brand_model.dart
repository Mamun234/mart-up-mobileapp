// To parse this JSON data, do
//
//     final brandCreateModel = brandCreateModelFromJson(jsonString);

import 'dart:convert';

List<BrandCreateModel> brandCreateModelFromJson(String str) => List<BrandCreateModel>.from(json.decode(str).map((x) => BrandCreateModel.fromJson(x)));

String brandCreateModelToJson(List<BrandCreateModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BrandCreateModel {
    BrandCreateModel({
        this.id,
        this.name,
        this.slug,
        this.description,
    });

    int? id;
    String? name;
    String? slug;
    String? description;

    factory BrandCreateModel.fromJson(Map<String, dynamic> json) => BrandCreateModel(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        description: json["description"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "description": description,
    };
}
