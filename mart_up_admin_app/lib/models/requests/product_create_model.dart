// To parse this JSON data, do
//
//     final productCreateModel = productCreateModelFromJson(jsonString);

import 'dart:convert';

List<ProductCreateModel> productCreateModelFromJson(String str) => List<ProductCreateModel>.from(json.decode(str).map((x) => ProductCreateModel.fromJson(x)));

String productCreateModelToJson(List<ProductCreateModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductCreateModel {
    ProductCreateModel({
        this.id,
        this.name,
        this.slug,
        this.description,
        this.shortDescription,
        this.price,
        this.regularPrice,
        this.salePrice,
        this.dateOnSaleFrom,
        this.dateOnSaleTo,
         this.categories,
         this.images,
         this.relatedIds,
         this.attributes,
    });

    int? id;
    String? name;
    String? slug;
    String? description;
    String? shortDescription;
    String? price;
    String? regularPrice;
    String? salePrice;
    String? dateOnSaleFrom;
    String? dateOnSaleTo;
    List<Category>? categories=[];
    List<Image>? images=[];
    List<int>? relatedIds;
    List<Attribute>? attributes=[];

    factory ProductCreateModel.fromJson(Map<String, dynamic> json) => ProductCreateModel(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        description: json["description"],
        shortDescription: json["short_description"],
        price: json["price"],
        regularPrice: json["regular_price"],
        salePrice: json["sale_price"],
        dateOnSaleFrom: json["date_on_sale_from"],
        dateOnSaleTo: json["date_on_sale_to"],
        categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
        images: List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
        relatedIds: List<int>.from(json["related_ids"].map((x) => x)),
        attributes: List<Attribute>.from(json["attributes"].map((x) => Attribute.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "description": description,
        "short_description": shortDescription,
        "price": price,
        "regular_price": regularPrice,
        "sale_price": salePrice,
        "date_on_sale_from": dateOnSaleFrom,
        "date_on_sale_to": dateOnSaleTo,
        "categories": List<dynamic>.from(categories!.map((x) => x.toJson())),
        "images": List<dynamic>.from(images!.map((x) => x.toJson())),
        "related_ids": List<dynamic>.from(relatedIds!.map((x) => x)),
        "attributes": List<dynamic>.from(attributes!.map((x) => x.toJson())),
    };
}

class Attribute {
    Attribute({
        this.name,
        this.options,
    });

    String? name;
    List<String>? options;

    factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
        name: json["name"],
        options: List<String>.from(json["options"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "options": List<dynamic>.from(options!.map((x) => x)),
    };
}

class Category {
    Category({
        this.id,
    });

    int? id;

    factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
    };
}

class Image {
    Image({
        this.src,
    });

    String? src;

    factory Image.fromJson(Map<String, dynamic> json) => Image(
        src: json["src"],
    );

    Map<String, dynamic> toJson() => {
        "src": src,
    };
}
