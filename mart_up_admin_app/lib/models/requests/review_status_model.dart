// To parse this JSON data, do
//
//     final reviewStatusModel = reviewStatusModelFromJson(jsonString);

import 'dart:convert';

List<ReviewStatusModel> reviewStatusModelFromJson(String str) => List<ReviewStatusModel>.from(json.decode(str).map((x) => ReviewStatusModel.fromJson(x)));

String reviewStatusModelToJson(List<ReviewStatusModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ReviewStatusModel {
    ReviewStatusModel({
        this.status,
    });

    String? status;

    factory ReviewStatusModel.fromJson(Map<String, dynamic> json) => ReviewStatusModel(
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
    };
}
