// // // To parse this JSON data, do
// // //
// // //     final categoryCreateModel = categoryCreateModelFromJson(jsonString);

// // import 'dart:convert';

// // List<CategoryCreateModel> categoryCreateModelFromJson(String str) => List<CategoryCreateModel>.from(json.decode(str).map((x) => CategoryCreateModel.fromJson(x)));

// // String categoryCreateModelToJson(List<CategoryCreateModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// // class CategoryCreateModel {
// //     CategoryCreateModel({
// //         this.name,
// //         this.slug,
// //         this.description,
// //         this.parent,
// //         this.src,
// //     });

// //     String? name;
// //     String? slug;
// //     String? description;
// //     int? parent;
// //     src? src;

// //     factory CategoryCreateModel.fromJson(Map<String, dynamic> json) => CategoryCreateModel(
// //         name: json["name"],
// //         slug: json["slug"],
// //         description: json["description"],
// //         parent: json["parent"],
// //         src: src.fromJson(json["src"]),
// //     );

// //     Map<String, dynamic> toJson() => {
// //         "name": name,
// //         "slug": slug,
// //         "description": description,
// //         "parent": parent,
// //         "src": src!.toJson(),
// //     };
// // }

// // class src {
// //     src({
// //         this.src,
// //     });

// //     String? src;

// //     factory src.fromJson(Map<String, dynamic> json) => src(
// //         src: json["src"],
// //     );

// //     Map<String, dynamic> toJson() => {
// //         "src": src,
// //     };
// // }

// // To parse this JSON data, do
// //
// //     final categoryCreateModel = categoryCreateModelFromJson(jsonString);

// import 'dart:convert';

// List<CategoryCreateModel> categoryCreateModelFromJson(String str) => List<CategoryCreateModel>.from(json.decode(str).map((x) => CategoryCreateModel.fromJson(x)));

// String categoryCreateModelToJson(List<CategoryCreateModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class CategoryCreateModel {
//     CategoryCreateModel({
//         this.name,
//         this.slug,
//         this.description,
//         this.parent,
//         this.src,
//     });

//     String? name;
//     String? slug;
//     String? description;
//     int? parent;
//     String? src;

//     factory CategoryCreateModel.fromJson(Map<String, dynamic> json) => CategoryCreateModel(
//         name: json["name"],
//         slug: json["slug"],
//         description: json["description"],
//         parent: json["parent"],
//         src: json["src"],
//     );

//     Map<String, dynamic> toJson() => {
//         "name": name,
//         "slug": slug,
//         "description": description,
//         "parent": parent,
//         "src": src,
//     };
// }

// To parse this JSON data, do
//
//     final categoryCreateModel = categoryCreateModelFromJson(jsonString);

import 'dart:convert';

List<CategoryCreateModel> categoryCreateModelFromJson(String str) =>
    List<CategoryCreateModel>.from(
        json.decode(str).map((x) => CategoryCreateModel.fromJson(x)));

String categoryCreateModelToJson(List<CategoryCreateModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryCreateModel {
  CategoryCreateModel({
    this.id,
    this.name,
    this.slug,
    this.description,
    this.parent,
    this.image,
  });
  int? id;
  String? name;
  String? slug;
  String? description;
  int? parent;
  Image? image;

  factory CategoryCreateModel.fromJson(Map<String, dynamic> json) =>
      CategoryCreateModel(
        name: json["name"],
        slug: json["slug"],
        description: json["description"],
        parent: json["parent"],
        image: Image.fromJson(json["image"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "slug": slug,
        "description": description,
        "parent": parent,
        "image": image!.toJson(),
      };
}

class Image {
  Image({
    this.src,
  });

  String? src;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        src: json["src"],
      );

  Map<String, dynamic> toJson() => {
        "src": src,
      };
}
