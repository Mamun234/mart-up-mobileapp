

import 'dart:convert';

AdminLoginModel adminLoginModelFromJson(String str) => AdminLoginModel.fromJson(json.decode(str));

String adminLoginModelToJson(AdminLoginModel data) => json.encode(data.toJson());

class AdminLoginModel {
    AdminLoginModel({
        this.username,
        this.password,
    });

    String? username;
    String? password;

    factory AdminLoginModel.fromJson(Map<String, dynamic> json) => AdminLoginModel(
        username: json["username"],
        password: json["password"],
    );

    Map<String, dynamic> toJson() => {
        "username": username,
        "password": password,
    };
}
