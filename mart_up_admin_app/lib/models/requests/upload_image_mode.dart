// // To parse this JSON data, do
// //
// //     final uploadImageModel = uploadImageModelFromJson(jsonString);

// import 'dart:convert';

// List<UploadImageModel> uploadImageModelFromJson(String str) => List<UploadImageModel>.from(json.decode(str).map((x) => UploadImageModel.fromJson(x)));

// String uploadImageModelToJson(List<UploadImageModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class UploadImageModel {
//     UploadImageModel({
//         this.images,
//     });

//     Image? images;

//     factory UploadImageModel.fromJson(Map<String, dynamic> json) => UploadImageModel(
//         images: Image.fromJson(json["image"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "image": images!.toJson(),
//     };
// }

// class Image {
//     Image({
//         this.src,
//     });

//     String? src;

//     factory Image.fromJson(Map<String, dynamic> json) => Image(
//         src: json["src"],
//     );

//     Map<String, dynamic> toJson() => {
//         "src": src,
//     };
// }
// // To parse this JSON data, do
// //
// //     final profileUpdateModel = profileUpdateModelFromJson(jsonString);

import 'dart:convert';



import 'package:image_picker/image_picker.dart';

List<PhotoUploadeModel> profileUpdateModelFromJson(String str) =>
    List<PhotoUploadeModel>.from(
        json.decode(str).map((x) => PhotoUploadeModel.fromJson(x)));

String profileUpdateModelToJson(List<PhotoUploadeModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PhotoUploadeModel {
  PhotoUploadeModel({this.image});

  XFile? image;

  factory PhotoUploadeModel.fromJson(Map<String, dynamic> json) =>
      PhotoUploadeModel(
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
      };
}
