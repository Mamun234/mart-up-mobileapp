// To parse this JSON data, do
//
//     final categoryHeaderModel = categoryHeaderModelFromJson(jsonString);

import 'dart:convert';

List<CategoryHeaderModel> categoryHeaderModelFromJson(String str) => List<CategoryHeaderModel>.from(json.decode(str).map((x) => CategoryHeaderModel.fromJson(x)));

String categoryHeaderModelToJson(List<CategoryHeaderModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryHeaderModel {
    CategoryHeaderModel({
        this.xWpTotal,
    });

    int? xWpTotal;

    factory CategoryHeaderModel.fromJson(Map<String, dynamic> json) => CategoryHeaderModel(
        xWpTotal: json["x-wp-total"],
    );

    Map<String, dynamic> toJson() => {
        "x-wp-total": xWpTotal,
    };
}
