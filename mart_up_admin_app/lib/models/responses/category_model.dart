// //import 'package:flutter/material.dart' hide Action;

import 'dart:convert';

List<CategoryModel> categoryFromJson(String str) => List<CategoryModel>.from(
    json.decode(str).map((x) => CategoryModel.fromJson(x)));

String categoryToJson(List<CategoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryModel {
  int? id;
  String? name;
  String? slug;
  int? parent;
  String? description;
  String? display;
  int? count;
  Images? image;
  

  CategoryModel(
      {this.id,
      this.name,
      this.slug,
      this.parent,
      this.description,
      this.display,
      this.count,
      this.image});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    parent = json['parent'];
    description = json['description'];
    display = json['display'];
    count=json['count'];
    image = json['image'] != null ? new Images.fromJson(json['image']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['parent'] = this.parent;
    data['description'] = this.description;
    data['display'] = this.display;
    data['count']=this.count;
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    return data;
  }
}

class Images {
  int? id;
  String? dateCreated;
  String? dateCreatedGmt;
  String? dateModified;
  String? dateModifiedGmt;
  String? src;
  String? name;
  String? alt;

  Images(
      {this.id,
      this.dateCreated,
      this.dateCreatedGmt,
      this.dateModified,
      this.dateModifiedGmt,
      this.src,
      this.name,
      this.alt});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    dateCreated = json['date_created'];
    dateCreatedGmt = json['date_created_gmt'];
    dateModified = json['date_modified'];
    dateModifiedGmt = json['date_modified_gmt'];
    src = json['src'];
    name = json['name'];
    alt = json['alt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date_created'] = this.dateCreated;
    data['date_created_gmt'] = this.dateCreatedGmt;
    data['date_modified'] = this.dateModified;
    data['date_modified_gmt'] = this.dateModifiedGmt;
    data['src'] = this.src;
    data['name'] = this.name;
    data['alt'] = this.alt;
    return data;
  }
}

//New category Model

// To parse this JSON data, do
//
//     final categoryModel = categoryModelFromJson(jsonString);

// import 'dart:convert';

// List<CategoryModel> categoryModelFromJson(String str) => List<CategoryModel>.from(json.decode(str).map((x) => CategoryModel.fromJson(x)));

// String categoryModelToJson(List<CategoryModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class CategoryModel {
//     CategoryModel({
//         this.id,
//         this.name,
//         this.slug,
//         this.parent,
//         this.description,
//         this.images,
//         this.count,
//     });

//     int? id;
//     String? name;
//     String? slug;
//     int? parent;
//     String? description;
//     Images? images;
//     int? count;

//     factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
//         id: json["id"],
//         name: json["name"],
//         slug: json["slug"],
//         parent: json["parent"],
//         description: json["description"],
//         images: Images.fromJson(json["images"]),
//         count: json["count"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "slug": slug,
//         "parent": parent,
//         "description": description,
//         "images": images!.toJson(),
//         "count": count,
//     };
// }

// class Images {
//     Images({
//         this.id,
//         this.dateCreated,
//         this.dateCreatedGmt,
//         this.dateModified,
//         this.dateModifiedGmt,
//         this.src,
//         this.name,
//         this.alt,
//     });

//     int? id;
//     DateTime? dateCreated;
//     DateTime? dateCreatedGmt;
//     DateTime? dateModified;
//     DateTime? dateModifiedGmt;
//     String? src;
//     String? name;
//     String? alt;

//     factory Images.fromJson(Map<String, dynamic> json) => Images(
//         id: json["id"],
//         dateCreated: DateTime.parse(json["date_created"]),
//         dateCreatedGmt: DateTime.parse(json["date_created_gmt"]),
//         dateModified: DateTime.parse(json["date_modified"]),
//         dateModifiedGmt: DateTime.parse(json["date_modified_gmt"]),
//         src: json["src"],
//         name: json["name"],
//         alt: json["alt"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "date_created": dateCreated!.toIso8601String(),
//         "date_created_gmt": dateCreatedGmt!.toIso8601String(),
//         "date_modified": dateModified!.toIso8601String(),
//         "date_modified_gmt": dateModifiedGmt!.toIso8601String(),
//         "src": src,
//         "name": name,
//         "alt": alt,
//     };
// }

