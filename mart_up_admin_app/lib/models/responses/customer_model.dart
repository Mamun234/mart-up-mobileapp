// To parse this JSON data, do
//
//     final profileInfoModel = profileInfoModelFromJson(jsonString);

import 'dart:convert';

List<CustomerModel> profileInfoModelFromJson(String str) => List<CustomerModel>.from(json.decode(str).map((x) => CustomerModel.fromJson(x)));

String profileInfoModelToJson(List<CustomerModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CustomerModel {
    CustomerModel({
        this.id,
        this.email,
        this.firstName,
        this.username,
        this.avatarUrl,
    });

    int? id;
    String? email;
    String? firstName;
    String? username;
    String? avatarUrl;

    factory CustomerModel.fromJson(Map<String, dynamic> json) => CustomerModel(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        username: json["username"],
        avatarUrl: json["avatar_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "username": username,
        "avatar_url": avatarUrl,
    };
}
