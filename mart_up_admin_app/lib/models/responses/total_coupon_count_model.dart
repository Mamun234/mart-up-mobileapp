// To parse this JSON data, do
//
//     final totalCouponModel = totalCouponModelFromJson(jsonString);

import 'dart:convert';

List<TotalCouponModel> totalCouponModelFromJson(String str) => List<TotalCouponModel>.from(json.decode(str).map((x) => TotalCouponModel.fromJson(x)));

String totalCouponModelToJson(List<TotalCouponModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TotalCouponModel {
    TotalCouponModel({
        this.slug,
        this.name,
        this.total,
    });

    String? slug;
    String? name;
    int? total;

    factory TotalCouponModel.fromJson(Map<String, dynamic> json) => TotalCouponModel(
        slug: json["slug"],
        name: json["name"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "slug": slug,
        "name": name,
        "total": total,
    };
}
