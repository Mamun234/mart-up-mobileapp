// To parse this JSON data, do
//
//     final couponModel = couponModelFromJson(jsonString);

import 'dart:convert';

List<CouponCheckModel> couponModelFromJson(String str) => List<CouponCheckModel>.from(json.decode(str).map((x) => CouponCheckModel.fromJson(x)));

String couponModelToJson(List<CouponCheckModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CouponCheckModel {
    CouponCheckModel({
        this.id,
        this.code,
        this.amount,
    });

    int? id;
    String? code;
    String? amount;

    factory CouponCheckModel.fromJson(Map<String, dynamic> json) => CouponCheckModel(
        id: json["id"],
        code: json["code"],
        amount: json["amount"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "amount": amount,
    };
}
