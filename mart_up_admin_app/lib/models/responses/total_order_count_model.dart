// To parse this JSON data, do
//
//     final totalOrderModel = totalOrderModelFromJson(jsonString);

import 'dart:convert';

List<TotalOrderModel> totalOrderModelFromJson(String str) => List<TotalOrderModel>.from(json.decode(str).map((x) => TotalOrderModel.fromJson(x)));

String totalOrderModelToJson(List<TotalOrderModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TotalOrderModel {
    TotalOrderModel({
        this.slug,
        this.name,
        this.total,
    });

    String? slug;
    String? name;
    int? total;

    factory TotalOrderModel.fromJson(Map<String, dynamic> json) => TotalOrderModel(
        slug: json["slug"],
        name: json["name"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "slug": slug,
        "name": name,
        "total": total,
    };
}
