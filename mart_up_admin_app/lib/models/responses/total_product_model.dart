// To parse this JSON data, do
//
//     final totalProductModel = totalProductModelFromJson(jsonString);

import 'dart:convert';

List<TotalProductModel> totalProductModelFromJson(String str) => List<TotalProductModel>.from(json.decode(str).map((x) => TotalProductModel.fromJson(x)));

String totalProductModelToJson(List<TotalProductModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TotalProductModel {
    TotalProductModel({
        this.slug,
        this.name,
        this.total,
    });

    String? slug;
    String? name;
    int? total;

    factory TotalProductModel.fromJson(Map<String, dynamic> json) => TotalProductModel(
        slug: json["slug"],
        name: json["name"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "slug": slug,
        "name": name,
        "total": total,
    };
}
