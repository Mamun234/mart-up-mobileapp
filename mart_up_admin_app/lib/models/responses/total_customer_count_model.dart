// To parse this JSON data, do
//
//     final totalCustomerModel = totalCustomerModelFromJson(jsonString);

import 'dart:convert';

List<TotalCustomerModel> totalCustomerModelFromJson(String str) => List<TotalCustomerModel>.from(json.decode(str).map((x) => TotalCustomerModel.fromJson(x)));

String totalCustomerModelToJson(List<TotalCustomerModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TotalCustomerModel {
    TotalCustomerModel({
        this.slug,
        this.name,
        this.total,
    });

    String? slug;
    String? name;
    int? total;

    factory TotalCustomerModel.fromJson(Map<String, dynamic> json) => TotalCustomerModel(
        slug: json["slug"],
        name: json["name"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "slug": slug,
        "name": name,
        "total": total,
    };
}
