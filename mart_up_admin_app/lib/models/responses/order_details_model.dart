import 'dart:convert';

List<OrderDetailsModel> OrderDetailsModelFromJson(String str) =>
    List<OrderDetailsModel>.from(
        json.decode(str).map((x) => OrderDetailsModel.fromJson(x)));

String OrderDetailsModelToJson(List<OrderDetailsModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderDetailsModel {
  OrderDetailsModel({
    this.id,
    this.status,
    this.dateCreated,
    this.shippingTotal,
    this.total,
    this.totalTax,
    this.customerId,
    this.shipping,
    this.lineItems,
  });

  int? id;
  String? status;
  DateTime? dateCreated;
  String? shippingTotal;
  String? total;
  String? totalTax;
  int? customerId;
  Shipping? shipping;
  List<LineItem>? lineItems;

  factory OrderDetailsModel.fromJson(Map<String, dynamic> json) =>
      OrderDetailsModel(
        id: json["id"],
        status: json["status"],
        dateCreated: DateTime.parse(json["date_created"]),
        shippingTotal: json["shipping_total"],
        total: json["total"],
        totalTax: json["total_tax"],
        customerId: json["customer_id"],
        shipping: Shipping.fromJson(json["shipping"]),
        lineItems: List<LineItem>.from(
            json["line_items"].map((x) => LineItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "status": status,
        "date_created": dateCreated?.toIso8601String(),
        "shipping_total": shippingTotal,
        "total": total,
        "total_tax": totalTax,
        "customer_id": customerId,
        "shipping": shipping?.toJson(),
        "line_items": List<dynamic>.from(lineItems!.map((x) => x.toJson())),
      };
}

class LineItem {
  LineItem({
    this.id,
    this.name,
    this.productId,
    this.variationId,
    this.quantity,
    this.taxClass,
    this.subtotal,
    this.subtotalTax,
    this.total,
    this.totalTax,
    this.taxes,
    this.metaData,
    this.sku,
  });

  int? id;
  String? name;
  int? productId;
  int? variationId;
  int? quantity;
  String? taxClass;
  String? subtotal;
  String? subtotalTax;
  String? total;
  String? totalTax;
  List<dynamic>? taxes;
  List<dynamic>? metaData;
  String? sku;

  factory LineItem.fromJson(Map<String, dynamic> json) => LineItem(
        id: json["id"],
        name: json["name"],
        productId: json["product_id"],
        variationId: json["variation_id"],
        quantity: json["quantity"],
        taxClass: json["tax_class"],
        subtotal: json["subtotal"],
        subtotalTax: json["subtotal_tax"],
        total: json["total"],
        totalTax: json["total_tax"],
        taxes: List<dynamic>.from(json["taxes"].map((x) => x)),
        metaData: List<dynamic>.from(json["meta_data"].map((x) => x)),
        sku: json["sku"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "product_id": productId,
        "variation_id": variationId,
        "quantity": quantity,
        "tax_class": taxClass,
        "subtotal": subtotal,
        "subtotal_tax": subtotalTax,
        "total": total,
        "total_tax": totalTax,
        "taxes": List<dynamic>.from(taxes!.map((x) => x)),
        "meta_data": List<dynamic>.from(metaData!.map((x) => x)),
        "sku": sku,
      };
}

class Shipping {
  Shipping({
    this.firstName,
    this.lastName,
    this.company,
    this.address1,
    this.address2,
    this.city,
    this.state,
    this.postcode,
    this.country,
    this.phone,
  });

  String? firstName;
  String? lastName;
  String? company;
  String? address1;
  String? address2;
  String? city;
  String? state;
  String? postcode;
  String? country;
  String? phone;

  factory Shipping.fromJson(Map<String, dynamic> json) => Shipping(
        firstName: json["first_name"],
        lastName: json["last_name"],
        company: json["company"],
        address1: json["address_1"],
        address2: json["address_2"],
        city: json["city"],
        state: json["state"],
        postcode: json["postcode"],
        country: json["country"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "company": company,
        "address_1": address1,
        "address_2": address2,
        "city": city,
        "state": state,
        "postcode": postcode,
        "country": country,
        "phone": phone,
      };
}
