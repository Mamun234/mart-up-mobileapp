// To parse this JSON data, do
//
//     final totalReviewModel = totalReviewModelFromJson(jsonString);

import 'dart:convert';

List<TotalReviewModel> totalReviewModelFromJson(String str) => List<TotalReviewModel>.from(json.decode(str).map((x) => TotalReviewModel.fromJson(x)));

String totalReviewModelToJson(List<TotalReviewModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TotalReviewModel {
    TotalReviewModel({
        this.slug,
        this.name,
        this.total,
    });

    String? slug;
    String? name;
    int? total;

    factory TotalReviewModel.fromJson(Map<String, dynamic> json) => TotalReviewModel(
        slug: json["slug"],
        name: json["name"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "slug": slug,
        "name": name,
        "total": total,
    };
}
