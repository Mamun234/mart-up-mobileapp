// To parse this JSON data, do
//
//     final adminLoginResponseModel = adminLoginResponseModelFromJson(jsonString);

import 'dart:convert';

List<AdminLoginResponseModel> adminLoginResponseModelFromJson(String str) => List<AdminLoginResponseModel>.from(json.decode(str).map((x) => AdminLoginResponseModel.fromJson(x)));

String adminLoginResponseModelToJson(List<AdminLoginResponseModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AdminLoginResponseModel {
    AdminLoginResponseModel({
        this.success,
        this.statusCode,
        this.code,
        this.message,
        this.data,
    });

    bool? success;
    int? statusCode;
    String? code;
    String? message;
    Data? data;

    factory AdminLoginResponseModel.fromJson(Map<String, dynamic> json) => AdminLoginResponseModel(
        success: json["success"],
        statusCode: json["statusCode"],
        code: json["code"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "statusCode": statusCode,
        "code": code,
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.token,
        this.id,
        this.email,
        this.nicename,
        this.firstName,
        this.lastName,
        this.displayName,
        this.roles,
    });

    String? token;
    int? id;
    String? email;
    String? nicename;
    String? firstName;
    String? lastName;
    String? displayName;
    List<String>? roles;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        token: json["token"],
        id: json["id"],
        email: json["email"],
        nicename: json["nicename"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        displayName: json["displayName"],
        roles: List<String>.from(json["roles"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "token": token,
        "id": id,
        "email": email,
        "nicename": nicename,
        "firstName": firstName,
        "lastName": lastName,
        "displayName": displayName,
        "roles": List<dynamic>.from(roles!.map((x) => x)),
    };
}
