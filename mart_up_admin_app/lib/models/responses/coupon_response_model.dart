// To parse this JSON data, do
//
//     final couponResponseModel = couponResponseModelFromJson(jsonString);

import 'dart:convert';

List<CouponResponseModel> couponResponseModelFromJson(String str) => List<CouponResponseModel>.from(json.decode(str).map((x) => CouponResponseModel.fromJson(x)));

String couponResponseModelToJson(List<CouponResponseModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CouponResponseModel {
    CouponResponseModel({
        this.id,
        this.code,
        this.amount,
        this.discountType,
        this.description,
        this.dateExpires,
        this.dateExpiresGmt,
        this.usageCount,
        this.individualUse,
        this.productIds,
        this.excludedProductIds,
        this.usageLimit,
        this.usageLimitPerUser,
        this.limitUsageToXItems,
        this.freeShipping,
        this.productCategories,
        this.excludedProductCategories,
        this.excludeSaleItems,
        this.minimumAmount,
        this.maximumAmount,
        this.emailRestrictions,
        this.usedBy,
    });

    int? id;
    String? code;
    String? amount;
    String? discountType;
    String? description;
    String? dateExpires;
    String? dateExpiresGmt;
    int? usageCount;
    bool? individualUse;
    List<int>? productIds;
    List<int>? excludedProductIds;
    int? usageLimit;
    int? usageLimitPerUser;
    int? limitUsageToXItems;
    bool? freeShipping;
    List<int>? productCategories;
    List<int>? excludedProductCategories;
    bool? excludeSaleItems;
    String? minimumAmount;
    String? maximumAmount;
    List<String>? emailRestrictions;
    List<String>? usedBy;

    factory CouponResponseModel.fromJson(Map<String, dynamic> json) => CouponResponseModel(
        id: json["id"],
        code: json["code"],
        amount: json["amount"],
        discountType: json["discount_type"],
        description: json["description"],
        dateExpires: json["date_expires"],
        dateExpiresGmt: json["date_expires_gmt"],
        usageCount: json["usage_count"],
        individualUse: json["individual_use"],
        productIds: List<int>.from(json["product_ids"].map((x) => x)),
        excludedProductIds: List<int>.from(json["excluded_product_ids"].map((x) => x)),
        usageLimit: json["usage_limit"],
        usageLimitPerUser: json["usage_limit_per_user"],
        limitUsageToXItems: json["limit_usage_to_x_items"],
        freeShipping: json["free_shipping"],
        productCategories: List<int>.from(json["product_categories"].map((x) => x)),
        excludedProductCategories: List<int>.from(json["excluded_product_categories"].map((x) => x)),
        excludeSaleItems: json["exclude_sale_items"],
        minimumAmount: json["minimum_amount"],
        maximumAmount: json["maximum_amount"],
        emailRestrictions: List<String>.from(json["email_restrictions"].map((x) => x)),
        usedBy: List<String>.from(json["used_by"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "amount": amount,
        "discount_type": discountType,
        "description": description,
        "date_expires": dateExpires,
        "date_expires_gmt": dateExpiresGmt,
        "usage_count": usageCount,
        "individual_use": individualUse,
        "product_ids": List<dynamic>.from(productIds!.map((x) => x)),
        "excluded_product_ids": List<dynamic>.from(excludedProductIds!.map((x) => x)),
        "usage_limit": usageLimit,
        "usage_limit_per_user": usageLimitPerUser,
        "limit_usage_to_x_items": limitUsageToXItems,
        "free_shipping": freeShipping,
        "product_categories": List<dynamic>.from(productCategories!.map((x) => x)),
        "excluded_product_categories": List<dynamic>.from(excludedProductCategories!.map((x) => x)),
        "exclude_sale_items": excludeSaleItems,
        "minimum_amount": minimumAmount,
        "maximum_amount": maximumAmount,
        "email_restrictions": List<dynamic>.from(emailRestrictions!.map((x) => x)),
        "used_by": List<dynamic>.from(usedBy!.map((x) => x)),
    };
}
