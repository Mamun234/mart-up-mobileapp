// import 'dart:convert';

// List<ProductModel> productFromJson(String str) =>
//     List<ProductModel>.from(json.decode(str).map((x) => ProductModel.fromJson(x)));

// String productToJson(List<ProductModel> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class ProductModel {
//   ProductModel({
//     this.id,
//     this.quantity,
//     this.name,
//     this.shortDescription,
//     this.description,
//     this.price,
//     this.regularPrice,
//     this.salePrice,
//     this.categories,
//     this.images,
//     this.attributes,
//     this.relatedIds,
//     this.averageRating,
//     this.ratingCount,
//   });

//   int? id;
//   int? quantity;
//   String? name;
//   String? shortDescription;
//   String? description;
//   String? price;
//   String? regularPrice;
//   String? salePrice;
//   List<Category>? categories;
//   List<Images>? images;
//   List<Attribute>? attributes;
//   List<int>? relatedIds;
//   String? averageRating;
//   int? ratingCount;

//   factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
//         id: json["id"],
//         quantity: json['quantity'],
//         name: json["name"],
//         shortDescription: json["short_description"],
//         description: json["description"],
//         price: json["price"],
//         regularPrice: json["regular_price"],
//         salePrice: json["sale_price"],
//         categories: List<Category>.from(
//             json["categories"].map((x) => Category.fromJson(x))),
//         images:
//             List<Images>.from(json["images"].map((x) => Images.fromJson(x))),
//         attributes: List<Attribute>.from(
//             json["attributes"].map((x) => Attribute.fromJson(x))),
//         //relatedIds: List<int>.from(json['related_ids'].map((x) => int.fromJson(x))),
//         relatedIds: List<int>.from(json["related_ids"].map((x) => x)),
//         averageRating: json["average_rating"],
//         ratingCount: json["rating_count"],
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "quantity": quantity,
//         "name": name,
//         "short_description": shortDescription,
//         "description": description,
//         "price": price,
//         "regular_price": regularPrice,
//         "sale_price": salePrice,
//         "categories": List<dynamic>.from(categories!.map((x) => x.toJson())),
//         "images": List<dynamic>.from(images!.map((x) => x.toJson())),
//         "attributes": List<dynamic>.from(attributes!.map((x) => x.toJson())),
//         //"related_ids":List<int>.from(relatedIds!.map((x) => x.toJson()))
//         "related_ids": List<dynamic>.from(relatedIds!.map((x) => x)),
//         "average_rating": averageRating,
//         "rating_count": ratingCount,
//       };
//   Map<String, dynamic> toMap() {
//     // ignore: prefer_collection_literals
//     final map = Map<String, dynamic>();
//     map['id'] = id;
//     map['quantity'] = quantity;
//     map['name'] = name;
//     map['short_description'] = shortDescription;
//     map['description'] = description;
//     map['regular_price'] = regularPrice;
//     map['sale_price'] = salePrice;
//     map['categories'] = categories;
//     map['images'] = images;
//     map['attributes'] = attributes;
//     map['related_ids'] = relatedIds;

//     return map;
//   }

//   factory ProductModel.fromMap(Map<String, dynamic> map) {
//     return ProductModel(
//         id: map['id'],
//         quantity: map['quantity'],
//         name: map['name'],
//         shortDescription: map['short_description'],
//         description: map["description"],
//         regularPrice: map['regular_price'],
//         salePrice: map['sale_price'],
//         categories: map['categories'],
//         images: map['images'],
//         attributes: map['attributes'],
//         relatedIds: map['related_ids']

//         // amount: map['amount'],
//         // id: map['id'],
//         // categoryType: map['categoryType'],
//         // createdDate: map['createdDate'],
//         // trnscnType: map['trnscnType']

//         );
//   }
// }

// class Attribute {
//   Attribute({
//     this.id,
//     this.name,
//     this.position,
//     this.visible,
//     this.variation,
//     this.options,
//   });

//   int? id;
//   String? name;
//   int? position;
//   bool? visible;
//   bool? variation;
//   List<String>? options;

//   factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
//         id: json["id"],
//         name: json["name"],
//         position: json["position"],
//         visible: json["visible"],
//         variation: json["variation"],
//         options: List<String>.from(json["options"].map((x) => x)),
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "position": position,
//         "visible": visible,
//         "variation": variation,
//         "options": List<dynamic>.from(options!.map((x) => x)),
//       };
// }

// class Category {
//   Category({
//     this.id,
//     this.name,
//     this.slug,
//   });

//   int? id;
//   String? name;
//   String? slug;

//   factory Category.fromJson(Map<String, dynamic> json) => Category(
//         id: json["id"],
//         name: json["name"],
//         slug: json["slug"],
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "slug": slug,
//       };
// }

// class Images {
//   Images({
//     this.id,
//     this.dateCreated,
//     this.dateCreatedGmt,
//     this.dateModified,
//     this.dateModifiedGmt,
//     this.src,
//     this.name,
//     this.alt,
//   });

//   int? id;
//   DateTime? dateCreated;
//   DateTime? dateCreatedGmt;
//   DateTime? dateModified;
//   DateTime? dateModifiedGmt;
//   String? src;
//   String? name;
//   String? alt;

//   factory Images.fromJson(Map<String, dynamic> json) => Images(
//         id: json["id"],
//         dateCreated: DateTime.parse(json["date_created"]),
//         dateCreatedGmt: DateTime.parse(json["date_created_gmt"]),
//         dateModified: DateTime.parse(json["date_modified"]),
//         dateModifiedGmt: DateTime.parse(json["date_modified_gmt"]),
//         src: json["src"],
//         name: json["name"],
//         alt: json["alt"],
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "date_created": dateCreated!.toIso8601String(),
//         "date_created_gmt": dateCreatedGmt!.toIso8601String(),
//         "date_modified": dateModified!.toIso8601String(),
//         "date_modified_gmt": dateModifiedGmt!.toIso8601String(),
//         "src": src,
//         "name": name,
//         "alt": alt,
//       };
// }
// // class RelatedId {
// //   List<int>? relatedIds;

// //   RelatedId({this.relatedIds});

// //   RelatedId.fromJson(Map<String, dynamic> json) {
// //     relatedIds = json['related_ids'].cast<int>();
// //   }

// //   Map<String, dynamic> toJson() {
// //     final Map<String, dynamic> data = new Map<String, dynamic>();
// //     data['related_ids'] = this.relatedIds;
// //     return data;
// //   }

// // }

// To parse this JSON data, do
//
//     final productModel = productModelFromJson(jsonString);

import 'dart:convert';

List<ProductModel> productModelFromJson(String str) => List<ProductModel>.from(
    json.decode(str).map((x) => ProductModel.fromJson(x)));

String productModelToJson(List<ProductModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductModel {
  ProductModel({
    this.id,
    this.name,
    this.slug,
    this.quantity,
    this.description,
    this.shortDescription,
    this.price,
    this.regularPrice,
    this.salePrice,
    this.averageRating,
    this.dateOnSaleFrom,
    this.dateOnSaleTo,
    this.ratingCount,
    this.categories,
    this.images,
    this.attributes,
    this.relatedIds,
  });

  int? id;
  String? name;
  String? slug;
  int? quantity;
  String? description;
  String? shortDescription;
  String? price;
  String? regularPrice;
  String? salePrice;
  String? averageRating;
  String? dateOnSaleFrom;
  String? dateOnSaleTo;
  int? ratingCount;
  List<Category>? categories;
  List<Image>? images;
  List<Attribute>? attributes;
  List<int>? relatedIds;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        quantity: json["quantity"],
        description: json["description"],
        shortDescription: json["short_description"],
        price: json["price"],
        regularPrice: json["regular_price"],
        salePrice: json["sale_price"],
        averageRating: json["average_rating"],
        dateOnSaleFrom: json["date_on_sale_from"],
        dateOnSaleTo: json["date_on_sale_to"],
        ratingCount: json["rating_count"],
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))),
        images: List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
        attributes: List<Attribute>.from(
            json["attributes"].map((x) => Attribute.fromJson(x))),
        relatedIds: List<int>.from(json["related_ids"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "quantity": quantity,
        "description": description,
        "short_description": shortDescription,
        "price": price,
        "regular_price": regularPrice,
        "sale_price": salePrice,
        "average_rating": averageRating,
        "date_on_sale_from": dateOnSaleFrom,
        "date_on_sale_to": dateOnSaleTo,
        "rating_count": ratingCount,
        "categories": List<dynamic>.from(categories!.map((x) => x.toJson())),
        "images": List<dynamic>.from(images!.map((x) => x.toJson())),
        "attributes": List<dynamic>.from(attributes!.map((x) => x.toJson())),
        "related_ids": List<dynamic>.from(relatedIds!.map((x) => x)),
      };
}

class Attribute {
  Attribute({
    this.id,
    this.name,
    this.position,
    this.visible,
    this.variation,
    this.options,
  });

  int? id;
  String? name;
  int? position;
  bool? visible;
  bool? variation;
  List<String>? options;

  factory Attribute.fromJson(Map<String, dynamic> json) => Attribute(
        id: json["id"],
        name: json["name"],
        position: json["position"],
        visible: json["visible"],
        variation: json["variation"],
        options: List<String>.from(json["options"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "position": position,
        "visible": visible,
        "variation": variation,
        "options": List<dynamic>.from(options!.map((x) => x)),
      };
}

class Category {
  Category({
    this.id,
    this.name,
    this.slug,
  });

  int? id;
  String? name;
  String? slug;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
      };
}

class Image {
  Image({
    this.id,
    this.dateCreated,
    this.dateCreatedGmt,
    this.dateModified,
    this.dateModifiedGmt,
    this.src,
    this.name,
    this.alt,
  });

  int? id;
  DateTime? dateCreated;
  DateTime? dateCreatedGmt;
  DateTime? dateModified;
  DateTime? dateModifiedGmt;
  String? src;
  String? name;
  String? alt;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        id: json["id"],
        dateCreated: DateTime.parse(json["date_created"]),
        dateCreatedGmt: DateTime.parse(json["date_created_gmt"]),
        dateModified: DateTime.parse(json["date_modified"]),
        dateModifiedGmt: DateTime.parse(json["date_modified_gmt"]),
        src: json["src"],
        name: json["name"],
        alt: json["alt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date_created": dateCreated!.toIso8601String(),
        "date_created_gmt": dateCreatedGmt!.toIso8601String(),
        "date_modified": dateModified!.toIso8601String(),
        "date_modified_gmt": dateModifiedGmt!.toIso8601String(),
        "src": src,
        "name": name,
        "alt": alt,
      };
}
