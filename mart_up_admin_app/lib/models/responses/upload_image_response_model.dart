// To parse this JSON data, do
//
//     final UploadImageResponseModel = UploadImageResponseModelFromJson(jsonString);

import 'dart:convert';

List<UploadImageResponseModel> UploadImageResponseModelFromJson(String str) => List<UploadImageResponseModel>.from(json.decode(str).map((x) => UploadImageResponseModel.fromJson(x)));

String UploadImageResponseModelToJson(List<UploadImageResponseModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UploadImageResponseModel {
    UploadImageResponseModel({
        this.id,
        this.guid,
        this.slug,
        this.altText,
    });

    int? id;
    Guid? guid;
    String? slug;
    String? altText;

    factory UploadImageResponseModel.fromJson(Map<String, dynamic> json) => UploadImageResponseModel(
        id: json["id"],
        guid: Guid.fromJson(json["guid"]),
        slug: json["slug"],
        altText: json["alt_text"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "guid": guid!.toJson(),
        "slug": slug,
        "alt_text": altText,
    };
}

class Guid {
    Guid({
        this.rendered,
        this.raw,
    });

    String? rendered;
    String? raw;

    factory Guid.fromJson(Map<String, dynamic> json) => Guid(
        rendered: json["rendered"],
        raw: json["raw"],
    );

    Map<String, dynamic> toJson() => {
        "rendered": rendered,
        "raw": raw,
    };
}
