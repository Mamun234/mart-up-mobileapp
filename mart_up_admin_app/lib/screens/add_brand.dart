import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/category_create_model.dart';
import 'package:mart_up_admin_app/models/requests/create_brand_model.dart';
import 'package:mart_up_admin_app/models/requests/upload_image_mode.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/upload_image_response_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/brand_screen.dart';
import 'package:mart_up_admin_app/screens/category_screen.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/create_brand_service.dart';
import 'package:mart_up_admin_app/services/create_category_service.dart';
import 'package:mart_up_admin_app/services/image_upload_service.dart';
import 'package:mart_up_admin_app/services/product_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
//import 'package:snippet_coder_utils/multi_images_utils.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:provider/provider.dart';

class AddBrand extends StatefulWidget {
  BrandModel? singleBrand;
  
  AddBrand({this.singleBrand});

  @override
  _AddBrandState createState() => _AddBrandState();
}

class _AddBrandState extends State<AddBrand> {
  final formKey = GlobalKey<FormState>();
  String? valueChoose;
  List<Object> images = [];
  List listItem = ['Dhaka', 'Chattogram', 'Rajshahi'];
  TextEditingController brandNameCtlr = new TextEditingController();
  TextEditingController slugCtlr = new TextEditingController();
  TextEditingController descriptionCtlr = new TextEditingController();
  //late List<UploadImageModel> uploadImageModel;
  List<Asset> invoiceimages = [];
  List<Asset> selectedIamges = [];
  int? productId;
  //  CategoryModel? categoryModel;
  //  Future<List<CategoryModel>>? _category;
  ProductModel? productModel;
  Future<List<ProductModel>>? _product;
  ViewModelProvider? viewModelProvider;
  String? key;
  UploadImageResponseModel? categoryImage;
  bool _isLoading = false;
  String? token;
  int page = 1;
  int limit = 5;
  BrandModel brandModel=BrandModel();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    //getCategory();

    //_category= CategoryService.fetchCategory(key);
    _product = ProductService.fetchProducts(page, limit, key);
    valueAssign();
    getAdminLoginInfo();
  }

  getAdminLoginInfo() async {
    await viewModelProvider!.fatchAdminLoginInfo();
    print(viewModelProvider!.adminLoginInfoProvider!.data!.token!);
    token = viewModelProvider!.adminLoginInfoProvider!.data!.token!;
    setState(() {});
  }
  valueAssign(){
    if(widget.singleBrand !=null){
      brandNameCtlr.text=widget.singleBrand!.name!;
      slugCtlr.text=widget.singleBrand!.slug!;
      descriptionCtlr.text=widget.singleBrand!.description!;
      TermThumbnailImage imageSrc=TermThumbnailImage();
      //imageSrc.id=widget.singleBrand!.termThumbnailImage!.id;
      imageSrc.url=widget.singleBrand!.termThumbnailImage!.url;
      brandModel.termThumbnailImage= imageSrc;
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            // Navigator.pop(
            //     context, MaterialPageRoute(builder: (context) => ToShip()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
         widget.singleBrand ==null? "Add Brand":"Edit Brand",
          //widget.id == null ? "Add Address" : "Update address",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //                 SizedBox(
                //   height: 20,
                // ),
                Text(
                  'Brand Name*',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: brandNameCtlr,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Brand Name is Empty";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Slug',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: slugCtlr,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    // validator: (value) {
                    //   if (value!.isEmpty) {
                    //     return "Slug Name is Empty";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Description',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: descriptionCtlr,
                    decoration: InputDecoration(
                      // contentPadding: new EdgeInsets.symmetric(
                      //     vertical: 25.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    maxLines: 4,
                    // validator: (value) {
                    //   if (value!.isEmpty) {
                    //     return "Enter your description";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                // Text(
                //   "Add Product",
                //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                // ),

                //    SizedBox(
                //     height: 10,
                //   ),
                //   DropdownSearch<ProductModel>(
                //   itemAsString: (ProductModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _product!,
                //   label: "Product*",
                //   onChanged: (ProductModel? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       productModel = data;
                //     });

                //     // setState(() {
                //     //   subDistrict = null;
                //     // });
                //     // _subDistricts = APIService.fetchSubDistricts(data.id);
                //     setState(() {
                //       productId=data!.id;
                //       print(productId);
                //     });
                //   },
                //   showSearchBox: true,
                // ),
                // //buildDistrict(),
                // SizedBox(
                //   height: 10,
                // ),

//                 MultiImagePicker(
//     totalImages: 6,
//     imageSource: ImagePickSource.gallery,
//     //initialValue: this.productModel.images.map((e) => e.src).toList(), //for showing images from collection
//    //initialValue: uploadImageModel.map((e) => e.images!.src!).toList(),
//     onImageChanged: (images) {
//         this.images = images;
//     },
// ),

//  SizedBox(
//                   height: 10,
//                 ),
//                 Text(
//                   "Add Photo",
//                   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 GestureDetector(
//                   onTap: () {
//                     pickInvoiceImages();
//                   },
//                   child: Container(
//                     height: SizeConfig.safeBlockVertical! *8,
//                           width: SizeConfig.safeBlockHorizontal! *23,
//                     decoration:
//                         BoxDecoration(border: Border.all(color: Colors.grey)),
//                     child: Icon(
//                       Icons.add_a_photo_outlined,
//                       size: 32,
//                     ),
//                   ),
//                 ),
//                 Visibility(
//                     visible: invoiceimages.length > 0,
//                     child: Container(
//                       height: SizeConfig.safeBlockVertical! *13,

//                       child: Container(
//                         child: ListView.builder(
//                             scrollDirection: Axis.horizontal,
//                             itemCount: invoiceimages.length,
//                             itemBuilder: (context, index) {
//                               Asset asset = invoiceimages[index];

//                               return Container(
//                                 padding: EdgeInsets.all(5),
//                                 child: AssetThumb(
//                                   asset: asset,
//                                   width: 90,
//                                   height: 90,
//                                 ),
//                               );
//                             }),
//                       ),
//                     )
//                     ),
              ],
            ),
          ),
        ),
      )),
      bottomNavigationBar: Builder(builder: (context) {
        return _isLoading
            ? BoxLoader()
            : Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                child: RoundedButton(
                    buttonText: widget.singleBrand ==null?"Save":"Update",
                    func: () async {
                      if (formKey.currentState!.validate()) {
                        setState(() {
                          _isLoading = true;
                        });
                        //PhotoUploadeModel photos=PhotoUploadeModel(image: invoiceimages.);
                        //      selectedIamges=invoiceimages;
                        // Response data=  await ImageUplodeService().imageupload(selectedIamges);
                        if(widget.singleBrand !=null){
                          brandModel.id=widget.singleBrand!.id;
                          brandModel.name ==null? brandModel.name=brandNameCtlr.text:'';
                          brandModel.slug == null? brandModel.slug=slugCtlr.text:'';
                          brandModel.description ==null? brandModel.description=descriptionCtlr.text:'';

                          Response? res =
                            await CreateBrandService().updateBrand(brandModel, widget.singleBrand!.id);
                        if (res!.statusCode == 200 || res.statusCode == 201) {
                          EasyLoading.showSuccess('Success!');
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BrandScreen()));
                        } else {
                         EasyLoading.showError("Failed!");
                          setState(() {
                            _isLoading = false;
                          });
                        }
                        }
                        else{

                        BrandCreateModel brand = BrandCreateModel();
                        //CategoryModel category=CategoryModel();
                        //Images srcUrl=Images();
                        //srcUrl.src=categoryImage!.guid!.rendered!;
                        // Imagess srcUrl=Imagess();
                        // srcUrl.src=categoryImage!.guid!.rendered!;
                        brand.name == null?brand.name = brandNameCtlr.text:'';
                        brand.slug == null?brand.slug = slugCtlr.text:'';
                        brand.description == null?brand.description = descriptionCtlr.text:'';
                        //category.parent=productId;
                        //  category.image=srcUrl;
                        //category.image=srcUrl;
                        //category.display="default";

                        //category.image!.src=categoryImage!.guid!.rendered;

                        //  category.image!.alt=categoryImage!.altText;
                        //  category.image!.name=categoryImage!.slug;
                        Response? res =
                            await CreateBrandService().createBrand(brand);
                        if (res!.statusCode == 200 || res.statusCode == 201) {
                          CustomSnackbar.snackbar(
                              context, "Brand created succesfully");
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BrandScreen()));
                        } else {
                          await CustomSnackbar.snackbar(
                              context, "Brand is not created ");
                          setState(() {
                            _isLoading = false;
                          });
                        }
                        }
                      } else {}
                      
                    }));
      }),
    );
  }

  //add pic
  Future<void> pickInvoiceImages() async {
    List<Asset> resultList = [];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: true,
        selectedAssets: invoiceimages,
        materialOptions: MaterialOptions(
          statusBarColor: "#4CAF50",
          actionBarColor: "#4CAF50",
          actionBarTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#4CAF50",
        ),
      );
    } on Exception catch (e) {
      print(e);
    }
    setState(() {
      invoiceimages = resultList;
    });
  }
}
