import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/loader_sales_report.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:provider/provider.dart';

class SalesReportScreen extends StatefulWidget {
  const SalesReportScreen({Key? key}) : super(key: key);

  @override
  _SalesReportScreenState createState() => _SalesReportScreenState();
}

class _SalesReportScreenState extends State<SalesReportScreen> {
  late ViewModelProvider viewModelProvider;
  bool _isloading = true;
  List<String> periodList = ['week', 'month', 'last_month','year'];
  String? selectedPeriod;
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
      String? minDate;
  String? maxDate;
  DateTime selectedDate = DateTime.now();
  DateTime selectedDate2 = DateTime.now();
  DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  


  @override
  void initState() {
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);

    getSalesReports(selectedPeriod,minDate,maxDate);
  }

  getSalesReports(selectedPeriod,minDate,maxDate) async {
    await viewModelProvider.getSaleReport(salesPeriod: selectedPeriod,minDate: minDate,maxDate: maxDate);
    setState(() {
      _isloading = false;
    });
  }
  //   _selectDate(BuildContext context) async {
  //   final DateTime? picked = await showDatePicker(
  //     context: context,
  //     initialDate: selectedDate, // Refer step 1
  //     firstDate: DateTime(2022),
  //     lastDate: DateTime(2025),
  //   );
  //   if (picked != null && picked != selectedDate)
  //     setState(() {
  //       selectedDate = picked;
  //       print(selectedDate);
  //       minDate = dateFormat.format(selectedDate);
       
  //     });
  //     setState(() {
  //       minDate;
  //     });
  // }

  //   _selectDate2(BuildContext context) async {
  //   final DateTime? picked = await showDatePicker(
  //     context: context,
  //     initialDate: selectedDate2, // Refer step 1
  //     firstDate: DateTime(2022),
  //     lastDate: DateTime(2025),
  //   );
  //   if (picked != null && picked != selectedDate)
  //     setState(() {
  //       selectedDate2 = picked;
  //       maxDate = dateFormat.format(selectedDate2);
        
  //     });
  //     setState(() {
    
  //     });
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Sales Report",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        actions: [
            IconButton(
                  onPressed: () {
                     showDialog(
                                                             context: context,
                                                              builder:
                                                                  (contex) {
                                                                 
                                                              
                                                                  
                                                                     return StatefulBuilder(
                                                                        builder: (context, setState) {
                                                                        return AlertDialog(
                                                                          // title:
                                                                          //     Text(
                                                                          //   'DELETE',
                                                                          //   textScaleFactor:
                                                                          //       1,
                                                                          // ),
                                                                          //titlePadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                                                                          //contentPadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                                                                          
                                                                          shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(10),
                                                                          ),
                                                                          title: 
                                                                              Column(
                                                                                children: [
                                                                                  Row(
                                                                                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Text("Filter",style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w700),),
                                                                                      GestureDetector(
                                                                                        onTap: (){
                                                                                          Navigator.of(context)
                                                                                                 .pop(true);
                                                                                        },
                                                                                        child: Card(
                                                                                         
                                                                                           shape: RoundedRectangleBorder(
                                                                                           borderRadius: BorderRadius.circular(50),
                                                                                            ),
                                                                                            child: Center(child: Icon(Icons.close,color: Colors.redAccent,size: 20,)),
                                                                                        ),
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                  Divider(),
                                                                                ],
                                                                              ),
                                                                          
                                                                           
                                                                          content: 
                                                                                  
                                                                              Container(
                                                                                height: SizeConfig.blockSizeVertical! * 28,
                                                                                child: Column(
                                                                                  children: [
                                                                                    // Text(
                                                                                    //     'Do you want to delete ?',
                                                                                    //     textScaleFactor:
                                                                                    //         1),
                                                                      
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        Container(
                                                                                            decoration: BoxDecoration(
                                                                                              border: Border.all(),
                                                                                              borderRadius: BorderRadius.circular(5)
                                                                                            ),
                                                                                            child: Row(
                                                                                              children: [
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                                                                                  child: 
                                                                                                  Text(minDate !=null?DateFormat('dd MMM').format(DateTime.parse(minDate!)):"select"),
                                                                                                ),
                                                                                                minDate==null?IconButton(
                                                                                                  onPressed: ()async{
                                                                                                   
         
 final DateTime? picked=await showDatePicker(context: context, initialDate:selectedDate, firstDate: DateTime(2022), lastDate: DateTime(2025),);
        if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print(selectedDate);
        minDate = dateFormat.format(selectedDate);
       
      });                                                                                              
                                                                                                     
                                                                                                   
                                                                                                  },
                                                                                                 icon: Icon(Icons.calendar_today_outlined),
                                                                                                 ):IconButton(onPressed: (){
                                                                                                    setState(() {
        
                                                                                                  minDate = null;
       
                                                                                                   }); 
                                                                                                 }, 
                                                                                                 icon: Icon(Icons.close_outlined,)
                                                                                                 )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                          SizedBox(width:2),
                                                                                          Text("To",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w700),),
                                                                                           SizedBox(width:2),
                                                                                          
                                                                                          
                                                                                          
                                                                                           Container(
                                                                                            decoration: BoxDecoration(
                                                                                              border: Border.all(),
                                                                                              borderRadius: BorderRadius.circular(5)
                                                                                            ),
                                                                                            child: Row(
                                                                                              
                                                                                              children: [
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                                                                                  child: Text(maxDate !=null?DateFormat('dd MMM').format(DateTime.parse(maxDate!)):"select"),
                                                                                                ),
                                                                                                maxDate==null?IconButton(
                                                                                                  onPressed: ()async{
                                                                                                    
                                                                                               
                                                                                final DateTime? picked=await showDatePicker(context: context, initialDate:selectedDate2, firstDate: DateTime(2022), lastDate: DateTime(2025),);
        if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate2 = picked;
        print(selectedDate2);
        maxDate = dateFormat.format(selectedDate2);
       
      }); 
                                                                                                      
                                                                                                  },
                                                                                                 icon: Icon(Icons.calendar_today_outlined),
                                                                                                 ):IconButton(onPressed: (){
                                                                                                    setState(() {
        
                                                                                                  maxDate = null;
       
                                                                                                   }); 
                                                                                                 }, 
                                                                                                 icon: Icon(Icons.close_outlined,)
                                                                                                 )
                                                                                              ],
                                                                                            ),
                                                                                          )
                                                                      
                                                                                      ],
                                                                                    ),
                                                                                    SizedBox(height: 10,),
                                                                                    Text("OR"),
                                                                                     SizedBox(height: 10,),
                                                                                    DropdownSearch<String>(
                                                                                        //key: _productKey,
                                                                                        //itemAsString:  color.toString(),'
                                                                                        maxHeight: SizeConfig.blockSizeVertical! * 40,
                                                                                        items: periodList,
                                                                                        // maxHeight: deviceHeight * .80,
                                                                                        //onFind: (String? filter) => color!,
                                                                                        selectedItem: selectedPeriod,
                                                                                         showClearButton: true,
                                                                                         clearButton: IconButton(onPressed: (){
                                                                                           setState(() {
                                                                                            selectedPeriod=null;
                                                                                            print("tap");
                                                                                            print(selectedPeriod);
                                                                                                });
                                                                                         }, icon: Icon(Icons.close_outlined),
                                                                                         ),
                                                                                        label: "Period",
                                                                                        //selectedItems: selectedColor,
                                                                                        onChanged: (data) {
                                                                                          // print(data.toJson()),
                                                                                          // selectedColor.clear();
                                                                                          setState(() {
                                                                                            //selectedProduct = data;
                                                                                            
                                                                                            selectedPeriod = data;
                                                                                            viewModelProvider.salesPeriod=selectedPeriod;
                                                                                            
                                                                                            
                                                                                            //getSalesReports(viewModelProvider.salesPeriod);
                                                                                            print(viewModelProvider.salesPeriod);
                                                                                          });
                                                                                        },
                                                                                        showSearchBox: true,
                                                                                      ),
                                                                      
                                                                                            SizedBox(height: 20,),
                                                                                            Container(
                                                                                              height: 50,
                                                                                              width: double.infinity,
                                                                                              child: RaisedButton(
                                                                                                color: themeColor,
                                                                                                onPressed: (){
                                                                                                    Navigator.of(context)
                                                                                                 .pop(true);
                                                                                                                                                                     setState(
                                                                                () {
                                                                              _isloading=true; 
                                                                            });
                                                                                                 if((minDate !=null || maxDate !=null) && selectedPeriod !=null){
        Fluttertoast.showToast(  
        msg: 'Select Date OR Period At a Time',  
        toastLength: Toast.LENGTH_SHORT,  
        gravity: ToastGravity.BOTTOM,  
        //timeInSecForIos: 1,  
        backgroundColor: Colors.red,  
        textColor: Colors.white  
    );
                                                                                                 }
                                                                                                 else if(selectedPeriod ==null && (minDate ==null || maxDate ==null)){
                                                                                                     Fluttertoast.showToast(  
        msg: 'Select Date OR Period',  
        toastLength: Toast.LENGTH_SHORT,  
        gravity: ToastGravity.BOTTOM,  
        //timeInSecForIos: 1,  
        backgroundColor: Colors.red,  
        textColor: Colors.white  
    );
                                                                                                 }
                                                                                                 else{
                                                                             
                                                                                                  
                                                                             
                                                                                                  viewModelProvider.salesPeriod=selectedPeriod;
                                                                                                  viewModelProvider.minDate=minDate;
                                                                                                  viewModelProvider.maxDate=maxDate;
                                                                                                  viewModelProvider.fetchSaleReportListProvider!.clear();
                                                                            
                                                                                                   getSalesReports(viewModelProvider.salesPeriod, viewModelProvider.minDate, viewModelProvider.maxDate);
                                                                                                 }
                                                                                                
                                                                                                },
                                                                                                child: Text("Apply",style: TextStyle(color: Colors.white),),
                                                                                              ),
                                                                                            )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                          
                                                                          // actions: <
                                                                          //     Widget>[
                                                                          //   TextButton(
                                                                          //       child: Text('Cancel'),
                                                                          //       onPressed: () {
                                                                          //         Navigator.of(context).pop(true);
                                                                          //       }),
                                                                          //   TextButton(
                                                                          //       child: Text('Delete'),
                                                                          //       onPressed: () async {
                                                                          //         //Navigator.of(context,rootNavigator: true).pop();
                                                                          //         Navigator.of(context).pop(true);
                                                                          //       }
                                                                                  
                                                                          //       ),
                                                                          // ],
                                                                        );
                                                                        }
                                                                      );
                                                                     
                                                                  });
                   
                    
                  },
                  icon: Icon(Icons.filter_alt),
                  color: tabSelectColor,
                ),
        ],
      ),
      drawer: AppDrawer(),
      body:  SafeArea(
              child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: _isloading
          ? LoaderSalesReportScreen()
          : ListView.builder(
               itemCount:
                   viewModelProvider.fetchSaleReportListProvider!.length,
               itemBuilder: (BuildContext context, index) {
                 return Container(
                   child: Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: [
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(25),
                             child: Card(
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.green),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/gross.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].totalSales!}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),
                                         Text(
                                           "Gross Sales",
                                         ),
                                         Text(
                                           "In This Period",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.green),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-02.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].averageSales!}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),
                                         Text(
                                           "Average Gross",
                                         ),
                                         Text(
                                           "Daily Sales",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ],
                       ),
                       SizedBox(
                         height: 10,
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0,
                                         color: Colors.blueAccent),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-03.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].netSales!}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),
                                         Text(
                                           "Net Sales",
                                         ),
                                         Text(
                                           "In This Period",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0,
                                         color: Colors.blueAccent),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-04.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].averageSales!}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),
                                         Text(
                                           "Average Net",
                                         ),
                                         Text(
                                           "Daily Sales",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ],
                       ),
                       SizedBox(
                         height: 10,
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.grey),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-05.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           viewModelProvider
                                               .fetchSaleReportListProvider![
                                                   index]
                                               .totalOrders!
                                               .toString(),
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),

                                         //Text("net sales",),

                                         Text(
                                           "Order Place",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.grey),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-06.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           viewModelProvider
                                               .fetchSaleReportListProvider![
                                                   index]
                                               .totalItems!
                                               .toString(),
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),

                                         //Text("average net",),

                                         Text(
                                           "Item Purchased",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ],
                       ),
                       SizedBox(
                         height: 10,
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.red),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-07.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].totalRefunds!.toString()}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),

                                         //Text("net sales",),

                                         Text(
                                           "Refunds",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.green),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-08.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].totalShipping!}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),
                                         Text(
                                           "Charged",
                                         ),
                                         Text(
                                           "For Shipping",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ],
                       ),
                       SizedBox(
                         height: 10,
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           ClipRRect(
                             borderRadius: BorderRadius.circular(20),
                             child: Card(
                               // decoration: BoxDecoration(
                               //     border: Border.all(
                               //       color: Colors.grey, width: 4)
                               //       ),
                               child: Container(
                                 height: SizeConfig.blockSizeVertical! * 18,
                                 width: SizeConfig.blockSizeHorizontal! * 45,
                                 decoration: const BoxDecoration(
                                   //color: Colors.grey,
                                   border: Border(
                                     left: BorderSide(
                                         width: 3.0, color: Colors.amber),
                                   ),
                                 ),
                                 child: Center(
                                   child: Padding(
                                     padding: const EdgeInsets.symmetric(
                                         vertical: 8.0),
                                     child: Column(
                                       children: [
                                         Image.asset(
                                           'assets/images/Untitled-1-09.png',
                                           width: 30.0,
                                           height: 30.0,
                                         ),
                                         SizedBox(
                                           height: 4,
                                         ),
                                         Text(
                                           '৳${viewModelProvider.fetchSaleReportListProvider![index].totalDiscount!.toString()}',
                                           style: TextStyle(
                                               color: Colors.black,
                                               fontSize: 18,
                                               fontWeight: FontWeight.bold),
                                         ),
                                         Text(
                                           "Worth Of ",
                                         ),
                                         Text(
                                           "Coupons Used",
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ],
                       )
                     ],
                   ),
                 );
               }),
            )),
    );
  }
}
