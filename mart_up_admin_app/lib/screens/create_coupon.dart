import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/create_coupon_model.dart';
import 'package:mart_up_admin_app/models/responses/coupon_response_model.dart';
import 'package:mart_up_admin_app/screens/category_screen.dart';
import 'package:mart_up_admin_app/screens/coupon_screen.dart';
import 'package:mart_up_admin_app/services/coupon_update_delete_service.dart';
import 'package:mart_up_admin_app/services/create_coupon_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
import 'package:intl/intl.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class CreateCoupon extends StatefulWidget {
  CouponResponseModel? singleCoupon;
   CreateCoupon({this.singleCoupon});

  @override
  _CreateCouponState createState() => _CreateCouponState();
}

class _CreateCouponState extends State<CreateCoupon> {
  bool _isLoading=false;
  String? expireDateTime;
  DateTime selectedDate = DateTime.now();
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
   List listItem = ['percent', 'fixed_cart', 'fixed_product'];
   
  String? valueChoose;
 TextEditingController codeCnt=TextEditingController();
 TextEditingController descriptionCnt=TextEditingController();
 TextEditingController amountCnt=TextEditingController();
 TextEditingController expireDateCnt=TextEditingController();
 TextEditingController totalUseageCnt=TextEditingController();
 TextEditingController usagePerUserCnt=TextEditingController();
 TextEditingController cartProductNumberCnt=TextEditingController();
 TextEditingController minPriceCnt=TextEditingController();
 TextEditingController maxpriceCnt=TextEditingController();

  @override
  void initState() {
    super.initState();
    valueAsign();

  }

  valueAsign(){
    // usageLimit:int.parse(totalUseageCnt.text),
    //            usageLimitPerUser:int.parse(usagePerUserCnt.text),
    //            limitUsageToXItems: int.parse(cartProductNumberCnt.text),
    //            discountType: valueChoose,
    //            minimumAmount: minPriceCnt.text,
    //            maximumAmount: maxpriceCnt.text
    if(widget.singleCoupon !=null){
      codeCnt.text=widget.singleCoupon!.code !=null? widget.singleCoupon!.code!:'';
      descriptionCnt.text=widget.singleCoupon!.description !=null?widget.singleCoupon!.description!:'';
      amountCnt.text=widget.singleCoupon!.amount !=null?widget.singleCoupon!.amount!:'';
      totalUseageCnt.text=widget.singleCoupon!.usageLimit !=null?widget.singleCoupon!.usageLimit!.toString():'';
      usagePerUserCnt.text=widget.singleCoupon!.usageLimitPerUser !=null?widget.singleCoupon!.usageLimitPerUser.toString():'';
      cartProductNumberCnt.text= widget.singleCoupon!.limitUsageToXItems !=null?widget.singleCoupon!.limitUsageToXItems.toString():'';
      valueChoose= widget.singleCoupon!.discountType ?? '';
      minPriceCnt.text=widget.singleCoupon!.minimumAmount !=null?widget.singleCoupon!.minimumAmount!:'';
      maxpriceCnt.text= widget.singleCoupon!.maximumAmount !=null?widget.singleCoupon!.maximumAmount!:'';
      expireDateTime=widget.singleCoupon!.dateExpires;

    }
  }

  _selectDate(BuildContext context) async {
  final DateTime? picked = await showDatePicker(
    context: context,
    initialDate: selectedDate, // Refer step 1
    firstDate: DateTime(2022),
    lastDate: DateTime(2025),
  );
  if (picked != null && picked != selectedDate)
    setState(() {
      selectedDate = picked;
      expireDateTime=dateFormat.format(selectedDate);
    });
}
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
   EasyLoading.init();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: (){
          Navigator.pop(context,MaterialPageRoute(builder: (context)=>CouponScreen()));
        },
         icon: Icon(
           Icons.arrow_back_ios_new_rounded,
         color: Colors.grey,
         )),
        title: Text(
          widget.singleCoupon==null?"Add Coupon":"Edit Coupon",
        style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(child: 
      SingleChildScrollView(
        child: Padding(padding: EdgeInsets.all(16),
        
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             Text(
                'Discount Type*',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
             Container(
                    padding: EdgeInsets.only(left: 16, right: 16),
                    decoration: BoxDecoration(
                        border: Border.all(
                      color: Colors.grey,
                      width: 1,
                    )),
                    child: DropdownButton(
                        value: valueChoose,
                        hint: Text(
                          'Select',
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        isExpanded: true,
                        underline: SizedBox(),
                        icon: Icon(
                          Icons.expand_more,
                          color: Colors.grey,
                        ),
                        items: listItem.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            valueChoose = newValue.toString();
                          });
                        }),
                  ),
                   SizedBox(height: 20,),
          Text(
                'Code*',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: codeCnt,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ),
                  SizedBox(height: 20,), 
                  Text(
                'Coupon Description',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: descriptionCnt,
                      maxLines: 3,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        
                      ),
                      
              
                    ),
                  ), 
                  SizedBox(height: 20,),
                  Text(
                'Coupon Amount*',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: amountCnt,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ), 
                    SizedBox(height: 20,),
                     SizedBox(
                  height: 20,
                ),
                Text(
                      'Expire Date',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    SizedBox(height: 10,),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(
                      
                    ),
                    borderRadius: BorderRadius.circular(5)
                    
                  ),
                  child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(expireDateTime !=null?DateFormat('dd MMM yyyy').format(DateTime.parse(expireDateTime!)):"Select Date"),
                      ),
                      IconButton(
                        onPressed: (){
                           _selectDate(context);
                        },
                       icon: Icon(Icons.calendar_today_outlined),
                       )
                    ],
                  ),
                ),
                //   Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //       Text(
                // 'Expire Date',
                //         style: TextStyle(
                //             color: Colors.black,
                //             fontWeight: FontWeight.bold,
                //             fontSize: 20),
                //       ),
                //       TextButton(onPressed: (){
                //           _selectDate(context);
                //       }, child: Text(
                // 'select',
                //         style: TextStyle(
                //             color: themeColor,
                //             fontWeight: FontWeight.bold,
                //             fontSize: 20),
                //       ),
                //       )
                //     ],
                //   ),
                  // SizedBox(height: 20,),
                  //  Container(
                  //   child: TextFormField(
                  //     controller: expireDateCnt,
                  //     keyboardType: TextInputType.datetime,
                  //     decoration: InputDecoration(
                  //      hintText: "DD/MM/YY",
                  //       border: OutlineInputBorder(
                  //         borderRadius: BorderRadius.circular(10),
                  //       ),
                  //     ),
                      
              
                  //   ),
                  // ),
                   SizedBox(height: 20,),
                //   Text(
                // 'Product',
                //     style: TextStyle(
                //         color: Colors.black,
                //         fontWeight: FontWeight.bold,
                //         fontSize: 20),
                //   ), 
                //   SizedBox(height: 20,),
                //      Container(
                //     child: TextFormField(
                //       keyboardType: TextInputType.number,
                //       decoration: InputDecoration(
                       
                //         border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //         ),
                //       ),
                      
              
                //     ),
                //   ),
                //    SizedBox(height: 20,),
                //   Text(
                // 'Category',
                //     style: TextStyle(
                //         color: Colors.black,
                //         fontWeight: FontWeight.bold,
                //         fontSize: 20),
                //   ), 
                //   SizedBox(height: 20,),
                //      Container(
                //     child: TextFormField(
                //       keyboardType: TextInputType.number,
                //       decoration: InputDecoration(
                       
                //         border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //         ),
                //       ),
                      
              
                //     ),
                //   ), 
                //    SizedBox(height: 20,),
                //   Text(
                // 'Brand',
                //     style: TextStyle(
                //         color: Colors.black,
                //         fontWeight: FontWeight.bold,
                //         fontSize: 20),
                //   ), 
                //   SizedBox(height: 20,),
                //      Container(
                //     child: TextFormField(
                //       keyboardType: TextInputType.number,
                //       decoration: InputDecoration(
                       
                //         border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //         ),
                //       ),
                      
              
                //     ),
                //   ), 
                   SizedBox(height: 20,),
                  Text(
                'Total Usage Limit',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: totalUseageCnt,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ),
                   SizedBox(height: 20,),
                  Text(
                'Usage Limit Per User',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: usagePerUserCnt,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ),
                  SizedBox(height: 20,),
                  Text(
                'Coupon applied to Max number of cart item ',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: cartProductNumberCnt,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ),   
                  SizedBox(height: 20,),
                  Text(
                'Minimum Price*',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: minPriceCnt,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ),
                   SizedBox(height: 20,),
                  Text(
                'Maximum Price*',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ), 
                  SizedBox(height: 20,),
                     Container(
                    child: TextFormField(
                      controller: maxpriceCnt,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                       
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      
              
                    ),
                  ),
                  
          ],
        ),
        
        
        ),

        
        
        ),
        
      ),
     bottomNavigationBar: Builder(
       builder: (context) {
         return _isLoading
                            ?  BoxLoader():Padding(
           padding: const EdgeInsets.all(8.0),
           child: RoundedButton(
             buttonText: widget.singleCoupon ==null?"Save":"Update", 

           func:()async{
             CouponModel createCouponModel =CouponModel(
              //  code:codeCnt.text ,
              // description:descriptionCnt.text,
              // amount: amountCnt.text,
              // dateExpires:expireDateTime?? '',
              //  usageLimit:int.parse(totalUseageCnt.text),
              //  usageLimitPerUser:int.parse(usagePerUserCnt.text),
              //  limitUsageToXItems: int.parse(cartProductNumberCnt.text),
              //  discountType: valueChoose??'',
              //  minimumAmount: minPriceCnt.text,
              //  maximumAmount: maxpriceCnt.text,
               
               //id: widget.singleCoupon!.id!
             );
          createCouponModel.code ==null  ?createCouponModel.code=codeCnt.text:'';
          createCouponModel.description ==null?  createCouponModel.description=descriptionCnt.text:'';
          createCouponModel.amount ==null?createCouponModel.amount=amountCnt.text:'';
          createCouponModel.dateExpires ==null?createCouponModel.dateExpires=expireDateTime:'';
          createCouponModel.usageLimit !=null?createCouponModel.usageLimit=int.parse(totalUseageCnt.text):int.parse(totalUseageCnt.text=0.toString());
           createCouponModel.usageLimitPerUser !=null?createCouponModel.usageLimitPerUser=int.parse(usagePerUserCnt.text):int.parse(usagePerUserCnt.text=0.toString());
           createCouponModel.limitUsageToXItems != null?createCouponModel.limitUsageToXItems=int.parse(cartProductNumberCnt.text):int.parse(cartProductNumberCnt.text=0.toString());
           createCouponModel.discountType ==null? createCouponModel.discountType=valueChoose:'';
           createCouponModel.minimumAmount == null? createCouponModel.minimumAmount=minPriceCnt.text:'';
           createCouponModel.maximumAmount == null? createCouponModel.maximumAmount=maxpriceCnt.text:''; 
             
               
                setState(() {
                  _isLoading=true;
                });
             if(widget.singleCoupon !=null){
               createCouponModel.id=widget.singleCoupon!.id;
               Response? data = await UpdateDeleteCouponService().updateCoupon(createCouponModel,widget.singleCoupon!.id);
             if(data!.statusCode==201 || data.statusCode==200){
               
                // setState(() {
                //   _isLoading=false;
                // });
                
               Navigator.push(context, MaterialPageRoute(builder: (context)=>CouponScreen()));
                EasyLoading.showSuccess('Success!');
             }
             else{
              
               setState(() {
                 _isLoading=false;
               });
                EasyLoading.showError('Failed');
             }
             }
             else{
               Response? data = await CreateCouponService().createCoupon(createCouponModel);
             if(data!.statusCode==201){
                //await CustomSnackbar.snackbar(context, "Coupon Added");
               
                // setState(() {
                //   _isLoading=false;
                // });
                 
               Navigator.push(context, MaterialPageRoute(builder: (context)=>CouponScreen()));
               EasyLoading.showSuccess('Success!');
             }
             else{
               //CustomSnackbar.snackbar(context, "Coupon not created");
               setState(() {
                 _isLoading=false;
               });
               EasyLoading.showError('Failed');
             }
             }
             
                                      
           },
           
           ),
         );
       }
     )
          
    );
  }
}