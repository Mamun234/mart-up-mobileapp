

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/category_create_model.dart';
import 'package:mart_up_admin_app/models/requests/upload_image_mode.dart';
import 'package:mart_up_admin_app/models/responses/upload_image_response_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/category_screen.dart';
import 'package:mart_up_admin_app/screens/loginScreen.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/create_category_service.dart';
import 'package:mart_up_admin_app/services/image_upload_service.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
//import 'package:snippet_coder_utils/multi_images_utils.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:provider/provider.dart';
import 'package:flutter/src/widgets/image.dart' as img;
import 'package:mart_up_admin_app/models/requests/category_create_model.dart'
    as image;

class AddCategory extends StatefulWidget {
  CategoryModel? singleCategore;
  //CategoryCreateModel? categoryCreateModel;

  AddCategory({
    this.singleCategore,
  });

  @override
  _AddCategoryState createState() => _AddCategoryState();
}

class _AddCategoryState extends State<AddCategory> {
  final formKey = GlobalKey<FormState>();
  String? valueChoose;
  List<Object> images = [];
  List listItem = ['Dhaka', 'Chattogram', 'Rajshahi'];
  TextEditingController categoryNameCtlr = new TextEditingController();
  TextEditingController slugCtlr = new TextEditingController();
  TextEditingController descriptionCtlr = new TextEditingController();
  //late List<UploadImageModel> uploadImageModel;
  List<Asset> invoiceimages = [];
  List<Asset> selectedIamges = [];
  int? categoryId;
  CategoryModel? categoryModel;
  CategoryModel? selectedCategory;
  Future<List<CategoryModel>>? _category;
  ViewModelProvider? viewModelProvider;
  String? key;
  UploadImageResponseModel? categoryImage;
  bool _isLoading = false;
  bool? isTokenExpired;
  String? token;
  int page = 1;
  int limit = 5;
  CategoryCreateModel categoryCreate = CategoryCreateModel();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    widget.singleCategore;
    //getCategory();

    _category = CategoryService.fetchCategory(key,page,limit);
    
    getAdminLoginInfo();
    decodeToken();
    valueAssign();

    
  }
  valueAssign(){
if (widget.singleCategore != null) {
  
      categoryNameCtlr.text =  widget.singleCategore!.name !=null?widget.singleCategore!.name.toString():'';
      slugCtlr.text = widget.singleCategore!.slug !=null?widget.singleCategore!.slug.toString():'';
      descriptionCtlr.text =  widget.singleCategore!.description !=null?widget.singleCategore!.description.toString():'';
      //image baind
      categoryId = widget.singleCategore!.id;
      image.Image scr = image.Image();
     
        scr.src = widget.singleCategore!.image!.src;
     
      categoryCreate.image = scr;
    }
  }

  getAdminLoginInfo() async {
    await viewModelProvider!.fatchAdminLoginInfo();
    print(viewModelProvider!.adminLoginInfoProvider!.data!.token!);
    token = viewModelProvider!.adminLoginInfoProvider!.data!.token!;
    setState(() {});
  }
    decodeToken() {
      if(viewModelProvider!.adminLoginInfoProvider !=null){
        token = viewModelProvider!.adminLoginInfoProvider!.data!.token!;
      }else{
        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
      }
    
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token!);
     isTokenExpired = JwtDecoder.isExpired(token!);
    print('decodec: $decodedToken');
    DateTime expirationDate = JwtDecoder.getExpirationDate(token!);

   
    print('date: $expirationDate');
  }

  tokenCheck(){
 setState(() {
      if (isTokenExpired!) {
        DBHelper().removeLoginAdminData();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
    });
  }

   void showToast() {  
    Fluttertoast.showToast(  
        msg: 'Please Select Image',  
        toastLength: Toast.LENGTH_SHORT,  
        gravity: ToastGravity.BOTTOM,  
        //timeInSecForIos: 1,  
        backgroundColor: Colors.red,  
        textColor: Colors.white  
    );  
  }  

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context,
                MaterialPageRoute(builder: (context) => AddCategory()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          //"Add Category",
          widget.singleCategore == null ? "Add Category" : "Update Category",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //                 SizedBox(
                //   height: 20,
                // ),
                Text(
                  'Category Name*',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: categoryNameCtlr,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Category Name is Empty";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Slug',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: slugCtlr,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                        
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Description',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: descriptionCtlr,
                    decoration: InputDecoration(
                      // contentPadding: new EdgeInsets.symmetric(
                      //     vertical: 25.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    maxLines: 4,
                    // validator: (value) {
                    //   if (value!.isEmpty) {
                    //     return "Enter your description";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Add Parent Category",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),


               

                SizedBox(
                  height: 10,
                ),
                DropdownSearch<CategoryModel>(
                  itemAsString: (CategoryModel? p) => p!.name!,
                  // maxHeight: deviceHeight * .80,
                  maxHeight: 300,
                selectedItem:selectedCategory,
                 showClearButton: true,
                 //showSelectedItems: true,
                  onFind: (String? filter) => _category!,
                  //items: viewModelProvider?.categoryListProvider,
                  clearButton: IconButton(onPressed: (){
                                                                                           setState(() {
                                                                                           selectedCategory=null;
                                                                                           categoryId=null;
                                                                                           print("tap");
                                                                                                });
                                                                                         }, icon: Icon(Icons.close_outlined),
                                                                                         ),
                  label: "Category",
                  //selectedItem: widget.singleCategore,
                  onChanged: (CategoryModel? data) {
                    // print(data.toJson()),
                    // setState(() {
                    //   categoryModel = data;
                    // });

                    // setState(() {
                    //   subDistrict = null;
                    // });
                    // _subDistricts = APIService.fetchSubDistricts(data.id);
                    setState(() {
                      selectedCategory=data;
                      categoryId = data!.id;
                      print(categoryId);
                    });
                  },
                  showSearchBox: true,
                ),
                //buildDistrict(),
                SizedBox(
                  height: 10,
                ),
                //  DropdownSearch<CategoryModel>.multiSelection(
                //  // key: _categoryKey,
                //   itemAsString: (CategoryModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                 
                //   onFind: (String? filter) => _category!,
                //   label: "Category",
                //   //selectedItems: selectedCategory!,
                //   selectedItems: widget.singleCategore,
                  
                //   onChanged: (List<CategoryModel>? data) {
                //     selectedCategory!.clear();
                    
                   
                    
                //     setState(() {
                      
                //     });
                //     // print(data.toJson()),
                //     setState(() {
                      
                //       selectedCategory = data;
                //       print(selectedCategory);
                //     });

                //     // setState(() {
                //     //   //brandId=data!.id;
                //     //  //selectedCategory!.map((e)=>categoryIdList!.add(e.id!)).toList();
                //     //  data!.map((e)=>e.id);
                //     //   print(categoryIdList);
                //     // });
                //   },
                //   showSearchBox: true,
                // ),

//                 MultiImagePicker(
//     totalImages: 6,
//     imageSource: ImagePickSource.gallery,
//     //initialValue: this.productModel.images.map((e) => e.src).toList(), //for showing images from collection
//    //initialValue: uploadImageModel.map((e) => e.images!.src!).toList(),
//     onImageChanged: (images) {
//         this.images = images;
//     },
// ),

                SizedBox(
                  height: 10,
                ),
                Text(
                  "Add Photo",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () {
                    pickInvoiceImages();
                  },
                  child: Container(
                    height: SizeConfig.safeBlockVertical! * 8,
                    width: SizeConfig.safeBlockHorizontal! * 23,
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.grey)),
                    child: Icon(
                      Icons.add_a_photo_outlined,
                      size: 32,
                    ),
                  ),
                ),
                invoiceimages.length != 0
                    ? Visibility(
                        visible: invoiceimages.length > 0,
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 13,
                          child: Container(
                            child: ListView.builder(
                              
                                scrollDirection: Axis.horizontal,
                                itemCount: invoiceimages.length,
                                itemBuilder: (context, index) {
                                  Asset asset = invoiceimages[index];

                                  return Container(
                                    padding: EdgeInsets.all(5),
                                    child: Stack(
                                      children: [
                                        AssetThumb(
                                        asset: asset,
                                        width: 90,
                                        height: 90,
                                      ),
                                       Positioned(
                                      bottom: 72,
                                      left: 64,
                                      child:  GestureDetector(
                                  onTap: (){
                                     invoiceimages.removeAt(index);
                                    setState(() {
                                      
                                    });
                                  },
                                  child: Card(
                                                                                           
                                     shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                      ),
                                      child: Center(child: Icon(Icons.close,color: Colors.redAccent,size: 20,)),
                                      ),
                                ),
                                    //  child: IconButton(
                                    //   onPressed: (){
                                    //       // widget.singleProduct!.images!.removeAt(index);
                                    //       // imgList.removeAt(index);
                                    //       // print(imgList.length);
                                    //       //selectedIamges.removeAt(index);
                                    //       invoiceimages.removeAt(index);
                                    //       setState(() {
                                            
                                    //       });
                                         
                                    //                               },
                                    //                                icon: Icon(Icons.close,color: Colors.red,size: 16,)
                                    //                               )
                                      )
                                      ]
                                    ),
                                  );
                                }),
                          ),
                        ))
                    : Text(""),
                widget.singleCategore?.image != null 
                    ? Visibility(
                        visible: widget.singleCategore!.image != null,
                        child: Stack(
                          children: [
                            Container(
                            height: 88,
                            width: 112,
                             decoration: BoxDecoration(
                                                          border: Border.all(width: 1,color: themeColor)
                                                        ),
                            child: img.Image.network(
                              widget.singleCategore!.image!.src!,
                              fit: BoxFit.cover,
                            ),
                          ),
                           Positioned(
                                  //height:  SizeConfig.safeBlockVertical! * .1,
                                                        //width: SizeConfig.blockSizeHorizontal! *52,
                                  
                                  //  bottom: SizeConfig.blockSizeHorizontal! * 10,
                                  //  left:SizeConfig.blockSizeHorizontal! *19,
                                  bottom: 62,
                                  left: 86,
                                child:  GestureDetector(
                                  onTap: (){
                                    widget.singleCategore!.image=null ;
                                    setState(() {
                                      
                                    });
                                  },
                                  child: Card(
                                                                                           
                                     shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                      ),
                                      child: Center(child: Icon(Icons.close,color: Colors.redAccent,size: 20,)),
                                      ),
                                ),
                                  // child: Card(
                                  //   shape: RoundedRectangleBorder(
                                  //     borderRadius: BorderRadius.circular(50),
                                  //   ),
                                  //   child: Center(
                                  //     child: IconButton(
                                  //       onPressed: (){
                                  //           // widget.singleProduct!.images!.removeAt(index);
                                  //           // imgList.removeAt(index);
                                  //           // print(imgList.length);
                                           
                                  //           widget.singleCategore!.image=null ;
                                  //          setState(() {
                                             
                                  //          });
                                  //                                   },
                                  //                                    icon: Icon(Icons.close,color: Colors.red,size: 16,)
                                  //                                   ),
                                  //   ),
                                  // )
                                )
                          ]
                        ))
                    : Text("")
              ],
            ),
          ),
        ),
      )),
      bottomNavigationBar: Builder(builder: (context) {
        return _isLoading
            ? BoxLoader()
            : Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                child: RoundedButton(
                    buttonText: widget.singleCategore ==null?"Save":"Update",
                    func: () async {
                      if (formKey.currentState!.validate()&& (invoiceimages.isNotEmpty || widget.singleCategore !=null)) {
                         setState(() {
                           tokenCheck();
                         });
                        setState(() {
                          _isLoading = true;
                        });
                        //PhotoUploadeModel photos=PhotoUploadeModel(image: invoiceimages.);
                        selectedIamges = invoiceimages;

                        if (selectedIamges.isNotEmpty) {
                          Response data = await ImageUplodeService()
                              .imageupload(selectedIamges, token!);

                          if (data.statusCode == 200 ||
                              data.statusCode == 201) {
                            categoryImage =
                                await ImageUplodeService.parseCategoryImage(
                                    data.body);
                          } else {
                            print("not uploaded");
                          }
                        } else {
                          print("Not created");
                        }

                        //CategoryCreateModel category = CategoryCreateModel();
                        //CategoryModel category=CategoryModel();
                        //Images srcUrl=Images();
                        //srcUrl.src=categoryImage!.guid!.rendered!;
                        // Imagess srcUrl=Imagess();
                        // srcUrl.src=categoryImage!.guid!.rendered!;
                        categoryCreate.name==null?categoryCreate.name = categoryNameCtlr.text:'';
                        categoryCreate.slug==null?categoryCreate.slug = slugCtlr.text:'';
                        categoryCreate.description==null?categoryCreate.description = descriptionCtlr.text:'';
                        categoryCreate.parent==null?categoryCreate.parent = categoryId:0;
                        //  category.image=srcUrl;
                        //category.image=srcUrl;
                        //category.display="default";

                        //category.image!.src=categoryImage!.guid!.rendered;

                        //  category.image!.alt=categoryImage!.altText;
                        //  category.image!.name=categoryImage!.slug;

                        if (widget.singleCategore != null) {
                          categoryCreate.id = widget.singleCategore!.id;
                          Response? res = await CreateCategoryService()
                              .updateCategory(
                                  categoryId:categoryId,
                                  categoryImage: categoryImage,
                                  token: token,
                                  createCategory: categoryCreate);
                                    if (res!.statusCode == 200 || res.statusCode == 201) {
                                       EasyLoading.showSuccess('Success!');
                                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CategoryScreen()));
                                 
                                    }else{
                                      setState(() {
                            _isLoading = false;
                          });
                          EasyLoading.showError("Failed!");
                                    }

                          

                          // await CustomSnackbar.snackbar(
                          //     context, "category is not created ");
                          
                        } else {
                          Response? res = await CreateCategoryService()
                              .createCategory(
                                  categoryCreate, categoryImage!, token!);
                          if (res!.statusCode == 200 || res.statusCode == 201) {
                            // CustomSnackbar.snackbar(
                            //     context, "category created succesfully");
                              EasyLoading.showSuccess('Success!');
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CategoryScreen()));
                          } else {
                            // await CustomSnackbar.snackbar(
                            //     context, "category is not created ");
                            setState(() {
                              _isLoading = false;
                            });
                              EasyLoading.showError("Failed!");
                          }
                        }
                      }
                      else{
                        showToast();
                      }
                    }));
      }),
    );
  }

  //add pic
  Future<void> pickInvoiceImages() async {
    List<Asset> resultList = [];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        selectedAssets: invoiceimages,
        
        materialOptions: MaterialOptions(
          statusBarColor: "#4CAF50",
          actionBarColor: "#4CAF50",
          actionBarTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#4CAF50",
        ),
      );
    } on Exception catch (e) {
      print(e);
    }
    setState(() {
      invoiceimages = resultList;
      if(widget.singleCategore !=null){
        widget.singleCategore!.image =null;
      }
    });
  }
}
