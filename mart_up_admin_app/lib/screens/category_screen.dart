import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/providers/category_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/add_category_screen.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/create_category_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/loader_categorey_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  late CategoryProvider categoryProvider;
  bool _isConnection = false;
  bool _isloading = true;
   bool _isloading2 = true;
  TextEditingController _searchKeyController = TextEditingController();
  List<CategoryModel>? allCategory;
  //String? searchKey;
  int _page = 1;
  int _limit = 5;
  int deleteCount=0;

  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    checkNetConnectivity();
    categoryProvider = Provider.of<CategoryProvider>(context, listen: false);

    getCategory();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }

  getCategory() async {
    categoryProvider.page = _page;
    categoryProvider.limit = _limit;

    await categoryProvider.getCategory(
        categoryProvider.page, categoryProvider.limit);

    setState(() {
      _isloading = false;
        _isloading2 = false;
      //allCategory = categoryProvider.categoryListProvider;
    });
  }

  void _onLoading() async {
    _page++;

    setState(() {});

    var list = await CategoryService.fetchCategory(
        categoryProvider.searchKey, _page, _limit);
    if (list != null) {
      categoryProvider.categoryListProvider!.addAll(list);
      //print(viewModelProvider.productListProvider!.length);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchCategory() async {
    categoryProvider.categoryListProvider!.clear();
    setState(() {
      _isloading = true;
       _isloading2 = true;
    });
    var list = await CategoryService.fetchCategory(
        categoryProvider.searchKey = _searchKeyController.text,
        categoryProvider.page = _page,
        categoryProvider.limit = _limit);

    if (list != null) {
      categoryProvider.categoryListProvider!.addAll(list);
    }
    setState(() {
      _isloading = false;
       _isloading2 = false;
        categoryProvider.searchKey =null;
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
            appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon: 
            hintText: hintText,
            hintStyle: TextStyle(
                color: Colors.black54,),
          ),
        ),
        actions: [
          IconButton(
                  onPressed: () {
                    searchCategory();
                    
                  },
                  icon: Icon(Icons.search_outlined,size: 28,),
                  color: tabSelectColor,
                ),
        ],
      ),
      drawer: AppDrawer(),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 14, vertical: 14),
        child: _isConnection
            ? _isloading && _isloading2
                ? LoaderCategoryScreen(): _isloading2?Center(child: BoxLoader(),)
                : categoryProvider.categoryListProvider!.isEmpty
                    ? EmptyScreen()
                    : SmartRefresher(
                        controller: refreshController,
                        // scrollDirection: Axis.vertical,
                        // physics: BouncingScrollPhysics(),
                        enablePullUp: true,
                        onLoading: _onLoading,
                        onRefresh: _onRefresh,
                        footer: ClassicFooter(
                          loadStyle: LoadStyle.ShowWhenLoading,
                        ),
                        child: ListView.builder(
                            itemCount:
                                categoryProvider.categoryListProvider!.length,
                            itemBuilder: (BuildContext context, index) {
                              return Card(
                                elevation: 1,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Container(
                                    height: SizeConfig.safeBlockVertical! * 15,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                                height: SizeConfig
                                                        .safeBlockVertical! *
                                                    16,
                                                width: SizeConfig
                                                        .safeBlockHorizontal! *
                                                    28,
                                                child: Image.network(
                                                  categoryProvider
                                                      .categoryListProvider![
                                                          index]
                                                      .image!
                                                      .src!,
                                                  fit: BoxFit.contain,
                                                )),
                                          SizedBox(width: 10,),
                                            Container(
                                              //color: Colors.amber,
                                              width: SizeConfig.blockSizeHorizontal! * 40,
                                              child: Text(
                                                categoryProvider
                                                    .categoryListProvider![index]
                                                    .name!,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 24,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            GestureDetector(
                                                onTap: () {
                                                  print("tap");

                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              AddCategory(
                                                                  singleCategore:
                                                                      categoryProvider.categoryListProvider![
                                                                          index])));
                                                },
                                                child: Icon(Icons.edit)),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            GestureDetector(
                                                onTap: () {
                                                  print("tap");

                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (context) =>
                                                              AlertDialog(
                                                                title: Text(
                                                                    'Delete',
                                                                    textScaleFactor:
                                                                        1),
                                                                content: Text(
                                                                    'Are you sure delete',
                                                                    textScaleFactor:
                                                                        1),
                                                                actions: <
                                                                    Widget>[
                                                                  TextButton(
                                                                      child: Text(
                                                                          'Cancle'),
                                                                      onPressed:
                                                                          () {
                                                                        Navigator.of(context)
                                                                            .pop(false);
                                                                      }),
                                                                  TextButton(
                                                                      child: Text(
                                                                          'Delete'),
                                                                      onPressed:
                                                                          () async {
                                                                           
                                                                        Navigator.of(context)
                                                                            .pop(true);
                                                                            
                                                                            setState(
                                                                              () {
                                                                            _isloading2 =
                                                                                true;
                                                                          });
                                                                        // _isloading =
                                                                        //     true;
                                                                       
                                                                        Response?
                                                                            res =
                                                                            await CreateCategoryService().deleteCategory(categoryId: categoryProvider.categoryListProvider![index].id);
                                                                        // Response?
                                                                        //     res =
                                                                        //     CreateCategoryService().deleteCategory(categoryId: allCategory![index].id);
                                                                        if (res!.statusCode ==
                                                                                200 ||
                                                                            res.statusCode ==
                                                                                201) {
                                                                                   setState(
                                                                              () {
                                                                            _isloading2 =
                                                                                false;
                                                                          });
                                                                          EasyLoading.showSuccess("Success!");
                                                                          
                                                                       getCategory();
                                                                          
                                                                        } else {
                                                                            setState(
                                                                              () {
                                                                            _isloading2 =
                                                                                false;
                                                                          });
                                                                             EasyLoading.showError("Failed!");
                                                                        
                                                                        }

                                                                        // getCategory();
                                                                        // setState(
                                                                        //     () {});
                                                                      }),
                                                                ],
                                                              ));
                                                },
                                                child: Icon(Icons.delete))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }),
                      )
            : NoInternet(
                pressed: () async {
                  await checkNetConnectivity();
                  _isConnection == false
                      ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                      : checkNetConnectivity();
                  getCategory();
                },
              ),
      )),
      floatingActionButton:  FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddCategory(
                            // allCategory: allCategory,
                            )));
              },
              backgroundColor: themeColor,
              child: const Icon(
                Icons.add_outlined,
                size: 50,
              ),
            ),
    );
  }
}
