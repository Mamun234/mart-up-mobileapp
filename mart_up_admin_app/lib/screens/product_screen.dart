import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/links.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart'
    as product;
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/providers/product_provider.dart';

import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/add_product.dart';
import 'package:mart_up_admin_app/screens/cart_screen.dart';
import 'package:mart_up_admin_app/screens/filter_screen.dart';
import 'package:mart_up_admin_app/screens/product_details.dart';
import 'package:mart_up_admin_app/services/coupon_update_delete_service.dart';
import 'package:mart_up_admin_app/services/product_edit_delete_service.dart';
import 'package:mart_up_admin_app/services/product_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/loader_product_card_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter/src/widgets/image.dart' as img;

class ProductScreen extends StatefulWidget {
  bool? isOrder;
  int? id;
  int? brandId;
  var minPrice, maxPrice;
  ProductScreen({
    this.isOrder,
    this.id,
    this.brandId,
    this.minPrice,
    this.maxPrice,
  });

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  late ProductProvider productProvider;
  bool _isloading = true;
  bool _isloading2 = true;
  bool _isConnection = false;
  TextEditingController _searchKeyController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int _page = 1;
  int _limit = 5;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  //List<String>? displayImageSrc;
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();
  //List<ProductModel>? displayImage;
  String? searchKey;

  @override
  void initState() {
    super.initState();
    checkNetConnectivity();
    productProvider = Provider.of<ProductProvider>(context, listen: false);

    getProduct();
    //displayImage();
    //setDisplayImage();

    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }

  getProduct() async {
    productProvider.page = _page;
    productProvider.limit = _limit;
    if (widget.id != null ||
        widget.brandId != null ||
        widget.minPrice != null ||
        widget.maxPrice != null) {
      productProvider.id = widget.id;
      productProvider.brandId = widget.brandId;
      productProvider.minPrice = widget.minPrice;
      productProvider.maxPrice = widget.maxPrice;
    }

    await productProvider.getProduct(
      productProvider.page,
      productProvider.limit,
      searchKey,
      productProvider.id,
      productProvider.brandId,
      productProvider.minPrice,
      productProvider.maxPrice,
    );

    setState(() {
      _isloading = false;
      _isloading2 = false;
      productProvider.id = null;
      productProvider.brandId = null;
      productProvider.minPrice = null;
      productProvider.maxPrice = null;
    });
    for (int i = 0; i < productProvider.productListProvider!.length; i++) {
      product.Image im = product.Image();
      if (productProvider.productListProvider![i].images!.isEmpty) {
        im.src = emptyImage;
        productProvider.productListProvider![i].images!.add(im);
        //displayImage![i].images![i].src =await emptyImage;
      }
    }
  }

  void _onLoading() async {
    _page++;
    // viewModelProvider.page = _page++;
    // viewModelProvider.limit = _limit;
    setState(() {});
    // var list = await viewModelProvider.getProduct(
    //     viewModelProvider.page, viewModelProvider.limit);
    var list = await ProductService.fetchProducts(
        _page, _limit, productProvider.searchKey,
        categoryId: widget.id,
        brandId: widget.brandId,
        minPrice: widget.minPrice,
        maxPrice: widget.maxPrice);
    if (list != null) {
      productProvider.productListProvider!.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchProduct() async {
    productProvider.productListProvider!.clear();
    setState(() {
      _isloading = true;
      _isloading2 = true;
      //viewModelProvider.brandListProvider!.isEmpty;
    });
    var list = await ProductService.fetchProducts(
      productProvider.page = _page,
      productProvider.limit = _limit,
      productProvider.searchKey = _searchKeyController.text,
    );

    if (list != null) {
      productProvider.productListProvider!.addAll(list);
    }
    setState(() {
      _isloading = false;
      _isloading2 = false;
      productProvider.searchKey = null;
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  void dispose() {
    super.dispose();
  }

  // displayImage()async{
  //           for(int i=0;i< viewModelProvider.productListProvider![i].images!.length;i++) {
  //      displayImageSrc![i] =  viewModelProvider.productListProvider![i].images![0].src!;
  //   }

  // }

  // setDisplayImage()async{
  //    for(int i=0;i< viewModelProvider.productListProvider!.length;i++) {
  //      product.Image im=product.Image();
  //      if(viewModelProvider.productListProvider![i].images!.isEmpty){
  //        im.src=emptyImage;
  //        viewModelProvider.productListProvider![i].images!.add(im);
  //       //displayImage![i].images![i].src =await emptyImage;
  //      }

  //    }

  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    EasyLoading.init();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon:
            hintText: hintText,
            hintStyle: TextStyle(
              color: Colors.black54,
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              searchProduct();
            },
            icon: Icon(
              Icons.search_outlined,
              size: 28,
            ),
            color: tabSelectColor,
          ),
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FilterPage()));
            },
            icon: Icon(
              Icons.filter_alt_outlined,
              size: 32,
            ),
            color: tabSelectColor,
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: SafeArea(
          child: SmartRefresher(
              controller: refreshController,
              // scrollDirection: Axis.vertical,
              // physics: BouncingScrollPhysics(),
              enablePullUp: true,
              onLoading: _onLoading,
              onRefresh: _onRefresh,
              footer: ClassicFooter(
                loadStyle: LoadStyle.ShowWhenLoading,
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: _isConnection
                    ? _isloading && _isloading2
                        ? LoaderProductCardScreen()
                        : _isloading2
                            ? Center(
                                child: BoxLoader(),
                              )
                            : _isloading == false &&
                                    productProvider.productListProvider!.isEmpty
                                ? EmptyScreen()
                                : GridView.builder(
                                    shrinkWrap: true,
                                    physics: BouncingScrollPhysics(),

                                    // gridDelegate:
                                    //     SliverGridDelegateWithFixedCrossAxisCount(
                                    //   crossAxisCount: 2,
                                    //   // mainAxisExtent:
                                    //   //     270,
                                    //       childAspectRatio: 1/1.4,
                                    // ),
                                    gridDelegate:
                                        SliverGridDelegateWithMaxCrossAxisExtent(
                                      maxCrossAxisExtent:
                                          SizeConfig.blockSizeHorizontal! * 46,
                                      childAspectRatio: 1 / 1.41,
                                      crossAxisSpacing: 8,
                                      mainAxisSpacing: 8,
                                    ),
                                    itemCount: productProvider
                                        .productListProvider!.length,
                                    itemBuilder: (BuildContext context, index) {
                                      product.ProductModel pdt = productProvider
                                          .productListProvider![index];

                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ProductDetailPage(
                                                        id: productProvider
                                                            .productListProvider![
                                                                index]
                                                            .id,
                                                      )));
                                        },
                                        child: Card(
                                          elevation: 1.0,
                                          shadowColor: Colors.black38,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.all(12.0),
                                            child: Container(
                                              //color: Colors.amber,

                                              //height: 300,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Stack(
                                                    children: [
                                                      Stack(
                                                        children: [
                                                          Container(
                                                              height: SizeConfig
                                                                      .safeBlockVertical! *
                                                                  17,
                                                              width: SizeConfig
                                                                      .safeBlockHorizontal! *
                                                                  40,
                                                              child: img.Image
                                                                  .network(
                                                                productProvider
                                                                            .productListProvider![index]
                                                                            .images ==
                                                                        null
                                                                    ? emptyImage
                                                                    : '${productProvider.productListProvider![index].images![0].src!}',
                                                                fit: BoxFit
                                                                    .cover,
                                                              )),
                                                          Positioned(
                                                            right: 5,
                                                            top: 5,
                                                            child:
                                                                GestureDetector(
                                                              onTap: () {
                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder: (context) =>
                                                                            AddProduct(
                                                                              singleProduct: productProvider.productListProvider![index],
                                                                            )));
                                                              },
                                                              child: Container(
                                                                  height: SizeConfig
                                                                          .safeBlockVertical! *
                                                                      3,
                                                                  width: SizeConfig
                                                                          .safeBlockVertical! *
                                                                      6,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: Colors
                                                                        .white,
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(4),
                                                                  ),
                                                                  child: Icon(
                                                                    Icons
                                                                        .edit_outlined,
                                                                    color: Color(
                                                                        0xff40BFFF),
                                                                    size: 16,
                                                                  )),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Positioned(
                                                        right: 5,
                                                        top: 5,
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            AddProduct(
                                                                              singleProduct: productProvider.productListProvider![index],
                                                                            )));
                                                          },
                                                          child: Container(
                                                              height: SizeConfig
                                                                      .safeBlockVertical! *
                                                                  3,
                                                              width: SizeConfig
                                                                      .safeBlockVertical! *
                                                                  6,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: Colors
                                                                    .white,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            4),
                                                              ),
                                                              child: Icon(
                                                                Icons
                                                                    .edit_outlined,
                                                                color: Color(
                                                                    0xff40BFFF),
                                                                size: 16,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    productProvider
                                                            .productListProvider![
                                                                index]
                                                            .name!
                                                            .isNotEmpty
                                                        ? '${productProvider.productListProvider![index].name!}'
                                                        : "",
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        color: productNameColor,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        "Category:",
                                                        style: TextStyle(
                                                            fontSize: 11),
                                                      ),
                                                      Text(
                                                        '${productProvider.productListProvider![index].categories![0].name}',
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 12),
                                                      )
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        productProvider
                                                                .productListProvider![
                                                                    index]
                                                                .salePrice!
                                                                .isNotEmpty
                                                            ? '৳${productProvider.productListProvider![index].salePrice!}'
                                                            : "",
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color:
                                                                salePriceColor,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        productProvider
                                                                .productListProvider![
                                                                    index]
                                                                .regularPrice!
                                                                .isNotEmpty
                                                            ? '৳${productProvider.productListProvider![index].regularPrice!}'
                                                            : "",
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color:
                                                                previousPriceColor,
                                                            decoration:
                                                                TextDecoration
                                                                    .lineThrough,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          // Text(
                                                          //   viewModelProvider
                                                          //           .productListProvider![
                                                          //               index]
                                                          //           .salePrice!
                                                          //           .isNotEmpty
                                                          //       ? '৳${viewModelProvider.productListProvider![index].salePrice!}'
                                                          //       : "",
                                                          //   maxLines: 1,
                                                          //   overflow: TextOverflow
                                                          //       .ellipsis,
                                                          //   style: TextStyle(
                                                          //       color:
                                                          //           salePriceColor,
                                                          //       fontWeight:
                                                          //           FontWeight
                                                          //               .bold),
                                                          // ),
                                                          // Text(
                                                          //   viewModelProvider
                                                          //           .productListProvider![
                                                          //               index]
                                                          //           .regularPrice!
                                                          //           .isNotEmpty
                                                          //       ? '৳${viewModelProvider.productListProvider![index].regularPrice!}'
                                                          //       : "",
                                                          //   maxLines: 1,
                                                          //   overflow: TextOverflow
                                                          //       .ellipsis,
                                                          //   style: TextStyle(
                                                          //       color:
                                                          //           previousPriceColor,
                                                          //       decoration:
                                                          //           TextDecoration
                                                          //               .lineThrough,
                                                          //       fontWeight:
                                                          //           FontWeight
                                                          //               .w400),
                                                          // ),
                                                          widget.isOrder != true
                                                              ? GestureDetector(
                                                                  onTap: () {
                                                                    showDialog(
                                                                        context:
                                                                            _scaffoldKey
                                                                                .currentContext!,
                                                                        builder: (contex) =>
                                                                            AlertDialog(
                                                                              title: Text(
                                                                                'DELETE',
                                                                                textScaleFactor: 1,
                                                                              ),
                                                                              content: Text('Do you want to delete ?', textScaleFactor: 1),
                                                                              actions: <Widget>[
                                                                                TextButton(
                                                                                    child: Text('Cancel'),
                                                                                    onPressed: () {
                                                                                      Navigator.of(context).pop(true);
                                                                                    }),
                                                                                TextButton(
                                                                                    child: Text('Delete'),
                                                                                    onPressed: () async {
                                                                                      //Navigator.of(context,rootNavigator: true).pop();
                                                                                      Navigator.of(context).pop(true);
                                                                                      setState(() {
                                                                                        _isloading2 = true;
                                                                                      });
                                                                                      Response? res = await ProductEditDeleteService().deleteProduct(productProvider.productListProvider![index].id!);
                                                                                      if (res!.statusCode == 200 || res.statusCode == 201) {
                                                                                        setState(() {
                                                                                          _isloading2 = false;
                                                                                        });
                                                                                        EasyLoading.showSuccess("Success!");
                                                                                        getProduct();
                                                                                      } else {
                                                                                        setState(() {
                                                                                          _isloading2 = false;
                                                                                        });
                                                                                        EasyLoading.showError("Failed!");
                                                                                      }
                                                                                    }),
                                                                              ],
                                                                            ));
                                                                  },
                                                                  child: Container(
                                                                      height: SizeConfig.safeBlockVertical! * 3,
                                                                      width: SizeConfig.safeBlockVertical! * 6,
                                                                      decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(4),
                                                                        color: Color(
                                                                            0xffFB7181),
                                                                      ),
                                                                      child: Icon(
                                                                        Icons
                                                                            .delete_forever_outlined,
                                                                        color: Color(
                                                                            0xffFFFFFF),
                                                                        size:
                                                                            16,
                                                                      )),
                                                                )
                                                              : GestureDetector(
                                                                  onTap: () {
                                                                    int? t = 0;
                                                                    Iterable<
                                                                        product
                                                                            .ProductModel> containsItem = productProvider
                                                                        .cartList
                                                                        .where((element) => element
                                                                            .name!
                                                                            .contains(pdt.name!));

                                                                    if (containsItem
                                                                        .isNotEmpty) {
                                                                      CustomSnackbar.snackbar(
                                                                          context,
                                                                          'Item already added!');
                                                                    } else {
                                                                      productProvider.cartList.add(ProductModel(
                                                                          id: pdt
                                                                              .id,
                                                                          name: pdt
                                                                              .name,
                                                                          images: pdt
                                                                              .images,
                                                                          price: pdt
                                                                              .price,
                                                                          salePrice: pdt
                                                                              .salePrice,
                                                                          regularPrice: pdt
                                                                              .regularPrice,
                                                                          quantity:
                                                                              1));
                                                                      for (var element
                                                                          in productProvider
                                                                              .cartList) {
                                                                        t = int.parse(element.price!) +
                                                                            t!;
                                                                      }
                                                                      productProvider
                                                                          .setCart(
                                                                              productProvider.cartList);
                                                                      productProvider
                                                                          .setTotalModel(
                                                                              t);
                                                                    }

                                                                    setState(
                                                                        () {});
                                                                  },
                                                                  child: Container(
                                                                      height: SizeConfig.safeBlockVertical! * 3,
                                                                      width: SizeConfig.safeBlockVertical! * 6,
                                                                      decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(4),
                                                                        color: Color(
                                                                            0xffFB7181),
                                                                      ),
                                                                      child: Icon(
                                                                        Icons
                                                                            .shopping_cart,
                                                                        color: Color(
                                                                            0xffFFFFFF),
                                                                        size:
                                                                            16,
                                                                      )),
                                                                ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    })
                    : NoInternet(
                        pressed: () async {
                          await checkNetConnectivity();
                          _isConnection == false
                              ? CustomSnackbar.snackbar(
                                  context, "Turn Internet ON")
                              : checkNetConnectivity();
                          getProduct();
                        },
                      ),
              ))),
      floatingActionButton: widget.isOrder != true
          ? FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddProduct()));
              },
              backgroundColor: themeColor,
              child: const Icon(
                Icons.add_outlined,
                size: 50,
              ),
            )
          : Consumer<ProductProvider>(
              builder: (context, productProvider, child) {
              return FloatingActionButton(
                onPressed: () {
                  // Add your onPressed code here!
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CartPage()));
                },
                backgroundColor: Colors.white,
                child: Stack(children: [
                  Align(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.shopping_cart,
                      size: 30,
                      color: Colors.grey,
                    ),
                  ),
                  Positioned(
                      bottom: 32,
                      left: 34,
                      child: Text(
                        productProvider.cartList.isEmpty
                            ? '0'
                            : productProvider.cartList.length.toString(),
                        style: TextStyle(
                            color: Colors.redAccent,
                            fontWeight: FontWeight.w700,
                            fontFamily: fontFamily),
                      )),
                ]),
              );
            }),
    );
  }
}
