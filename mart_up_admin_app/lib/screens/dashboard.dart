import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/providers/brand_provider.dart';
import 'package:mart_up_admin_app/providers/category_provider.dart';
import 'package:mart_up_admin_app/providers/coupon_provider.dart';
import 'package:mart_up_admin_app/providers/customer_provider.dart';
import 'package:mart_up_admin_app/providers/order_provider.dart';
import 'package:mart_up_admin_app/providers/product_provider.dart';
import 'package:mart_up_admin_app/providers/review_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/brand_screen.dart';
import 'package:mart_up_admin_app/screens/category_screen.dart';
import 'package:mart_up_admin_app/screens/coupon_screen.dart';
import 'package:mart_up_admin_app/screens/customer_screen.dart';
import 'package:mart_up_admin_app/screens/order_screen.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/screens/review_screen.dart';
import 'package:mart_up_admin_app/screens/sales_report_screen.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/linear_indicator.dart';
import 'package:provider/provider.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  late ViewModelProvider viewModelProvider;
  late CategoryProvider categoryProvider;
  late BrandProvider brandProvider;
  late CouponProvider couponProvider;
  late ProductProvider productProvider;
  late CustomerProvider customerProvider;
  late OrderProvider orderProvider;
  late ReviewProvider reviewProvider;
  //ProfileInfoModel? admin;
  bool _isLoading = true;
  int totalProductCOunt = 0;
  int totalCouponCount = 0;
  int totalCustomerCount = 0;
  int totalOrderCount = 0;
  int totalReviewCount = 0;
  int? pendingOrderCount = 0;
  int? processingOrderCount = 0;
  int? completeOrderCount = 0;
  int? returnOrderCount = 0;
  String? salesPeriod;
  String? minDate;
  String? maxDate;
  String? pending = 'pending';
  String? complete = 'completed';
  String? processing = 'processing';
  String? refunded = 'refunded';
  @override
  void initState() {
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    categoryProvider = Provider.of<CategoryProvider>(context, listen: false);
    brandProvider = Provider.of<BrandProvider>(context, listen: false);
    couponProvider = Provider.of<CouponProvider>(context, listen: false);
    productProvider = Provider.of<ProductProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    orderProvider = Provider.of<OrderProvider>(context, listen: false);
    reviewProvider = Provider.of<ReviewProvider>(context, listen: false);
    getAdmin();
    //getAllCount();
    getTotalBrand();
    getTotalProduct();
    getTotalCoupon();
    getTotalCustomer();
    getTotalOrder();
    getTotalReview();
    getSalesReports();
    getTotalCategory();

    setState(() {});
  }

  getAllCount() {
    getTotalProduct();
    setState(() {
      getTotalCoupon();
    });
    setState(() {
      getTotalCustomer();
    });
    setState(() {
      getTotalOrder();
    });
    setState(() {
      getTotalReview();
    });
    setState(() {
      getSalesReports();
    });
  }

  getAdmin() async {
    await viewModelProvider.getAdminInfo();
    setState(() {});
  }

  getTotalProduct() async {
    await productProvider.getTotalProduct();
    setState(() {});

    for (int i = 0; i < productProvider.totalProductList!.length; i++) {
      totalProductCOunt =
          totalProductCOunt + productProvider.totalProductList![i].total!;
    }
    setState(() {});
    //print(totalProductCOunt);
  }

  getTotalCoupon() async {
    await couponProvider.getTotalCouponCount();
    setState(() {});

    // for(int i=0;i<viewModelProvider.totalCouponList!.length;i++){
    //   totalCouponCount=totalCouponCount +viewModelProvider.totalCouponList![i].total!;
    // }
    // setState(() {

    // });
    //print(totalCouponCount);
  }

  getTotalCustomer() async {
    await customerProvider.getTotalCustomerCount();
    setState(() {});

    for (int i = 0; i < customerProvider.totalCustomerList!.length; i++) {
      totalCustomerCount =
          totalCustomerCount + customerProvider.totalCustomerList![i].total!;
    }
    setState(() {});
    // print(totalCustomerCount);
  }

  getTotalOrder() async {
    await orderProvider.getTotalOrderCount();
    setState(() {});

    for (int i = 0; i < orderProvider.totalOrderList!.length; i++) {
      totalOrderCount =
          totalOrderCount + orderProvider.totalOrderList![i].total!;
    }
    setState(() {});
    for (int i = 0; i < orderProvider.totalOrderList!.length; i++) {
      if (orderProvider.totalOrderList![i].slug == "pending") {
        pendingOrderCount = orderProvider.totalOrderList![i].total;
      } else if (orderProvider.totalOrderList![i].slug == "processing") {
        processingOrderCount = orderProvider.totalOrderList![i].total;
      } else if (orderProvider.totalOrderList![i].slug == "completed") {
        completeOrderCount = orderProvider.totalOrderList![i].total;
      } else if (orderProvider.totalOrderList![i].slug == "refunded") {
        returnOrderCount = orderProvider.totalOrderList![i].total;
      }
    }
    setState(() {});
    // print(totalOrderCount);
    // print(pendingOrderCount);
  }

  getTotalReview() async {
    await reviewProvider.getTotalReviewCount();
    setState(() {});

    // for(int i=0;i<viewModelProvider.totalReviewList!.length;i++){
    //   totalReviewCount=totalReviewCount +viewModelProvider.totalReviewList![i].total!;
    // }
    // setState(() {

    // });
    // print(totalReviewCount);
  }

  getSalesReports() async {
    await viewModelProvider.getSaleReport();
    setState(() {});
  }

  getTotalCategory() async {
    await categoryProvider.getTotalCategory();
    setState(() {});
  }

  getTotalBrand() async {
    await brandProvider.getTotalBrand();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Dashbord",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      drawer: AppDrawer(),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 10, right: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductScreen()));
                      },
                      child: Card(
                        elevation: 2.0,
                        shadowColor: Colors.black38,
                        color: productCardColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 18,
                          width: SizeConfig.safeBlockHorizontal! * 29,
                          //color: productCardColor,
                          // decoration: BoxDecoration(

                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: SizeConfig.safeBlockVertical! * 9,
                                width: SizeConfig.safeBlockHorizontal! * 15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.safeBlockHorizontal! * 12),
                                    color: Colors.white),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/product.png",
                                      height: SizeConfig.safeBlockVertical! * 8,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 10,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Product",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: SizeConfig.safeBlockVertical! * 3,
                                width: SizeConfig.safeBlockHorizontal! * 12,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white),
                                child: Center(
                                  child: totalProductCOunt != 0
                                      ? Text(
                                          totalProductCOunt.toString(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )
                                      : LinearIndicator(),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    //Category
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CategoryScreen()));
                      },
                      child: Card(
                        elevation: 2.0,
                        shadowColor: Colors.black38,
                        color: categoryCardColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 18,
                          width: SizeConfig.safeBlockHorizontal! * 29,
                          // decoration: BoxDecoration(
                          //   color: categoryCardColor,
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: SizeConfig.safeBlockVertical! * 9,
                                width: SizeConfig.safeBlockHorizontal! * 15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.blockSizeHorizontal! * 12),
                                    color: Colors.white),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/category.png",
                                      height: SizeConfig.safeBlockVertical! * 5,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 10,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Category",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: SizeConfig.safeBlockVertical! * 3,
                                width: SizeConfig.safeBlockHorizontal! * 12,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white),
                                child: Center(
                                  child: categoryProvider
                                              .totalCategoryListProvider !=
                                          null
                                      ? Text(
                                          categoryProvider
                                                      .totalCategoryListProvider !=
                                                  null
                                              ? categoryProvider
                                                  .totalCategoryListProvider!
                                              : '0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )
                                      : LinearIndicator(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    //Brand
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BrandScreen()));
                      },
                      child: Card(
                        elevation: 2.0,
                        shadowColor: Colors.black38,
                        color: brandCardColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 18,
                          width: SizeConfig.safeBlockHorizontal! * 29,
                          decoration: BoxDecoration(
                            color: brandCardColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: SizeConfig.safeBlockVertical! * 9,
                                width: SizeConfig.safeBlockHorizontal! * 15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.blockSizeHorizontal! * 12),
                                    color: Colors.white),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/brand.png",
                                      height: SizeConfig.safeBlockVertical! * 5,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 10,
                                      fit: BoxFit.cover,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Brand",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: SizeConfig.safeBlockVertical! * 3,
                                width: SizeConfig.safeBlockHorizontal! * 12,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white),
                                child: Center(
                                  child: brandProvider.totalBrandListProvider !=
                                          null
                                      ? Text(
                                          brandProvider
                                                      .totalBrandListProvider !=
                                                  null
                                              ? brandProvider
                                                  .totalBrandListProvider!
                                              : '0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )
                                      : LinearIndicator(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //coupon
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CouponScreen()));
                      },
                      child: Card(
                        elevation: 2.0,
                        shadowColor: Colors.black38,
                        color: couponCarColord,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 18,
                          width: SizeConfig.safeBlockHorizontal! * 29,
                          // decoration: BoxDecoration(
                          //   color: couponCarColord,
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: SizeConfig.safeBlockVertical! * 9,
                                width: SizeConfig.safeBlockHorizontal! * 15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.blockSizeHorizontal! * 12),
                                    color: Colors.white),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/coupons.png",
                                      height: SizeConfig.safeBlockVertical! * 5,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 10,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Coupons",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: SizeConfig.safeBlockVertical! * 3,
                                width: SizeConfig.safeBlockHorizontal! * 12,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white),
                                child: Center(
                                  child: couponProvider.totalCouponList != null
                                      ? Text(
                                          couponProvider.totalCouponList!,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )
                                      : LinearIndicator(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    //customer
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CustomerScreen()));
                      },
                      child: Card(
                        elevation: 2.0,
                        shadowColor: Colors.black38,
                        color: customertCardColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 18,
                          width: SizeConfig.safeBlockHorizontal! * 29,
                          // decoration: BoxDecoration(
                          //   color: customertCardColor,
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: SizeConfig.safeBlockVertical! * 9,
                                width: SizeConfig.safeBlockHorizontal! * 15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.blockSizeHorizontal! * 12),
                                    color: Colors.white),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/customer.png",
                                      height: SizeConfig.safeBlockVertical! * 5,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 10,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Customers",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: SizeConfig.safeBlockVertical! * 3,
                                width: SizeConfig.safeBlockHorizontal! * 12,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white),
                                child: Center(
                                    child: totalCustomerCount != 0
                                        ? Text(
                                            totalCustomerCount.toString(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          )
                                        : LinearIndicator()),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    //orders
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => OrderScreen()));
                      },
                      child: Card(
                        elevation: 2.0,
                        shadowColor: Colors.black38,
                        color: orderCardColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: SizeConfig.safeBlockVertical! * 18,
                          width: SizeConfig.safeBlockHorizontal! * 29,
                          // decoration: BoxDecoration(
                          //   color: orderCardColor,
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: SizeConfig.safeBlockVertical! * 9,
                                width: SizeConfig.safeBlockHorizontal! * 15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        SizeConfig.blockSizeHorizontal! * 12),
                                    color: Colors.white),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/orders.png",
                                      height: SizeConfig.safeBlockVertical! * 5,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 10,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Orders",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                height: SizeConfig.safeBlockVertical! * 3,
                                width: SizeConfig.safeBlockHorizontal! * 12,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white),
                                child: Center(
                                    child: totalOrderCount != 0
                                        ? Text(
                                            totalOrderCount.toString(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          )
                                        : LinearIndicator()),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Container(
                height: 10,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.grey.shade300,
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Reports",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //Total Review
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ReviewScreen()));
                    },
                    child: Card(
                      elevation: 3.0,
                      shadowColor: Colors.black38,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        height: SizeConfig.safeBlockVertical! * 18,
                        width: SizeConfig.safeBlockHorizontal! * 29,
                        // decoration: BoxDecoration(
                        //   borderRadius: BorderRadius.circular(12),
                        // ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/review.png",
                              height: SizeConfig.safeBlockVertical! * 5,
                              width: SizeConfig.safeBlockHorizontal! * 10,
                              fit: BoxFit.cover,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Total Reviews",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              height: SizeConfig.safeBlockVertical! * 3,
                              width: SizeConfig.safeBlockHorizontal! * 12,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.grey),
                              child: Center(
                                  child: reviewProvider.totalReviewProvider !=
                                          null
                                      ? Text(
                                          reviewProvider.totalReviewProvider!,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        )
                                      : LinearIndicator()),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  //Total sales
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SalesReportScreen()));
                    },
                    child: Card(
                      elevation: 3.0,
                      shadowColor: Colors.black,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        height: SizeConfig.safeBlockVertical! * 18,
                        width: SizeConfig.safeBlockHorizontal! * 29,
                        // decoration: BoxDecoration(
                        //   borderRadius: BorderRadius.circular(12),
                        // ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/sales.png",
                              height: SizeConfig.safeBlockVertical! * 5,
                              width: SizeConfig.safeBlockHorizontal! * 10,
                              fit: BoxFit.cover,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Total Sales",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              height: SizeConfig.safeBlockVertical! * 3,
                              width: SizeConfig.safeBlockHorizontal! * 12,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.grey),
                              child: Center(
                                  child: viewModelProvider
                                              .fetchSaleReportListProvider !=
                                          null
                                      ? Text(
                                          viewModelProvider
                                                      .fetchSaleReportListProvider !=
                                                  null
                                              ? viewModelProvider
                                                  .fetchSaleReportListProvider![
                                                      0]
                                                  .totalItems!
                                                  .toString()
                                              : '0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        )
                                      : LinearIndicator()),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Orders in details",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: SizeConfig.safeBlockVertical! * 17,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.lightBlue[500],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //Pending Orders

                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => OrderScreen(
                                              getStatus: pending,
                                            )));
                              },
                              child: Row(
                                children: [
                                  Image.asset(
                                    "assets/images/pending.png",
                                    height: SizeConfig.safeBlockVertical! * 3,
                                    width: SizeConfig.safeBlockHorizontal! * 6,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  Text(
                                    "Pending Orders",
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  Container(
                                    height: SizeConfig.safeBlockVertical! * 3,
                                    width: SizeConfig.safeBlockHorizontal! * 11,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white),
                                    child: Center(
                                        child: pendingOrderCount != 0
                                            ? Text(
                                                pendingOrderCount.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )
                                            : LinearIndicator()),
                                  ),
                                ],
                              ),
                            )
                            //processing
                            ,
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => OrderScreen(
                                              getStatus: processing,
                                            )));
                              },
                              child: Row(
                                children: [
                                  Image.asset(
                                    "assets/images/processing.png",
                                    height: SizeConfig.safeBlockVertical! * 3,
                                    width: SizeConfig.safeBlockHorizontal! * 6,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  Text(
                                    "Processing Orders",
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  Container(
                                    height: SizeConfig.safeBlockVertical! * 3,
                                    width: SizeConfig.safeBlockHorizontal! * 11,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white),
                                    child: Center(
                                      child: processingOrderCount != 0
                                          ? Text(
                                              processingOrderCount.toString(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            )
                                          : LinearIndicator(),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        OrderScreen(getStatus: complete)));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              //Complete Orders
                              Row(
                                children: [
                                  Image.asset(
                                    "assets/images/complete.png",
                                    height: SizeConfig.safeBlockVertical! * 3,
                                    width: SizeConfig.safeBlockHorizontal! * 6,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  Text(
                                    "Complete Orders",
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  Container(
                                    height: SizeConfig.safeBlockVertical! * 3,
                                    width: SizeConfig.safeBlockHorizontal! * 10,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white),
                                    child: Center(
                                        child: completeOrderCount != 0
                                            ? Text(
                                                completeOrderCount.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )
                                            : LinearIndicator()),
                                  ),
                                ],
                              ),

                              //Return

                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => OrderScreen(
                                                getStatus: refunded,
                                              )));
                                },
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Image.asset(
                                      "assets/images/return.png",
                                      height: SizeConfig.safeBlockVertical! * 3,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 6,
                                      fit: BoxFit.cover,
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Text(
                                      "Refunded",
                                      style: TextStyle(
                                          fontSize: 13, color: Colors.white),
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Container(
                                      height: SizeConfig.safeBlockVertical! * 3,
                                      width:
                                          SizeConfig.safeBlockHorizontal! * 11,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: Colors.white),
                                      child: Center(
                                          child: orderProvider.totalOrderList !=
                                                  null
                                              ? Text(
                                                  returnOrderCount.toString(),
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w700),
                                                )
                                              : LinearIndicator()),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
