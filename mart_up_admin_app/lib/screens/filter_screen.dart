import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/providers/brand_provider.dart';
import 'package:mart_up_admin_app/providers/category_provider.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loader_brane_filter_screen.dart';
import 'package:mart_up_admin_app/widgets/loader_category_filter_screen.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
import 'package:provider/provider.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  State<FilterPage> createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  var min_price;
  var max_price;
  var stock_status;
  var category_id;
  var brand_id;
  late CategoryProvider categoryProvider;
  late BrandProvider brandProvider;
  bool _isloading = true;
  bool _isConnection = false;
  TextEditingController minPriceCnt = TextEditingController();
  TextEditingController maxPriceCnt = TextEditingController();
  int _page = 1;
  int _limit = 5;
  int _selectedIndex = -1;
  int _selectedIndexForBrand = -1;

  @override
  void initState() {
    super.initState();

    checkNetConnectivity();
    brandProvider = Provider.of<BrandProvider>(context, listen: false);
    categoryProvider = Provider.of<CategoryProvider>(context, listen: false);
    getCategory();
    getBrand();
  }

  getCategory() async {
    categoryProvider.page = _page;
    categoryProvider.limit = _limit;

    await categoryProvider.getCategory(
        categoryProvider.page, categoryProvider.limit);

    setState(() {
      _isloading = false;
      //_isloading2 = false;
    });
  }

  getBrand() async {
    await brandProvider.getBrand(_page, _limit);

    setState(() {
      _isloading = false;
      //_isloading2 = false;
    });
  }

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  _onSelectedForBrand(int index) {
    setState(() => _selectedIndexForBrand = index);
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    print(result);

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.clear),
            color: Colors.grey,
          ),
          title: Text(
            'Filter Search',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 14.0, horizontal: 14.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Price Range',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 60,
                        width: 160,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1.5),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: 16,
                          ),
                          child: Center(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: minPriceCnt,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '৳100',
                                  hintStyle: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 60,
                        width: 160,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1.5),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: 16,
                          ),
                          child: Center(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: maxPriceCnt,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '৳1000',
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),

                  SizedBox(
                    height: 14,
                  ),

                  //SizedBox(height: 20,),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                'Category',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
            ),
            _isConnection
                ? Container(
                    child: categoryProvider.categoryListProvider != null
                        ? GridView.builder(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              mainAxisExtent: 80,
                            ),
                            itemCount:
                                categoryProvider.categoryListProvider!.length,
                            itemBuilder: (BuildContext context, index) {
                              //CateoryCardModel categoryCardModel =categoryList[index];
                              //BrandCardModel brandCardModel = brandList[index];
                              //ProductCardModel productCardModel=productList[index];
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: 8,
                                  horizontal: 12,
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    _onSelected(index);
                                    setState(() {
                                      category_id = categoryProvider
                                          .categoryListProvider![index].id!;

                                      //_hasBeenPressed = !_hasBeenPressed;
                                    });
                                  },
                                  child: (Container(
                                    //height: 60,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      color: _selectedIndex != null &&
                                              _selectedIndex == index
                                          ? borderColor
                                          : Colors.white,
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 1.5,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Center(
                                      child: Text(
                                        categoryProvider
                                            .categoryListProvider![index].name!,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  )),
                                ),
                              );
                            })
                        : LoadetFilterCat(),
                  )
                : NoInternet(
                    pressed: () async {
                      await checkNetConnectivity();
                      _isConnection == false
                          ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                          : checkNetConnectivity();

                      getCategory();
                    },
                  ),
            SizedBox(
              height: 14,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                'Brand',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
            ),
            _isConnection
                ? Container(
                    child: brandProvider.brandListProvider != null
                        ? GridView.builder(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisExtent: 80,
                            ),
                            itemCount: brandProvider.brandListProvider!.length,
                            itemBuilder: (BuildContext context, index) {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: 8,
                                  horizontal: 12,
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    _onSelectedForBrand(index);
                                    setState(() {
                                      brand_id = brandProvider
                                          .brandListProvider![index].id!;

                                      //_hasBeenPressed = !_hasBeenPressed;
                                    });
                                  },
                                  child: (Container(
                                    //height: 60,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      color: _selectedIndexForBrand != null &&
                                              _selectedIndexForBrand == index
                                          ? borderColor
                                          : Colors.white,
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 1.5,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Center(
                                      child: Text(
                                        brandProvider
                                            .brandListProvider![index].name!,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  )),
                                ),
                              );
                            })
                        : LoaderFilterBrand(),
                  )
                : NoInternet(
                    pressed: () async {
                      await checkNetConnectivity();
                      _isConnection == false
                          ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                          : checkNetConnectivity();

                      getBrand();
                    },
                  ),
          ],
        ),
      )),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(8),
        child: RoundedButton(
            buttonText: "Apply",
            func: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProductScreen(
                            id: category_id,
                            brandId: brand_id,
                            minPrice: minPriceCnt.text,
                            maxPrice: maxPriceCnt.text,
                          )));
            }),
      ),
    );
  }
}
