import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/category_create_model.dart';
import 'package:mart_up_admin_app/models/requests/create_brand_model.dart';
import 'package:mart_up_admin_app/models/requests/product_create_model.dart';
import 'package:mart_up_admin_app/models/requests/product_create_model.dart'
    as create;
import 'package:mart_up_admin_app/models/requests/upload_image_mode.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/upload_image_response_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/providers/category_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/brand_screen.dart';
import 'package:mart_up_admin_app/screens/category_screen.dart';
import 'package:mart_up_admin_app/screens/loginScreen.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/create_brand_service.dart';
import 'package:mart_up_admin_app/services/create_category_service.dart';
import 'package:mart_up_admin_app/services/image_upload_service.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';
import 'package:mart_up_admin_app/services/multi_image_upload_service.dart';
import 'package:mart_up_admin_app/services/product_create_service.dart';
import 'package:mart_up_admin_app/services/product_edit_delete_service.dart';
import 'package:mart_up_admin_app/services/product_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
//import 'package:snippet_coder_utils/multi_images_utils.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:provider/provider.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:flutter/src/widgets/image.dart' as img;
import 'package:html/parser.dart';

class AddProduct extends StatefulWidget {
  List<CategoryModel>? allCategory;
  ProductModel? singleProduct; 
  AddProduct({this.allCategory,this.singleProduct});

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  final formKey = GlobalKey<FormState>();
  String? valueChoose;
  List<Object> images = [];
  String? saleFromDateTime;
  String? saleToDateTime;
  DateTime selectedDate = DateTime.now();
  DateTime selectedDate2 = DateTime.now();
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  final _categoryKey = GlobalKey<DropdownSearchState<String>>();
  final _productKey = GlobalKey<DropdownSearchState<String>>();
  TextEditingController productNameCtlr = new TextEditingController();
  TextEditingController slugCtlr = new TextEditingController();
  TextEditingController descriptionCtlr = new TextEditingController();
  TextEditingController shortDescriptionCtlr = new TextEditingController();
  TextEditingController priceCtlr = new TextEditingController();
  TextEditingController saleCtlr = new TextEditingController();
  TextEditingController regularCtlr = new TextEditingController();
  //late List<UploadImageModel> uploadImageModel;
  List<Asset> invoiceimages = [];
  List<Asset> selectedIamges = [];
  int? productId;
  int? categoryId;
  List<int>? categoryIdList = [];
  int? brandId;
  //  CategoryModel? categoryModel;
  //  Future<List<CategoryModel>>? _category;
  ProductModel? productModel;
  Future<List<ProductModel>>? _product;
  CategoryModel? categoryModel;
  Future<List<CategoryModel>>? _category;
  List<CategoryModel>? categories = [];
  List<CategoryModel>? selectedCategory = [];
  List<ProductModel>? selectedProduct = [];
  BrandModel? brandModel;
  List<BrandModel>? selectedBrand = [];
  Future<List<BrandModel>>? _brand;
  ViewModelProvider? viewModelProvider;
  String? key;
  UploadImageResponseModel? uploadImage;
  List<UploadImageResponseModel> uploadedImages = [];
  bool _isLoading = false;
  String? token;
  int uploadImagesCount = 0;
  List<String> color = ['Red', 'Blue', 'Green'];
  List<String> selectedColor = [];
  List<String> size = ['S', 'M', 'L', 'XL'];
  List<String> selectedSize = [];
  List<int> categoryIds = [];
  List<String> selectedCategoryName=[];
   List<create.Image> imgList = [];
  int page = 1;
  int limit = 5;
    bool? isTokenExpired;
    late CategoryProvider categoryProvider;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    categoryProvider=Provider.of<CategoryProvider>(context, listen: false);
    //getCategory();

    //_category= CategoryService.fetchCategory(key);
    _product = ProductService.fetchProducts(page, limit,key);
    _category = CategoryService.fetchCategory(key,page,limit);
    //  _category!.then((val){
    //    setState(() {
    //      categories!.addAll(val);
    //    });
    //  });
    //futureToList();

    _brand = BrandService.fetchBrand(page,limit,searchKey: key);
    getAdminLoginInfo();
     decodeToken();
  valueAsign();
    // final _items = _category!
    // .map((ct) => MultiSelectItem<CategoryModel>(ct, ct.name))
    // .toList();
  }

  futureToList() async {
    categories = await _category;
  }

  getAdminLoginInfo() async {
    await viewModelProvider!.fatchAdminLoginInfo();
    //print(viewModelProvider!.adminLoginInfoProvider!.data!.token!);
    token = viewModelProvider!.adminLoginInfoProvider!.data!.token!;
    setState(() {});
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2022),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        saleFromDateTime = dateFormat.format(selectedDate);
      });
  }

  
  

  _selectDate2(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2022),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate2 = picked;
        saleToDateTime = dateFormat.format(selectedDate2);
      });
  }

  valueAsign()async{
    if(widget.singleProduct !=null){
      //String shDes=HtmlWidget(widget.singleProduct!.shortDescription!.toString())
      var document = parse(widget.singleProduct!.shortDescription!);
    String parsedShortDescription = parse(document.body!.text).documentElement!.text;
    var document1 = parse(widget.singleProduct!.description!);
    String parsedDescription = parse(document1.body!.text).documentElement!.text;
      productNameCtlr.text=widget.singleProduct!.name!;
      slugCtlr.text=widget.singleProduct!.slug!;
      shortDescriptionCtlr.text=parsedShortDescription;
      descriptionCtlr.text=parsedDescription;
      priceCtlr.text=widget.singleProduct!.price!;
      saleCtlr.text=widget.singleProduct!.salePrice!;
      regularCtlr.text=widget.singleProduct!.regularPrice!;
      saleFromDateTime=widget.singleProduct!.dateOnSaleFrom ;
      saleToDateTime=widget.singleProduct!.dateOnSaleTo ;
      //selectedColor.addAll(widget.singleProduct!.attributes!);
      for(int i=0;i<1;i++){
        if(widget.singleProduct!.attributes![i].name=="Color"){
          selectedColor.addAll(widget.singleProduct!.attributes![i].options!);
        }
      }
      for(int s=0;s<2;s++){
        if(widget.singleProduct!.attributes![s].name=="Size"){
           selectedSize.addAll(widget.singleProduct!.attributes![s].options!);
        }
       
      }
      // for(int c=0;c<1;c++){
      //  // selectedCategoryName.addAll(widget.singleProduct!.categories!.map((e) => e.name!));
      //   selectedCategory![c].name![c].add(widget.singleProduct!.categories![c].name);
      // }
       for (int c = 0; c < widget.singleProduct!.categories!.length; c++) {

                          CategoryModel ca=CategoryModel();
                          ca.name=widget.singleProduct!.categories![c].name;
                           ca.id=widget.singleProduct!.categories![c].id;
                          selectedCategory!.add(ca);

                  
                        }
                        //categories=selectedCategory;
      // for (int c = 0; c < widget.singleProduct!.categories!.length; c++) {

      //                     CategoryModel ca=CategoryModel();
      //                     ca.id=widget.singleProduct!.categories![c].id;
                          
      //                     selectedCategory!.add(ca);

                  
      //                   }

        // List<create.Image> imgList = [];

        //                 //product.categories!.id.addAll(categoryIds);
        //                 for (int im = 0; im < uploadedImages.length; im++) {
        //                   create.Image img = create.Image();
        //                   img.src = uploadedImages[im].guid!.rendered;
        //                   //product.images![im].src=uploadedImages[im].guid!.rendered;
        //                   imgList.add(img);
        //                 }
        //                 product.images = imgList;

          //List<create.Image> singleProductImageList = [];
        for(int im=0;im<widget.singleProduct!.images!.length;im++){

          create.Image imgs=create.Image();
          imgs.src = widget.singleProduct!.images![im].src;
          imgList.add(imgs);

        }
        images=imgList;
        print(images);
                       
      
      

    }
  }
      decodeToken() {
    token = viewModelProvider!.adminLoginInfoProvider!.data!.token!;
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token!);
     isTokenExpired = JwtDecoder.isExpired(token!);
    print('decodec: $decodedToken');
    DateTime expirationDate = JwtDecoder.getExpirationDate(token!);

   
    print('date: $expirationDate');
  }

  tokenCheck(){
 setState(() {
      if (isTokenExpired!) {
        DBHelper().removeLoginAdminData();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    EasyLoading.init();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            // Navigator.pop(
            //     context, MaterialPageRoute(builder: (context) => ToShip()));
             Navigator.pop(context,MaterialPageRoute(builder: (context)=>ProductScreen()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
         widget.singleProduct ==null? "Add Product":"Edit Product",
          //widget.id == null ? "Add Address" : "Update address",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //                 SizedBox(
                //   height: 20,
                // ),
                Text(
                  'Product Name*',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: productNameCtlr,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Product Name is Empty";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Slug',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: slugCtlr,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    // validator: (value) {
                    //   if (value!.isEmpty) {
                    //     return "Slug Name is Empty";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Short Description',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: shortDescriptionCtlr,
                    decoration: InputDecoration(
                      // contentPadding: new EdgeInsets.symmetric(
                      //     vertical: 25.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    maxLines: 5,
                    // validator: (value) {
                    //   if (value!.isEmpty) {
                    //     return "Enter your short description";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Description',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: descriptionCtlr,
                    decoration: InputDecoration(
                      // contentPadding: new EdgeInsets.symmetric(
                      //     vertical: 25.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    maxLines: 5,
                    // validator: (value) {
                    //   if (value!.isEmpty) {
                    //     return "Enter your description";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Price*',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: priceCtlr,
                     keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Price is Empty";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Sale Price*',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: saleCtlr,
                     keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    maxLines: 1,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter your sale price";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Regular Price*',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: regularCtlr,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    maxLines: 1,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter your regular price";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                      'Sale Start',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(5)
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(saleFromDateTime !=null?DateFormat('dd MMM yyyy').format(DateTime.parse(saleFromDateTime!)):"select date"),
                          ),
                          IconButton(
                            onPressed: (){
                               _selectDate(context);
                            },
                           icon: Icon(Icons.calendar_today_outlined),
                           )
                        ],
                      ),
                    )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                      'Sale End',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    SizedBox(height: 10,),
                     Container(
                      decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(5)
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(saleToDateTime !=null?DateFormat('dd MMM yyyy').format(DateTime.parse(saleToDateTime!)):"select date"),
                          ),
                          IconButton(
                            onPressed: (){
                               _selectDate2(context);
                            },
                           icon: Icon(Icons.calendar_today_outlined),
                           )
                        ],
                      ),
                    )

                      ],
                    )

                  ],
                ),
                
               
               
               

                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: [
                //     Text(
                //       'Sale From',
                //       style: TextStyle(
                //           color: Colors.black,
                //           fontWeight: FontWeight.bold,
                //           fontSize: 20),
                //     ),
                //     TextButton(
                //       onPressed: () {
                //         _selectDate(context);
                //       },
                //       child: Text(
                //         'select',
                //         style: TextStyle(
                //             color: themeColor,
                //             fontWeight: FontWeight.bold,
                //             fontSize: 20),
                //       ),
                //     )
                //   ],
                // ),
                // SizedBox(
                //   height: 20,
                // ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: [
                //     Text(
                //       'Sale To',
                //       style: TextStyle(
                //           color: Colors.black,
                //           fontWeight: FontWeight.bold,
                //           fontSize: 20),
                //     ),
                //     TextButton(
                //       onPressed: () {
                //         _selectDate2(context);
                //       },
                //       child: Text(
                //         'select',
                //         style: TextStyle(
                //             color: themeColor,
                //             fontWeight: FontWeight.bold,
                //             fontSize: 20),
                //       ),
                //     )
                //   ],
                // ),
                SizedBox(
                  height: 20,
                ),

                Text(
                  "Add Category",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),

                SizedBox(
                  height: 10,
                ),
                DropdownSearch<CategoryModel>.multiSelection(
                  key: _categoryKey,
                  itemAsString: (CategoryModel? p) => p!.name!,
                  // maxHeight: deviceHeight * .80,
                  items: selectedCategory!,
                  onFind: (String? filter) => _category!,
                  label: "Category",
                  selectedItems: selectedCategory!,
                  
                  onChanged: (List<CategoryModel>? data) {
                   
                    
                   
                    
                    // setState(() {
                    //    //selectedCategory!.clear();
                    //    data!.clear();
                    // });
                    // print(data.toJson()),
                    setState(() {
                      categoryProvider.categoryListProvider!.addAll(data!);
                      // selectedCategory = data;
                      print(selectedCategory);
                    });

                    // setState(() {
                    //   //brandId=data!.id;
                    //  //selectedCategory!.map((e)=>categoryIdList!.add(e.id!)).toList();
                    //  data!.map((e)=>e.id);
                    //   print(categoryIdList);
                    // });
                  },
                  showSearchBox: true,
                ),
                //   DropdownSearch<CategoryModel>(
                //   itemAsString: (CategoryModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _category!,
                //   label: "Category*",
                //   onChanged: (CategoryModel? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       categoryModel = data;
                //     });

                //     // setState(() {
                //     //   subDistrict = null;
                //     // });
                //     // _subDistricts = APIService.fetchSubDistricts(data.id);
                //     setState(() {
                //       categoryId=data!.id;
                //       print(productId);
                //     });
                //   },
                //   showSearchBox: true,
                // ),

                SizedBox(
                  height: 20,
                ),
                //  Text(
                //   "Add Brand",
                //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                // ),

                //    SizedBox(
                //     height: 10,
                //   ),
                //   DropdownSearch<BrandModel>(
                //   itemAsString: (BrandModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _brand!,
                //   label: "Brand*",
                //   onChanged: (BrandModel? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       brandModel = data;
                //     });

                //     // setState(() {
                //     //   subDistrict = null;
                //     // });
                //     // _subDistricts = APIService.fetchSubDistricts(data.id);
                //     setState(() {
                //       brandId=data!.id;
                //       print(productId);
                //     });
                //   },
                //   showSearchBox: true,
                // ),
                //   DropdownSearch<BrandModel>.multiSelection(

                //   itemAsString: (BrandModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _brand!,
                //   label: "Brand*",

                //   onChanged: (List<BrandModel>? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       selectedBrand = data;
                //     });

                //     // setState(() {
                //     //   subDistrict = null;
                //     // });
                //     // _subDistricts = APIService.fetchSubDistricts(data.id);
                //     setState(() {
                //       //brandId=data!.id;
                //       print(selectedBrand);
                //     });
                //   },
                //   showSearchBox: true,
                // ),
                // SizedBox(height: 20,),

                //   DropdownSearch<BrandModel>.multiSelection(
                //     key: _multiKey,
                //   itemAsString: (BrandModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _brand!,
                //   label: "Brand*",

                //   onChanged: (List<BrandModel>? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       selectedBrand = data;
                //     });

                //     // setState(() {
                //     //   subDistrict = null;
                //     // });
                //     // _subDistricts = APIService.fetchSubDistricts(data.id);
                //     setState(() {
                //       //brandId=data!.id;
                //       print(selectedBrand);
                //     });
                //   },
                //   showSearchBox: true,
                // ),

                // Text(
                //   "Add Product",
                //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                // ),

                //    SizedBox(
                //     height: 10,
                //   ),
                //   DropdownSearch<ProductModel>(
                //   itemAsString: (ProductModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _product!,
                //   label: "Product*",
                //   onChanged: (ProductModel? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       productModel = data;
                //     });

                //     // setState(() {
                //     //   subDistrict = null;
                //     // });
                //     // _subDistricts = APIService.fetchSubDistricts(data.id);
                //     setState(() {
                //       productId=data!.id;
                //       print(productId);
                //     });
                //   },
                //   showSearchBox: true,
                // ),
                // //buildDistrict(),
                // SizedBox(
                //   height: 10,
                // ),

                // Text(
                //   "Add Related Product",
                //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                // ),

                // SizedBox(
                //   height: 10,
                // ),
                // DropdownSearch<ProductModel>.multiSelection(
                //   key: _productKey,
                //   itemAsString: (ProductModel? p) => p!.name!,
                //   // maxHeight: deviceHeight * .80,
                //   onFind: (String? filter) => _product!,
                //   label: "Product*",

                //   onChanged: (List<ProductModel>? data) {
                //     // print(data.toJson()),
                //     setState(() {
                //       selectedProduct = data;
                //     });
                //   },
                //   showSearchBox: true,
                // ),

                // SizedBox(
                //   height: 20,
                // ),
                Text(
                  "Add Color",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),

                SizedBox(
                  height: 10,
                ),
                DropdownSearch<String>.multiSelection(
                  //key: _productKey,
                  //itemAsString:  color.toString(),
                  items: color,
                  // maxHeight: deviceHeight * .80,
                  //onFind: (String? filter) => color!,
                  label: "Color",
                  selectedItems: selectedColor,
                  onChanged: (data) {
                    // print(data.toJson()),
                    selectedColor.clear();
                    setState(() {
                      //selectedProduct = data;
                      
                      selectedColor = data;
                      print(selectedColor);
                    });
                  },
                  showSearchBox: true,
                ),

                SizedBox(
                  height: 20,
                ),
                Text(
                  "Add Size",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),

                SizedBox(
                  height: 10,
                ),
                DropdownSearch<String>.multiSelection(
                  //key: _productKey,
                  //itemAsString:  color.toString(),
                  items: size,
                  // maxHeight: deviceHeight * .80,
                  //onFind: (String? filter) => color!,
                  label: "Size",
                  selectedItems: selectedSize,
                  onChanged: (data) {
                    // print(data.toJson()),
                    selectedSize.clear();
                    setState(() {
                      //selectedProduct = data;
                      selectedSize = data;
                      print(selectedSize);
                    });
                  },
                  showSearchBox: true,
                ),

                SizedBox(
                  height: 10,
                ),
                Text(
                  "Add Photo",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () {
                    pickInvoiceImages();
                  },
                  child: Container(
                    height: SizeConfig.safeBlockVertical! * 8,
                    width: SizeConfig.safeBlockHorizontal! * 23,
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.grey)),
                    child: Icon(
                      Icons.add_a_photo_outlined,
                      size: 32,
                    ),
                  ),
                ),
                 invoiceimages.length !=0?Visibility(
                    visible: invoiceimages.length > 0,
                    child: Container(
                     // height: SizeConfig.safeBlockVertical! * 13,
                     height: 105,
                      child: Container(
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            
                            itemCount: invoiceimages.length,
                            itemBuilder: (context, index) {
                              Asset asset = invoiceimages[index];

                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 8,vertical: 8),
                                child: Container(
                                 
                                  decoration: BoxDecoration(
                                   
                                                            border: Border.all(width: 1,color: themeColor)
                                                          ),
                                 
                                  child: Stack(
                                    children: [
                                      AssetThumb(
                                      asset: asset,
                                      width: 120,
                                      height: 120,
                                    ),
                                    Positioned(
                                      bottom: 58,
                                      left: 58,
                                     child: IconButton(
                                      onPressed: (){
                                          // widget.singleProduct!.images!.removeAt(index);
                                          // imgList.removeAt(index);
                                          // print(imgList.length);
                                          //selectedIamges.removeAt(index);
                                          invoiceimages.removeAt(index);
                                          setState(() {
                                            
                                          });
                                         
                                                                  },
                                                                   icon: Icon(Icons.close,color: Colors.red,size: 16,)
                                                                  )
                                      )
                                    ]
                                  ),
                                  
                                ),
                              );
                            }),
                      ),
                    )
                    ):Text(''),
                    widget.singleProduct !=null?Visibility(
                      visible: widget.singleProduct!.images!.length >0 ,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Container(
                         // height:  SizeConfig.safeBlockVertical! * 12,
                         height: 96,
                          //width: SizeConfig.blockSizeHorizontal! *30,
                          
                          child: ListView.builder
                          (
                            scrollDirection: Axis.horizontal,
                            itemCount: widget.singleProduct!.images!.length,
                            itemBuilder: (context,index){
                            var image = img.Image.network(widget.singleProduct!.images![index].src!,fit: BoxFit.cover,);
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Stack(
                                children: [
                                  Container(
                                    height: 88,
                                  //height:  SizeConfig.safeBlockVertical! * 11,
                                                        //width: SizeConfig.blockSizeHorizontal! *28,
                                                        width: 112,
                                                        //color: Colors.amber,
                                                        decoration: BoxDecoration(
                                                          border: Border.all(width: 1,color: themeColor)
                                                        ),
                                  child: image,
                                ),
                                Positioned(
                                  //height:  SizeConfig.safeBlockVertical! * .1,
                                                        //width: SizeConfig.blockSizeHorizontal! *52,
                                  
                                  //  bottom: SizeConfig.blockSizeHorizontal! * 10,
                                  //  left:SizeConfig.blockSizeHorizontal! *19,
                                  bottom: 66,
                                  left: 80,
                                  child: IconButton(
                                    onPressed: (){
                                        widget.singleProduct!.images!.removeAt(index);
                                        imgList.removeAt(index);
                                        print(imgList.length);
                                        setState(() {
                                          
                                        });
                                                                },
                                                                 icon: Icon(Icons.close,color: Colors.red,size: 16,)
                                                                )
                                )
                                ]
                              ),
                            );
                          }
                          ),
                        ),
                      ),
                    ):Text("")
                    ,
              ],
            ),
          ),
        ),
      )),
      bottomNavigationBar: Builder(builder: (context) {
        return _isLoading
            ? BoxLoader()
            : Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                child: RoundedButton(
                    buttonText: widget.singleProduct ==null?"Save":"Update",
                    func: () async {
                      if(formKey.currentState!.validate()){

                      
                      setState(() {
                         tokenCheck();
                      });
                      setState(() {
                                        _isLoading = true;
                                      });
                      //PhotoUploadeModel photos=PhotoUploadeModel(image: invoiceimages.);
                      selectedIamges = invoiceimages;
                      if(selectedIamges.isNotEmpty){
                         for (int i = 0; i < selectedIamges.length; i++) {
                        Response data = await MultiImageUplodeService()
                            .imageupload(selectedIamges[i], token!);
                        if (data.statusCode == 200 || data.statusCode == 201) {
                          uploadImage =
                              await ImageUplodeService.parseCategoryImage(
                                  data.body);
                          uploadedImages.add(uploadImage!);
                          uploadImagesCount++;
                        }
                      }

                      }
                      else{
                        print('image upload not needed');
                      }
                     
                      if (uploadImagesCount == selectedIamges.length || widget.singleProduct !=null) {
                        ProductCreateModel product = ProductCreateModel(
                            //name:productNameCtlr.text,

                            );
                        product.name ==null ?product.name = productNameCtlr.text:"";
                        product.slug ==null?product.slug = slugCtlr.text:'';
                        product.shortDescription ==null?product.shortDescription = shortDescriptionCtlr.text:'';
                        product.description ==null?product.description = descriptionCtlr.text:'';
                        product.price ==null?product.price = priceCtlr.text:'';
                        product.salePrice ==null?product.salePrice = saleCtlr.text:'';
                         product.regularPrice ==null?product.regularPrice = regularCtlr.text:'';
                         product.dateOnSaleFrom ==null?product.dateOnSaleFrom = saleFromDateTime:'';
                       product.dateOnSaleTo ==null ?product.dateOnSaleTo = saleToDateTime:'';
                        //product.images =uploadedImages;
                        List<create.Category> cate = [];

                        for (int c = 0; c < selectedCategory!.length; c++) {
                          create.Category cat = create.Category();
                          cat.id = selectedCategory![c].id;
                          // product.categories![c].id=selectedCategory![c].id;
                          cate.add(cat);

                          //product.categories![c].id[c]!.add(selectedCategory![c].id);
                        }
                        //selectedCategory!.clear();
                        product.categories = cate;
                        if(product.categories ==null){
                          product.categories=[];
                        }
                       

                        //product.categories!.id.addAll(categoryIds);
                        for (int im = 0; im < uploadedImages.length; im++) {
                          create.Image img = create.Image();
                          img.src = uploadedImages[im].guid!.rendered;
                          //product.images![im].src=uploadedImages[im].guid!.rendered;
                          imgList.add(img);
                        }
                        product.images = imgList;
                        if(product.images ==null){
                          product.images=[];
                        }
                        List<create.Attribute> colorList = [],
                            sizeList = [],
                            allList = [];
                        create.Attribute clr = create.Attribute();
                        clr.name = "Color";
                        for (int a = 0; a < 1; a++) {
                          // product.attributes![a].options=selectedColor;

                          clr.options = selectedColor;
                          colorList.add(clr);
                        }
                        //product.attributes=colorList;
                        //product.attributes!.addAll(colorList);
                        allList.addAll(colorList);
                        create.Attribute sz = create.Attribute();
                        sz.name = "Size";
                        for (int s = 0; s < 1; s++) {
                          sz.options = selectedSize;
                          sizeList.add(sz);
                        }
                        allList.addAll(sizeList);
                        product.attributes = allList;
                        if(product.attributes==null){
                          product.attributes=[];
                        }
                        //product.attributes=sizeList;
                        //product.attributes!.addAll(sizeList);
                         List<int> ids=[];
                        //ids=product.relatedIds;
                        //List<ProductCreateModel>
                        
                        
                        if (product.relatedIds != null) {
                          for (int p = 0; p < selectedProduct!.length; p++) {
                            product.relatedIds!.add(selectedProduct![p].id!);
                            // ids.add(selectedProduct![p].id!);

                          }
                        }
                        {
                          product.relatedIds = [0,0];
                        }

                        //product.relatedIds!.addAll(ids);
                        // for(int b=0;b<selectedBrand!.length; b++){
                        //   if(product.attributes![b].name=="Brand"){
                        //     product.attributes![b].options =selectedBrand![b].name as List<String>?;
                        //   }
                        // }

                        



                      if(widget.singleProduct !=null){
                        product.id=widget.singleProduct!.id;
                        Response? updateRes=await ProductEditDeleteService().updateProduct(product, token!, widget.singleProduct!.id!);
                      if(updateRes!.statusCode ==200 || updateRes.statusCode == 201){
                          
                          
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductScreen()));
                            EasyLoading.showSuccess('Success!');
                      }
                      else{
                          //await  CustomSnackbar.snackbar(context, "product is not updated ");
                            setState(() {
                              _isLoading=false;
                            });
                              EasyLoading.showError('Failed!');
                         }

                      } 
                      
                      else {

                        Response? res = await ProductCreateService()
                            .createProduct(product, token!);
                             if(res!.statusCode ==200 || res.statusCode == 201){
                           
                           setState(() {
                              _isLoading=false;
                            });
                           
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductScreen()));
                            EasyLoading.showSuccess("Success!");
                         }
                         else{
                         
                            setState(() {
                              _isLoading=false;
                            });
                              EasyLoading.showError('Failed!');
                         }

                      }


                      
                      }

                      

                      //  BrandCreateModel brand=BrandCreateModel();
                      //  //CategoryModel category=CategoryModel();
                      // //Images srcUrl=Images();
                      // //srcUrl.src=categoryImage!.guid!.rendered!;
                      // // Imagess srcUrl=Imagess();
                      // // srcUrl.src=categoryImage!.guid!.rendered!;
                      //  brand.name=productNameCtlr.text;
                      //  brand.slug=slugCtlr.text;
                      //  brand.description=descriptionCtlr.text;
                      //category.parent=productId;
                      //  category.image=srcUrl;
                      //category.image=srcUrl;
                      //category.display="default";

                      //category.image!.src=categoryImage!.guid!.rendered;

                      //  category.image!.alt=categoryImage!.altText;
                      //  category.image!.name=categoryImage!.slug;
                      //  Response? res=await CreateBrandService().createBrand(brand);
                      //  if(res!.statusCode ==200 || res.statusCode == 201){

                      //    CustomSnackbar.snackbar(context, "Brand created succesfully");
                      //    Navigator.push(context, MaterialPageRoute(builder: (context)=>BrandScreen()));
                      //  }
                      //  else{
                      //   await  CustomSnackbar.snackbar(context, "Brand is not created ");
                      //     setState(() {
                      //       _isLoading=false;
                      //     });
                      //  }
                    }
                    }));
      }),
    );
  }

  //add pic
  Future<void> pickInvoiceImages() async {
    List<Asset> resultList = [];
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: true,
        selectedAssets: invoiceimages,
        materialOptions: MaterialOptions(
          statusBarColor: "#4CAF50",
          actionBarColor: "#4CAF50",
          actionBarTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#4CAF50",
        ),
      );
    } on Exception catch (e) {
      print(e);
    }
    setState(() {
      invoiceimages = resultList;
    });
  }
}
