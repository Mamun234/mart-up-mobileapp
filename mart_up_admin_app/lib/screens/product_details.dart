import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:mart_up_admin_app/common/colors.dart';

// import 'package:mart_up/widgets/product_card.dart';
// import 'package:mart_up/widgets/product_color_card.dart';
// import 'package:mart_up/widgets/product_size_card.dart';
// import 'package:mart_up/widgets/rating.dart';

import 'package:mart_up_admin_app/common/links.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/models/responses/review_model.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/services/product_service.dart';
import 'package:mart_up_admin_app/services/review_service.dart';
import 'package:mart_up_admin_app/widgets/carousel_image.dart';
import 'package:mart_up_admin_app/widgets/loader_product_details.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/product_card.dart';
import 'package:mart_up_admin_app/widgets/review_card.dart';
//import 'package:mart_up/widgets/thumbnail_image.dart';
import 'package:provider/provider.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter/src/widgets/image.dart' as img;

// ignore: must_be_immutable
class ProductDetailPage extends StatefulWidget {
 
  int? id;
  // bool? btnShowstatus;
  // int? orderDetailsToWriteReview;

  int? discountPercentRound;
  ProductDetailPage(
      {
      this.id,
      // this.discountPercentRound,
      // this.btnShowstatus,
      // this.orderDetailsToWriteReview
      });

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  //List<Images> imgList = [];

  int price = 299;
  String name = "bata";
  String image = 'assets/images/nike.png';
  //Product? _favoriteProduct;
  bool isReviewLoading = true;
  List<dynamic>? size, color;

  //double? difference;
  int _selectedIndex = -1;
  //List<FavoriteModel> favoriteProducts = [];
  var dbHelper;
  //List<CartModel> cartProducts = [];
  late ViewModelProvider viewModelProvider;
  //List<Product>? _compareProduct;
  ProductModel? singleProduct;
  int? productIdForFetchProduct;
  int? compareProductId;
  bool isDisable = false;
  bool productToReview = false;
  int _page = 1, _limit = 5;

  List<ProductModel> products = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
        double? difference;
  double? discountPercent;
  int? discountPercentRound;

  //productIdForFetchProduct=widget.id;
  getProductById() async {
    var _product =
        await ProductService.fetchProductsById(widget.id)
            .then((value) => setState(() {
                  singleProduct = value;
                      if (singleProduct!.salePrice != "" &&
        singleProduct!.regularPrice != "") {
      difference = double.parse(singleProduct!.regularPrice!) -
          double.parse(singleProduct!.salePrice!);
      discountPercent =
          (difference! / double.parse(singleProduct!.regularPrice!)) *
              100;
      discountPercentRound = discountPercent!.round();
    } else {}
                  for (int i = 0; i < singleProduct!.attributes!.length; i++) {
                    if (singleProduct!.attributes![i].name == "Size") {
                      setState(() {
                        size = singleProduct!.attributes![i].options;
                      });
                      print("size: $size");
                    }
                  }
                  for (int i = 0; i < singleProduct!.attributes!.length; i++) {
                    if (singleProduct!.attributes![i].name == "Color") {
                      setState(() {
                        color = singleProduct!.attributes![i].options;
                      });
                      print("color: $color");
                    }
                  }
                }));
    //print('average rating:${singleProduct!.averageRating}');
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    //await viewModelProvider.getCompare();
    //getProducts();

    getAllLogic();
  }

  @override
  void initState() {
    super.initState();
    productIdForFetchProduct = widget.id;
    getProductById();
    //singleProduct!=singleProduct;
  }

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

//   getSize(){
// // ignore: avoid_function_literals_in_foreach_calls

// for(int i=0;i<singleProduct!.attributes!.length; i++){
//   // if(singleProduct!.attributes![i].name == "Size"){
//   //   size=singleProduct!.attributes![i].options;
//   // }
//   size=singleProduct!.attributes![i].name == "Size";
// }
// print(size);
//   }

  getAllLogic() {
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);

    // imgList = singleProduct!.images!;
    // print('q=${singleProduct!.quantity}');
    //_favoriteProduct = singleProduct;

    getreview();
    getProducts();
    // getFavorite();
    // getCart();
    // getCompareList();

    //getSize();
    // size=singleProduct!.attributes!.forEach((element) {element.name=="Size";});

    //size=singleProduct!.attributes![i].name == "Size";
    // print('size: $size');
    // print('size: $color');
  }

  // getCompareList() {
  //   //var compare=await viewModelProvider.getCompare();

  //   setState(() async {
  //     // _compareProduct=compare;
  //     for (int i = 0; i < viewModelProvider.compareProviderList!.length; i++) {
  //       if (viewModelProvider.compareProviderList![i].id == singleProduct!.id) {
  //         compareProductId = await viewModelProvider.compareProviderList![i].id;
  //       }
  //     }
  //     print('cmpid: $compareProductId');
  //   });
  // }

  List<ReviewModel> review = [];
  double averageRating = 0.0;
  double ratingSum = 0.0;

  void getreview() {
    print("call1");
    ReviewService.fetchRivewbyProductId( singleProduct!.id)
        .then((value) {
      setState(() {
        review = value;
        // for (int i = 0; i < review.length; i++) {
        //   ratingSum = ratingSum + review[i].rating!;
        // }
        // averageRating = (ratingSum) / (review.length);
        // print('rating sum: $ratingSum');
        // print('rating length: ${review.length}');
        // print('average: $averageRating');
        isReviewLoading = false;
      });

      var val = jsonEncode(review);
      print(val);
    });
  }
  //String? desc= '${singleProduct!.shortDescription}';

  

  bool _isloading = true;

  getProducts() {
    print("call1");
    ProductService.fetchRelatedProducts(
             singleProduct!.relatedIds)
        .then((value) {
      // setState({
      //   _categories
      // })
      setState(() {
        products = value;

        _isloading = false;

        _page++;
      });

      print("products: ");
      //print(value);
      var val = jsonEncode(value);
      print(val);
    });
  }

  void _onLoading() async {
    _page++;
    // var list = await ProductService.fetchRelatedProducts(
    //     _page, _limit, singleProduct!.relatedIds);
    // if (list != null) {
    //   products.addAll(list);
    // }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  @override
  Widget build(BuildContext context) {
      SizeConfig().init(context);
    return Scaffold(
      appBar: singleProduct != null
          ? AppBar(
              backgroundColor: Colors.white,
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProductScreen()));
                },
                icon: Icon(
                  Icons.arrow_back_ios_new_rounded,
                  color: Colors.grey,
                  size: 22,
                ),
              ),
              title: Text(
                singleProduct!.name != null ? '${singleProduct!.name}' : "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              actions: [
                // IconButton(
                //     onPressed: () {
                //       Navigator.push(context,
                //           MaterialPageRoute(builder: (context) => SearchPage()));
                //     },
                //     icon: Icon(
                //       Icons.search_outlined,
                //       color: Colors.grey,
                //     )),
                // IconButton(
                //     onPressed: () {},
                //     icon: Icon(
                //       Icons.more_vert,
                //       color: Colors.grey,
                //     )
                //     ),
              ],
            )
          : AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
            ),
      body: singleProduct != null
          ? SafeArea(
              child: SingleChildScrollView(
                  child: Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    singleProduct!.images!.isNotEmpty
                        ? CarouselWithDotsPage(imgList1: singleProduct!.images!)
                        : img.Image.network(emptyImage),
                    SizedBox(
                      height: 20,
                    ),
                    // Container(
                    //   height: ScreenUtil().setHeight(70),
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: imgList.length,
                    //     itemBuilder: (context,index){
                    //       return ThumbnailImage(imgList: imgList,);
                    //   }
                    //   ),
                    // ),
                    //humbTnailImage(imgList: imgList,),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 286,
                          //color: Colors.amber,
                          child: Text(
                            '${singleProduct!.name}',
                            maxLines: 2,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                                //fontFamily: fontFamily
                                ),
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              width: 40,
                              height:50,
                              //color: Colors.amber,
                            ),
                            // Positioned(
                            //   //left: 4,
                            //   bottom: ScreenUtil().setWidth(5),
                            //   child: IconButton(
                            //       onPressed: () async {
                            //         // Navigator.push(context, MaterialPageRoute(
                            //         //   builder: (context)=>FavoritProductScreen(fav: _favoriteProduct,)));
                            //         if (favoriteProductId ==
                            //             singleProduct!.id) {
                            //           CustomSnackbar.snackbar(context,
                            //               "Product already added to favorites");
                            //         } else {
                            //           CustomSnackbar.snackbar(
                            //               context, 'Added to Favorite');
                            //           FavoriteModel? pr = FavoriteModel(
                            //               id: singleProduct!.id,
                            //               name: singleProduct!.name,
                            //               salePrice: singleProduct!.salePrice,
                            //               regularPrice:
                            //                   singleProduct!.regularPrice,
                            //               quantity: singleProduct!.quantity,
                            //               img: imgList[0].src);
                            //           await DBHelper().saveToFavoriteList(pr);
                            //           await viewModelProvider
                            //               .getFavoriteModel();
                            //           setState(() {
                            //             for (int i = 0;
                            //                 i <
                            //                     viewModelProvider
                            //                         .favoriteListLength!.length;
                            //                 i++) {
                            //               if (viewModelProvider
                            //                       .favoriteListLength![i].id ==
                            //                   singleProduct!.id) {
                            //                 favoriteProductId =
                            //                     viewModelProvider
                            //                         .favoriteListLength![i].id;
                            //               }
                            //             }
                            //           });
                            //         }
                            //       },
                            //       icon: Icon(
                            //         Icons.favorite_outline,
                            //         color: Colors.grey,
                            //       )),
                            // )
                          ],
                        )
                      ],
                    ),
                    // Card(
                    //   color: Colors.amberAccent,
                    //   child: Container(
                    //     height: 40,
                    //     width: 40,
                    //   )
                    // ),
                    // Card(
                    //   color: Colors.redAccent,
                    //   child: Container(
                    //     height: 40,
                    //     width: 40,
                    //   )
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // //Rating
                    // RatingBar.builder(
                    //   itemSize: 16,
                    //   initialRating:
                    //       singleProduct!.averageRating.toString() == "0.0"
                    //           ? 1.0
                    //           : double.parse(
                    //               singleProduct!.averageRating.toString()),
                    //   minRating: 1,
                    //   direction: Axis.horizontal,
                    //   allowHalfRating: false,
                    //   itemCount: 5,
                    //   itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    //   itemBuilder: (context, _) => Icon(
                    //     Icons.star,
                    //     color: Colors.amber,
                    //   ),
                    //   ignoreGestures: true,
                    //   onRatingUpdate: (rating) {
                    //     print(rating);
                    //     // widget.reviewModel!.rating;
                    //   },
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                     singleProduct!.ratingCount !=0?Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(Icons.star,color: Colors.amber,size: 30,),
                                SizedBox(width: 5,),
                                Text(
                                  
                                  singleProduct!.averageRating!.isNotEmpty?"${singleProduct!.averageRating}":'0',
                                
                                style: TextStyle(fontSize: 26,fontWeight: FontWeight.bold, ),),
                      ],
                    ):Text(''),
                    
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //Text("data"),
                        
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 15,),
                                Text(
                                singleProduct!.regularPrice!.isNotEmpty
                                    ? '৳${singleProduct!.regularPrice}'
                                    : '',
                                style: TextStyle(
                                    color: Colors.grey,
                                    decoration: TextDecoration.lineThrough,
                                    fontSize:18,
                                    fontWeight: FontWeight.bold,
                                    
                                    ),
                              ),

                                Text(
                                singleProduct!.salePrice!.isNotEmpty
                                    ? '৳${singleProduct!.salePrice}'
                                    : '',
                                style: TextStyle(
                                    color: themeColor,
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                   
                                    ),
                              ),
                              ],
                            ),
                            SizedBox(width: 20,),
                            Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FractionalTranslation(
                              translation: Offset(0, 0.135),
                              child: Card(
                                shape: RoundedRectangleBorder(  
                                        borderRadius: BorderRadius.circular(12.0),  
                                      ),  
                                child: Container(
                                  height: 40,
                                  width: 60,
                                 
                                  child: Center(
                                    child: Text(
                                    discountPercentRound != null
                                        ? '${(discountPercentRound)}%'
                                        : "",
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                         
                                        ),
                                  ),
                                  ),
                                ),
                              ),
                            ),
                            //DashLine(),
                            
                            
                            Container(
                              child: Card(
                    shape: RoundedRectangleBorder(  
            borderRadius: BorderRadius.circular(12.0),
                                                ),
                                child: Container(
                                   height: 40,
                                  width: 60,
                                  
                                  child: Center(
                                    child: Text(
                                    "OFF",
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                         
                                        ),
                                  ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),


                          ],
                        ),
                        // SizedBox(width: 20,),
                        
                        // SizedBox(width: 40,),
                        Column(
                           crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            // Row(
                            //   children: [
                            //     Icon(Icons.star,color: Colors.amber,),
                            //     SizedBox(width: 5,),
                            //     Text("4.5",style: TextStyle(fontSize: 26,fontWeight: FontWeight.bold),),
                            //   ],
                            // ),
                            // SizedBox(height: 6,),
                            GestureDetector(
                              onTap: (){
                                if(singleProduct!.ratingCount != 0){
                                  // Navigator.push(context, MaterialPageRoute(builder: (context)=>ReviewScreen()));
                                }
                               
                              },
                              child: singleProduct!.ratingCount != 0?Row(
                                children: [
                                  Text("( ",style: TextStyle( color: Colors.black,fontWeight: FontWeight.bold,fontSize: 22,),),
                                  Text(singleProduct!.ratingCount != 0
                                    ? '${singleProduct!.ratingCount} reviews'
                                    : "No reviews",style: TextStyle(
                                      color: Colors.black,fontWeight: FontWeight.bold,fontSize: 22,
                                       
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.black87
                                      ),),
                                  Text(' )',style: TextStyle( color: Colors.black,fontWeight: FontWeight.bold,fontSize: 22,)),
                                ],
                              ):Text(''),
                            ),
                            Container(
                              height: 60,
                              width: 10,
                              //color: Colors.amberAccent,
                            )
                          ],
                        ),


                      ],
                    ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: [
                    //     Card(
                    //       elevation: 1.0,
                    //       color: themeColor,
                    //       child: Center(
                    //         child: Padding(
                    //           padding: const EdgeInsets.symmetric(
                    //               horizontal: 8, vertical: 4),
                    //           child: Text(
                    //             singleProduct!.salePrice!.isNotEmpty
                    //                 ? '৳${singleProduct!.salePrice}'
                    //                 : '',
                    //             style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontSize: ScreenUtil().setSp(20),
                    //                 fontWeight: FontWeight.bold),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     Card(
                    //       elevation: 1,
                    //       color: themeColor,
                    //       child: Center(
                    //         child: Padding(
                    //           padding: const EdgeInsets.symmetric(
                    //               horizontal: 8, vertical: 4),
                    //           child: Text(
                    //             singleProduct!.regularPrice!.isNotEmpty
                    //                 ? '৳${singleProduct!.regularPrice}'
                    //                 : '',
                    //             style: TextStyle(
                    //                 color: Colors.white,
                    //                 decoration: TextDecoration.lineThrough,
                    //                 fontSize: ScreenUtil().setSp(20),
                    //                 fontWeight: FontWeight.bold),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     Card(
                    //       elevation: 1,
                    //       color: themeColor,
                    //       child: Center(
                    //         child: Padding(
                    //           padding: const EdgeInsets.symmetric(
                    //               horizontal: 8, vertical: 4),
                    //           child: Text(
                    //             widget.discountPercentRound != null
                    //                 ? '${(widget.discountPercentRound)}% off'
                    //                 : "",
                    //             style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontSize: ScreenUtil().setSp(20),
                    //                 fontWeight: FontWeight.bold),
                    //           ),
                    //         ),
                    //       ),
                    //     )
                    //   ],
                    // ),
                    // SizedBox(
                    //   height: 20,
                    // ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: [
                    //     GestureDetector(
                    //       onTap: () async {
                    //         if (compareProductId == singleProduct!.id) {
                    //           CustomSnackbar.snackbar(context,
                    //               "Product Already Added to Compare list");
                    //         } else {
                    //           CustomSnackbar.snackbar(context, "Product Added");
                    //           viewModelProvider.discountProvider!
                    //               .add(widget.discountPercentRound!);
                    //           viewModelProvider.compareProviderList!
                    //               .add(singleProduct!);
                    //           await viewModelProvider.getCompare();
                    //           await viewModelProvider.discountProvider;
                    //           if (viewModelProvider.compareProviderList !=
                    //                   null &&
                    //               viewModelProvider.discountProvider != null) {
                    //             viewModelProvider.compareAddCount++;
                    //           }
                    //           setState(() async {
                    //             // _compareProduct=compare;
                    //             for (int i = 0;
                    //                 i <
                    //                     viewModelProvider
                    //                         .compareProviderList!.length;
                    //                 i++) {
                    //               if (viewModelProvider
                    //                       .compareProviderList![i].id ==
                    //                   singleProduct!.id) {
                    //                 compareProductId = await viewModelProvider
                    //                     .compareProviderList![i].id;
                    //               }
                    //             }
                    //             print('cmpid: $compareProductId');
                    //           });
                    //           //_compareProduct!.add(singleProduct!);

                    //         }

                    //         //print(_compareProduct!.length);
                    //         // setState(() {
                    //         //     CompareModel compare=CompareModel();
                    //         //  compare.id=singleProduct!.id;
                    //         //   compare.name=singleProduct!.name;
                    //         //   compare.quantity=singleProduct!.quantity;
                    //         //   compare.salePrice=singleProduct!.salePrice;
                    //         //   compare.regularPrice=singleProduct!.regularPrice;
                    //         //   compare.shortDescription=singleProduct!.shortDescription;
                    //         //   compare.description=singleProduct!.description;
                    //         //   compare.imagesCompare=singleProduct!.images!.cast<ImagesCompare>();
                    //         //   compare.attributes=singleProduct!.attributes!.cast<AttributeCompare>();
                    //         // });
                    //       },
                    //       child: Card(
                    //         elevation: 1,
                    //         child: Container(
                    //           color: tabSelectColor,
                    //           height: ScreenUtil().setHeight(30),
                    //           width: ScreenUtil().setWidth(180),
                    //           child: Center(
                    //             child: Text(
                    //               'Add To Comapare',
                    //               style: TextStyle(
                    //                   color: Colors.white,
                    //                   fontWeight: FontWeight.bold),
                    //             ),
                    //           ),
                    //           // decoration: BoxDecoration(
                    //           //     color: tabSelectColor,
                    //           //     borderRadius: BorderRadius.circular(20)),
                    //         ),
                    //       ),
                    //     ),
                    //     Padding(
                    //       padding:
                    //           EdgeInsets.only(top: ScreenUtil().setWidth(12.0)),
                    //       child: Stack(
                    //         children: [
                    //           IconButton(
                    //             onPressed: () {
                    //               if (viewModelProvider
                    //                       .compareProviderList!.length >
                    //                   1) {
                    //                 Navigator.push(
                    //                     context,
                    //                     MaterialPageRoute(
                    //                         builder: (context) => ComparePage(

                    //                             // compare: viewModelProvider
                    //                             //     .compareProviderList!,

                    //                             )));
                    //               } else {
                    //                 CustomSnackbar.snackbar(context,
                    //                     "Please add product to compare");
                    //               }
                    //             },
                    //             icon: Icon(
                    //               Icons.compare_arrows_outlined,
                    //               color: Colors.grey,
                    //               size: 30,
                    //             ),
                    //           ),
                    //           Positioned(
                    //             left: ScreenUtil().setWidth(24.0),
                    //             bottom: ScreenUtil().setWidth(26.0),
                    //             child: Container(
                    //               height: ScreenUtil().setHeight(15),
                    //               width: ScreenUtil().setWidth(15.0),
                    //               alignment: Alignment.center,
                    //               decoration: BoxDecoration(
                    //                 color: Colors.red,
                    //                 borderRadius: BorderRadius.circular(15.0),
                    //               ),
                    //               child: Consumer<ViewModelProvider>(
                    //                   builder: (context, viewModel, child) {
                    //                 return Text(
                    //                   '${viewModelProvider.compareProviderList!.length}',
                    //                   style: TextStyle(
                    //                     color: Colors.white,
                    //                   ),
                    //                 );
                    //               }),
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //     )
                    //   ],
                    // ),
                    SizedBox(
                      height: 20,
                    ),
                    size != null
                        ? Text('Select Size',
                            style: TextStyle(
                                color: lebelColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 18))
                        : Text(''),
                    size != null
                        ? SizedBox(
                            height: 20,
                          )
                        : SizedBox(
                            height: 0,
                          ),
                    size != null
                        ? Container(
                            height: 70,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                //itemCount: singleProduct!.attributes!.length,
                                shrinkWrap: true,
                                itemCount: size!.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _onSelected(index);
                                      },
                                      child: Container(
                                        height: 70,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            color: _selectedIndex != null &&
                                                    _selectedIndex == index
                                                ? Colors.grey
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(70),
                                            border: Border.all(
                                              color: Colors.grey,
                                            )),
                                        child: Center(
                                          child: Text(
                                            '${size![index]}',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        // child: ListView.builder(
                                        //   itemCount: singleProduct!.attributes![index].options!.length,
                                        //   itemBuilder: (context,index){
                                        //    return singleProduct!.attributes![index].name== "Size"? Text('${singleProduct!.attributes![index].options}',
                                        //  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                                        //    ):Text('');
                                        // }),
                                      ),
                                    ),
                                  );
                                }),
                          )
                        : Center(
                            child: Text(""),
                          ),
                    // ProductSizeCard(),
                    size != null
                        ? SizedBox(
                            height: 20,
                          )
                        : SizedBox(
                            height: 0,
                          ),
                    color != null
                        ? Text('Select Color',
                            style: TextStyle(
                                color: lebelColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 18))
                        : Text(''),
                    color != null
                        ? SizedBox(
                            height: 20,
                          )
                        : SizedBox(
                            height: 0,
                          ),
                    color != null
                        ? Container(
                            height: 70,
                            //color: Colors.indigo,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                //itemCount: singleProduct!.attributes!.length,
                                itemCount: color!.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _onSelected(index);
                                      },
                                      child: Container(
                                        height: 70,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            color: _selectedIndex != null &&
                                                    _selectedIndex == index
                                                ? Colors.grey
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(70),
                                            border: Border.all(
                                              color: Colors.grey,
                                            )),
                                        child: Center(
                                          child: Text(
                                            '${color![index]}',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        // child: ListView.builder(
                                        //   itemCount: singleProduct!.attributes![index].options!.length,
                                        //   itemBuilder: (context,index){
                                        //    return singleProduct!.attributes![index].name== "Size"? Text('${singleProduct!.attributes![index].options}',
                                        //  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                                        //    ):Text('');
                                        // }),
                                      ),
                                    ),
                                  );
                                }),
                          )
                        : Center(
                            child: Text(""),
                          ),
                    // Container(
                    //                              height: 50,
                    //                              width: 50,
                    //                             decoration: BoxDecoration(
                    //                                color:  Colors.white,
                    //           borderRadius: BorderRadius.circular(50),

                    //           border: Border.all(
                    //             color: Colors.grey,
                    //           )

                    //                             ),
                    //                             child: Center(
                    //           child:
                    //            Text('XL',
                    //           style: TextStyle(color: Colors.redAccent,fontWeight: FontWeight.bold),
                    //           ),
                    //                             ),
                    //                             // child: ListView.builder(
                    //                             //   itemCount: singleProduct!.attributes![index].options!.length,
                    //                             //   itemBuilder: (context,index){
                    //                             //    return singleProduct!.attributes![index].name== "Size"? Text('${singleProduct!.attributes![index].options}',
                    //                             //  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                    //                             //    ):Text('');
                    //                             // }),
                    //                           ),
                    // ProductColorCard(),
                    SizedBox(
                      height: 20,
                    ),
                    Text('Short Description',
                        style: TextStyle(
                            color: lebelColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18)),

                    SizedBox(
                      height: 40,
                    ),
                    // Text(singleProduct!.shortDescription!,
                    //     style: TextStyle(
                    //         color: Colors.grey, fontSize: ScreenUtil().setSp(14))),
                    // Html(data: singleProduct!.shortDescription),
                    HtmlWidget(
                        singleProduct!.shortDescription!.isNotEmpty
                            ? singleProduct!.shortDescription.toString()
                            : '',
                        textStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 14)),
                    SizedBox(
                      height: 20,
                      //jdfjhfu
                    ),
                    Text('Description',
                        style: TextStyle(
                            color: lebelColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18)),
                    SizedBox(
                      height: 40,
                    ),
                    HtmlWidget(
                        singleProduct!.description!.isNotEmpty
                            ? singleProduct!.description.toString()
                            : '',
                        textStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 14)),

                    SizedBox(
                      height: 20,
                    ),
                    singleProduct!.ratingCount == 0
                        ? Text('')
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Review Product',
                                  style: TextStyle(
                                      color: lebelColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18)),
                              // GestureDetector(
                              //   onTap: () {
                              //     // Navigator.push(
                              //     //     context,
                              //     //     MaterialPageRoute(
                              //     //         builder: (context) => ReviewScreen(
                              //     //               productId: singleProduct!.id,
                              //     //               //btnShowstatus: btnShowstatus,
                              //     //               // review: review,
                              //     //               orderDetailsToWriteReview: widget
                              //     //                   .orderDetailsToWriteReview,
                              //     //             )));
                              //   },
                              //   child: Text('See More',
                              //       style: TextStyle(
                              //           color: themeColor,
                              //           fontWeight: FontWeight.bold,
                              //           fontSize: 16)),
                              // ),
                            ],
                          ),
                    singleProduct!.ratingCount == 0
                        ? SizedBox(
                            height: 0,
                          )
                        : SizedBox(
                            height: 10,
                          ),
                    Row(
                      children: [
                        //Rating
                        singleProduct!.ratingCount == 0
                            ? Text("")
                            : RatingBar.builder(
                                itemSize: 16,
                                initialRating: double.parse(singleProduct!
                                            .averageRating
                                            .toString()) ==
                                        "0.00"
                                    ? 1
                                    : double.parse(singleProduct!.averageRating
                                        .toString()),
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 2.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                ignoreGestures: true,
                                onRatingUpdate: (rating) {
                                  print(rating);
                                  // widget.reviewModel!.rating;
                                },
                              ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          singleProduct!.averageRating.toString() == "0.00"
                              ? ""
                              : singleProduct!.averageRating.toString(),
                          style: TextStyle(color: Colors.black),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          singleProduct!.ratingCount != 0
                              ? '(${singleProduct!.ratingCount} Reviews)'
                              : "",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    singleProduct!.ratingCount == 0
                        ? SizedBox(
                            height: 0,
                          )
                        : SizedBox(
                            height: 10,
                          ),
                    isReviewLoading == true
                        ? 
                        //LoderReview()
                        BuildLoading()
                        : review.length != 0
                            ? 
                            ReviewCard(reviewModel: review[0])
                            
                            : Text(""),

                    singleProduct!.ratingCount == 0
                        ? SizedBox(
                            height: 0,
                          )
                        : SizedBox(
                            height: 20,
                          ),
                    Text('You might also like',
                        style: TextStyle(
                            color: lebelColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18)),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      //color: Colors.amber,
                      height: SizeConfig.safeBlockVertical! * 37,
                      child: _isloading
                          ? BuildLoading()
                          : ListView.builder(
                              //reverse: true,
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: products.length,
                              itemBuilder: (BuildContext context, index) {
                                // var productCardModel =
                                //     _categories[index];

                                return (ProductCardNew(
                                  productCardModel: products[index],
                                  //discount: discountPercent,
                                ));
                              }),
                    ),
                  ],
                ),
              )),
            )
          : 
          //LoaderProductDetails(),
         LoaderProductDetails(),
      // bottomNavigationBar: singleProduct != null
      //     ? Padding(
      //         padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
      //         child: RoundedButton(
      //             buttonText: "Add to Cart",
      //             func: () async {
      //               if (cartProductId == singleProduct!.id) {
      //                 CustomSnackbar.snackbar(
      //                     context, 'product already added to cart');
      //               } else {
      //                 CustomSnackbar.snackbar(context, 'added to cart');
      //                 CartModel c = CartModel(
      //                     id: singleProduct!.id,
      //                     name: singleProduct!.name,
      //                     salePrice: singleProduct!.salePrice == null
      //                         ? '0'
      //                         : singleProduct!.salePrice,
      //                     regularPrice: singleProduct!.regularPrice == null
      //                         ? '0'
      //                         : singleProduct!.regularPrice,
      //                     quantity: singleProduct!.quantity = 1,
      //                     img: imgList[0].src);
      //                 await DBHelper().saveToCartList(c);
      //                 await viewModelProvider.getCartModel();
      //                 print('cartLent: ${viewModelProvider.cartListN!.length}');

      //                 setState(() {
      //                   for (int i = 0;
      //                       i < viewModelProvider.cartListN!.length;
      //                       i++) {
      //                     if (viewModelProvider.cartListN![i].id ==
      //                         singleProduct!.id) {
      //                       cartProductId = viewModelProvider.cartListN![i].id;
      //                     }
      //                   }
      //                 });
      //                 Navigator.push(context,
      //                     MaterialPageRoute(builder: (context) => CartPage()));
      //               }
      //             }),
      //       )
      //     : Text(''),
    );
  }
}
