// import 'package:flutter/material.dart';
// import 'package:http/http.dart';
// import 'package:mart_up_admin_app/common/colors.dart';
// import 'package:mart_up_admin_app/common/links.dart';
// import 'package:mart_up_admin_app/models/responses/order_details_model.dart';
// import 'package:mart_up_admin_app/models/responses/order_get_model.dart'
//     as odrMod;
// import 'package:mart_up_admin_app/models/responses/order_get_model.dart';
// import 'package:mart_up_admin_app/models/responses/product_model.dart';
// import 'package:mart_up_admin_app/screens/create_order_product.dart';
// import 'package:mart_up_admin_app/screens/order_screen.dart';
// import 'package:mart_up_admin_app/services/order_service.dart';
// import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
// import 'package:mart_up_admin_app/widgets/empty_screen.dart';
// import 'package:mart_up_admin_app/widgets/loader_product_card_screen.dart';
// import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
// import 'package:mart_up_admin_app/widgets/roundedButton.dart';
// import 'package:flutter/src/widgets/image.dart' as img;

// class CreateOrder extends StatefulWidget {
//   List<ProductModel>? selectedProduct;
//   CreateOrder({this.selectedProduct});

//   @override
//   _CreateOrderState createState() => _CreateOrderState();
// }

// class _CreateOrderState extends State<CreateOrder> {
//   bool _isloading = true;
//   TextEditingController NameCtlr = new TextEditingController();
//   TextEditingController addressCtlr = new TextEditingController();
//   TextEditingController mobileNoCtlr = new TextEditingController();
//   final formKey = GlobalKey<FormState>();
//   String? valueChoose;
//   List listItem = ['Dhaka', 'Chattogram', 'Rajshahi'];
//   OrderGetModel? orderModel;
//   int? id;

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     //_isloading = false;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         elevation: 1,
//         leading: IconButton(
//           onPressed: () {
//             Navigator.pop(context,
//                 MaterialPageRoute(builder: (context) => OrderScreen()));
//           },
//           icon: Icon(
//             Icons.arrow_back_ios_new_rounded,
//             color: Colors.grey,
//             size: 22,
//           ),
//         ),
//         title: Text(
//           "Create Order",
//           style: TextStyle(
//               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
//         ),
//       ),
//       body: Container(
//         child: SafeArea(
//           child: Form(
//             key: formKey,
//             child: SingleChildScrollView(
//               child: Padding(
//                 padding: const EdgeInsets.all(16.0),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(
//                       'City',
//                       style: TextStyle(
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 20),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Container(
//                       padding: EdgeInsets.only(left: 16, right: 16),
//                       decoration: BoxDecoration(
//                           border: Border.all(
//                         color: Colors.grey,
//                         width: 1,
//                       )),
//                       child: DropdownButton(
//                           value: valueChoose,
//                           hint: Text(
//                             'Select',
//                             style: TextStyle(
//                               color: Colors.grey,
//                               fontWeight: FontWeight.w700,
//                             ),
//                           ),
//                           isExpanded: true,
//                           underline: SizedBox(),
//                           icon: Icon(
//                             Icons.expand_more,
//                             color: Colors.grey,
//                           ),
//                           items: listItem.map((valueItem) {
//                             return DropdownMenuItem(
//                               value: valueItem,
//                               child: Text(valueItem),
//                             );
//                           }).toList(),
//                           onChanged: (newValue) {
//                             setState(() {
//                               valueChoose = newValue.toString();
//                             });
//                           }),
//                     ),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     Text(
//                       'First Name',
//                       style: TextStyle(
//                         color: Colors.black,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 20,
//                       ),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Container(
//                       child: TextFormField(
//                         controller: NameCtlr,
//                         decoration: InputDecoration(
//                           contentPadding: const EdgeInsets.only(left: 15),
//                           border: OutlineInputBorder(
//                               borderRadius: BorderRadius.circular(10)),
//                         ),
//                         validator: (value) {
//                           if (value!.isEmpty) {
//                             return "First Name is Empty";
//                           } else {
//                             return null;
//                           }
//                         },
//                       ),
//                     ),
//                     Text(
//                       'Mobile No',
//                       style: TextStyle(
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 20),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Container(
//                       child: TextFormField(
//                         controller: mobileNoCtlr,
//                         keyboardType: TextInputType.number,
//                         decoration: InputDecoration(
//                           contentPadding: const EdgeInsets.only(left: 15),
//                           border: OutlineInputBorder(
//                               borderRadius: BorderRadius.circular(10)),
//                         ),
//                         validator: (value) {
//                           if (value!.isEmpty && value.length < 11) {
//                             return "Enter correct Mobile Number";
//                           } else {
//                             return null;
//                           }
//                         },
//                       ),
//                     ),
//                     Text(
//                       'Address',
//                       style: TextStyle(
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 20),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Container(
//                       child: TextFormField(
//                         controller: addressCtlr,
//                         decoration: InputDecoration(
//                           contentPadding: new EdgeInsets.symmetric(
//                               vertical: 25.0, horizontal: 10.0),
//                           border: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(10),
//                           ),
//                         ),
//                         maxLines: 4,
//                         validator: (value) {
//                           if (value!.isEmpty) {
//                             return "Enter your address";
//                           } else {
//                             return null;
//                           }
//                         },
//                       ),
//                     ),
//                     Padding(
//                       padding: EdgeInsets.all(8.0),
//                       child: RoundedButton(
//                           buttonText: "Select Product",
//                           func: () async {
//                             //print(valueChoose);

//                             Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) =>
//                                         CreateOrderProduct()));

//                             // update();
//                             //print(AddAddressModel.fromJson());
//                           }),
//                     ),
//                     widget.selectedProduct != null
//                         ? Container(
//                             height: 200,
//                             child: ListView.builder(
//                                 itemCount: widget.selectedProduct!.length,
//                                 itemBuilder: (BuildContext context, index) {
//                                   return Card(
//                                       elevation: 1,
//                                       child: Container(
//                                           child: Padding(
//                                         padding: EdgeInsets.all(8),
//                                         child: Row(
//                                             // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                             children: [
//                                               Padding(
//                                                 padding: EdgeInsets.only(
//                                                   right: 16,
//                                                 ),
//                                                 child: Container(
//                                                     height: 100,
//                                                     width: 100,
//                                                     //color: Colors.amber,
//                                                     child: img.Image.network(
//                                                       widget.selectedProduct![index]
//                                                                   .images ==
//                                                               null
//                                                           ? emptyImage
//                                                           : '${widget.selectedProduct![index].images![0].src!}',
//                                                       fit: BoxFit.cover,
//                                                     )),
//                                               ),
//                                               Column(
//                                                   crossAxisAlignment:
//                                                       CrossAxisAlignment.start,
//                                                   children: [
//                                                     Row(
//                                                       mainAxisAlignment:
//                                                           MainAxisAlignment
//                                                               .spaceBetween,
//                                                       children: [
//                                                         Container(
//                                                           height: 50,
//                                                           width: 130,
//                                                           //color: Colors.amber,
//                                                           child: Text(
//                                                             '${widget.selectedProduct![index].name!}',
//                                                             maxLines: 2,
//                                                             overflow:
//                                                                 TextOverflow
//                                                                     .ellipsis,
//                                                             style: TextStyle(
//                                                                 color: Colors
//                                                                     .black,
//                                                                 fontWeight:
//                                                                     FontWeight
//                                                                         .bold,
//                                                                 fontSize: 16),
//                                                           ),
//                                                         ),
//                                                         Row(
//                                                           children: [
//                                                             Stack(
//                                                               children: [
//                                                                 Container(
//                                                                   //width: 33,
//                                                                   //height: 50,
//                                                                   color: Colors
//                                                                       .amber,
//                                                                 ),
//                                                                 Positioned(
//                                                                   // left: 8,
//                                                                   // bottom: 16,
//                                                                   // right: 2,
//                                                                   child:
//                                                                       IconButton(
//                                                                           onPressed:
//                                                                               () {
//                                                                             // CustomSnackbar.snackbar(
//                                                                             //     context, 'Item deleted');
//                                                                             // DBHelper()
//                                                                             //     .deleteCartItem(fs[index].id!);
//                                                                             //     await view.getCartModel();
//                                                                             showDialog(
//                                                                                 context: context,
//                                                                                 builder: (context) => AlertDialog(
//                                                                                       title: Text('Delete', textScaleFactor: 1),
//                                                                                       content: Text('Are you sure delete', textScaleFactor: 1),
//                                                                                       actions: <Widget>[
//                                                                                         TextButton(
//                                                                                             child: Text('Cancle'),
//                                                                                             onPressed: () {
//                                                                                               Navigator.of(context).pop(false);
//                                                                                             }),
//                                                                                         TextButton(
//                                                                                             child: Text('Delete'),
//                                                                                             onPressed: () async {
//                                                                                               Navigator.of(context).pop(true);
//                                                                                               CustomSnackbar.snackbar(context, 'Item deleted');
//                                                                                               // DBHelper()
//                                                                                               //     .deleteCartItem(
//                                                                                               //         fs[index]
//                                                                                               //             .id!);
//                                                                                               // await view
//                                                                                               //     .getCartModel();
//                                                                                             }),
//                                                                                       ],
//                                                                                     ));
//                                                                           },
//                                                                           icon:
//                                                                               Icon(
//                                                                             Icons.delete_outline_outlined,
//                                                                             color:
//                                                                                 Colors.grey,
//                                                                           )),
//                                                                 )
//                                                               ],
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       ],
//                                                     ),
//                                                     Row(
//                                                       mainAxisAlignment:
//                                                           MainAxisAlignment
//                                                               .spaceBetween,
//                                                       children: [
//                                                         Container(
//                                                           width: 90,
//                                                           //color: Colors.amber,
//                                                           child: Text(
//                                                             widget
//                                                                     .selectedProduct![
//                                                                         index]
//                                                                     .salePrice!
//                                                                     .isNotEmpty
//                                                                 ? '৳${widget.selectedProduct![index].salePrice!}'
//                                                                 : "",
//                                                             maxLines: 1,
//                                                             overflow:
//                                                                 TextOverflow
//                                                                     .ellipsis,
//                                                             style: TextStyle(
//                                                                 color:
//                                                                     tabSelectColor,
//                                                                 fontWeight:
//                                                                     FontWeight
//                                                                         .bold,
//                                                                 fontSize: 16),
//                                                           ),
//                                                         ),
//                                                         Container(
//                                                           //color: Colors.amber,
//                                                           child: Row(
//                                                             children: [
//                                                               Container(
//                                                                 height: 30,
//                                                                 width: 42,
//                                                                 child:
//                                                                     GestureDetector(
//                                                                         onTap:
//                                                                             () {
//                                                                           if (widget.selectedProduct![index].quantity! >
//                                                                               1) {
//                                                                             widget.selectedProduct![index].quantity =
//                                                                                 widget.selectedProduct![index].quantity! - 1;
//                                                                             ProductModel
//                                                                                 productModel =
//                                                                                 ProductModel(
//                                                                               quantity: widget.selectedProduct![index].quantity! - 1,
//                                                                             );

//                                                                             setState(() {});
//                                                                           }
//                                                                         },
//                                                                         child:
//                                                                             Icon(
//                                                                           Icons
//                                                                               .remove_outlined,
//                                                                           color:
//                                                                               Colors.grey,
//                                                                         )),
//                                                                 decoration:
//                                                                     BoxDecoration(
//                                                                   //color: Colors.redAccent,

//                                                                   border: Border
//                                                                       .all(
//                                                                     color: Colors
//                                                                         .grey,
//                                                                   ),
//                                                                   borderRadius: BorderRadius.only(
//                                                                       topLeft: Radius
//                                                                           .circular(
//                                                                               5),
//                                                                       bottomLeft:
//                                                                           Radius.circular(
//                                                                               5)),
//                                                                 ),
//                                                               ),
//                                                               Container(
//                                                                 height: 30,
//                                                                 width: 42,
//                                                                 child: Center(
//                                                                     child: Text(
//                                                                   '${widget.selectedProduct![index].quantity}',
//                                                                   style:
//                                                                       TextStyle(
//                                                                     color: Colors
//                                                                         .white,
//                                                                   ),
//                                                                 )),
//                                                                 decoration:
//                                                                     BoxDecoration(
//                                                                   color: Colors
//                                                                       .grey,
//                                                                   border: Border
//                                                                       .all(
//                                                                     color: Colors
//                                                                         .grey,
//                                                                   ),
//                                                                   //borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5)),
//                                                                 ),
//                                                               ),
//                                                               Container(
//                                                                 height: 30,
//                                                                 width: 42,
//                                                                 child:
//                                                                     GestureDetector(
//                                                                         onTap:
//                                                                             () {
//                                                                           widget
//                                                                               .selectedProduct![index]
//                                                                               .quantity = widget.selectedProduct![index].quantity! + 1;

//                                                                           setState(
//                                                                               () {});
//                                                                           ProductModel
//                                                                               productModel =
//                                                                               ProductModel(
//                                                                             quantity:
//                                                                                 widget.selectedProduct![index].quantity! + 1,
//                                                                           );
//                                                                           print(widget
//                                                                               .selectedProduct![index]
//                                                                               .quantity!);
//                                                                         },
//                                                                         child:
//                                                                             Icon(
//                                                                           Icons
//                                                                               .add,
//                                                                           color:
//                                                                               Colors.grey,
//                                                                         )),
//                                                                 decoration:
//                                                                     BoxDecoration(
//                                                                   //color: Colors.redAccent,
//                                                                   border: Border
//                                                                       .all(
//                                                                     color: Colors
//                                                                         .grey,
//                                                                   ),
//                                                                   borderRadius: BorderRadius.only(
//                                                                       topRight:
//                                                                           Radius.circular(
//                                                                               5),
//                                                                       bottomRight:
//                                                                           Radius.circular(
//                                                                               5)),
//                                                                 ),
//                                                               ),
//                                                             ],
//                                                           ),
//                                                         )
//                                                       ],
//                                                     ),
//                                                   ]),
//                                             ]),
//                                       )));
//                                 }),
//                           )
//                         : Text("")
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//       bottomNavigationBar: Padding(
//         padding: EdgeInsets.all(8.0),
//         child: RoundedButton(
//             buttonText: "Confirm Order",
//             func: () async {
//               print("tap confirm order");

//               setState(() {
//                 odrMod.Shipping shipAddress = odrMod.Shipping();
//                 shipAddress.firstName = NameCtlr.text;
//                 shipAddress.lastName = "none";
//                 shipAddress.city = valueChoose;
//                 shipAddress.address1 = "none";
//                 shipAddress.address2 = addressCtlr.text;
//                 shipAddress.phone = mobileNoCtlr.text;
//                 shipAddress.country = "Bangladesh";
//                 //doc not provide phone
//                 shipAddress.postcode = mobileNoCtlr.text;
//                 shipAddress.state = "none";
//                 orderModel!.shipping = shipAddress;
//                 List<odrMod.LineItem> items = [];
//                 for (var i = 0; i < widget.selectedProduct!.length; i++) {
//                   odrMod.LineItem item = odrMod.LineItem();
//                   item.productId = widget.selectedProduct![i].id;
//                   item.quantity = widget.selectedProduct![i].quantity;
//                   item.variation_id = 0;
//                   items.add(item);
//                 }
//                 orderModel!.lineItems = items;
//                 orderModel!.customerId = 0;
//               });

//               Response data = await OrderService().createOrder(
//                 orderModel!,
//                 id!,
//               );
//               if (data.statusCode < 300) {
//                 print("Success");
//                 //  var d= await compute(ProductService.parseProduct, data.body);
//                 // Navigator.push(
//                 //     context,
//                 //     MaterialPageRoute(
//                 //         builder: (context) =>
//                 //             SuccessScreen())
//                 //             );
//               } else {
//                 CustomSnackbar.snackbar(
//                     context, "Your order not comfirm, Please try agian.");
//                 //_isLoading = false;
//               }
//             }),
//       ),
//     );
//   }
// }
