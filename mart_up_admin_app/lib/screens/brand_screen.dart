import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/links.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/providers/brand_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/add_brand.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/create_brand_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/loader_brand_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BrandScreen extends StatefulWidget {
  const BrandScreen({Key? key}) : super(key: key);

  @override
  _BrandScreenState createState() => _BrandScreenState();
}

class _BrandScreenState extends State<BrandScreen> {
  late BrandProvider brandProvider;
  bool _isloading = true;
  bool _isConnection = false;
  bool _isloading2 = true;
  TextEditingController _searchKeyController = TextEditingController();

  //String? searchKey;
  
  int _page = 1;
  int _limit = 5;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();
  @override
  void initState() {
    super.initState();

    checkNetConnectivity();
    brandProvider = Provider.of<BrandProvider>(context, listen: false);

    getBrand();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }

  getBrand() async {
    await brandProvider.getBrand(
        brandProvider.page = _page, brandProvider.limit = _limit);

    setState(() {
     
      _isloading = false;
      _isloading2 = false;
    });
  }

  void _onLoading() async {
    _page++;

    setState(() {});

    var list = await BrandService.fetchBrand(_page, _limit);
    if (list != null) {
      brandProvider.brandListProvider!.addAll(list);
      //print(viewModelProvider.productListProvider!.length);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchBrand() async {
    brandProvider.brandListProvider!.clear();
    setState(() {
      _isloading = true;
      _isloading2 = true;
      //viewModelProvider.brandListProvider!.isEmpty;
    });
    // var list = await BrandService.fetchBrand(
    //     // _page,
    //     // _limit,
    brandProvider.searchKey = _searchKeyController.text;
    //     viewModelProvider.page = _page,
    //     viewModelProvider.limit = _limit);
    var list = await BrandService.fetchBrand(_page, _limit,
        searchKey: brandProvider.searchKey);
    //page = 1;
    if (list != null) {
      brandProvider.brandListProvider!.addAll(list);
    }
    setState(() {
      //searchKey=_searchKeyController.text;
      _isloading = false;
      _isloading2 = false;
      brandProvider.searchKey = null;
      //viewModelProvider.brandListProvider!.isNotEmpty;
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    // double deviceHeight = MediaQuery.of(context).size.height;
    // double deviceWidth = MediaQuery.of(context).size.width;
    // double horizontal=MediaQuery.of(context).padding.left+MediaQuery.of(context).padding.right;
    // double vertical=MediaQuery.of(context).padding.top+MediaQuery.of(context).padding.bottom;
    // double safeBlockHorizontal = (deviceWidth - horizontal)/100;
    // double safeBlockVertical = (deviceHeight - vertical)/100;
    // // double safeBlockHorizontal1 = deviceWidth/100;
    // // double safeBlockVertical1 = deviceHeight/100;
    // print(safeBlockVertical);
    // print(safeBlockHorizontal);
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon:
            hintText: hintText,
            hintStyle: TextStyle(
              color: Colors.black54,
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              searchBrand();
            },
            icon: Icon(
              Icons.search_outlined,
              size: 28,
            ),
            color: tabSelectColor,
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.safeBlockHorizontal! * 3,
            vertical: SizeConfig.safeBlockVertical! * 1),
        child: _isConnection
            ? _isloading && _isloading2
                ? LoaderBrandScreen()
                : _isloading2
                    ? Center(
                        child: BoxLoader(),
                      )
                    : brandProvider.brandListProvider!.isEmpty
                        ? EmptyScreen()
                        : SmartRefresher(
                            controller: refreshController,
                            // scrollDirection: Axis.vertical,
                            // physics: BouncingScrollPhysics(),
                            enablePullUp: true,
                            onLoading: _onLoading,
                            onRefresh: _onRefresh,
                            footer: ClassicFooter(
                              loadStyle: LoadStyle.ShowWhenLoading,
                            ),
                            child: ListView.builder(
                                itemCount:
                                    brandProvider.brandListProvider!.length,
                                itemBuilder: (BuildContext context, index) {
                                  return Card(
                                    elevation: 1,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Container(
                                        height:
                                            SizeConfig.safeBlockVertical! * 17,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                    height: SizeConfig
                                                            .safeBlockVertical! *
                                                        22,
                                                    width: SizeConfig
                                                            .safeBlockHorizontal! *
                                                        28,
                                                    child: Image.network(
                                                      //'${widget.brandModel!.termThumbnailImage!.url}',
                                                      brandProvider
                                                              .brandListProvider![
                                                                  index]
                                                              .termThumbnailImage!
                                                              .url!
                                                              .isNotEmpty
                                                          ? brandProvider
                                                              .brandListProvider![
                                                                  index]
                                                              .termThumbnailImage!
                                                              .url!
                                                          : emptyImage,

                                                      fit: BoxFit.contain,
                                                    )),
                                                SizedBox(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal! *
                                                      5,
                                                ),
                                                Container(
                                                  width: SizeConfig
                                                          .safeBlockHorizontal! *
                                                      30,
                                                  child: Text(
                                                    brandProvider
                                                        .brandListProvider![
                                                            index]
                                                        .name!,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 24,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      AddBrand(
                                                                        singleBrand:
                                                                            brandProvider.brandListProvider![index],
                                                                      )));
                                                    },
                                                    icon: Icon(Icons.edit)),
                                                //SizedBox(width: 15,),
                                                IconButton(
                                                    onPressed: () {
                                                      print("tap");

                                                      showDialog(
                                                          context: context,
                                                          builder:
                                                              (context) =>
                                                                  AlertDialog(
                                                                    title: Text(
                                                                        'Delete',
                                                                        textScaleFactor:
                                                                            1),
                                                                    content: Text(
                                                                        'Are you sure delete',
                                                                        textScaleFactor:
                                                                            1),
                                                                    actions: <
                                                                        Widget>[
                                                                      TextButton(
                                                                          child: Text(
                                                                              'Cancle'),
                                                                          onPressed:
                                                                              () {
                                                                            Navigator.of(context).pop(false);
                                                                          }),
                                                                      TextButton(
                                                                          child: Text(
                                                                              'Delete'),
                                                                          onPressed:
                                                                              () async {
                                                                            Navigator.of(context).pop(true);
                                                                            setState(() {
                                                                              _isloading2 = true;
                                                                            });
                                                                            Response?
                                                                                res =
                                                                                await CreateBrandService().deleteBrand(brandId: brandProvider.brandListProvider![index].id);
                                                                            // Response?
                                                                            //     res =
                                                                            //     CreateCategoryService().deleteCategory(categoryId: allCategory![index].id);
                                                                            if (res!.statusCode == 200 ||
                                                                                res.statusCode == 201) {
                                                                              // CustomSnackbar.snackbar(
                                                                              //     context,
                                                                              //     "category delete succesfully");
                                                                              setState(() {
                                                                                _isloading2 = false;
                                                                              });
                                                                              EasyLoading.showSuccess("Success!");
                                                                              getBrand();

                                                                              // setState(
                                                                              //     () {});
                                                                            } else {
                                                                              setState(() {
                                                                                _isloading2 = false;
                                                                              });
                                                                              EasyLoading.showError("Failed!");
                                                                            }

                                                                            // getCategory();
                                                                            setState(() {});
                                                                          }),
                                                                    ],
                                                                  ));
                                                    },
                                                    icon: Icon(Icons.delete)),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          )
            : NoInternet(
                pressed: () async {
                  await checkNetConnectivity();
                  _isConnection == false
                      ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                      : checkNetConnectivity();
                  getBrand();
                },
              ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your onPressed code here!
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddBrand()));
        },
        backgroundColor: themeColor,
        child: const Icon(
          Icons.add_outlined,
          size: 50,
        ),
      ),
    );
  }
}
