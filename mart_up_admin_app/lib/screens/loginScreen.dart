import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/admin_login_model.dart';
import 'package:mart_up_admin_app/models/responses/admin_login_response_model.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/services/admin_login_service.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
import 'package:provider/provider.dart';

import 'dashboard.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();
  final formKey = GlobalKey<FormState>(); //key for form
  bool _obscureText = true;
  bool _isLoading = false;
  int? count;
  late ViewModelProvider viewModelProvider;
  AdminLoginResponseModel? loginData;
  // @override
  // void initState() {
  //   // TODO: implement initState
  //    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
  //   super.initState();
  // }
  getAdminLoginInfo() async {
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    await viewModelProvider.fatchAdminLoginInfo();
    //print(viewModelProvider.adminLoginInfoProvider!.data!.token!);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Builder(
          builder: (context) => Scaffold(
            body: SafeArea(
              child: Form(
                key: formKey,
                child: Container(
                  margin: EdgeInsets.all(25),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Image.asset(
                          "assets/images/enlight.png",
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        const Text(
                          "Welcome to Mart-Up",
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Sign in to continue",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 7.0),
                          child: Container(
                            child: TextFormField(
                              controller: userName,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: "User Name",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(
                                    Icons.email,
                                    size: 30,
                                  ),
                                ),
                                //hintStyle: kBodyText,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Enter correct User Name";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 7.0),
                          child: Container(
                            child: TextFormField(
                              controller: password,
                              decoration: InputDecoration(
                                  contentPadding:
                                      const EdgeInsets.symmetric(vertical: 16),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  hintText: "Password",
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: Icon(Icons.lock),
                                  ),
                                  suffixIcon: IconButton(
                                    icon: Icon(_obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                  )),
                              obscureText: _obscureText,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Enter correct Password";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        _isLoading
                            ? BuildLoading()
                            : RoundedButton(
                                buttonText: "Sign in",
                                func: () async {
                                  if (formKey.currentState!.validate()) {
                                    AdminLoginModel alm = AdminLoginModel(
                                        username: userName.text,
                                        password: password.text);
                                    setState(() {
                                      _isLoading = true;
                                    });

                                    Response? response =
                                        await AdminLoginService()
                                            .loginAdmin(alm);

                                    if (response!.statusCode == 200) {
                                      var userdata = await compute(
                                          AdminLoginService().parseLogin,
                                          response.body);
                                      setState(() {});

                                      if (userdata.data!.roles!
                                          .contains("administrator")) {
                                        DBHelper().removeLoginAdminData();
                                        DBHelper()
                                            .saveLoginAdminData(response.body);

                                        await CustomSnackbar.snackbar(
                                            context, "sign in succesful");
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Dashboard()));
                                      } else {
                                        //DBHelper().removeLoginAdminData();
                                        await CustomSnackbar.snackbar(context,
                                            "Sign In With Adminstrator Credential");

                                        setState(() {
                                          _isLoading = false;
                                        });
                                      }
                                    } else if (response.statusCode == 403) {
                                      setState(() {
                                        _isLoading = false;
                                      });

                                      await CustomSnackbar.snackbar(context,
                                          "Login Failed check email and password");
                                    }
                                  }
                                }),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
