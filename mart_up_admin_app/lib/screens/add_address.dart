import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:mart_up_admin_app/models/requests/create_order_model.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/screens/order_screen.dart';
import 'package:mart_up_admin_app/services/order_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';


// ignore: must_be_immutable
class AddAddressPage extends StatefulWidget {
  String? copuneCtr;
  List<ProductModel>? cartList;
  AddAddressPage({this.copuneCtr,this.cartList});

  @override
  _AddAddressPageState createState() => _AddAddressPageState();
}

class _AddAddressPageState extends State<AddAddressPage> {
  TextEditingController firstNameCtlr = new TextEditingController();
  TextEditingController lastNameCtlr = new TextEditingController();
  TextEditingController roadNoCtlr = new TextEditingController();
  TextEditingController houseNoCtlr = new TextEditingController();
  TextEditingController mobileNoCtlr = new TextEditingController();
  bool showBtn = false;
  final formKey = GlobalKey<FormState>();
  OrderCreateModel orderModel= OrderCreateModel();
   bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    // readCity();
    // readFirstName();
    //readText();
  widget.cartList;
  print(widget.cartList!.length);
    
  }

  String? valueChoose;

  List listItem = ['Dhaka', 'Chattogram', 'Rajshahi'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            // Navigator.pop(
            //     context, MaterialPageRoute(builder: (context) => ToShip()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Add Address",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        child: SafeArea(
            child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'City',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 16, right: 16),
                    decoration: BoxDecoration(
                        border: Border.all(
                      color: Colors.grey,
                      width: 1,
                    )),
                    child: DropdownButton(
                        value: valueChoose,
                        hint: Text(
                          'Select',
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        isExpanded: true,
                        underline: SizedBox(),
                        icon: Icon(
                          Icons.expand_more,
                          color: Colors.grey,
                        ),
                        items: listItem.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            valueChoose = newValue.toString();
                          });
                        }),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'First Name',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: TextFormField(
                      controller: firstNameCtlr,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(left: 15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "First Name is Empty";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Last Name',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,)
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: TextFormField(
                      controller: lastNameCtlr,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(left: 15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Last Name is Empty";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Address',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: TextFormField(
                      controller: houseNoCtlr,
                      decoration: InputDecoration(
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 25.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      maxLines: 4,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Enter your address";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Mobile No',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    // decoration: BoxDecoration(
                    //   border: Border.all(color: Colors.black26, width: 1.5),
                    //   borderRadius: BorderRadius.circular(16),
                    // ),
                    child: TextFormField(
                      controller: mobileNoCtlr,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(left: 15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty && value.length < 11) {
                          return "Enter correct Mobile Number";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        )),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(8.0),
        child:  _isLoading
                              ? BoxLoader()
                              :RoundedButton(
            buttonText:  "Confirm Order",
            func: () async {
              //print(valueChoose);
              if (formKey.currentState!.validate()) {
                                                
                                    //  final  snackBar= SnackBar(content: Text("Select Address"));
                                    //  Scaffold.of(context).showSnackBar(snackBar);
                                    // print('Confirm order btn tap');
                                    setState(() {
                                      orderModel.paymentMethod = "Cash On Delivery";
                                      orderModel.paymentMethodTitle = "cash";
                                      orderModel.setPaid = false;
                                      //orderModel.status="pending";
                                      Shipping shipAddress = Shipping();
                                      // shipAddress.firstName="s";
                                      // shipAddress.lastName="h";
                                      // shipAddress.company="en";
                                      // shipAddress.city="r";
                                      // shipAddress.address1="4";
                                      // shipAddress.address2="23";
                                      // shipAddress.phone="0923809238";
                                      // shipAddress.country="bd";
                                      // shipAddress.postcode="222";
                                      // shipAddress.state="arizona";
                                      shipAddress.firstName =
                                          firstNameCtlr.text;
                                      shipAddress.lastName =
                                          lastNameCtlr.text;
                                      //shipAddress.company="default";
                                      shipAddress.city = valueChoose ==null?"Not Specified": valueChoose;
                                      shipAddress.address1 = "none";
                                      shipAddress.address2 =
                                         houseNoCtlr.text;
                                      shipAddress.phone =
                                          "0000000";
                                      shipAddress.country = "Bangladesh";
                                      shipAddress.postcode = mobileNoCtlr.text;
                                      shipAddress.state = "none";
                                      orderModel.shipping = shipAddress;
                                      List<LineItem> items = [];
                                      List<CouponLine> coupones = [];
                                      CouponLine coupon = CouponLine();
                                      coupon.code = widget.copuneCtr;
                                      coupones.add(coupon);
                                      // coupones[0].code = widget.copuneCtr;
                                      if (widget.copuneCtr != null) {
                                        orderModel.couponLines = coupones;
                                      } else {}
                                      //        cartProducts.map((e)  {
                                      //     // var item={
                                      //     //           'id':e.id,
                                      //     //            'id': e.quantity};
                                      //     LineItem item= LineItem();

                                      //     item.productId=e.id;
                                      //     item.quantity=e.quantity;
                                      //     item.variation_id=0;
                                      //       items.add(item);
                                      //   });
                                      // orderModel.lineItems=items;
                                      for (var i = 0;
                                          i < widget.cartList!.length;
                                          i++) {
                                        LineItem item = LineItem();
                                        item.productId = widget.cartList![i].id;
                                        item.quantity =
                                            widget.cartList![i].quantity;
                                        item.variation_id = 0;
                                        items.add(item);
                                      }
                                      orderModel.lineItems = items;
                                      orderModel.customerId =
                                          0;
                                      // change login user
                                      // LineItem litem=LineItem();
                                      // // orderModel.lineItems![0].productId=2;
                                      // // orderModel.lineItems![0].quantity=1;
                                      // // orderModel.lineItems![0].variation_id=3;
                                      // litem.productId=2;
                                      // litem.quantity=1;
                                      // litem.variation_id=3;
                                      // orderModel.lineItems=litem;
                                    });

                                    // OrderModel or=OrderModel(paymentMethod: widget.pay,paymentMethodTitle: "cash",
                                    //             );
                                    //or.lineItems=cartProducts;
                                    // List<LineItem> items=[];
                                    // //  Ing? ships;
                                    // //or.paymentMethod=widget.pay;
                                    // //  or.lineItems![0].productId=cartProducts[0].id;
                                    // //  or.lineItems![0].quantity=cartProducts[0].quantity;
                                    // var orderItem=cartProducts.map((e)  {
                                    //   // var item={
                                    //   //           'id':e.id,
                                    //   //            'id': e.quantity};
                                    //   LineItem item= LineItem();

                                    //   item.productId=e.id;
                                    //   item.quantity=e.quantity;
                                    //     items.add(item);
                                    // });
                                    // or.lineItems=items;
                                    // // print(items);
                                    // // var shipItem=selectedAddresses.map((e) {
                                    // //   Ing ship=Ing();
                                    // //   ship.firstName=e.fname;
                                    // //   ship.lastName=e.lname;
                                    // //   ship.city=e.city;
                                    // //   ship.address1=e.roadNo;
                                    // //   ship.address2=e.houseNo;
                                    // //   ship.phone=e.mobileNo;
                                    // //   ships;
                                    // // });
                                    // // or.lineItems=items;
                                    // // or.shipping=ships;
                                    // // print(ships);

                                    //or.paymentMethodTitle="djjkf";
                                    // or.setPaid=false;
                                    //  or.shipping!.firstName="s";
                                    //  or.shipping!.lastName="h";
                                    //  or.shipping!.city="d";
                                    //  or.shipping!.address1="f";
                                    //  or.shipping!.address2="t";
                                    //  or.shipping!.phone="32423543";
                                    //  or.shipping!.email="d@d.com";
                                    //  or.shipping!.country="bd";
                                    //  or.shipping!.postcode="333";
                                    //  or.shipping!.state="delware";
                                  
                                   
                                     
                                         
                                        _isLoading = true;
                                        Response data = await OrderService()
                                            .createOrder(
                                                orderModel);

                                        if (data.statusCode < 300) {
                                          //  var d= await compute(ProductService.parseProduct, data.body);
                                          _isLoading=false;
                                          EasyLoading.showSuccess("Success!");
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      OrderScreen()));
                                        } else {
                                          
                                          _isLoading = false;
                                          EasyLoading.showError("Failed!");
                                        }
                                       
                                   
                                 
              }

              // update();
              //print(AddAddressModel.fromJson());
            }),
      ),
    );
  }
}
