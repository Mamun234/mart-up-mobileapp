import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/responses/admin_login_response_model.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/dashboard.dart';
import 'package:mart_up_admin_app/screens/loginScreen.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late ViewModelProvider viewModelProvider;
  bool _isConnection = false;
  bool? isTokenExpired;
  String? token;
  AdminLoginResponseModel info = AdminLoginResponseModel();
  @override
  void initState() {
    navigateTo();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    getAdminLoginInfo();
 
    super.initState();
  }

  getAdminLoginInfo() async {
    await viewModelProvider.fatchAdminLoginInfo();
    //print(viewModelProvider.adminLoginInfoProvider!.data!.token!);
    setState(() {});
    if(viewModelProvider.adminLoginInfoProvider !=null){
      decodeToken();
    }
  }
        decodeToken() {
    token = viewModelProvider.adminLoginInfoProvider!.data!.token!;
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token!);
     isTokenExpired = JwtDecoder.isExpired(token!);
    print('decodec: $decodedToken');
    DateTime expirationDate = JwtDecoder.getExpirationDate(token!);
     if (isTokenExpired!) {
        DBHelper().removeLoginAdminData();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }

   
    print('date: $expirationDate');
  }

  navigateTo() async {
   await  checkNetConnectivity();
    _isConnection?await Future.delayed(Duration(seconds: 2), () {
      if (viewModelProvider.adminLoginInfoProvider != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Dashboard()));
      }
      // else  if (isTokenExpired!) {
      //   DBHelper().removeLoginAdminData();
      //   Navigator.push(
      //       context, MaterialPageRoute(builder: (context) => LoginScreen()));
      // }
       else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
    }) : CustomSnackbar.snackbar(
            context,
            "No Internet. Check Connection",
          );
  }

            checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    
    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
          child: Column(
        children: [
          SizedBox(
             height: SizeConfig.safeBlockVertical! *36,
          ),
          Container(
            height: SizeConfig.safeBlockVertical! *7,
            width:  SizeConfig.safeBlockHorizontal! *75,
            child: Image.asset(
              'assets/images/enlight.png',
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: SizeConfig.safeBlockVertical! *5,
          ),
          Text(
            'Welcome To Mart-Up',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24),
          ),
        ],
      )),
    );
  }
}
