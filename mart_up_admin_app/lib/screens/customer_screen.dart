import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/providers/customer_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/services/customer_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/loader_customer_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CustomerScreen extends StatefulWidget {
  const CustomerScreen({Key? key}) : super(key: key);

  @override
  _CustomerScreenState createState() => _CustomerScreenState();
}

class _CustomerScreenState extends State<CustomerScreen> {
  late CustomerProvider customerProvider;
  bool _isloading = true;
  bool _isConnection = false;
  TextEditingController _searchKeyController = TextEditingController();
  int _page = 1;
  int _limit = 5;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    checkNetConnectivity();
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);

    getCustomer();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }

  getCustomer() async {
    customerProvider.page = _page;
    customerProvider.limit = _limit;

    await customerProvider.getCustomer(
        customerProvider.page, customerProvider.limit);

    setState(() {
      _isloading = false;
    });
  }

  void _onLoading() async {
    _page++;

    setState(() {});

    var list = await CustomerService.fetchCustomer(
        customerProvider.searchKey, _page, _limit);
    if (list != null) {
      customerProvider.customerListProvider!.addAll(list);
      //print(viewModelProvider.productListProvider!.length);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchCustomer() async {
    // int _page = 1;
    // int _limit=5;
    customerProvider.customerListProvider!.clear();
    setState(() {
      _isloading = true;
      //viewModelProvider.brandListProvider!.isEmpty;
    });
    var list = await CustomerService.fetchCustomer(
        customerProvider.searchKey = _searchKeyController.text,
        customerProvider.page = _page,
        customerProvider.limit = _limit);
    //page = 1;
    if (list != null) {
      customerProvider.customerListProvider!.addAll(list);
    }
    setState(() {
      //searchKey=_searchKeyController.text;
      _isloading = false;
      customerProvider.searchKey=null;
      //viewModelProvider.brandListProvider!.isNotEmpty;
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon: 
            hintText: hintText,
            hintStyle: TextStyle(
                color: Colors.black54,),
          ),
        ),
        actions: [
          IconButton(
                  onPressed: () {
                    searchCustomer();
                    
                  },
                  icon: Icon(Icons.search_outlined,size: 28,),
                  color: tabSelectColor,
                ),
        ],
      ),
      drawer: AppDrawer(),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
        child: _isConnection
            ? _isloading
                ? Center(
                    child: LoaderCustomerScreen(),
                  )
                : _isloading == false &&
                        customerProvider.customerListProvider!.isEmpty
                    ? EmptyScreen()
                    : SmartRefresher(
                        controller: refreshController,
                        // scrollDirection: Axis.vertical,
                        // physics: BouncingScrollPhysics(),
                        enablePullUp: true,
                        onLoading: _onLoading,
                        onRefresh: _onRefresh,
                        footer: ClassicFooter(
                          loadStyle: LoadStyle.ShowWhenLoading,
                        ),
                        child: ListView.builder(
                            itemCount:
                                customerProvider.customerListProvider!.length,
                            itemBuilder: (BuildContext context, index) {
                              return Card(
                                elevation: 1,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0),
                                  child: Container(
                                    height: SizeConfig.safeBlockVertical! * 15,
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 40,
                                          backgroundColor: Colors.white,
                                          backgroundImage: NetworkImage(
                                              customerProvider
                                                  .customerListProvider![index]
                                                  .avatarUrl!),
                                        ),
                                        SizedBox(
                                          width: 25,
                                        ),
                                        Container(
                                          //width: 90,
                                          //color: Colors.amber,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: 35,
                                              ),
                                              Text(
                                                '${customerProvider.customerListProvider![index].firstName}',
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                '${customerProvider.customerListProvider![index].email}',
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }),
                      )
            : NoInternet(
                pressed: () async {
                  await checkNetConnectivity();
                  _isConnection == false
                      ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                      : checkNetConnectivity();
                  getCustomer();
                },
              ),
      )),
    );
  }
}
