
import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/links.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/responses/coupon_check_model.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/providers/product_provider.dart';
import 'package:mart_up_admin_app/screens/add_address.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/services/coupon_check_service.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/dash_line.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
import 'package:provider/provider.dart';
import 'package:flutter/src/widgets/image.dart' as img;

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
    String? couponCode;
  List<CouponCheckModel> couponInfo = [];
    TextEditingController couponCtr = TextEditingController();
  final formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    
    
  }

  deleteItem(index, name) {
    int? t = 0;
    final ProductProvider productProvider =
        Provider.of<ProductProvider>(context, listen: false);
    productProvider.cartList.removeAt(index);
    for (var element in productProvider.cartList) {
      t = int.parse(element.salePrice!) + t!;
      setState(() {});
    }
    productProvider.setCart(productProvider.cartList);
    productProvider.setTotalModel(t);
    CustomSnackbar.snackbar(context, '$name deleted');
  }

  addItemQuantity(price, name) {
    final ProductProvider productProvider =
        Provider.of<ProductProvider>(context, listen: false);

    productProvider.total = productProvider.total! + int.parse(price!);
    productProvider.setTotalModel(productProvider.total);
  }

  removeItemQuantity(price) {
    final ProductProvider productProvider =
        Provider.of<ProductProvider>(context, listen: false);

    productProvider.total = productProvider.total! - int.parse(price!);
    productProvider.setTotalModel(productProvider.total);
  }
    getCoupon(code) {
       final ProductProvider productProvider =
        Provider.of<ProductProvider>(context, listen: false);
    CouponCheckService.fetchCoupon(couponCode: code).then((value) {
      if (value !=null) {
        setState(() {
          couponCode = code;
          couponInfo = value;
          //totalAmountDiscount = DBHelper().getCartTotalAmount();
        });
                    if (couponInfo != null &&
                couponInfo.length > 0 &&
                couponInfo[0].amount != null) {
              // amount = amount - double.parse(couponInfo[0].amount.toString());
              productProvider.total = productProvider.total! - num.parse(couponInfo[0].amount!);
               productProvider.setTotalModel(productProvider.total);
            }
      } else {
        CustomSnackbar.snackbar(context, "Coupon code is invalid");
      }
      // setState(() {
      //   couponInfo = value;
      //   //  couponInfo.length > 0
      //   //     ? couponInfo[0].amount
      //   //     : CustomSnackbar.snackbar(
      //   //         context, "Coupon code is invalid");

      // });
     
    });
  }
  _buildItems(ProductProvider productProvider ){

  return                       ListView.builder(
    scrollDirection: Axis.vertical,
          shrinkWrap: true,

                          itemCount: productProvider.cartList.length,
                          itemBuilder: (context, i) {
                            ProductModel im = productProvider.cartList[i];
                           
                            return Card(
                      elevation: 1,
                      child: Container(
                          child: Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      right: 16.0),
                                  child: Container(
                                      height: 100.0,
                                      width: 100.0,
                                      //color: Colors.amber,
                                      child: img.Image.network(
                                        '${im.images![0].src}',
                                        fit: BoxFit.cover,
                                      )),
                                ),
                                Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            height: 50.0,
                                            width: 130.0,
                                            //color: Colors.amber,
                                            child: Text(
                                              '${im.name}',
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16.0),
                                            ),
                                          ),
                                          Row(
                                            children: [
                                             
                                              Stack(
                                                children: [
                                                  Container(
                                                    width:
                                                        85.0,
                                                    height:
                                                        50,
                                                    //color: Colors.amber,
                                                  ),
                                                  Positioned(
                                                    left: 53,
                                                    bottom:
                                                    16.0,
                                                    //right: 2,
                                                    child: IconButton(
                                                        onPressed: () {
                                                          // CustomSnackbar.snackbar(
                                                          //     context, 'Item deleted');
                                                          // DBHelper()
                                                          //     .deleteCartItem(fs[index].id!);
                                                          //     await view.getCartModel();
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) =>
                                                                      AlertDialog(
                                                                        title: Text(
                                                                            'Delete',
                                                                            textScaleFactor:
                                                                                1),
                                                                        content: Text(
                                                                            'Are you sure delete',
                                                                            textScaleFactor:
                                                                                1),
                                                                        actions: <
                                                                            Widget>[
                                                                          TextButton(
                                                                              child: Text(
                                                                                  'Cancle'),
                                                                              onPressed:
                                                                                  () {
                                                                                Navigator.of(context)
                                                                                    .pop(false);
                                                                              }),
                                                                          TextButton(
                                                                              child: Text(
                                                                                  'Delete'),
                                                                              onPressed:
                                                                                  () async {
                                                                                Navigator.of(context)
                                                                                    .pop(true);
                                                                                // CustomSnackbar.snackbar(
                                                                                //     context,
                                                                                //     'Item deleted');
                                                                                 deleteItem(i, im.name);
                                                                                // DBHelper()
                                                                                //     .deleteCartItem(fs[index].id!);
                                                                                // await view
                                                                                //     .getCartModel();
                                                                              }),
                                                                        ],
                                                                      ));
                                                        },
                                                        icon: Icon(
                                                          Icons
                                                              .delete_outline_outlined,
                                                          color: Colors.grey,
                                                        )),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            width: 90.0,
                                            //color: Colors.amber,
                                            child: Text(
                                              '৳${im.salePrice}',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: tabSelectColor,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                            ),
                                          ),
                                          Container(
                                            //color: Colors.amber,
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 30,
                                                  width: 42.0,
                                                  // child: IconButton(
                                                  //   onPressed: () {
                                                  //     if (fs[index].quantity! > 1) {
                                                  //       CartModel cq = CartModel(
                                                  //           id: fs[index].id,
                                                  //           name: fs[index].name,
                                                  //           salePrice:
                                                  //               fs[index].salePrice,
                                                  //           regularPrice: fs[index]
                                                  //               .regularPrice,
                                                  //           quantity: fs[index]
                                                  //                   .quantity! -
                                                  //               1,
                                                  //           img: fs[index].img);
                                                  //       DBHelper().update(cq);
                                                  //     }
                                                  //   },
                                                  //   icon: Icon(
                                                  //     Icons.remove_outlined,
                                                  //     color: Colors.grey,
                                                  //   ),
                                                  // ),
                                                  child: GestureDetector(
                                                      onTap: () {
                                                     if (im.quantity! > 1) {
                                                  im.quantity = im.quantity! - 1;
                                                }
                                                setState(() {});
                                                removeItemQuantity(im.price);
                                                      },
                                                      child: Icon(
                                                        Icons.remove_outlined,
                                                        color: Colors.grey,
                                                      )),
                                                  decoration: BoxDecoration(
                                                    //color: Colors.redAccent,
                      
                                                    border: Border.all(
                                                      color: Colors.grey,
                                                    ),
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(5),
                                                        bottomLeft:
                                                            Radius.circular(5)),
                                                  ),
                                                ),
                                                Container(
                                                  height: 30,
                                                  width: 42.0,
                                                  child: Center(
                                                    child: Text(
                                                      '${im.quantity}',
                                                      style: TextStyle(
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                    // color: borderColor,
                                                    border: Border.all(
                                                      color: Colors.grey,
                                                    ),
                                                    //borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5)),
                                                  ),
                                                ),
                                                Container(
                                                  height: 30,
                                                  width: 42.0,
                                                  // child: Center(
                                                  //   child: IconButton(
                                                  //     icon: Icon(
                                                  //       Icons.add,
                                                  //       color: Colors.grey,
                                                  //     ),
                                                  //     onPressed: () {
                                                  //       CartModel cq = CartModel(
                                                  //           id: fs[index].id,
                                                  //           name: fs[index].name,
                                                  //           salePrice:
                                                  //               fs[index].salePrice,
                                                  //           regularPrice: fs[index]
                                                  //               .regularPrice,
                                                  //           quantity:
                                                  //               fs[index].quantity! +
                                                  //                   1,
                                                  //           img: fs[index].img);
                                                  //       DBHelper().update(cq);
                                                  //     },
                                                  //   ),
                                                  // ),
                                                  child: GestureDetector(
                                                      onTap: () {
                                                        im.quantity = im.quantity! + 1;
                                                setState(() {});
                                                addItemQuantity(im.price, im.name);
                                                      },
                                                      child: Icon(
                                                        Icons.add,
                                                        color: Colors.grey,
                                                      )),
                                                  decoration: BoxDecoration(
                                                    //color: Colors.redAccent,
                                                    border: Border.all(
                                                      color: Colors.grey,
                                                    ),
                                                    borderRadius: BorderRadius.only(
                                                        topRight: Radius.circular(5),
                                                        bottomRight:
                                                            Radius.circular(5)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ]),
                              ])));
                 
                      
                          });
}
 
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    print(SizeConfig.blockSizeVertical);
    print(SizeConfig.blockSizeHorizontal);
    return Consumer<ProductProvider>(
      builder: (context, productProvider, child) {
      return Scaffold(
       appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(
                context, MaterialPageRoute(builder: (context) => ProductScreen()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Your Cart",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
        body:productProvider.cartList.isEmpty
          ? EmptyScreen()
          : SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 8.0),
                child: SingleChildScrollView(
                  child: Column(
                   
                      children: [
                        Container(
                          
                          child: FutureBuilder(
                            //future: productProvider.cartList!,
                            builder:(context, snapshot){
                              return _buildItems(productProvider);
                            } ,
                            ),
                        ),
                        SizedBox(height: 10,),
                        
                               
                        Form(
                          key: formKey,
                          child: Row(
                                  children: [
                                    Container(
                                      //color: Colors.amberAccent,
                                      height: 60,
                                      width: SizeConfig.blockSizeHorizontal! *70,
                                      decoration: BoxDecoration(
                                          border: Border.all(color: Colors.grey)),
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 16),
                                        child: Center(
                                          child: TextFormField(
                                            controller: couponCtr,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'Enter Coupone Code',
                                              hintStyle: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return "Enter your coupon";
                                              } else {
                                                return null;
                                              }
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 0.01,),
                                    GestureDetector(
                                      onTap: () {
                                        if (formKey.currentState!.validate()) {
                                          getCoupon(couponCtr.text);
                                          // getCoupon(couponCtr.text);
                                          // setState(() {
                                          //   //couponCode = couponCtr.text;
                                          //   getCoupon(couponCtr.text);
                                          //   couponInfo.length > 0
                                          //       ? couponInfo[0].amount
                                          //       : CustomSnackbar.snackbar(
                                          //           context, "Coupon code is invalid");
                                          // });
                                        } else {
                                          print("vsldjufh");
                                        }
                            
                                        print("tap applay btn");
                                      },
                                      child: Container(
                                        height: 60,
                                        width: SizeConfig.blockSizeHorizontal! * 25,
                                        color: tabSelectColor,
                                        child: Center(
                                          child: Text(
                                            'Apply ',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                        ),
                              SizedBox(height: 10,),
                              Card(
                                elevation: 1,
                                child: Padding(
                                  padding: EdgeInsets.all(16.0),
                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  'item: ',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w700),
                                                ),
                                                Text(
                               productProvider.cartList.isEmpty?'0':productProvider.cartList.length.toString(),style: TextStyle(color: Colors.black,fontWeight: FontWeight.w700),
                            ),
                                                //_buildQuentityNumber(),
                                                //  Text(totalQuantity.toString(),
                                                //  style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w700),
                                                //  ),
                            
                                              ],
                                            ),
                                                                                               Text(
                              
                                  productProvider.total == null ? '0' : '৳ ${productProvider.total}',
                                  style: TextStyle(color: Colors.black)
                            ),
                                            //  Text('\$599.25',
                                            //  style: TextStyle(color: Colors.black),
                                            //  )
                                            //_buildTotalAmount(),
                                          ],
                                        ),
                            //             SizedBox(
                            //               height: 20,
                            //             ),
                            //             Row(
                            //               mainAxisAlignment:
                            //                   MainAxisAlignment.spaceBetween,
                            //               children: [
                            //                                Text(
                              
                            //       productProvider.total == null ? '0' : '${productProvider.total}',
                            //       style: TextStyle(color: tabSelectColor)
                            // ),
                              
                            //               ],
                            //             ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Discount',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            Text(
                                              // "dsk",
                                              '৳ ${couponInfo.length > 0 ? couponInfo[0].amount : "0"}',
                                              style: TextStyle(color: Colors.black),
                                            ),
                                          ],
                                        ),
                                        //  Row(
                                        //    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        //    children: [
                                        //      Text('Import Charge',
                                        //      style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w700),
                                        //      ),
                                        //      Text('\$25',
                                        //      style: TextStyle(color: Colors.black),
                                        //      )
                                        //    ],
                                        //  ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        DashLine(
                                          color: Colors.grey,
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Total',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            //  totalPrice==null ?  Text('0'):Text(totalPrice.toString(),
                                            //  style: TextStyle(color: tabSelectColor),
                                            //  )
                                            Text(
                              
                                  productProvider.total == null ? '0' : '৳ ${productProvider.total}',
                                  style: TextStyle(color: Colors.black)
                            ),
                                            // _buildTotalAmount(),
                                           
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                        
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //   children: [
                        //     const Text(
                        //      'Total',
                        //     ),
                        //     Text(
                              
                        //           productProvider.total == null ? '0' : '${productProvider.total}',
                        //     ),
                        //   ],
                        // )
                      ],
                    ),
                ),
              ),
             
              
            ),
                   bottomNavigationBar: 
            productProvider.cartList.isNotEmpty?Padding(
                padding: EdgeInsets.all(8.0),
                child: RoundedButton(
                    buttonText: "Check Out",
                    func: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddAddressPage(
                                    copuneCtr: couponCode,cartList: productProvider.cartList,
                                  )));
                    }),
              ):Text(''),
      );
          
    }
    );
    
  }
  
}

// _buildItems(){

//   return                       ListView.builder(
//                           itemCount: productProvider.cartList.length,
//                           itemBuilder: (context, i) {
//                             ProductModel im = productProvider.cartList[i];
                           
//                             return Card(
//                       elevation: 1,
//                       child: Container(
//                           child: Padding(
//                         padding: EdgeInsets.all(8.0),
//                         child: Row(
//                             // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Padding(
//                                 padding: EdgeInsets.only(
//                                     right: 16.0),
//                                 child: Container(
//                                     height: 100.0,
//                                     width: 100.0,
//                                     //color: Colors.amber,
//                                     child: img.Image.network(
//                                       '${im.images![0]}',
//                                       fit: BoxFit.cover,
//                                     )),
//                               ),
//                               Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Container(
//                                           height: 50.0,
//                                           width: 130.0,
//                                           //color: Colors.amber,
//                                           child: Text(
//                                             '${im.name}',
//                                             maxLines: 2,
//                                             overflow: TextOverflow.ellipsis,
//                                             style: TextStyle(
//                                                 color: Colors.black,
//                                                 fontWeight: FontWeight.bold,
//                                                 fontSize: 16.0),
//                                           ),
//                                         ),
//                                         Row(
//                                           children: [
                                           
//                                             Stack(
//                                               children: [
//                                                 Container(
//                                                   width:
//                                                       85.0,
//                                                   height:
//                                                       50,
//                                                   //color: Colors.amber,
//                                                 ),
//                                                 Positioned(
//                                                   left: 55,
//                                                   bottom:
//                                                   16.0,
//                                                   //right: 2,
//                                                   child: IconButton(
//                                                       onPressed: () {
//                                                         // CustomSnackbar.snackbar(
//                                                         //     context, 'Item deleted');
//                                                         // DBHelper()
//                                                         //     .deleteCartItem(fs[index].id!);
//                                                         //     await view.getCartModel();
//                                                         showDialog(
//                                                             context: context,
//                                                             builder:
//                                                                 (context) =>
//                                                                     AlertDialog(
//                                                                       title: Text(
//                                                                           'Delete',
//                                                                           textScaleFactor:
//                                                                               1),
//                                                                       content: Text(
//                                                                           'Are you sure delete',
//                                                                           textScaleFactor:
//                                                                               1),
//                                                                       actions: <
//                                                                           Widget>[
//                                                                         TextButton(
//                                                                             child: Text(
//                                                                                 'Cancle'),
//                                                                             onPressed:
//                                                                                 () {
//                                                                               Navigator.of(context)
//                                                                                   .pop(false);
//                                                                             }),
//                                                                         TextButton(
//                                                                             child: Text(
//                                                                                 'Delete'),
//                                                                             onPressed:
//                                                                                 () async {
//                                                                               Navigator.of(context)
//                                                                                   .pop(true);
//                                                                               CustomSnackbar.snackbar(
//                                                                                   context,
//                                                                                   'Item deleted');
//                                                                                deleteItem(i, im.name);
//                                                                               // DBHelper()
//                                                                               //     .deleteCartItem(fs[index].id!);
//                                                                               // await view
//                                                                               //     .getCartModel();
//                                                                             }),
//                                                                       ],
//                                                                     ));
//                                                       },
//                                                       icon: Icon(
//                                                         Icons
//                                                             .delete_outline_outlined,
//                                                         color: Colors.grey,
//                                                       )),
//                                                 )
//                                               ],
//                                             ),
//                                           ],
//                                         ),
//                                       ],
//                                     ),
//                                     Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Container(
//                                           width: 90.0,
//                                           //color: Colors.amber,
//                                           child: Text(
//                                             '৳${im.salePrice}',
//                                             maxLines: 1,
//                                             overflow: TextOverflow.ellipsis,
//                                             style: TextStyle(
//                                                 color: tabSelectColor,
//                                                 fontWeight: FontWeight.bold,
//                                                 fontSize: 16),
//                                           ),
//                                         ),
//                                         Container(
//                                           //color: Colors.amber,
//                                           child: Row(
//                                             children: [
//                                               Container(
//                                                 height: 30,
//                                                 width: 42.0,
//                                                 // child: IconButton(
//                                                 //   onPressed: () {
//                                                 //     if (fs[index].quantity! > 1) {
//                                                 //       CartModel cq = CartModel(
//                                                 //           id: fs[index].id,
//                                                 //           name: fs[index].name,
//                                                 //           salePrice:
//                                                 //               fs[index].salePrice,
//                                                 //           regularPrice: fs[index]
//                                                 //               .regularPrice,
//                                                 //           quantity: fs[index]
//                                                 //                   .quantity! -
//                                                 //               1,
//                                                 //           img: fs[index].img);
//                                                 //       DBHelper().update(cq);
//                                                 //     }
//                                                 //   },
//                                                 //   icon: Icon(
//                                                 //     Icons.remove_outlined,
//                                                 //     color: Colors.grey,
//                                                 //   ),
//                                                 // ),
//                                                 child: GestureDetector(
//                                                     onTap: () {
//                                                    if (im.quantity! > 1) {
//                                                 im.quantity = im.quantity! - 1;
//                                               }
//                                               setState(() {});
//                                               removeItemQuantity(im.price);
//                                                     },
//                                                     child: Icon(
//                                                       Icons.remove_outlined,
//                                                       color: Colors.grey,
//                                                     )),
//                                                 decoration: BoxDecoration(
//                                                   //color: Colors.redAccent,
                      
//                                                   border: Border.all(
//                                                     color: Colors.grey,
//                                                   ),
//                                                   borderRadius: BorderRadius.only(
//                                                       topLeft: Radius.circular(5),
//                                                       bottomLeft:
//                                                           Radius.circular(5)),
//                                                 ),
//                                               ),
//                                               Container(
//                                                 height: 30,
//                                                 width: 42.0,
//                                                 child: Center(
//                                                   child: Text(
//                                                     '${im.quantity}',
//                                                     style: TextStyle(
//                                                       color: Colors.grey,
//                                                     ),
//                                                   ),
//                                                 ),
//                                                 decoration: BoxDecoration(
//                                                   // color: borderColor,
//                                                   border: Border.all(
//                                                     color: Colors.grey,
//                                                   ),
//                                                   //borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5)),
//                                                 ),
//                                               ),
//                                               Container(
//                                                 height: 30,
//                                                 width: 42.0,
//                                                 // child: Center(
//                                                 //   child: IconButton(
//                                                 //     icon: Icon(
//                                                 //       Icons.add,
//                                                 //       color: Colors.grey,
//                                                 //     ),
//                                                 //     onPressed: () {
//                                                 //       CartModel cq = CartModel(
//                                                 //           id: fs[index].id,
//                                                 //           name: fs[index].name,
//                                                 //           salePrice:
//                                                 //               fs[index].salePrice,
//                                                 //           regularPrice: fs[index]
//                                                 //               .regularPrice,
//                                                 //           quantity:
//                                                 //               fs[index].quantity! +
//                                                 //                   1,
//                                                 //           img: fs[index].img);
//                                                 //       DBHelper().update(cq);
//                                                 //     },
//                                                 //   ),
//                                                 // ),
//                                                 child: GestureDetector(
//                                                     onTap: () {
//                                                       im.quantity = im.quantity! + 1;
//                                               setState(() {});
//                                               addItemQuantity(im.price, im.name);
//                                                     },
//                                                     child: Icon(
//                                                       Icons.add,
//                                                       color: Colors.grey,
//                                                     )),
//                                                 decoration: BoxDecoration(
//                                                   //color: Colors.redAccent,
//                                                   border: Border.all(
//                                                     color: Colors.grey,
//                                                   ),
//                                                   borderRadius: BorderRadius.only(
//                                                       topRight: Radius.circular(5),
//                                                       bottomRight:
//                                                           Radius.circular(5)),
//                                                 ),
//                                               ),
//                                             ],
//                                           ),
//                                         )
//                                       ],
//                                     ),
//                                   ]),
//                             ]),
//                       )));
                 
                      
//                           });
// }