import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/review_status_model.dart';
import 'package:mart_up_admin_app/providers/review_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/dashboard.dart';
import 'package:mart_up_admin_app/screens/product_details.dart';

import 'package:mart_up_admin_app/services/review_service.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/loder_review_screen.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ReviewScreen extends StatefulWidget {
  const ReviewScreen({Key? key}) : super(key: key);

  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  late ReviewProvider reviewProvider;
  bool _isloading = true;
 
  TextEditingController _searchKeyController = TextEditingController();
 List<String> statusList = ['approved','hold', 'spam', 'unspam','trash','untrash'];
String? selectedStatus;
  int _page = 1;
  int _limit = 5;
  int counter=0;
  String? searchKey;
   ReviewStatusModel sts=ReviewStatusModel();
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
        String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    reviewProvider = Provider.of<ReviewProvider>(context, listen: false);
    getReview();
        focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }
loader(){
  if(counter >0){
    _isloading=true;
  }
}
  getReview() async {
    await reviewProvider.getReview(searchKey,_page,_limit,selectedStatus
       );

    setState(() {
      _isloading = false;
  
    });
  }

  void _onLoading() async {
    _page++;

    setState(() {});

    var list = await ReviewService().fetchRivew(
          searchKey: _searchKeyController.text,page: _page,limit: _limit);
    if (list != null) {
      reviewProvider.reviewGetListProvider!.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchReview() async {
    reviewProvider.reviewGetListProvider!.clear();
    setState(() {
      _isloading = true;
    
    });
    var list = await ReviewService().fetchRivew(
        limit: _limit, page: _page, searchKey: _searchKeyController.text);

    if (list != null) {
      reviewProvider.reviewGetListProvider!.addAll(list);
    }
    setState(() {
      _isloading = false;
      
       reviewProvider.searchKey=null;
    });
  }

  @override
  Widget build(BuildContext context) {
      SizeConfig().init(context);
    return Scaffold(
     appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon: 
            hintText: hintText,
            hintStyle: TextStyle(
                color: Colors.black54,),
          ),
        ),
        actions: [
          IconButton(
                  onPressed: () {
               searchReview();
                    
                  },
                  icon: Icon(Icons.search_outlined,size: 28,),
                  color: tabSelectColor,
                ),
                          IconButton(
                  onPressed: () {
                     showDialog(
                                                             context: context,
                                                              builder:
                                                                  (contex) {
                                                                 
                                                              
                                                                  
                                                                     return StatefulBuilder(
                                                                        builder: (context, setState) {
                                                                        return AlertDialog(
                                                                          // title:
                                                                          //     Text(
                                                                          //   'DELETE',
                                                                          //   textScaleFactor:
                                                                          //       1,
                                                                          // ),
                                                                          //titlePadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                                                                          //contentPadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                                                                          
                                                                          shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(10),
                                                                          ),
                                                                          title: 
                                                                              Column(
                                                                                children: [
                                                                                  Row(
                                                                                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Text("Filter",style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w700),),
                                                                                      GestureDetector(
                                                                                        onTap: (){
                                                                                          Navigator.of(context)
                                                                                                 .pop(true);
                                                                                        },
                                                                                        child: Card(
                                                                                         
                                                                                           shape: RoundedRectangleBorder(
                                                                                           borderRadius: BorderRadius.circular(50),
                                                                                            ),
                                                                                            child: Center(child: Icon(Icons.close,color: Colors.redAccent,size: 20,)),
                                                                                        ),
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                  Divider(),
                                                                                ],
                                                                              ),
                                                                          
                                                                           
                                                                          content: 
                                                                                  
                                                                              Container(
                                                                                height: SizeConfig.blockSizeVertical! * 18,
                                                                                child: Column(
                                                                                  children: [
                                                                                    // Text(
                                                                                    //     'Do you want to delete ?',
                                                                                    //     textScaleFactor:
                                                                                    //         1),
                                                                      
                                                                                    
                                                                                     SizedBox(height: 10,),
                                                                                     DropdownSearch<String>(
                                                                                        //key: _productKey,
                                                                                        //itemAsString:  color.toString(),'
                                                                                        maxHeight: SizeConfig.blockSizeVertical! * 40,
                                                                                        items: statusList,
                                                                                        // maxHeight: deviceHeight * .80,
                                                                                        //onFind: (String? filter) => color!,
                                                                                        selectedItem: selectedStatus,
                                                                                         showClearButton: true,
                                                                                         clearButton: IconButton(onPressed: (){
                                                                                           setState(() {
                                                                                            selectedStatus=null;
                                                                                            print("tap");
                                                                                            print(selectedStatus);
                                                                                                });
                                                                                         }, icon: Icon(Icons.close_outlined),
                                                                                         ),
                                                                                        label: "Status",
                                                                                        //selectedItems: selectedColor,
                                                                                        onChanged: (data) {
                                                                                          // print(data.toJson()),
                                                                                          // selectedColor.clear();
                                                                                          setState(() {
                                                                                            //selectedProduct = data;
                                                                                            
                                                                                            selectedStatus = data;
                                                                                            //orderProvider.status=selectedStatus;
                                                                                            // viewModelProvider.salesPeriod=selectedPeriod;
                                                                                            
                                                                                          
                                                                                            //getSalesReports(viewModelProvider.salesPeriod);
                                                                                            //print(viewModelProvider.salesPeriod);
                                                                                          });
                                                                                        },
                                                                                        showSearchBox: true,
                                                                                      ),
                                                                      
                                                                                            SizedBox(height: 20,),
                                                                                            Container(
                                                                                              height: 50,
                                                                                              width: double.infinity,
                                                                                              child: RaisedButton(
                                                                                                color: themeColor,
                                                                                                onPressed: (){
                                                                                                    Navigator.of(context)
                                                                                                 .pop(true);
                                                                                                 counter++;
                                                                                                
                                                                         setState(
                                                                                () {
                                                                              
                                                                            });
                                                                            print("tapeed");
                                                                            loader();
                                                                              getReview();
                                                                                           //getOrder( orderProvider.page, orderProvider.limit, orderProvider.status);    

                                                                                                
                                                                                                },
                                                                                                child: Text("Apply",style: TextStyle(color: Colors.white),),
                                                                                              ),
                                                                                            )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                          
                                                                          
                                                                        );
                                                                        }
                                                                      );
                                                                     
                                                                  });
                   
                    
                  },
                  icon: Icon(Icons.filter_alt),
                  color: tabSelectColor,
                ),
               
        ],
      ),
      drawer: AppDrawer(),
      body: SafeArea(
        child: _isloading 
            ? LoderReview()
            : _isloading == false &&
                    reviewProvider.reviewGetListProvider!.isEmpty
                ? EmptyScreen()
                : SmartRefresher(
                    controller: refreshController,
                    // scrollDirection: Axis.vertical,
                    // physics: BouncingScrollPhysics(),
                    enablePullUp: true,
                    onLoading: _onLoading,
                    onRefresh: _onRefresh,
                    footer: ClassicFooter(
                      loadStyle: LoadStyle.ShowWhenLoading,
                    ),
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      //scrollDirection: Axis.vertical,
                      itemCount:
                          reviewProvider.reviewGetListProvider!.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            showDialog(
                                                             context: context,
                                                              builder:
                                                                  (contex) {
                                                                 
                                                              
                                                                  
                                                                     return StatefulBuilder(
                                                                        builder: (context, setState) {
                                                                        return AlertDialog(
                                                                          // title:
                                                                          //     Text(
                                                                          //   'DELETE',
                                                                          //   textScaleFactor:
                                                                          //       1,
                                                                          // ),
                                                                          //titlePadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                                                                          //contentPadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                                                                          
                                                                          shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(10),
                                                                          ),
                                                                          title: 
                                                                              Column(
                                                                                children: [
                                                                                  Row(
                                                                                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Text("Filter",style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w700),),
                                                                                      GestureDetector(
                                                                                        onTap: (){
                                                                                          Navigator.of(context)
                                                                                                 .pop(true);
                                                                                        },
                                                                                        child: Card(
                                                                                         
                                                                                           shape: RoundedRectangleBorder(
                                                                                           borderRadius: BorderRadius.circular(50),
                                                                                            ),
                                                                                            child: Center(child: Icon(Icons.close,color: Colors.redAccent,size: 20,)),
                                                                                        ),
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                  Divider(),
                                                                                ],
                                                                              ),
                                                                          
                                                                           
                                                                          content: 
                                                                                  
                                                                              Container(
                                                                                height: SizeConfig.blockSizeVertical! * 25,
                                                                                child: Column(
                                                                                  children: [
                                                                                    Container(
                                                                                      height: 50,
                                                                                      width: double.infinity,
                                                                                      child: RaisedButton(
                                                                                        color: themeColor,
                                                                                        child: Text("Product Info",style: TextStyle(color: Colors.white),),
                                                                                        onPressed: (){
                                                                                           Navigator.of(context)
                                                                                                 .pop(true);

                                                                                          Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ProductDetailPage(id: reviewProvider.reviewGetListProvider![index].productId,)));
                                                                                      }
                                                                                      ),
                                                                                    ),
                                                                      
                                                                                    
                                                                                     SizedBox(height: 10,),
                                                                                     DropdownSearch<String>(
                                                                                        //key: _productKey,
                                                                                        //itemAsString:  color.toString(),'
                                                                                        maxHeight: SizeConfig.blockSizeVertical! * 40,
                                                                                        items: statusList,
                                                                                        // maxHeight: deviceHeight * .80,
                                                                                        //onFind: (String? filter) => color!,
                                                                                        selectedItem: selectedStatus,
                                                                                         showClearButton: true,
                                                                                         clearButton: IconButton(onPressed: (){
                                                                                           setState(() {
                                                                                            selectedStatus=null;
                                                                                            print("tap");
                                                                                            print(selectedStatus);
                                                                                                });
                                                                                         }, icon: Icon(Icons.close_outlined),
                                                                                         ),
                                                                                        label: "Status",
                                                                                        //selectedItems: selectedColor,
                                                                                        onChanged: (data) {
                                                                                          // print(data.toJson()),
                                                                                          // selectedColor.clear();
                                                                                          setState(() {
                                                                                            //selectedProduct = data;
                                                                                            
                                                                                            selectedStatus = data;
                                                                                           
                                                                                            sts.status=selectedStatus;
                                                                                            //orderProvider.status=selectedStatus;
                                                                                            // viewModelProvider.salesPeriod=selectedPeriod;
                                                                                            
                                                                                          
                                                                                            //getSalesReports(viewModelProvider.salesPeriod);
                                                                                            //print(viewModelProvider.salesPeriod);
                                                                                          });
                                                                                        },
                                                                                        showSearchBox: true,
                                                                                      ),
                                                                      
                                                                                            SizedBox(height: 20,),
                                                                                            Container(
                                                                                              height: 50,
                                                                                              width: double.infinity,
                                                                                              child: RaisedButton(
                                                                                                color: themeColor,
                                                                                                onPressed: ()async{
                                                                                                    Navigator.of(context)
                                                                                                 .pop(true);
                                                                                                                                                                     setState(
                                                                                () {
                                                                              _isloading=true; 
                                                                            });
                                                                            print("tapeed");
                                                                             Response res=await ReviewService().reviewStatusUpdate(sts,reviewProvider.reviewGetListProvider![index].id);
                                                                             if(res.statusCode ==200 || res.statusCode ==201){
                                                                               EasyLoading.showSuccess("Success!");
                                                                               reviewProvider.status=selectedStatus;
                                                                               getReview();
                                                                             }
                                                                             else{
                                                                               _isloading=false;
                                                                               EasyLoading.showError("Failed!");

                                                                             }
                                                                                           //getOrder( orderProvider.page, orderProvider.limit, orderProvider.status);    

                                                                                                
                                                                                                },
                                                                                                child: Text("Apply",style: TextStyle(color: Colors.white),),
                                                                                              ),
                                                                                            )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                          
                                                                          
                                                                        );
                                                                        }
                                                                      );
                                                                     
                                                                  });
                            
                          },
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                   
                                    children: [
                                     Row(
                                       children: [
                                          CircleAvatar(
                                        backgroundColor: Colors.pinkAccent,
                                        backgroundImage: AssetImage(
                                            'assets/images/avater.jpeg'),
                                        radius: 24,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '${reviewProvider.reviewGetListProvider![index].reviewer}',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                          ),
                                          RatingBar.builder(
                                            itemSize: 16,
                                            initialRating: reviewProvider
                                                        .reviewGetListProvider![
                                                            index]
                                                        .rating ==
                                                    null
                                                ? 1.0
                                                : reviewProvider
                                                    .reviewGetListProvider![
                                                        index]
                                                    .rating!
                                                    .toDouble(),
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: false,
                                            itemCount: 5,
                                            itemPadding: EdgeInsets.symmetric(
                                                horizontal: 2.0),
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            ignoreGestures: true,
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                              // widget.reviewModel!.rating;
                                            },
                                          ),
                                        ],
                                      ),
                                       ],
                                     ),
                                      
                                       Card(
                                         elevation: 0.1,
                                         color: Colors.grey[100],
                                         child: Padding(
                                           padding: const EdgeInsets.all(4.0),
                                           child: Text(
                                                '${reviewProvider.reviewGetListProvider![index].status}',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    
                                                    fontSize: 16),
                                              ),
                                         ),
                                       ),
                                      
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  HtmlWidget(
                                      '${reviewProvider.reviewGetListProvider![index].review}',
                                      textStyle: TextStyle(
                                          color: Colors.grey, fontSize: 14)),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    DateFormat('dd MMM yyyy').format(DateTime.parse(
                                        '${reviewProvider.reviewGetListProvider![index].dateCreated}')),
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
      ),
    );
  }
}
