import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/links.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/providers/order_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/create_order.dart';
import 'package:mart_up_admin_app/screens/order_details_screen.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/services/order_service.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/empty_screen.dart';
import 'package:mart_up_admin_app/widgets/loader_order_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OrderScreen extends StatefulWidget {
  String? getStatus;
  OrderScreen({this.getStatus});

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  late OrderProvider orderProvider;
  bool _isloading = true;
  TextEditingController _searchKeyController = TextEditingController();

  int _page = 1;
  int _limit = 5;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();
  bool isOrder = true;
  List<String> statusList = [
    'pending',
    'processing',
    'completed',
    'cancelled',
    'refunded',
    'on-hold',
    'failed'
  ];
  String? selectedStatus;

  @override
  void initState() {
    super.initState();
    orderProvider = Provider.of<OrderProvider>(context, listen: false);
    getOrder(_page, _limit, selectedStatus);

    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }

  getOrder(_page, _limit, selectedStatus) async {
    orderProvider.page = _page;
    orderProvider.limit = _limit;
    if (widget.getStatus != null) {
      orderProvider.status = widget.getStatus;
    } else {
      orderProvider.status = selectedStatus;
    }

    await orderProvider.getOrder(orderProvider.page, orderProvider.limit,
        status: orderProvider.status);

    setState(() {
      _isloading = false;
      selectedStatus = null;
    });
  }

  void _onLoading() async {
    _page++;

    setState(() {});

    var list = await OrderService().fetchOrder(
        orderProvider.searchKey, _page, _limit,
        status: orderProvider.status);
    if (list != null) {
      orderProvider.orderGetListProvider!.addAll(list);
      //print(viewModelProvider.productListProvider!.length);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchOrder() async {
    int _page = 1;
    int _limit = 5;
    orderProvider.orderGetListProvider!.clear();
    setState(() {
      _isloading = true;
      //viewModelProvider.brandListProvider!.isEmpty;
    });
    var list = await OrderService().fetchOrder(
        orderProvider.searchKey = _searchKeyController.text,
        orderProvider.page = _page,
        orderProvider.limit = _limit);
    //page = 1;
    if (list != null) {
      orderProvider.orderGetListProvider!.addAll(list);
    }
    setState(() {
      //searchKey = _searchKeyController.text;
      _isloading = false;
      orderProvider.searchKey = null;
      //viewModelProvider.brandListProvider!.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon:
            hintText: hintText,
            hintStyle: TextStyle(
              color: Colors.black54,
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              searchOrder();
            },
            icon: Icon(
              Icons.search_outlined,
              size: 28,
            ),
            color: tabSelectColor,
          ),
          IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (contex) {
                    return StatefulBuilder(builder: (context, setState) {
                      return AlertDialog(
                        // title:
                        //     Text(
                        //   'DELETE',
                        //   textScaleFactor:
                        //       1,
                        // ),
                        //titlePadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),
                        //contentPadding: EdgeInsets.symmetric(horizontal: 24,vertical: 13),

                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        title: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Filter",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop(true);
                                  },
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Center(
                                        child: Icon(
                                      Icons.close,
                                      color: Colors.redAccent,
                                      size: 20,
                                    )),
                                  ),
                                )
                              ],
                            ),
                            Divider(),
                          ],
                        ),

                        content: Container(
                          height: SizeConfig.blockSizeVertical! * 18,
                          child: Column(
                            children: [
                              // Text(
                              //     'Do you want to delete ?',
                              //     textScaleFactor:
                              //         1),

                              SizedBox(
                                height: 10,
                              ),
                              DropdownSearch<String>(
                                //key: _productKey,
                                //itemAsString:  color.toString(),'
                                maxHeight: SizeConfig.blockSizeVertical! * 40,
                                items: statusList,
                                // maxHeight: deviceHeight * .80,
                                //onFind: (String? filter) => color!,
                                selectedItem: selectedStatus,
                                showClearButton: true,
                                clearButton: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      selectedStatus = null;
                                      print("tap");
                                      print(selectedStatus);
                                    });
                                  },
                                  icon: Icon(Icons.close_outlined),
                                ),
                                label: "Status",
                                //selectedItems: selectedColor,
                                onChanged: (data) {
                                  // print(data.toJson()),
                                  // selectedColor.clear();
                                  setState(() {
                                    //selectedProduct = data;

                                    selectedStatus = data;
                                    orderProvider.status = selectedStatus;
                                    orderProvider.page = _page;
                                    orderProvider.limit = _limit;
                                    // viewModelProvider.salesPeriod=selectedPeriod;

                                    //getSalesReports(viewModelProvider.salesPeriod);
                                    //print(viewModelProvider.salesPeriod);
                                  });
                                },
                                showSearchBox: true,
                              ),

                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                height: 50,
                                width: double.infinity,
                                child: RaisedButton(
                                  color: themeColor,
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                    setState(() {
                                      _isloading = true;
                                    });
                                    print("tapeed");
                                    getOrder(
                                        orderProvider.page,
                                        orderProvider.limit,
                                        orderProvider.status);
                                  },
                                  child: Text(
                                    "Apply",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),

                        // actions: <
                        //     Widget>[
                        //   TextButton(
                        //       child: Text('Cancel'),
                        //       onPressed: () {
                        //         Navigator.of(context).pop(true);
                        //       }),
                        //   TextButton(
                        //       child: Text('Delete'),
                        //       onPressed: () async {
                        //         //Navigator.of(context,rootNavigator: true).pop();
                        //         Navigator.of(context).pop(true);
                        //       }

                        //       ),
                        // ],
                      );
                    });
                  });
            },
            icon: Icon(Icons.filter_alt),
            color: tabSelectColor,
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        child: _isloading
            ? LoaderOrderScreen()
            : _isloading == false && orderProvider.orderGetListProvider!.isEmpty
                ? EmptyScreen()
                : SmartRefresher(
                    controller: refreshController,
                    // scrollDirection: Axis.vertical,
                    // physics: BouncingScrollPhysics(),
                    enablePullUp: true,
                    onLoading: _onLoading,
                    onRefresh: _onRefresh,
                    footer: ClassicFooter(
                      loadStyle: LoadStyle.ShowWhenLoading,
                    ),
                    child: ListView.builder(
                        itemCount: orderProvider.orderGetListProvider!.length,
                        itemBuilder: (BuildContext context, index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                orderProvider.orderId = orderProvider
                                    .orderGetListProvider![index].id;
                              });

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          OrderDetailScreen()));
                            },
                            child: Card(
                              elevation: 1,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Column(
                                children: [
                                  // Container(
                                  //     height: 40,
                                  //     color: Colors.redAccent[200],
                                  //   ),
                                  ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10.0),
                                        topLeft: Radius.circular(10)),
                                    child: Container(
                                      color: Color(0xff69C7F6),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 12.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              '#${orderProvider.orderGetListProvider![index].id.toString()}',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                  fontFamily: fontFamily),
                                            ),
                                            Text(
                                              orderProvider
                                                  .orderGetListProvider![index]
                                                  .status
                                                  .toString(),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 16,
                                                  fontFamily: fontFamily),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0, vertical: 8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Name: ${orderProvider.orderGetListProvider![index].shipping!.lastName.toString()}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16,
                                              color: Colors.black87,
                                              fontFamily: fontFamily),
                                        ),
                                        Text(
                                          DateFormat('dd MMM yyyy').format(
                                              DateTime.parse(orderProvider
                                                  .orderGetListProvider![index]
                                                  .dateCreated
                                                  .toString())),
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16,
                                              color: Colors.grey,
                                              fontFamily: fontFamily),
                                        ),
                                      ],
                                    ),
                                  ),

                                  SizedBox(
                                    height: 10,
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Address: ${orderProvider.orderGetListProvider![index].shipping!.address2.toString()}',
                                          maxLines: 1,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: fontFamily),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Divider(
                                    height: 7,
                                    color: Colors.grey,
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0, vertical: 8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          'Price: ${orderProvider.orderGetListProvider![index].total.toString()}',
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 18,
                                              color: Color(0xff69C7F6),
                                              fontFamily: fontFamily),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductScreen(
                        isOrder: isOrder,
                      )));
        },
        backgroundColor: themeColor,
        child: const Icon(
          Icons.add_outlined,
          size: 50,
        ),
      ),
    );
  }
}
