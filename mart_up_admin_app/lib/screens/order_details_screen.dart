import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/models/requests/order_status_model.dart';
import 'package:mart_up_admin_app/models/responses/order_details_model.dart';
import 'package:mart_up_admin_app/providers/order_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/order_screen.dart';
import 'package:mart_up_admin_app/services/order_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/loader_order_details_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/roundedButton.dart';
import 'package:provider/provider.dart';

class OrderDetailScreen extends StatefulWidget {
  OrderDetailScreen();

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  OrderStatusModel? orderStatusModel;
  late OrderProvider orderProvider;
  bool _isLoading = false;
  List listItem = [
    'pending',
    'processing',
    'completed',
    'cancelled',
    'refunded',
  ];
  String? valueChoose;

  @override
  void initState() {
    super.initState();
    orderProvider = Provider.of<OrderProvider>(context, listen: false);

    getOrder();
    getOrderDetails();
  }

  getOrder() async {
    await orderProvider.getOrder(
        orderProvider.page, orderProvider.limit);

    setState(() {
      //_isloading = false;
    });
  }

  getOrderDetails() async {
    await orderProvider.getOrderDetail();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
      SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => OrderScreen()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Order details",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
          child: orderProvider.fetchOrderDetailProvider != null
              ? Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Name: ${orderProvider.fetchOrderDetailProvider!.shipping!.lastName.toString()}',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                'Order Id: #${orderProvider.fetchOrderDetailProvider!.id}',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),

                          //

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Icon(
                                    Icons.call_outlined,
                                    color: Colors.black54,
                                    size: 19,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    '${orderProvider.fetchOrderDetailProvider!.shipping!.postcode.toString()}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              Text(
                                DateFormat('dd MMM yyyy').format(DateTime.parse(
                                    orderProvider
                                        .fetchOrderDetailProvider!.dateCreated
                                        .toString())),
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),

                          //
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.location_on_outlined,
                                color: Colors.black54,
                                size: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                '${orderProvider.fetchOrderDetailProvider!.shipping!.address2}',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),

                          ListView.builder(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                            itemCount: orderProvider
                                .fetchOrderDetailProvider!.lineItems!.length,
                            itemBuilder: (context, index) {
                              return Card(
                                elevation: 0.5,
                                child: Container(
                                  height: 90,
                                  child: Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${orderProvider.fetchOrderDetailProvider!.lineItems![index].name.toString()}',
                                              //maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Color(0xff223263),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16.0),
                                            ),
                                            Text(
                                              'Price: ${orderProvider.fetchOrderDetailProvider!.lineItems![index].subtotal.toString()}Tk',
                                              //maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: Colors.black,
                                                //fontWeight: FontWeight.bold,
                                                fontSize: 15.0,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Text(
                                          'x${orderProvider.fetchOrderDetailProvider!.lineItems![index].quantity.toString()}',
                                          //maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: Colors.grey.shade500,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 1.5,
                            width: double.infinity,
                            color: Colors.grey.shade300,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Total: ${orderProvider.fetchOrderDetailProvider!.total}',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            "Status",
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.black54,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 16, right: 16),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                border: Border.all(
                                  color: Colors.grey,
                                  width: 1,
                                )),
                            child: DropdownButton(
                                value: valueChoose,
                                hint: Text(
                                  '${orderProvider.fetchOrderDetailProvider!.status}',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                isExpanded: true,
                                underline: SizedBox(),
                                icon: Icon(
                                  Icons.expand_more,
                                  color: Colors.grey,
                                ),
                                items: listItem.map((valueItem) {
                                  return DropdownMenuItem(
                                    value: valueItem,
                                    child: Text(valueItem),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    valueChoose = newValue.toString();
                                    print(valueChoose);
                                  });
                                }),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : LoaderOrderDetailsScreen()),
      bottomNavigationBar: _isLoading
          ? BoxLoader()
          : Padding(
              padding: const EdgeInsets.all(12.0),
              child: RoundedButton(
                buttonText: "Update",
                func: () async {
                  setState(() {
                      _isLoading = true;
                    });
                  OrderStatusModel st = OrderStatusModel();
                  st.status = valueChoose;
                  Response res = await OrderService().orderStatusPost(
                      st, orderProvider.fetchOrderDetailProvider!.id);

                  if (res.statusCode == 200 || res.statusCode == 201) {
                    // CustomSnackbar.snackbar(
                    //     context, " Satus update succesfully");
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => OrderScreen()));
                        EasyLoading.showSuccess("Success!");
                  } else {
                    await CustomSnackbar.snackbar(context, "Satus not update ");
                    setState(() {
                      _isLoading = false;
                    });
                    EasyLoading.showError("Failed!");
                  }
                },
              ),
            ),
    );
  }
}
