import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/providers/coupon_provider.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/create_coupon.dart';
import 'package:mart_up_admin_app/services/coupon_service.dart';
import 'package:mart_up_admin_app/services/coupon_update_delete_service.dart';
import 'package:mart_up_admin_app/widgets/box_loader.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/drawer.dart';
import 'package:mart_up_admin_app/widgets/loader_coupon_screen.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CouponScreen extends StatefulWidget {
  const CouponScreen({Key? key}) : super(key: key);

  @override
  _CouponScreenState createState() => _CouponScreenState();
}

class _CouponScreenState extends State<CouponScreen> {
  late CouponProvider couponProvider;
  bool _isloading = true;
   bool _isloading2 = true;
  bool _isConnection = false;
  TextEditingController _searchKeyController = TextEditingController();
  int _page = 1;
  int _limit = 5;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    checkNetConnectivity();
    couponProvider = Provider.of<CouponProvider>(context, listen: false);

    getCoupon();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
  }

  getCoupon() async {
    couponProvider.page = _page;
    couponProvider.limit = _limit;

    await couponProvider.getCoupon(
        couponProvider.page, couponProvider.limit);

    setState(() {
      _isloading = false;
        _isloading2=false;
    });
  }

  void _onLoading() async {
    _page++;

    setState(() {});

    var list = await CouponService.fetchCoupon(
        couponProvider.searchKey, _page, _limit);
    if (list != null) {
      couponProvider.couponListProvider!.addAll(list);
      //print(viewModelProvider.productListProvider!.length);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  Future searchCoupon() async {
    // int _page = 1;
    // int _limit=5;
    couponProvider.couponListProvider!.clear();
    setState(() {
      _isloading = true;
      _isloading2=true;
      //viewModelProvider.brandListProvider!.isEmpty;
    });
    var list = await CouponService.fetchCoupon(
        couponProvider.searchKey = _searchKeyController.text,
        couponProvider.page = _page,
        couponProvider.limit = _limit);
    //page = 1;
    if (list != null) {
      couponProvider.couponListProvider!.addAll(list);
      setState(() {});
    }
    setState(() {
      //searchKey=_searchKeyController.text;
      _isloading = false;
       _isloading2 = false;
        couponProvider.searchKey =null;
      //viewModelProvider.brandListProvider!.isNotEmpty;
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    EasyLoading.init();
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: TextField(
          focusNode: focusNode,
          onChanged: (value) {
            //searchProduct();
          },
          controller: _searchKeyController,
          decoration: InputDecoration(
            //border: InputBorder.none,
            //suffixIcon: 
            hintText: hintText,
            hintStyle: TextStyle(
                color: Colors.black54,),
          ),
        ),
        actions: [
          IconButton(
                  onPressed: () {
                    searchCoupon();
                    
                  },
                  icon: Icon(Icons.search_outlined,size: 28,),
                  color: tabSelectColor,
                ),
        ],
      ),
      drawer: AppDrawer(),
      body: Builder(builder: (context) {
        return SafeArea(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          child: _isConnection
              ? Container(
                  child: _isloading && _isloading2
                      ? LoaderCouponScreen():_isloading2?Center(child: BoxLoader())
                      : SmartRefresher(
                          controller: refreshController,
                          // scrollDirection: Axis.vertical,
                          // physics: BouncingScrollPhysics(),
                          enablePullUp: true,
                          onLoading: _onLoading,
                          onRefresh: _onRefresh,
                          footer: ClassicFooter(
                            loadStyle: LoadStyle.ShowWhenLoading,
                          ),
                          child: ListView.builder(
                              itemCount:
                                  couponProvider.couponListProvider!.length,
                              itemBuilder: (BuildContext context, index) {
                                return Card(
                                  elevation: 1,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(12.0)),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    child: Container(
                                      height:
                                          SizeConfig.safeBlockVertical! * 19,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                  height: SizeConfig
                                                          .safeBlockVertical! *
                                                      18,
                                                  width: SizeConfig
                                                          .safeBlockHorizontal! *
                                                      24,
                                                  child: Image.asset(
                                                    'assets/images/coupons.png',
                                                    fit: BoxFit.contain,
                                                  )),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Container(
                                                width: SizeConfig.blockSizeHorizontal! *44 ,
                                                //color: Colors.amber,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      'Code: ${couponProvider.couponListProvider![index].code}',
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      'Amount: ${couponProvider.couponListProvider![index].amount!.toString()} ৳',
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 14,
                                                      ),
                                                    ),
                                                    Text(
                                                      couponProvider
                                                                  .couponListProvider![
                                                                      index]
                                                                  .dateExpires !=
                                                              null
                                                          ? 'Expiry date: ${DateFormat.yMd().format(DateTime.parse(couponProvider.couponListProvider![index].dateExpires!))}'
                                                          : "Expiry date: Not Selected",
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 14,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              SizedBox(
                                                width: SizeConfig
                                                        .blockSizeHorizontal! *
                                                    13,
                                              ),
                                              IconButton(
                                                  onPressed: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    CreateCoupon(
                                                                      singleCoupon:
                                                                          couponProvider
                                                                              .couponListProvider![index],
                                                                    )));
                                                  },
                                                  icon: Icon(Icons.edit)),

                                              //SizedBox(width: 15,),
                                              IconButton(
                                                  onPressed: () async {
                                                    showDialog(
                                                        context: context,
                                                        builder:
                                                            (context) =>
                                                                AlertDialog(
                                                                  title: Text(
                                                                    'DELETE',
                                                                    textScaleFactor:
                                                                        1,
                                                                  ),
                                                                  content: Text(
                                                                      'Do you want to delete ?',
                                                                      textScaleFactor:
                                                                          1),
                                                                  actions: <
                                                                      Widget>[
                                                                    TextButton(
                                                                        child: Text(
                                                                            'Cancel'),
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.of(context)
                                                                              .pop(true);
                                                                        }),
                                                                    TextButton(
                                                                        child: Text(
                                                                            'Delete'),
                                                                        onPressed:
                                                                            () async {
                                                                          Navigator.of(context)
                                                                              .pop(true);
                                                                          setState(
                                                                              () {
                                                                            _isloading2 =
                                                                                true;
                                                                          });
                                                                          Response?
                                                                              res =
                                                                              await UpdateDeleteCouponService().deleteCoupon(couponProvider.couponListProvider![index].id);
                                                                          if (res!.statusCode == 200 ||
                                                                              res.statusCode == 201) {
                                                                            setState(() {
                                                                              _isloading2 = false;
                                                                            });
                                                                            //CustomSnackbar.snackbar(context,'coupon deleted');
                                                                           EasyLoading.showSuccess('Success!');
                                                                            getCoupon();
                                                                            
                                                                             
                                                                          } else {
                                                                            setState(() {
                                                                              _isloading = false;
                                                                            });
                                                                            EasyLoading.showError('Failed!');
                                                                          }
                                                                        }),
                                                                  ],
                                                                ));
                                                  },
                                                  icon: Icon(Icons.delete)),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                )
              : NoInternet(
                  pressed: () async {
                    await checkNetConnectivity();
                    _isConnection == false
                        ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                        : checkNetConnectivity();
                    getCoupon();
                  },
                ),
        ));
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => CreateCoupon()));
        },
        backgroundColor: themeColor,
        child: const Icon(
          Icons.add_outlined,
          size: 50,
        ),
      ),
    );
  }
}
