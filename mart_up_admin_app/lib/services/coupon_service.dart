import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/responses/coupon_response_model.dart';


class CouponService {
  static var client = http.Client();

  static List<CouponResponseModel> parseCopun(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<CouponResponseModel> coupon =
        list.map((e) => CouponResponseModel.fromJson(e)).toList();
    return coupon;
  }

  static Future<List<CouponResponseModel>> fetchCoupon(String? searchKey,int page,int limit ) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "coupons?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +"&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}"
        );
    final couponresponse = await client.get(url);

    print(couponresponse);

    return compute(parseCopun, couponresponse.body);
  }

        static Future<String?> couponCount(
      ) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "coupons?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret );
    final reviewResponse = await client.get(url);



    var total=  reviewResponse.headers["x-wp-total"];
    return total;
  }


}
