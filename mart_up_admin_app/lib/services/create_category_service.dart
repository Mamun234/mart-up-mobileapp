import 'dart:convert';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/requests/category_create_model.dart';
import 'package:mart_up_admin_app/models/requests/create_coupon_model.dart';
import 'package:mart_up_admin_app/models/responses/upload_image_response_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/requests/category_create_model.dart'
    as image;

class CreateCategoryService {
  static var client = http.Client();

  // static CreateCouponModel parseCreateCoupon(String responseBody) {
  //   var data = jsonDecode(responseBody);
  //   CreateCouponModel log = CreateCouponModel.fromJson(data);

  //   return log;
  // }

  Future<Response?> createCategory(CategoryCreateModel createCategory,
      UploadImageResponseModel categoryImage, String token) async {
    //print(jsonEncode(createCoupon.toJson()));
// String token =
    // "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZGVtby5iYWJ1cmh1dC5jb20iLCJpYXQiOjE2NDQ0MDY3OTAsIm5iZiI6MTY0NDQwNjc5MCwiZXhwIjoxNjQ1MDExNTkwLCJkYXRhIjp7InVzZXIiOnsiaWQiOjEsImRldmljZSI6IiIsInBhc3MiOiI3YjE3YmYxYWZhMzY4MDIyNTA0MTk1N2ZkZWU1OTA5MSJ9fX0.kn8BiXj7kZ-tZGczDNzqPGJ0WS8kOFFZtP75cNnACL0";
    var apiUrl = Uri.parse(rootApi +
        "products/categories" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    // var src=createCategory.image!.src==null?"":"";
    // var image={src= createCategory.image!.src};
    //createCategory.image=image;

    Image scr = Image();
    scr.src = categoryImage.guid!.rendered;
    createCategory.image = scr;
    var data = jsonEncode(createCategory);
    //var apiUrl=Uri.parse("https://demo.baburhut.com/wp-json/wc/v3/coupons?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");

    try {
      final response = await client.post(
        apiUrl,
        body: jsonEncode(createCategory),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        },

        //headers: {"Content-Type": "application/x-www-form-urlencoded"},
      );
      final responseString = response.body;
      print(responseString);

      return response;
    } catch (e) {
      print(e);
    }
  }

  //update
  Future<Response?> updateCategory(
      {int? categoryId,
      CategoryCreateModel? createCategory,
      UploadImageResponseModel? categoryImage,
      String? token}) async {
    var apiUrl = Uri.parse(rootApi +
        "products/categories/${categoryId}" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
        
    if (categoryImage != null) {
      Image scr = Image();
      scr.src = categoryImage.guid!.rendered;
      createCategory!.image = scr;
      var data = jsonEncode(createCategory);
    }

    try {
      final response = await client.put(
        apiUrl,
        body: jsonEncode(createCategory),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        },
      );
      final responseString = response.body;
      print(responseString);

      return response;
    } catch (e) {
      print(e);
    }
  }

  //delete
  Future<Response?> deleteCategory({
    int? categoryId,
  }) async {
    var apiUrl = Uri.parse(rootApi +
        "products/categories/${categoryId}" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&force=true");

    try {
      final response = await client.delete(
        apiUrl,
      );
      final responseString = response.body;
      print(responseString);

      return response;
    } catch (e) {
      print(e);
    }
  }
}
