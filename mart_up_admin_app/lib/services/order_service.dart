import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/requests/create_order_model.dart';
import 'package:mart_up_admin_app/models/requests/order_status_model.dart';
import 'package:mart_up_admin_app/models/responses/order_get_model.dart';
import 'package:mart_up_admin_app/models/responses/order_details_model.dart';

class OrderService {
  static var client = http.Client();
  var url;
//   Future<Response> createOrder(OrderModel orderCreate,int id) async {
//     print('amar data:');
//     var orderBody;
//     // try {
//     //   if (orderCreate.couponLines == null) {
//     //     orderBody = jsonEncode(orderCreate.toJsonWithoutCoupon());
//     //   } else {
//     //     orderBody = jsonEncode(orderCreate.toJson());
//     //   }
//     //   // print();
//     // } catch (e) {
//     //   print(e);
//     // }
//     // print(jsonEncode(orderCreate.toJson()));
//     // var apiUrl = Uri.parse(
//     //     "https://demo.baburhut.com/wp-json/wc/v3/orders?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f&customer=2");
//     var apiUrl = Uri.parse(rootApi +
//         "orders" +
//         "?consumer_key=" +
//         consumerKey +
//         "&consumer_secret=" +
//         consumerSecret +
//         "&customer=" +
//         id.toString());
// //final response = await client.get(apiUrl);
//     // try {
//     final response = await client.post(
//       apiUrl,
//       body: orderBody,
//       headers: {"Content-Type": "application/json"},
//     );
//     final responseString = response.body;
//     // if(response.body.isNotEmpty){
//     //   _isLoading=true;
//     // }
//     print(responseString);

//     return response;
//   }

  //get order
  static List<OrderGetModel> parseOrder(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<OrderGetModel> orders =
        list.map((e) => OrderGetModel.fromJson(e)).toList();
    return orders;
  }

   Future<List<OrderGetModel>> fetchOrder(
      String? searchKey, int? page, int? limit,{String? status}) async {
        if(status == null){
           url = Uri.parse(rootApi +
        "orders?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}");
        }
        else{
          url = Uri.parse(rootApi +
        "orders?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}&status=${status == null ? '': status}");
        }
    //  url = Uri.parse(rootApi +
    //     "orders?consumer_key=" +
    //     consumerKey +
    //     "&consumer_secret=" +
    //     consumerSecret +
    //     "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}&status=${status == null ? '': status}");
    // "&customer=${customerId == null ? '' : customerId = 2}");
    final orderResponse = await client.get(url);

    print(orderResponse.body);

    return compute(parseOrder, orderResponse.body);
    //compute(parseOrder, oresponse.body);
  }
  //order details

  OrderDetailsModel parseOrderItem(String responseBody) {
    var order = jsonDecode(responseBody);

    OrderDetailsModel orderItem = OrderDetailsModel.fromJson(order);
    return orderItem;
  }

//order details
  Future<OrderDetailsModel> fetchOrderDetails(int? orderId) async {
    var url = Uri.parse(rootApi +
        "orders/${orderId}?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    // "&customer=${customerId == null ? '' : customerId = 2}");
    final orderItemResponse = await client.get(url);

    print(orderItemResponse);

    return compute(parseOrderItem, orderItemResponse.body);
    //compute(parseOrder, oresponse.body);
  }

  //status post
  // static OrderDetailsModel parseStatus(String responseBody) {
  //   var data = jsonDecode(responseBody);
  //   OrderDetailsModel status = OrderDetailsModel.fromJson(data);

  //   return status;
  // }

  Future<Response> orderStatusPost(OrderStatusModel osm, int? orderId) async {
    var apiUrl = Uri.parse(rootApi +
        "orders/${orderId}?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);

    final response = await client.put(
      apiUrl,
      body: jsonEncode(osm),
      headers: {"Content-Type": "application/json"},
    );
    final responseString = response.body;
    print(responseString);

    return response;
  }

  Future<Response> createOrder(OrderCreateModel orderCreate) async {
    print('amar data:');
    var orderBody;
    try {
      if (orderCreate.couponLines == null) {
        orderBody = jsonEncode(orderCreate.toJsonWithoutCoupon());
      } else {
        orderBody = jsonEncode(orderCreate.toJson());
      }
      // print();
    } catch (e) {
      print(e);
    }
    // print(jsonEncode(orderCreate.toJson()));
    // var apiUrl = Uri.parse(
    //     "https://demo.baburhut.com/wp-json/wc/v3/orders?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f&customer=2");
    var apiUrl = Uri.parse(rootApi +
        "orders" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
//final response = await client.get(apiUrl);
    // try {
    final response = await client.post(
      apiUrl,
      body: orderBody,
      headers: {"Content-Type": "application/json"},
    );
    final responseString = response.body;
    // if(response.body.isNotEmpty){
    //   _isLoading=true;
    // }
    print(responseString);

    return response;
  }
}
