import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/responses/total_coupon_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_customer_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_order_count_model.dart';


class TotalOrderService {
  //String status="approved";
  static var client = http.Client();

 
  static List<TotalOrderModel> parseOrderCount(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<TotalOrderModel> order =
        list.map((e) => TotalOrderModel.fromJson(e)).toList();
    return order;
  }

  static Future<List<TotalOrderModel>> fetchTotalOrder() async {
    var url = Uri.parse(rootApi +
        "reports/orders/totals?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);

    final response = await client.get(url);

    print(response.body);

    return compute(parseOrderCount, response.body);
  }
}
