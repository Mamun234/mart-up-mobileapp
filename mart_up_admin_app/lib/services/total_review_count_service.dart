import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/responses/total_coupon_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_customer_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_order_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_review_count_model.dart';


class TotalReviewService {
  //String status="approved";
  static var client = http.Client();

 
  static List<TotalReviewModel> parseReviewCount(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<TotalReviewModel> review =
        list.map((e) => TotalReviewModel.fromJson(e)).toList();
    return review;
  }

  static Future<List<TotalReviewModel>> fetchTotalReview() async {
    var url = Uri.parse(rootApi +
        "reports/reviews/totals?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);

    final response = await client.get(url);

    print(response.body);

    return compute(parseReviewCount, response.body);
  }
}
