import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';

class ProductService {
  static var client = http.Client();

  static List<ProductModel> parseProduct(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;
    List<ProductModel> products =
        list.map((model) => ProductModel.fromJson(model)).toList();
    return products;
  }

  static ProductModel parseProductById(String responseBody) {
    var data = jsonDecode(responseBody);
    ProductModel product = ProductModel.fromJson(data);

    return product;
  }

  static Future<List<ProductModel>> fetchProducts(
    int? page,
    int? limit, 
    String? searchKey,
    { int? categoryId,
    int? brandId,
    String? minPrice,
    String? maxPrice,
    
  }) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}&category=${categoryId == null ? '' : categoryId}&min_price=${minPrice == null ? ' ' : minPrice}&max_price=${maxPrice == null ? ' ' : maxPrice}&attribute=pa_brand&attribute_term=${brandId == null ? '' : brandId}");
    final response = await client.get(url);

    print(response.body);

    return compute(parseProduct, response.body);
  }

  static Future<ProductModel> fetchProductsById(int? id) async {
    var client = http.Client();

    // var url = Uri.parse(
    //     "https://demo.baburhut.com/wp-json/wc/v3/products/$id?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");

    var url = Uri.parse(rootApi +
        "products/" +
        id.toString() +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    final response = await client.get(url);

    print('product by id: ${response.body}');

    Future<ProductModel> product = compute(parseProductById, response.body);
    return product;
  }
    static Future<List<ProductModel>> fetchRelatedProducts(
       List<int>? relatedId) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&include=${relatedId}");
    final response = await client.get(url);

    print(response);

    return compute(parseProduct, response.body);

    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }


}
