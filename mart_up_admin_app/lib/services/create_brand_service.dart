import 'dart:convert';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/requests/create_brand_model.dart';

import 'package:mart_up_admin_app/models/requests/create_coupon_model.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';

class CreateBrandService {
  static var client = http.Client();

  // static CreateCouponModel parseCreateCoupon(String responseBody) {
  //   var data = jsonDecode(responseBody);
  //   CreateCouponModel log = CreateCouponModel.fromJson(data);

  //   return log;
  // }

  Future<Response?> createBrand(BrandCreateModel createBrand) async {
    //print(jsonEncode(createCoupon.toJson()));

    var apiUrl = Uri.parse(rootApi +
        "products/attributes/2/terms" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    //var apiUrl=Uri.parse("https://demo.baburhut.com/wp-json/wc/v3/coupons?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");
    try {
      final response = await client.post(
        apiUrl,
        body: jsonEncode(createBrand),
        headers: {"Content-Type": "application/json"},
        //headers: {"Content-Type": "application/x-www-form-urlencoded"},
      );
      final responseString = response.body;
      print(responseString);

      return response;
    } catch (e) {
      print(e);
    }
  }

    Future<Response?> updateBrand(BrandModel updateBrand,int? brandId,) async {
    //print(jsonEncode(createCoupon.toJson()));

    var apiUrl = Uri.parse(rootApi +
        "products/attributes/2/terms/${brandId}" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret );
    //var apiUrl=Uri.parse("https://demo.baburhut.com/wp-json/wc/v3/coupons?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");
    try {
      final response = await client.put(
        apiUrl,
        body: jsonEncode(updateBrand),
        headers: {"Content-Type": "application/json"},
        //headers: {"Content-Type": "application/x-www-form-urlencoded"},
      );
      final responseString = response.body;
      print(responseString);

      return response;
    } catch (e) {
      print(e);
    }
  }
  //delete
  Future<Response?> deleteBrand({
    int? brandId,
  }) async {
    var apiUrl = Uri.parse(rootApi +
        "products/attributes/2/terms/${brandId}" +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&force=true");

    try {
      final response = await client.delete(
        apiUrl,
      );
      final responseString = response.body;
      print(responseString);

      return response;
    } catch (e) {
      print(e);
    }
  }
}
