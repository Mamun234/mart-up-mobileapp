import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/responses/total_coupon_count_model.dart';


class TotalCouponService {
  //String status="approved";
  static var client = http.Client();

 
  static List<TotalCouponModel> parseCouponCount(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<TotalCouponModel> coupon =
        list.map((e) => TotalCouponModel.fromJson(e)).toList();
    return coupon;
  }

  static Future<List<TotalCouponModel>> fetchTotalCoupon() async {
    var url = Uri.parse(rootApi +
        "reports/coupons/totals?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);

    final salesResponse = await client.get(url);

    print(salesResponse.body);

    return compute(parseCouponCount, salesResponse.body);
  }
}
