import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/responses/customer_model.dart';


class CustomerService {
  static var client = http.Client();

  // static List<CustomerModel> parseUser(String responseBody) {
  //   //var data = jsonDecode(responseBody) as dynamic;
  //   var data= jsonDecode(responseBody);
  //   //CustomerModel user= CustomerModel.fromJson(data);
  //   List<CustomerModel> user =
  //       list.map((model) => CustomerModel.fromJson(model)).toList();
  //   return user;
  // }
 static List<CustomerModel> parseCustomer(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<CustomerModel> customers =
        list.map((e) => CustomerModel.fromJson(e)).toList();
    return customers;
  }

  static Future<List<CustomerModel>> fetchCustomer(String? searchKey,int? page,int? limit
     ) async {
    var client = http.Client();
    // var url = Uri.parse("https://demo.baburhut.com/wp-json/wc/v3/customers/${customerId == null ? '' : customerId}?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");
   var  url = Uri.parse(rootApi+ "customers/"+ "?consumer_key="+ consumerKey+ "&consumer_secret=" +
        consumerSecret+"&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}" );
    final response = await client.get(url);

    print(response);

    return compute(parseCustomer, response.body);
    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }

// static Future<List<OrderModel>> createOrder(var payment,List<dynamic> listItem,List<dynamic> shiping)async{

}
