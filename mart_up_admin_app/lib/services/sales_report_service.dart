import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/responses/sales_report_response_model.dart';

class SalesReportService {
  //String status="approved";
  static var client = http.Client();

 
  static List<ReportResponseModel> parseReport(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<ReportResponseModel> report =
        list.map((e) => ReportResponseModel.fromJson(e)).toList();
    return report;
  }

  static Future<List<ReportResponseModel>> fetchSaleReports({String? period,String? minDate,String? maxDate}) async {
    var url;
        if(period ==null && minDate !=null && maxDate !=null){
           url = Uri.parse(rootApi +
        "reports/sales?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret+"&date_min=$minDate&date_max=$maxDate");
        }
        else if(minDate ==null && maxDate ==null && period !=null){
   url = Uri.parse(rootApi +
        "reports/sales?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret+"&period=$period");
         
        }
        else{
            url = Uri.parse(rootApi +
        "reports/sales?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret+"&period=month");
        }

    final salesResponse = await client.get(url);

    print(salesResponse.body);

    return compute(parseReport, salesResponse.body);
  }
}
