import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/responses/total_product_model.dart';

class TotalProductService {
  //String status="approved";
  static var client = http.Client();

 
  static List<TotalProductModel> parseProductCount(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<TotalProductModel> product =
        list.map((e) => TotalProductModel.fromJson(e)).toList();
    return product;
  }

  static Future<List<TotalProductModel>> fetchTotalProduct() async {
    var url = Uri.parse(rootApi +
        "reports/products/totals?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret+"&period=month");

    final salesResponse = await client.get(url);

    print(salesResponse.body);

    return compute(parseProductCount, salesResponse.body);
  }
}
