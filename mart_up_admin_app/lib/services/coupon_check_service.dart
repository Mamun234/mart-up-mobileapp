import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/responses/coupon_check_model.dart';


class CouponCheckService {
  static var client = http.Client();

  static List<CouponCheckModel> parseCopun(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<CouponCheckModel> coupon =
        list.map((e) => CouponCheckModel.fromJson(e)).toList();
    return coupon;
  }

  static Future<List<CouponCheckModel>> fetchCoupon({String? couponCode}) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "coupons?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&code=${couponCode}");
    final couponresponse = await client.get(url);

    print(couponresponse);

    return compute(parseCopun, couponresponse.body);
  }
}
