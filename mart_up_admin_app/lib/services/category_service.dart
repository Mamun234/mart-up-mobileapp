import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/responses/category_header_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';

class CategoryService {
  static var client = http.Client();

  static List<CategoryModel> parseCategory(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;
   

    List<CategoryModel> categories =
        list.map((e) => CategoryModel.fromJson(e)).toList();
    return categories;
  }
  static CategoryHeaderModel? parseHeader(String headers){
    var total=jsonDecode(headers);
    CategoryHeaderModel tot=CategoryHeaderModel.fromJson(total);
    return tot;

  }

  static Future<List<CategoryModel>> fetchCategory(
      String? searchKey, page, limit) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products/categories?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}");
    final cresponse = await client.get(url);

    print(cresponse);

    return compute(parseCategory, cresponse.body);
    
  }
  static Future<String?> categoryCount(
     ) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products/categories?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret 
        );
    final cresponse = await client.get(url);

    print(cresponse);
    
    //compute(parseHeader,cresponse.headers);
    //  return compute(parseCategory, cresponse.body);
    var total=  cresponse.headers["x-wp-total"];
    return total;
    
  }
}
