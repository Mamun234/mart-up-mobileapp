import 'dart:convert';
import 'package:mart_up_admin_app/models/responses/admin_login_response_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DBHelper {
  //SharedPreferences initialize
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
//response decode
 static AdminLoginResponseModel parseProfileInfo(String data) {
    var resultdata = jsonDecode(data) as dynamic;
    AdminLoginResponseModel loginInfo =
        AdminLoginResponseModel.fromJson(resultdata);
    return loginInfo;
  }

//save login data
  Future<bool> saveLoginAdminData(String data) async {
    bool result = await prefs.then((value) => value.setString("admin", data));
    print(result);
    return result;
  }

//get login data
  Future<AdminLoginResponseModel?> fatchLoginAdminData() async {
    var result = await prefs.then((value) => value.getString("admin"));
    if (result != null && result != "") {
      var data = parseProfileInfo(result);
      return data;
    } else {
      return null;
    }
  }

//remove data
  removeLoginAdminData() {
    prefs.then((value) => value.remove("admin"));
  }

 
}
