import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/responses/total_coupon_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_customer_count_model.dart';


class TotalCustomerService {
  //String status="approved";
  static var client = http.Client();

 
  static List<TotalCustomerModel> parseCustomerCount(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<TotalCustomerModel> customer =
        list.map((e) => TotalCustomerModel.fromJson(e)).toList();
    return customer;
  }

  static Future<List<TotalCustomerModel>> fetchTotalCustomer() async {
    var url = Uri.parse(rootApi +
        "reports/customers/totals?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);

    final response = await client.get(url);

    print(response.body);

    return compute(parseCustomerCount, response.body);
  }
}
