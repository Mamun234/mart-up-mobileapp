import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/requests/review_status_model.dart';
import 'package:mart_up_admin_app/models/responses/review_model.dart';

class ReviewService {
  //String status="approved";
  static var client = http.Client();
  var url;

  //get reivew
  static List<ReviewModel> parseReview(String responseBody) {
     var list = jsonDecode(responseBody) as List<dynamic>;

    List<ReviewModel> reviews =
        list.map((e) => ReviewModel.fromJson(e)).toList();
    return reviews;
  }

 Future<List<ReviewModel>> fetchRivew(
    { String? searchKey,int? page, int? limit,String? status}
  ) async {
    if(status == null){
 url = Uri.parse(rootApi +
        "products/reviews?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret+
         "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}&status=approved");
    }
    else{
       url = Uri.parse(rootApi +
        "products/reviews?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret+
         "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}&status=${status==null?'':status}");
    }
    

    final reviewResponse = await client.get(url);

    print(reviewResponse.body);

    return compute(parseReview, reviewResponse.body);
  }



  static Future<List<ReviewModel>> fetchRivewbyProductId(
       int? productId) async {
    var url = Uri.parse(rootApi +
        "products/reviews?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&product=${productId}&status=approved");

    final reviewResponse = await client.get(url);

    print(reviewResponse);

    return compute(parseReview, reviewResponse.body);
    //compute(parseReview, oresponse.body);
  }
      static Future<String?> reviewCount(
      ) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products/reviews?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret );
    final reviewResponse = await client.get(url);



    var total=  reviewResponse.headers["x-wp-total"];
    return total;
  }
   Future<Response> reviewStatusUpdate(ReviewStatusModel osm, int? reviewId) async {
    var apiUrl = Uri.parse(rootApi +
        "products/reviews/${reviewId}?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);

    final response = await client.put(
      apiUrl,
      body: jsonEncode(osm),
      headers: {"Content-Type": "application/json"},
    );
    final responseString = response.body;
    print(responseString);

    return response;
  }

}
