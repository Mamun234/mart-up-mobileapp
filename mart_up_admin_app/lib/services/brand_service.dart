import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';

class BrandService {
  static var client = http.Client();

  static List<BrandModel> parseBrand(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<BrandModel> brands = list.map((e) => BrandModel.fromJson(e)).toList();
    return brands;
  }

  static Future<List<BrandModel>> fetchBrand(
       int? page, int? limit,{String? searchKey,}) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products/attributes/2/terms?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit == null ? '' : limit}&page=${page == null ? '' : page}&search=${searchKey == null ? ' ' : searchKey}");
    final brandresponse = await client.get(url);

    print(brandresponse.body);

    return compute(parseBrand, brandresponse.body);
  }
  
    static Future<String?> brandCount(
      ) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products/attributes/2/terms?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret );
    final brandresponse = await client.get(url);



    var total=  brandresponse.headers["x-wp-total"];
    return total;
  }
}
