import 'dart:convert';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';
import 'package:mart_up_admin_app/models/requests/admin_login_model.dart';
import 'package:mart_up_admin_app/models/responses/admin_login_response_model.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';

class AdminLoginService {
  static var client = http.Client();
  //int c=0;
 // String? adminInfo;

   AdminLoginResponseModel parseLogin(String responseBody) {
    String info=responseBody;
    var data = jsonDecode(responseBody);
    AdminLoginResponseModel log = AdminLoginResponseModel.fromJson(data);
    // if (log.data!.roles == 'administrator' ) {
    //      DBHelper().removeLoginAdminData();
    //     DBHelper().saveLoginAdminData(response.body);
    //   }
    // if(log.data!.roles!.contains("administrator")){

    // }
    // for(int i=0;i<log.data!.roles!.length;i++){
    //   if(log.data!.roles![i] == "administrator"){
    //   // DBHelper().removeLoginAdminData();
        
    //   //   DBHelper().saveLoginAdminData(info);
   
        
    //   }
    //   else{
    //     c++;
         
    //   }
      
    // }
    // print(c);
    
      //           if (c>0  ) {
      //         print(c);
      //    DBHelper().removeLoginAdminData();
      //   // DBHelper().saveLoginAdminData(adminInfo!);
      // }

    print(log.data!.email);
    return log;
  }

  Future<Response?> loginAdmin(AdminLoginModel login) async {
    print(jsonEncode(login.toJson()));
    

    var apiUrl = Uri.parse(rootUrl + loginUrl);

    try {
      final response = await client.post(
        apiUrl,
        body: login.toJson(),
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
      );
      final responseString = response.body;
      print(responseString);

      if (response.statusCode == 200  ) {
        //  DBHelper().removeLoginAdminData();
        // DBHelper().saveLoginAdminData(response.body);
        // adminInfo=response.body;
      }
  
  
      return response;
    } catch (e) {
      print(e);
    }
  }
  
}
