import 'dart:convert';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up_admin_app/environment/environment.dart';

import 'package:mart_up_admin_app/models/requests/create_coupon_model.dart';


class CreateCouponService {
  static var client = http.Client();

  // static CreateCouponModel parseCreateCoupon(String responseBody) {
  //   var data = jsonDecode(responseBody);
  //   CreateCouponModel log = CreateCouponModel.fromJson(data);

    
  //   return log;
  // }

  Future<Response?> createCoupon(CouponModel createCoupon) async {
    //print(jsonEncode(createCoupon.toJson()));

    var apiUrl = Uri.parse(rootApi+"coupons"+
    "?consumer_key="+ consumerKey +
        "&consumer_secret=" +
        consumerSecret
    );
  //var apiUrl=Uri.parse("https://demo.baburhut.com/wp-json/wc/v3/coupons?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");
    try {
      final response = await client.post(
        apiUrl,
         body: jsonEncode(createCoupon),
         headers: {"Content-Type": "application/json"},
        //headers: {"Content-Type": "application/x-www-form-urlencoded"},
      );
      final responseString = response.body;
      print(responseString);

      
      return response;
    } catch (e) {
      print(e);
    }
  }
  
}
