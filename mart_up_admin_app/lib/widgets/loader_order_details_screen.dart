import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderOrderDetailsScreen extends StatefulWidget {
  const LoaderOrderDetailsScreen({Key? key}) : super(key: key);

  @override
  _LoaderOrderDetailsScreenState createState() =>
      _LoaderOrderDetailsScreenState();
}

class _LoaderOrderDetailsScreenState extends State<LoaderOrderDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SkeletonLine(
                    style: SkeletonLineStyle(
                        height: 10,
                        width: 170,
                        alignment: Alignment.center,
                        borderRadius: BorderRadius.circular(5)),
                  ),
                  SkeletonLine(
                    style: SkeletonLineStyle(
                        height: 10,
                        width: 130,
                        alignment: Alignment.center,
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ],
              ),
              SizedBox(
                height: 7,
              ),

              //

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 130,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ],
                  ),
                  SkeletonLine(
                    style: SkeletonLineStyle(
                        height: 10,
                        width: 100,
                        alignment: Alignment.center,
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ],
              ),

              //
              SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  SkeletonLine(
                    style: SkeletonLineStyle(
                        height: 10,
                        width: 150,
                        alignment: Alignment.center,
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ],
              ),
              SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  SkeletonLine(
                    style: SkeletonLineStyle(
                        height: 10,
                        width: 100,
                        alignment: Alignment.center,
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),

              ListView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 0.5,
                    child: Container(
                      height: 90,
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 12,
                                      width: 150,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(7)),
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 12,
                                      width: 100,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(7)),
                                ),
                              ],
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 12,
                                  width: 30,
                                  alignment: Alignment.center,
                                  borderRadius: BorderRadius.circular(7)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 1.5,
                width: double.infinity,
                color: Colors.grey.shade300,
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SkeletonLine(
                    style: SkeletonLineStyle(
                        height: 15,
                        width: 100,
                        alignment: Alignment.center,
                        borderRadius: BorderRadius.circular(3)),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              SkeletonLine(
                style: SkeletonLineStyle(
                    height: 15,
                    width: 100,
                    borderRadius: BorderRadius.circular(3)),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 44,
                padding: EdgeInsets.only(left: 20, right: 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    border: Border.all(
                      color: Colors.grey,
                      width: 1,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 15,
                          width: 130,
                          borderRadius: BorderRadius.circular(7)),
                    ),
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 15,
                          width: 22,
                          borderRadius: BorderRadius.circular(7)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
