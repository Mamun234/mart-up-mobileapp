import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
//import 'package:mart_up/widgets/custom_snackbar.dart';

// ignore: must_be_immutable
class NoInternet extends StatelessWidget {
   VoidCallback pressed;
   NoInternet({required this.pressed});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Center(
      child: IconButton(
        onPressed: pressed,
        icon: Icon(Icons.refresh_outlined,color: Colors.grey,size: 50,),),
    );
  }
}
