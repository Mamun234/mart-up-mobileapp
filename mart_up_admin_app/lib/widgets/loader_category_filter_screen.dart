import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoadetFilterCat extends StatefulWidget {
  const LoadetFilterCat({Key? key}) : super(key: key);

  @override
  _LoadetFilterCatState createState() => _LoadetFilterCatState();
}

class _LoadetFilterCatState extends State<LoadetFilterCat> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisExtent: 85,
        ),
        itemCount: 6,
        itemBuilder: (BuildContext context, index) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.circular(10),
              ),
              child: SkeletonLine(
                style: SkeletonLineStyle(
                    height: 20,
                    width: 90,
                    alignment: Alignment.center,
                    borderRadius: BorderRadius.circular(6)),
              ),
            ),
          );
        });
  }
}
