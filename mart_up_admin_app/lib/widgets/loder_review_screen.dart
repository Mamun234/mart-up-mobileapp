import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoderReview extends StatefulWidget {
  const LoderReview({Key? key}) : super(key: key);

  @override
  _LoderReviewState createState() => _LoderReviewState();
}

class _LoderReviewState extends State<LoderReview> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, index) {
            return Card(
              elevation: 1,
              color: Colors.grey.shade100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0)),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: (Container(
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        height: 110,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 12, top: 12),
                              child: SkeletonAvatar(
                                style: SkeletonAvatarStyle(
                                    shape: BoxShape.circle,
                                    width: 90,
                                    height: 90),
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 60, left: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SkeletonLine(
                                    style: SkeletonLineStyle(
                                        height: 12,
                                        width: 100,
                                        // alignment: Alignment.center,
                                        borderRadius: BorderRadius.circular(6)),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  SkeletonLine(
                                    style: SkeletonLineStyle(
                                        height: 12,
                                        width: 160,
                                        // alignment: Alignment.center,
                                        borderRadius: BorderRadius.circular(6)),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      //email
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 8,
                                width: 300,
                                //alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),

                      //Number
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 8,
                                width: 300,
                                alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),

                      //Number
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 8,
                                width: 300,
                                alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),

                      //Number
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 8,
                                width: 300,
                                alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),

                      //Number
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 8,
                                width: 300,
                                alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),

                      //Number
                      // Row(
                      //   //mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.start,
                      //   children: [
                      //     SkeletonLine(
                      //       style: SkeletonLineStyle(
                      //           height: 10,
                      //           width: 90,
                      //           borderRadius: BorderRadius.circular(6)),
                      //     ),
                      //   ],
                      // ),
                    ],
                  ),
                )),
              ),
            );
          }),
    );
  }
}
