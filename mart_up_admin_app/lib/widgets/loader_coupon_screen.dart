import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:skeletons/skeletons.dart';

class LoaderCouponScreen extends StatefulWidget {
  const LoaderCouponScreen({Key? key}) : super(key: key);

  @override
  _LoaderCouponScreenState createState() => _LoaderCouponScreenState();
}

class _LoaderCouponScreenState extends State<LoaderCouponScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, index) {
            return Card(
              elevation: 1,
              color: Colors.grey.shade100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0)),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: SizeConfig.safeBlockVertical! * 19,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: SizeConfig.safeBlockVertical! * 16,
                            width: SizeConfig.safeBlockHorizontal! * 28,
                            child: SkeletonAvatar(style: SkeletonAvatarStyle()),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SkeletonLine(
                                style: SkeletonLineStyle(
                                    height: 15,
                                    width: 100,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SkeletonLine(
                                style: SkeletonLineStyle(
                                    height: 12,
                                    width: 130,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SkeletonLine(
                                style: SkeletonLineStyle(
                                    height: 12,
                                    width: 150,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                            ],
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SkeletonAvatar(
                              style: SkeletonAvatarStyle(
                            height: SizeConfig.safeBlockVertical! * 3,
                            width: SizeConfig.safeBlockHorizontal! * 5,
                          )),
                          SkeletonAvatar(
                              style: SkeletonAvatarStyle(
                            height: SizeConfig.safeBlockVertical! * 3,
                            width: SizeConfig.safeBlockHorizontal! * 5,
                          )),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
