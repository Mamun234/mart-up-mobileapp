import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:skeletons/skeletons.dart';

class LoaderBrandScreen extends StatefulWidget {
  const LoaderBrandScreen({Key? key}) : super(key: key);

  @override
  _LoaderBrandScreenState createState() => _LoaderBrandScreenState();
}

class _LoaderBrandScreenState extends State<LoaderBrandScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, index) {
            return Card(
              elevation: 1,
              color: Colors.grey.shade100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0)),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  height: SizeConfig.safeBlockVertical! * 15,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: SizeConfig.safeBlockVertical! * 16,
                            width: SizeConfig.safeBlockHorizontal! * 28,
                            child: SkeletonAvatar(style: SkeletonAvatarStyle()),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 20,
                                width: 110,
                                alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(3)),
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SkeletonAvatar(
                              style: SkeletonAvatarStyle(
                            height: SizeConfig.safeBlockVertical! * 3,
                            width: SizeConfig.safeBlockHorizontal! * 5,
                          )),
                          SizedBox(
                            height: 10,
                          ),
                          SkeletonAvatar(
                              style: SkeletonAvatarStyle(
                            height: SizeConfig.safeBlockVertical! * 3,
                            width: SizeConfig.safeBlockHorizontal! * 5,
                          )),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
