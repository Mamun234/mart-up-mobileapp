import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:mart_up_admin_app/common/colors.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:mart_up_admin_app/providers/view_model_provider.dart';
import 'package:mart_up_admin_app/screens/brand_screen.dart';
import 'package:mart_up_admin_app/screens/category_screen.dart';
import 'package:mart_up_admin_app/screens/coupon_screen.dart';
import 'package:mart_up_admin_app/screens/customer_screen.dart';
import 'package:mart_up_admin_app/screens/dashboard.dart';
import 'package:mart_up_admin_app/screens/loginScreen.dart';
import 'package:mart_up_admin_app/screens/order_screen.dart';
import 'package:mart_up_admin_app/screens/product_screen.dart';
import 'package:mart_up_admin_app/screens/sales_report_screen.dart';
import 'package:mart_up_admin_app/screens/splash_screen.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';
import 'package:mart_up_admin_app/widgets/custom_snackbar.dart';
import 'package:mart_up_admin_app/widgets/drawer_loader.dart';
import 'package:mart_up_admin_app/widgets/loading_indicator.dart';
import 'package:mart_up_admin_app/widgets/no_internet.dart';
import 'package:provider/provider.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  late ViewModelProvider viewModelProvider;
  //ProfileInfoModel? admin;
  //bool _isLoading=true;
  bool _isConnection = false;
  String? token;
  @override
  void initState() {
    super.initState();
    checkNetConnectivity();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);

    getAdmin();
    decodeToken();
  }

  getAdmin() async {
    await viewModelProvider.fatchAdminLoginInfo();
    //viewModelProvider.adminId=viewModelProvider.adminLoginInfoProvider!.data!.id!;
    await viewModelProvider.getAdminInfo();

    setState(() {});
  }

  decodeToken() {
    token = viewModelProvider.adminLoginInfoProvider!.data!.token!;
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token!);
    bool isTokenExpired = JwtDecoder.isExpired(token!);
    print('decodec: $decodedToken');
    DateTime expirationDate = JwtDecoder.getExpirationDate(token!);

    setState(() {
      if (isTokenExpired) {
        DBHelper().removeLoginAdminData();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
    });
    print('date: $expirationDate');
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
    print(result);
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            // const UserAccountsDrawerHeader(
            //   accountName: Text("Admin"),
            //   accountEmail: Text("admin@gmail.com"),
            //   currentAccountPicture: CircleAvatar(
            //     backgroundColor: Colors.orange,
            //     child: Text(
            //       "A",
            //       style: TextStyle(fontSize: 40.0),
            //     ),
            //   ),
            // ),
            _isConnection
                ? Container(
                    child: viewModelProvider.adminInfoProvider != null
                        ? DrawerHeader(
                            // ignore: prefer_const_constructors
                            decoration: BoxDecoration(
                              color: themeColor,
                            ),
                            child: Stack(children: [
                              Positioned(
                                left: 8,
                                bottom: 35,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // CircleAvatar(
                                    //   radius: 40,
                                    //   backgroundColor: Colors.white,
                                    //   // child: Text(
                                    //   //   "A",
                                    //   //   style: TextStyle(fontSize: 40.0),
                                    //   // ),
                                    //   backgroundImage: NetworkImage(
                                    //       viewModelProvider
                                    //           .adminInfoProvider!.avatarUrl!),
                                    // ),
                                    // SizedBox(
                                    //   height: 10,
                                    // ),
                                    Text(
                                      viewModelProvider
                                          .adminInfoProvider!.username!,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      viewModelProvider
                                          .adminInfoProvider!.email!,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ]),
                          )
                        : DrawerHeader(
                            decoration: BoxDecoration(
                              color: themeColor,
                            ),
                            child: Center(child: DrawerLoader())),
                  )
                : DrawerHeader(
                    child: NoInternet(
                      pressed: () async {
                        await checkNetConnectivity();
                        _isConnection == false
                            ? CustomSnackbar.snackbar(
                                context, "Turn Internet ON")
                            : checkNetConnectivity();
                        getAdmin();
                      },
                    ),
                  ),

            ListTile(
              leading: Icon(
                Icons.dashboard_outlined,
                color: categoryCardColor,
                size: 30,
              ),
              title: Text(
                "Dashboard",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Dashboard()));
              },
            ),
            Container(
              height: 0.5,
              color: drawerDividerColor,
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 5,
                  width: SizeConfig.blockSizeHorizontal! * 10,
                  child: Image.asset(
                    'assets/images/product.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Products",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductScreen()));
              },
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 4,
                  width: SizeConfig.blockSizeHorizontal! * 10,
                  child: Image.asset(
                    'assets/images/category.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Categories",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CategoryScreen()));
              },
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 4,
                  width: SizeConfig.blockSizeHorizontal! * 10,
                  child: Image.asset(
                    'assets/images/brand.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Brands",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => BrandScreen()));
              },
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 4,
                  width: SizeConfig.blockSizeHorizontal! * 10,
                  child: Image.asset(
                    'assets/images/coupons.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Coupons",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CouponScreen()));
              },
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 4,
                  width: SizeConfig.blockSizeHorizontal! * 10,
                  child: Image.asset(
                    'assets/images/customer.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Customers",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CustomerScreen()));
              },
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 4,
                  width: SizeConfig.blockSizeHorizontal! * 10,
                  child: Image.asset(
                    'assets/images/orders.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Orders",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OrderScreen()));
              },
            ),
            ListTile(
              leading: Container(
                  height: SizeConfig.blockSizeVertical! * 4,
                  width: SizeConfig.blockSizeHorizontal! * 7,
                  child: Image.asset(
                    'assets/images/sales.png',
                    fit: BoxFit.contain,
                  )),
              title: Text(
                "Sales Report",
                style: TextStyle(
                    color: greyTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SalesReportScreen()));
              },
            ),
            Container(
              height: 0.5,
              color: drawerDividerColor,
            ),
            Builder(builder: (context) {
              return ListTile(
                leading: Icon(
                  Icons.logout_outlined,
                  color: categoryCardColor,
                  size: 32,
                ),
                title: Text(
                  "Logout",
                  style: TextStyle(
                      color: greyTextColor,
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                ),
                onTap: () async {
                  await CustomSnackbar.snackbar(context, "you Logged Out");
                  await DBHelper().removeLoginAdminData();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SplashScreen()));
                },
              );
            })
          ],
        ),
      );
    });
  }
}
