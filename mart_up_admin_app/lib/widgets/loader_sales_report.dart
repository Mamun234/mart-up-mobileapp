import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:skeletons/skeletons.dart';

class LoaderSalesReportScreen extends StatefulWidget {
  const LoaderSalesReportScreen({Key? key}) : super(key: key);

  @override
  _SalesReportScreenState createState() => _SalesReportScreenState();
}

class _SalesReportScreenState extends State<LoaderSalesReportScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GridView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisExtent: SizeConfig.safeBlockVertical! * 22,
          ),
          itemCount: 10,
          itemBuilder: (BuildContext context, index) {
            return Padding(
              padding: const EdgeInsets.all(3.0),
              child: Card(
                elevation: 1,
                color: Colors.grey.shade100,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0)),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        SkeletonAvatar(
                            style: SkeletonAvatarStyle(
                          height: SizeConfig.safeBlockVertical! * 4,
                          width: SizeConfig.safeBlockHorizontal! * 7,
                        )),
                        SizedBox(
                          height: 12,
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 12,
                              width: 120,
                              alignment: Alignment.center,
                              borderRadius: BorderRadius.circular(3)),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 12,
                              width: 120,
                              alignment: Alignment.center,
                              borderRadius: BorderRadius.circular(3)),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 12,
                              width: 120,
                              alignment: Alignment.center,
                              borderRadius: BorderRadius.circular(3)),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 12,
                              width: 120,
                              alignment: Alignment.center,
                              borderRadius: BorderRadius.circular(3)),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
