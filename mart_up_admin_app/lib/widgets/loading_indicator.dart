import 'package:flutter/material.dart';

import 'package:mart_up_admin_app/common/colors.dart';


class BuildLoading extends StatefulWidget {
  const BuildLoading({Key? key}) : super(key: key);

  @override
  _BuildLoadingState createState() => _BuildLoadingState();
}

class _BuildLoadingState extends State<BuildLoading> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        color: tabSelectColor,
        backgroundColor: Colors.redAccent,
      ),
    );
  }
}
