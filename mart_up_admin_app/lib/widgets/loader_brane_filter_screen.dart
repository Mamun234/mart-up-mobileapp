import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderFilterBrand extends StatefulWidget {
  const LoaderFilterBrand({Key? key}) : super(key: key);

  @override
  _LoaderFilterBrandState createState() => _LoaderFilterBrandState();
}

class _LoaderFilterBrandState extends State<LoaderFilterBrand> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisExtent: 85,
        ),
        itemCount: 6,
        itemBuilder: (BuildContext context, index) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.circular(10),
              ),
              child: SkeletonLine(
                style: SkeletonLineStyle(
                    height: 20,
                    width: 90,
                    alignment: Alignment.center,
                    borderRadius: BorderRadius.circular(6)),
              ),
            ),
          );
        });
  }
}
