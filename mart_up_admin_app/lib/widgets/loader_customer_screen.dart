import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:skeletons/skeletons.dart';

class LoaderCustomerScreen extends StatefulWidget {
  const LoaderCustomerScreen({Key? key}) : super(key: key);

  @override
  _LoaderCustomerScreenState createState() => _LoaderCustomerScreenState();
}

class _LoaderCustomerScreenState extends State<LoaderCustomerScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, index) {
            return Card(
              elevation: 1,
               color: Colors.grey.shade100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0)),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Container(
                  height: SizeConfig.safeBlockVertical! * 15,
                  child: Row(
                    children: [
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(
                        shape: BoxShape.circle,
                        height: SizeConfig.safeBlockVertical! * 12,
                        width: SizeConfig.safeBlockHorizontal! * 27,
                      )),
                      SizedBox(
                        width: 25,
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 15,
                                  width: 100,
                                  alignment: Alignment.center,
                                  borderRadius: BorderRadius.circular(3)),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 15,
                                  width: 140,
                                  alignment: Alignment.center,
                                  borderRadius: BorderRadius.circular(3)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
