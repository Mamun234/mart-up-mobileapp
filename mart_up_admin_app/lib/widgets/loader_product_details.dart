import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderProductDetails extends StatefulWidget {
  const LoaderProductDetails({Key? key}) : super(key: key);

  @override
  _LoaderProductDetailsState createState() => _LoaderProductDetailsState();
}

class _LoaderProductDetailsState extends State<LoaderProductDetails> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              SkeletonAvatar(
                  style:
                      SkeletonAvatarStyle(width: double.infinity, height: 180)),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18, right: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 40)),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 40)),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 40)),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 40)),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 40)),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18, right: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 10,
                          width: 90,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            width: 30,
                            height: 30,
                            borderRadius: BorderRadius.circular(20))),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 10,
                          width: 70,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18, right: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 15)),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 15)),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 40, height: 15)),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18, right: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 10,
                          width: 90,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(width: 20, height: 20)),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 10,
                          width: 70,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18, right: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            width: 50,
                            height: 50,
                            borderRadius: BorderRadius.circular(40))),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            width: 50,
                            height: 50,
                            borderRadius: BorderRadius.circular(40))),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            width: 50,
                            height: 50,
                            borderRadius: BorderRadius.circular(40))),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            width: 50,
                            height: 50,
                            borderRadius: BorderRadius.circular(40))),
                    SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            width: 50,
                            height: 50,
                            borderRadius: BorderRadius.circular(40))),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 10,
                          width: 70,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              //dec
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 4,
                          width: 350,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 4,
                          width: 350,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 4,
                          width: 350,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 4,
                          width: 350,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 4,
                          width: 350,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
