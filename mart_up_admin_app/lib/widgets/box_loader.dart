import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class BoxLoader extends StatefulWidget {
  const BoxLoader({ Key? key }) : super(key: key);

  @override
  _BoxLoaderState createState() => _BoxLoaderState();
}

class _BoxLoaderState extends State<BoxLoader> {
  @override
  Widget build(BuildContext context) {
    return Card(
                      child: Container(
                        height: 100,
                        width: 100,
                        color: Colors.black,
                        child: SpinKitFadingCircle(
                          //color: Colors.black,
                          itemBuilder: (BuildContext context, int index) {
                        return DecoratedBox(
                          
                          decoration: BoxDecoration(
                            //backgroundBlendMode: BlendMode.darken,
                            shape: BoxShape.circle,
                            //color: Colors.black
                            //border: Border.all(color: Colors.black),
                            color: index.isEven ? Colors.white : Colors.white,
                          ),
                        );
                      },
                        ),
                      ),
                    );
  }
}