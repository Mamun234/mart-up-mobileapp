import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:skeletons/skeletons.dart';

class DrawerLoader extends StatefulWidget {
  const DrawerLoader({ Key? key }) : super(key: key);

  @override
  _DrawerLoaderState createState() => _DrawerLoaderState();
}

class _DrawerLoaderState extends State<DrawerLoader> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SkeletonAvatar(
          style: SkeletonAvatarStyle(
                                  shape: BoxShape.circle,
                                  width: 70,
                                  height: 70),
        ),
        SizedBox(height: 10,),
         SkeletonLine(
                 style: SkeletonLineStyle(
                    height: 10,
                  width: 50,
                                       alignment: Alignment.center,
                     borderRadius: BorderRadius.circular(6)),
             ),
                                 SizedBox(height: 10,),
         SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 12,
                                      width: 80,
                                       alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
      ],
    );
  }
}