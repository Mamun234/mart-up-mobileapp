import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/common/size_config.dart';
import 'package:skeletons/skeletons.dart';

class LoaderOrderScreen extends StatefulWidget {
  const LoaderOrderScreen({Key? key}) : super(key: key);

  @override
  _LoaderOrderScreenState createState() => _LoaderOrderScreenState();
}

class _LoaderOrderScreenState extends State<LoaderOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, index) {
            return Card(
              elevation: 1,
              color: Colors.grey.shade100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0)),
                  child: Column(
                    children: [
                      Container(
                        color: Colors.grey[350],
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 8.0),
                          child: Row(
                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                // color: Colors.grey[200],
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 50,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                               SizedBox(height: 10,),
                                  SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 50,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                                  ],
                                ),
                              ),
                                Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 50,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              SizedBox(height: 10,),
                                  SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 50,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                 SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 50,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              SizedBox(height: 10,),
                                  SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 50,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                 SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 60,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              SizedBox(height: 10,),
                                  SkeletonLine(
                                     style: SkeletonLineStyle(
                                    height: 10,
                                    width: 60,
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(3)),
                              ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 8.0),
                        child: Row(
                          mainAxisAlignment:MainAxisAlignment.end,
                          children: [
                               SkeletonLine(
                                       style: SkeletonLineStyle(
                                      height: 10,
                                      width: 100,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(3)),
                                ),
                          ],
                        ),
                      ),
                       Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 8.0),
                        child: Row(
                          mainAxisAlignment:MainAxisAlignment.end,
                          children: [
                            SkeletonLine(
                                       style: SkeletonLineStyle(
                                      height: 10,
                                      width: 40,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(3)),
                                ),
                                SizedBox(width: 10,),
                               SkeletonLine(
                                       style: SkeletonLineStyle(
                                      height: 10,
                                      width: 80,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(3)),
                                ),
                          ],
                        ),
                      )
                    ],
                  ),
              // child: Padding(
              //   padding: const EdgeInsets.all(16.0),
              //   child: Container(
              //     height: SizeConfig.safeBlockVertical! * 18,
              //     child: Column(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: [
              //         Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             Column(
              //               children: [
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 15,
              //                       width: 80,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //                 SizedBox(
              //                   height: 5,
              //                 ),
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 12,
              //                       width: 130,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //               ],
              //             ),
              //             Column(
              //               children: [
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 15,
              //                       width: 80,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //                 SizedBox(
              //                   height: 5,
              //                 ),
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 12,
              //                       width: 130,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //               ],
              //             ),
              //           ],
              //         ),

              //         //
              //         Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             Column(
              //               children: [
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 15,
              //                       width: 60,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //                 SizedBox(
              //                   height: 5,
              //                 ),
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 12,
              //                       width: 90,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //               ],
              //             ),
              //             Column(
              //               children: [
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 15,
              //                       width: 60,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //                 SizedBox(
              //                   height: 5,
              //                 ),
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 12,
              //                       width: 90,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //               ],
              //             ),
              //             Column(
              //               children: [
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 15,
              //                       width: 60,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //                 SizedBox(
              //                   height: 5,
              //                 ),
              //                 SkeletonLine(
              //                   style: SkeletonLineStyle(
              //                       height: 12,
              //                       width: 90,
              //                       alignment: Alignment.center,
              //                       borderRadius: BorderRadius.circular(3)),
              //                 ),
              //               ],
              //             ),
              //           ],
              //         )
              //       ],
              //     ),
              //   ),
              // ),
            );
          }),
    );
  }
}
