import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:intl/intl.dart';
import 'package:mart_up_admin_app/models/responses/review_model.dart';
//import 'package:mart_up/models/profileInfoModel.dart';

//import 'package:mart_up/widgets/rating.dart';

// ignore: must_be_immutable
class ReviewCard extends StatefulWidget {
  ReviewModel? reviewModel;

  ReviewCard({
    this.reviewModel,
  });

  @override
  _ReviewCardState createState() => _ReviewCardState();
}

class _ReviewCardState extends State<ReviewCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.pinkAccent,
                  backgroundImage: AssetImage('assets/images/avater.jpeg'),
                  radius: 30,
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${widget.reviewModel!.reviewer}',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    RatingBar.builder(
                      itemSize: 16,
                      initialRating: widget.reviewModel?.rating == null
                          ? 1.0
                          : widget.reviewModel!.rating!.toDouble(),
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: false,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      ignoreGestures: true,
                      onRatingUpdate: (rating) {
                        print(rating);
                        // widget.reviewModel!.rating;
                      },
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            HtmlWidget('${widget.reviewModel!.review}',
                textStyle: TextStyle(color: Colors.grey, fontSize: 14)),
            SizedBox(
              height: 20,
            ),
            Text(
              DateFormat('dd MMM yyyy')
                  .format(DateTime.parse('${widget.reviewModel!.dateCreated}')),
              style: TextStyle(fontSize: 12),
            )
          ],
        ),
      ),
    );
  }
}
