import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderProductCardScreen extends StatefulWidget {
  const LoaderProductCardScreen({Key? key}) : super(key: key);

  @override
  _LoaderProductCardScreenState createState() =>
      _LoaderProductCardScreenState();
}

class _LoaderProductCardScreenState extends State<LoaderProductCardScreen> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisExtent: 180,
        ),
        itemCount: 10,
        itemBuilder: (BuildContext context, index) {
          return Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(width: 40, height: 100)),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 120,
                            //alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 100,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 10,
                                width: 70,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          SkeletonAvatar(
                              style: SkeletonAvatarStyle(
                                  width: 22,
                                  height: 22,
                                  borderRadius: BorderRadius.circular(5))),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
