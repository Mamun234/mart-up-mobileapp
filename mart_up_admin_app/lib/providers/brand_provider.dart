import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';

class BrandProvider extends ChangeNotifier{

List<BrandModel>? _brandList;
  List<BrandModel>? get brandListProvider => _brandList;
  String? _totalBrand;
  String? get totalBrandListProvider => _totalBrand;
  String? searchKey;
  int? page;
  int? limit;
   setBrand(List<BrandModel>? list) {
    _brandList = list;
    notifyListeners();
  }

  getBrand(page, limit) async {
    var value =
        await BrandService.fetchBrand(page, limit, searchKey: searchKey);

    setBrand(value);
  }

  setTotalBrand(String? list) {
    _totalBrand = list;
    notifyListeners();
  }

  getTotalBrand() async {
    var value =
        await BrandService.brandCount();

    setTotalBrand(value);
  }
}