import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/customer_model.dart';
import 'package:mart_up_admin_app/models/responses/total_customer_count_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/customer_service.dart';
import 'package:mart_up_admin_app/services/total_customer_count_service.dart';

class CustomerProvider extends ChangeNotifier{
List<CustomerModel>? _customerModel;
  List<CustomerModel>? get customerListProvider => _customerModel;
    List<TotalCustomerModel>? totalCustomer;
  List<TotalCustomerModel>? get totalCustomerList=>totalCustomer;
  String? searchKey;
  int? page;
  int? limit;
  setCustomer(List<CustomerModel> list) async {
    _customerModel = list;
    notifyListeners();
  }

  getCustomer(page, limit) async {
    var value = await CustomerService.fetchCustomer(searchKey,page, limit);
    setCustomer(value);
  }

    setTotalCustomerCount(List<TotalCustomerModel> list)async{
    totalCustomer=list;
    notifyListeners();
  }
  getTotalCustomerCount()async{
    var value = await TotalCustomerService.fetchTotalCustomer();
   setTotalCustomerCount(value);
  }
}