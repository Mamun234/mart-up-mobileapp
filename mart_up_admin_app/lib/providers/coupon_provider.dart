import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/coupon_response_model.dart';
import 'package:mart_up_admin_app/models/responses/total_product_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/coupon_service.dart';

class CouponProvider extends ChangeNotifier{

  List<CouponResponseModel>? _couponModel;
  List<CouponResponseModel>? get couponListProvider => _couponModel;
  String? totalCoupon;
  String? get totalCouponList=>totalCoupon;
  String? searchKey;
  int? page;
  int? limit;
  setTotalCouponCount(String list)async{
    totalCoupon=list;
    notifyListeners();
  }
  getTotalCouponCount()async{
    var value = await CouponService.couponCount();
    setTotalCouponCount(value!);
  }

  setCoupon(List<CouponResponseModel> list) async {
    _couponModel = list;
    notifyListeners();
  }

  getCoupon(page, limit) async {
    var value = await CouponService.fetchCoupon(searchKey,page, limit);
    setCoupon(value);
  }
}