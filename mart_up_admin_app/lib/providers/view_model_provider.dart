import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:mart_up_admin_app/models/requests/create_coupon_model.dart';
import 'package:mart_up_admin_app/models/requests/order_status_model.dart';
import 'package:mart_up_admin_app/models/responses/admin_login_response_model.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';

import 'package:mart_up_admin_app/models/responses/coupon_response_model.dart';
import 'package:mart_up_admin_app/models/responses/customer_model.dart';
import 'package:mart_up_admin_app/models/responses/order_details_model.dart';
import 'package:mart_up_admin_app/models/responses/order_get_model.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/models/responses/profile_info_model.dart';
import 'package:mart_up_admin_app/models/responses/review_model.dart';
import 'package:mart_up_admin_app/models/responses/sales_report_response_model.dart';
import 'package:mart_up_admin_app/models/responses/total_coupon_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_customer_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_order_count_model.dart';
import 'package:mart_up_admin_app/models/responses/total_product_model.dart';
import 'package:mart_up_admin_app/models/responses/total_review_count_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/coupon_service.dart';
import 'package:mart_up_admin_app/services/customer_service.dart';
import 'package:mart_up_admin_app/services/get_admin_info.dart';
import 'package:mart_up_admin_app/services/local_db_helper.dart';
import 'package:mart_up_admin_app/services/order_service.dart';
import 'package:mart_up_admin_app/services/product_service.dart';
import 'package:mart_up_admin_app/services/review_service.dart';
import 'package:mart_up_admin_app/services/sales_report_service.dart';
import 'package:mart_up_admin_app/services/total_coupon_count_service.dart';
import 'package:mart_up_admin_app/services/total_customer_count_service.dart';
import 'package:mart_up_admin_app/services/total_order_count_service.dart';
import 'package:mart_up_admin_app/services/total_product_service.dart';
import 'package:mart_up_admin_app/services/total_review_count_service.dart';

class ViewModelProvider extends ChangeNotifier {
  AdminLoginResponseModel? _adminLoginResponseModel;
  AdminLoginResponseModel? get adminLoginInfoProvider =>
      _adminLoginResponseModel;

  ProfileInfoModel? _profileInfoModel;
  ProfileInfoModel? get adminInfoProvider => _profileInfoModel;

  // List<BrandModel>? _brandList;
  // List<BrandModel>? get brandListProvider => _brandList;
  // String? _totalBrand;
  // String? get totalBrandListProvider => _totalBrand;

  // List<CategoryModel>? _categoryModel;
  // List<CategoryModel>? get categoryListProvider => _categoryModel;
  //   String? _totalCategory;
  // String? get totalCategoryListProvider => _totalCategory;

  // List<CustomerModel>? _customerModel;
  // List<CustomerModel>? get customerListProvider => _customerModel;

  // List<CouponResponseModel>? _couponModel;
  // List<CouponResponseModel>? get couponListProvider => _couponModel;
  // List<ProductModel>? _productModel;
  // List<ProductModel>? get productListProvider => _productModel;

  // List<OrderGetModel>? ordeGetModel;
  // List<OrderGetModel>? get orderGetListProvider => ordeGetModel;

  // List<ReviewModel>? reviewModel;
  // List<ReviewModel>? get reviewGetListProvider => reviewModel;
  // String? totalReview;
  // String? get totalReviewProvider=>totalReview;

  List<ReportResponseModel>? reportResponseModel;
  List<ReportResponseModel>? get fetchSaleReportListProvider =>
      reportResponseModel;

  // OrderDetailsModel? orderDetailsModel;
  // OrderDetailsModel? get fetchOrderDetailProvider => orderDetailsModel;
  // List<TotalProductModel>? totalProduct;
  // List<TotalProductModel>? get totalProductList=>totalProduct;
  // String? totalCoupon;
  // String? get totalCouponList=>totalCoupon;
  // List<TotalCustomerModel>? totalCustomer;
  // List<TotalCustomerModel>? get totalCustomerList=>totalCustomer;
  //   List<TotalOrderModel>? totalOrder;
  // List<TotalOrderModel>? get totalOrderList=>totalOrder;
  // List<TotalReviewModel>? totalReview;
  // List<TotalReviewModel>? get  totalReviewList=> totalReview;


  String? searchKey;
  int? adminId;
  // int? orderId;
  int? page;
  int? limit;
  String? salesPeriod;
  String? minDate;
  String? maxDate;


  setAdminInfo(ProfileInfoModel list) {
    _profileInfoModel = list;
    //print(_cartList!.length);
    notifyListeners();
  }

  getAdminInfo() async {
    adminId = adminLoginInfoProvider!.data!.id!;
    var value = await GetAdminService.fetchAdminInfo(adminId);
    setAdminInfo(value);
  }

  // setBrand(List<BrandModel>? list) {
  //   _brandList = list;
  //   notifyListeners();
  // }

  // getBrand(searchKey, page, limit) async {
  //   var value =
  //       await BrandService.fetchBrand(page, limit, searchKey: searchKey);

  //   setBrand(value);
  // }

  // setTotalBrand(String? list) {
  //   _totalBrand = list;
  //   notifyListeners();
  // }

  // getTotalBrand() async {
  //   var value =
  //       await BrandService.brandCount();

  //   setTotalBrand(value);
  // }

  setAdminLoginInfo(AdminLoginResponseModel? list) {
    _adminLoginResponseModel = list;
    notifyListeners();
  }

  fatchAdminLoginInfo() async {
    var value = await DBHelper().fatchLoginAdminData();
    setAdminLoginInfo(value);
  }

  // setCategory(List<CategoryModel> list) async {
  //   _categoryModel = list;
  //   notifyListeners();
  // }

  // getCategory(page, limit) async {
  //   var value = await CategoryService.fetchCategory(searchKey,page, limit);
  //   setCategory(value);
  // }
  //   setTotalCategory(String list) async {
  //   _totalCategory = list;
  //   notifyListeners();
  // }

  // getTotalCategory() async {
  //   var value = await CategoryService.categoryCount();
  //   setTotalCategory(value!);
  // }

  // setCustomer(List<CustomerModel> list) async {
  //   _customerModel = list;
  //   notifyListeners();
  // }

  // getCustomer(page, limit) async {
  //   var value = await CustomerService.fetchCustomer(searchKey,page, limit);
  //   setCustomer(value);
  // }

  // setCoupon(List<CouponResponseModel> list) async {
  //   _couponModel = list;
  //   notifyListeners();
  // }

  // getCoupon(page, limit) async {
  //   var value = await CouponService.fetchCoupon(searchKey,page, limit);
  //   setCoupon(value);
  // }

  // setProduct(List<ProductModel> list) async {
  //   _productModel = list;
  //   notifyListeners();
  // }

  // getProduct(page, limit) async {
  //   var value = await ProductService.fetchProducts(page, limit,searchKey);
  //   setProduct(value);
  // }

  // setOrder(List<OrderGetModel> list) async {
  //   ordeGetModel = list;
  //   notifyListeners();
  // }

  // getOrder(page,limit) async {
  //   var value = await OrderService.fetchOrder(searchKey,page,limit);
  //   setOrder(value);
  // }

  // setReview(List<ReviewModel> list) async {
  //   reviewModel = list;
  //   notifyListeners();
  // }

  // getReview(searchKey,page,limit) async {
  //   var value = await ReviewService.fetchRivew(searchKey: searchKey,page: page,limit: limit,);
  //   setReview(value);
  // }

  setSaleReport(List<ReportResponseModel>? report) async {
    reportResponseModel = report;
    notifyListeners();
  }

  getSaleReport({salesPeriod,minDate,maxDate}) async {
    var value = await SalesReportService.fetchSaleReports(period: salesPeriod,minDate: minDate,maxDate: maxDate);
    setSaleReport(value);
  }

  // setOrderDetail(OrderDetailsModel? Order) async {
  //   orderDetailsModel = Order;
  //   notifyListeners();
  // }

  // getOrderDetail() async {
  //   var value = await OrderService().fetchOrderDetails(orderId);
  //   setOrderDetail(value);
  // }
  // setTotalProduct(List<TotalProductModel> list)async{
  //   totalProduct=list;
  //   notifyListeners();
  // }
  // getTotalProduct()async{
  //   var value = await TotalProductService.fetchTotalProduct();
  //   setTotalProduct(value);
  // }
  // setTotalCouponCount(String list)async{
  //   totalCoupon=list;
  //   notifyListeners();
  // }
  // getTotalCouponCount()async{
  //   var value = await CouponService.couponCount();
  //   setTotalCouponCount(value!);
  // }
  //  setTotalCustomerCount(List<TotalCustomerModel> list)async{
  //   totalCustomer=list;
  //   notifyListeners();
  // }
  // getTotalCustomerCount()async{
  //   var value = await TotalCustomerService.fetchTotalCustomer();
  //  setTotalCustomerCount(value);
  // }
  //    setTotalOrderCount(List<TotalOrderModel> list)async{
  //   totalOrder=list;
  //   notifyListeners();
  // }
  // getTotalOrderCount()async{
  //   var value = await TotalOrderService.fetchTotalOrder();
  //  setTotalOrderCount(value);
  // }

  //  setTotalReviewCount(String list)async{
  //   totalReview=list;
  //   notifyListeners();
  // }
  // getTotalReviewCount()async{
  //   var value = await ReviewService.reviewCount();
  //  setTotalReviewCount(value!);
  // }

}
