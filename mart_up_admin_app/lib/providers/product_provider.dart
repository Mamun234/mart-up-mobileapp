import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/product_model.dart';
import 'package:mart_up_admin_app/models/responses/total_product_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/product_service.dart';
import 'package:mart_up_admin_app/services/total_product_service.dart';

class ProductProvider extends ChangeNotifier {
  List<ProductModel>? _productModel;
  List<ProductModel>? get productListProvider => _productModel;
  List<TotalProductModel>? totalProduct;
  List<TotalProductModel>? get totalProductList => totalProduct;
  List<ProductModel> _cartList = [];
  List<ProductModel> get cartList => _cartList;
  String? searchKey;
  int? page;
  int? limit;
  int? id;
  int? brandId;
  var minPrice, maxPrice;
 
  num? total;

  setTotalModel(ttal) {
    total = ttal;
    notifyListeners();
  }

  setCart(List<ProductModel> list) {
    _cartList = list;
    notifyListeners();
  }

  setProduct(List<ProductModel> list) async {
    _productModel = list;
    notifyListeners();
  }

  getProduct(page, limit,searchKey, id, brandId, minPrice, maxPrice) async {
    var value = await ProductService.fetchProducts(
      page,
      limit,
      searchKey,
      categoryId: id,
      brandId: brandId,
      minPrice: minPrice,
      maxPrice: maxPrice,
    );
    setProduct(value);
  }

  setTotalProduct(List<TotalProductModel> list) async {
    totalProduct = list;
    notifyListeners();
  }

  getTotalProduct() async {
    var value = await TotalProductService.fetchTotalProduct();
    setTotalProduct(value);
  }
}
