import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/review_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/review_service.dart';

class ReviewProvider extends ChangeNotifier{

List<BrandModel>? _brandList;
  List<ReviewModel>? reviewModel;
  List<ReviewModel>? get reviewGetListProvider => reviewModel;
  String? totalReview;
  String? get totalReviewProvider=>totalReview;
  int? page;
  int? limit;
  String? searchKey;
  String? status;
  setReview(List<ReviewModel> list) async {
    reviewModel = list;
    notifyListeners();
  }

  getReview(searchKey,page,limit,status) async {
    var value = await ReviewService().fetchRivew(searchKey: searchKey,page: page,limit: limit,status: status);
    setReview(value);
  }

 setTotalReviewCount(String list)async{
    totalReview=list;
    notifyListeners();
  }
  getTotalReviewCount()async{
    var value = await ReviewService.reviewCount();
   setTotalReviewCount(value!);
  }
}