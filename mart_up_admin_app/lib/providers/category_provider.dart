import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/services/category_service.dart';

class CategoryProvider extends ChangeNotifier{

  List<CategoryModel>? _categoryModel;
  List<CategoryModel>? get categoryListProvider => _categoryModel;
    String? _totalCategory;
  String? get totalCategoryListProvider => _totalCategory;
  String? searchKey;
  int? page;
  int? limit;
    setCategory(List<CategoryModel> list) async {
    _categoryModel = list;
    notifyListeners();
  }

  getCategory(page, limit) async {
    var value = await CategoryService.fetchCategory(searchKey,page, limit);
    setCategory(value);
  }
    setTotalCategory(String list) async {
    _totalCategory = list;
    notifyListeners();
  }

  getTotalCategory() async {
    var value = await CategoryService.categoryCount();
    setTotalCategory(value!);
  }
}