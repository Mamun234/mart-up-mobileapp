import 'package:flutter/material.dart';
import 'package:mart_up_admin_app/models/responses/brand_model.dart';
import 'package:mart_up_admin_app/models/responses/category_model.dart';
import 'package:mart_up_admin_app/models/responses/order_details_model.dart';
import 'package:mart_up_admin_app/models/responses/order_get_model.dart';
import 'package:mart_up_admin_app/models/responses/total_order_count_model.dart';
import 'package:mart_up_admin_app/services/brand_service.dart';
import 'package:mart_up_admin_app/services/category_service.dart';
import 'package:mart_up_admin_app/services/order_service.dart';
import 'package:mart_up_admin_app/services/total_order_count_service.dart';

class OrderProvider extends ChangeNotifier{

  List<OrderGetModel>? ordeGetModel;
  List<OrderGetModel>? get orderGetListProvider => ordeGetModel;
    OrderDetailsModel? orderDetailsModel;
  OrderDetailsModel? get fetchOrderDetailProvider => orderDetailsModel;
    List<TotalOrderModel>? totalOrder;
  List<TotalOrderModel>? get totalOrderList=>totalOrder;
  String? searchKey;
  int? page;
  int? limit;
   int? orderId;
   String? status;
   setOrder(List<OrderGetModel> list) async {
    ordeGetModel = list;
    notifyListeners();
  }

  getOrder(page,limit,{status}) async {
    var value = await OrderService().fetchOrder(searchKey,page,limit,status:status );
    setOrder(value);
  }

  setOrderDetail(OrderDetailsModel? Order) async {
    orderDetailsModel = Order;
    notifyListeners();
  }

  getOrderDetail() async {
    var value = await OrderService().fetchOrderDetails(orderId);
    setOrderDetail(value);
  }
       setTotalOrderCount(List<TotalOrderModel> list)async{
    totalOrder=list;
    notifyListeners();
  }
  getTotalOrderCount()async{
    var value = await TotalOrderService.fetchTotalOrder();
   setTotalOrderCount(value);
  }
}