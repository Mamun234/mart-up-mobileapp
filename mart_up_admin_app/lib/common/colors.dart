import 'package:flutter/material.dart';

Color greyTextColor=Colors.grey;
var drawerDividerColor=Color(0xff326e84);
var lebelColor= Color(0xff0F0F10);
var themeColor=Color(0xff40BFFF);
var tabSelectColor=Color(0xff40BFFF);
var brandNameColor=Color(0xff000000);
var productNameColor=Color(0xff223263);
var salePriceColor=Color(0xff40BFFF);
var previousPriceColor=Color(0xff9098B1);
var discountPriceColor=Color(0xffFB7181);
var borderColor=Color(0xffEBF0FF);

// var productCardColor=Color(0xff394C81);#4c5d8d
var productCardColor=Color(0xff606f9a);
// var categoryCardColor=Color(0xff5B48A2);7B6CB4
 var categoryCardColor=Color(0xff7B6CB4);
// var brandCardColor=Color(0xff5D5D5D);7D7D7D
var brandCardColor=Color(0xff7D7D7D);
// var couponCarColord=Color(0xffD35400);DB7633
var couponCarColord=Color(0xffDB7633);
// var customertCardColor=Color(0xff27AE60);52BE7F
var customertCardColor=Color(0xff52BE7F);
// var orderCardColor=Color(0xff2980B9);5399C7
var orderCardColor=Color(0xff5399C7);