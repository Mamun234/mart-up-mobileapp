import 'package:flutter/material.dart';

import 'package:mart_up/models/cart_model.dart';
import 'package:mart_up/models/favorite_model.dart';
import 'package:mart_up/models/product.dart';
// import 'package:mart_up/screens/product_details_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
// import 'package:mart_up/services/product-service.dart';

class ViewModelProvider extends ChangeNotifier{
  List<CartModel>? _cartList;
    List<CartModel>? get cartListN => _cartList;
    List<FavoriteModel>? _favoriteList;
    List<FavoriteModel>? get favoriteListLength=>_favoriteList;
    // List<Product>? _compare;
    // List<Product>? get comapreList=>_compare;
    List<Product>? compareProviderList=[];
    List<Product>? _compare;
    List<Product>? get comapreList=>_compare;
    int? checkLogin;
     int compareDeleteCount=0;
     int compareAddCount=0;
    List<int>? discountProvider=[];
     //List<int>? discountProviderFinalList=[];
    setCompare(List<Product> list){
      _compare=list;
      notifyListeners();
    }
    getCompare()async{
      var value= await compareProviderList;
    setCompare(value!);
    }
   
    setCartModel(List<CartModel> list) {
    _cartList = list;
    //print(_cartList!.length);
    notifyListeners();
  }
  getCartModel()async{
    var value= await DBHelper().getCartListProducts();
    setCartModel(value);
  }
   setFavoriteModel(List<FavoriteModel> list) {
    _favoriteList = list;
    //print(_cartList!.length);
    notifyListeners();
  }
  getFavoriteModel()async{
    var value= await DBHelper().getFavoriteListProducts();
    setFavoriteModel(value);
  }
//   setCompare(List<Product> list){
//     _compare=list;
//   }
//   getCompare()async{
// var value=await ProductDetailPage();
//   }

}