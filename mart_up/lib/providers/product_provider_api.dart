import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:mart_up/Api/product_api.dart';
import 'package:mart_up/models/product_model_api.dart';

class ProductProviderApi extends ChangeNotifier{

  List<ProductModelApi> _productModelApi=[];
  List<ProductModelApi> get productModelList=> _productModelApi;

  _setList( List<ProductModelApi> list){ 

    _productModelApi=list;
    log(list.toString());
    notifyListeners();
  }
  getProductModelList() async{
 var data =   await  ProductApi().getProductList();
 _setList(data);
 
  }

}