import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:mart_up/models/product_model_api.dart';

class ProductApi{

Future<List<ProductModelApi>> getProductList(
   
  ) async {
    log('Approved leave applications');
    
    var urll = Uri.parse(
        'https://demo.baburhut.com/wp-json/wc/v3/products?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f');
    // ignore: prefer_typing_uninitialized_variables

    try {
      final response = await http
          .get(
            urll,
          )
          .timeout(const Duration(seconds: 25))
          .catchError((error) {
        // ignore: avoid_print
        print(error);
      });
      Map<String, dynamic> map = json.decode(response.body);
    // ProductModelApi.fromJson(jsonDecode(response.body) as Map<String, dynamic>);
     // List<dynamic> data = map.;
      log('$map');

      List<ProductModelApi> l =
          productFromJson(jsonEncode(map)).toList();
      return l;
    } catch (e) {
      // ignore: avoid_print
      print('error:$e');
      return List<ProductModelApi>.empty();
    }
  }

}