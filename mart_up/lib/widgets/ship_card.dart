import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/add_address_model.dart';
import 'package:mart_up/screens/add_address.dart';
import 'package:mart_up/screens/to_ship.dart';
import 'package:mart_up/services/local_db_helper.dart';
// import 'package:mart_up/services/local_db_helper.dart';
// import 'package:mart_up/services/local_db_helper.dart';

// ignore: must_be_immutable
class ShipCard extends StatefulWidget {
  AddAddressModel? addAddressModel;
  ToShip? toShip;
  int? tab;
  int? index;
  List<AddAddressModel> selectedAddresses = [];

  ShipCard({Key? key, this.toShip, this.addAddressModel, this.tab, this.index})
      : super(key: key);

  @override
  State<ShipCard> createState() => _ShipCardState();
}

class _ShipCardState extends State<ShipCard> {
  void deleteAddress() async {
    await DBHelper.deleteAddress(widget.addAddressModel!.id!);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3.0,
      shadowColor: Colors.black54,
      color: widget.tab != null && widget.tab == widget.index
          ? borderColor
          : Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          //color: pressed ? borderColor : Colors.white,

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.addAddressModel!.lname!,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                widget.addAddressModel!.houseNo!,
                style: TextStyle(fontSize: 18, color: Colors.grey),
              ),
              SizedBox(
                height: 15,
              ),

              // Text(
              //   widget.addAddressModel.mobileNo!,
              //   style: TextStyle(fontSize: 18, color: Colors.grey),
              // ),

              Visibility(
                visible: widget.addAddressModel!.mobileNo != null,
                child: Text(
                  widget.addAddressModel!.mobileNo!,
                  style: TextStyle(fontSize: 18, color: Colors.grey),
                ),
              ),

              SizedBox(
                height: 15,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddAddressPage(
                                    id: widget.addAddressModel?.id,
                                  )));
                    },
                    child: Container(
                      width: 85,
                      height: 60,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: themeColor),
                      child: Center(
                        child: Text(
                          "Edit",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  IconButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                title: Text('Delete', textScaleFactor: 1),
                                content: Text('Are you sure delete',
                                    textScaleFactor: 1),
                                actions: <Widget>[
                                  TextButton(
                                      child: Text('Cancle'),
                                      onPressed: () {
                                        Navigator.of(context).pop(false);
                                      }),
                                  TextButton(
                                      child: Text('Delete'),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        deleteAddress();
                                      }),
                                ],
                              ));
                      //deleteAddress();
                    },
                    icon: Icon(
                      Icons.delete_outlined,
                      size: 36,
                      color: Colors.grey,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
