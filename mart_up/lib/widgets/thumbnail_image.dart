import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/product.dart';

// ignore: must_be_immutable
class ThumbnailImage extends StatefulWidget {
  List<Images>? imgList;
  ThumbnailImage({this.imgList});

  @override
  _ThumbnailImageState createState() => _ThumbnailImageState();
}

class _ThumbnailImageState extends State<ThumbnailImage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 80,
        width: 80,
        decoration: BoxDecoration(
            //color: Colors.amber,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: tabSelectColor,
            )),
        child: Center(child: Image.network('${widget.imgList}')),
      ),
    );
  }
}
