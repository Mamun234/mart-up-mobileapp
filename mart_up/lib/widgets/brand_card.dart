import 'package:flutter/material.dart';
import 'package:mart_up/constants/links.dart';

import 'package:mart_up/models/brand_model.dart';
import 'package:mart_up/screens/search_result_screen.dart';

// ignore: must_be_immutable
class BrandCard extends StatefulWidget {
  BrandModel? brandModel;
  BrandCard({this.brandModel});

  @override
  State<BrandCard> createState() => _BrandCardState();
}

class _BrandCardState extends State<BrandCard> {
 
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SearchResultPage(
                      brandId: widget.brandModel!.id,brandName: widget.brandModel!.name,
                    )));
      },
      child: Card(
        elevation: 2.0,
        shadowColor: Colors.black38,
        child: Container(
          // height: 190,
          // width: 110,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(40)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                children: [
                  Container(
                      height: 50,
                      child: Image.network(
                        widget.brandModel!.termThumbnailImage!.url !=null?'${widget.brandModel!.termThumbnailImage!.url}':emptyImage,
                        fit: BoxFit.cover,
                      )),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    child: Text(
                      '${widget.brandModel!.name}',
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
