import 'package:flutter/material.dart';

class CustomSnackbar {
  final String msg;
  const CustomSnackbar({required this.msg});

  static snackbar(BuildContext context, String msg) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(
          elevation: 0,
          backgroundColor: Colors.deepPurpleAccent,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          behavior: SnackBarBehavior.floating,
          //action: SnackBarAction(label: Icon(Icons.forward), onPressed: onPressed),
          content: Text(
            msg,
            style: const TextStyle(color: Colors.white),
          ),
          duration: const Duration(seconds: 2),
        ))
        .closed;
  }
}
