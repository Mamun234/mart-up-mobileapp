import 'package:flutter/material.dart';
import 'package:mart_up/widgets/dash_line.dart';
import 'package:skeletons/skeletons.dart';

class LoaderOrderDetail extends StatefulWidget {
  const LoaderOrderDetail({Key? key}) : super(key: key);

  @override
  _LoaderOrderDetailState createState() => _LoaderOrderDetailState();
}

class _LoaderOrderDetailState extends State<LoaderOrderDetail> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Container(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                height: 80,
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                      children: [
                        SkeletonAvatar(
                          style: SkeletonAvatarStyle(
                              shape: BoxShape.circle, width: 32, height: 32),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 123,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonAvatar(
                          style: SkeletonAvatarStyle(
                              shape: BoxShape.circle, width: 32, height: 32),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 123,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonAvatar(
                          style: SkeletonAvatarStyle(
                              shape: BoxShape.circle, width: 32, height: 32),
                        ),
                      ],
                    ),
                  ],
                )),
            SizedBox(
              height: 22,
            ),
            Container(
              height: 90,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.grey.shade400,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 90,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 50,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 22,
            ),
            Container(
              height: 120,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.grey.shade400,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 50,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 70,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 22,
            ),
            Container(
              height: 130,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.grey.shade400,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 50,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 50,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    DashLine(color: Colors.grey.withOpacity(0.9)),
                    SizedBox(
                      height: 18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 90,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 50,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
