import 'package:flutter/material.dart';

class AlertDialogg extends StatelessWidget {
  AlertDialogg({
    Key? key,
    tTx: "ttx",
    cTX: "ctx",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: AlertDialog(
          title: Text('Very, very large title', textScaleFactor: 1),
          content: Text('Very, very large content', textScaleFactor: 1),
          actions: <Widget>[
            TextButton(child: Text('Button 1'), onPressed: () {}),
            TextButton(child: Text('Button 2'), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
