import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/order_get_model.dart';
import 'package:mart_up/screens/order_detaits_screen.dart';
import 'package:mart_up/widgets/dash_line.dart';


class OrdersCard extends StatefulWidget {
  final OrderGetModel? orderGetModel;
  OrdersCard({this.orderGetModel});

  @override
  _OrdersCardState createState() => _OrdersCardState();
}

class _OrdersCardState extends State<OrdersCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OrderDetailsScreen(
                      orderId: widget.orderGetModel?.id,
                    )));
      },
      child: Card(
        elevation: 3.0,
        shadowColor: Colors.black54,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Container(
            height: 190,
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'ID:${widget.orderGetModel?.id}',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Order at Mart-up: ",
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                    Text(
                      DateFormat('dd MMM yyyy').format(DateTime.parse(
                          "${widget.orderGetModel?.dateCreated}")),
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                DashLine(color: Colors.grey.withOpacity(0.3)),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Order Status",
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                    Text(
                      '${widget.orderGetModel?.status}',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Items",
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                    Text(
                      '${widget.orderGetModel?.lineItems!.length}',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Price",
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                    Text(
                      '৳${widget.orderGetModel?.total}',
                      style: TextStyle(fontSize: 18, color: salePriceColor),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
