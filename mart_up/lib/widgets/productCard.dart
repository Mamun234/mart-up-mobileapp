import 'package:flutter/material.dart' hide Action;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/constants/links.dart';
import 'package:mart_up/models/product.dart';
// import 'package:mart_up/models/product_card_model.dart';
// import 'package:mart_up/models/product_list.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:mart_up/screens/product_details_screen.dart';
//import 'package:mart_up/widgets/rating.dart';

class ProductCardNew extends StatefulWidget {
  //final double? height,width;
  final Product? productCardModel;

  ProductCardNew({
    this.productCardModel,
  });

  @override
  State<ProductCardNew> createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCardNew> {
  Product? _selectedProduct;
  double? difference;
  double? discountPercent;
  int? discountPercentRound;

  @override
  void initState() {
    super.initState();

    if (widget.productCardModel!.salePrice != "" &&
        widget.productCardModel!.regularPrice != "") {
      difference = double.parse(widget.productCardModel!.regularPrice!) -
          double.parse(widget.productCardModel!.salePrice!);
      discountPercent =
          (difference! / double.parse(widget.productCardModel!.regularPrice!)) *
              100;
      discountPercentRound = discountPercent!.round();
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.productCardModel!.quantity = 1;
          _selectedProduct = widget.productCardModel;

          print('qauntity${widget.productCardModel!.quantity}');
          _selectedProduct = widget.productCardModel;
        });
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetailPage(
                      id: _selectedProduct?.id,
                      //p: _selectedProduct,
                      discountPercentRound: discountPercentRound,
                    )));
      },
      child: Card(
        elevation: 2.0,
        shadowColor: Colors.black38,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
          ),
          height: ScreenUtil().setHeight(260),
          width: ScreenUtil().setHeight(140),
          child: Padding(
            padding: EdgeInsets.all(12.0),
            child: Container(
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 126,
                    //width: 170,

                    child: Center(
                      child: Image.network(
                        widget.productCardModel!.images!.isNotEmpty
                            ? '${widget.productCardModel!.images![0].src}'
                            : emptyImage,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    widget.productCardModel!.name!.isNotEmpty
                        ? '${widget.productCardModel!.name}'
                        : "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        color: productNameColor, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RatingBar.builder(
                    itemSize: 16,
                    initialRating: widget.productCardModel!.averageRating
                                .toString() ==
                            "0.0"
                        ? 1.0
                        : double.parse(
                            widget.productCardModel!.averageRating.toString()),
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    ignoreGestures: true,
                    onRatingUpdate: (rating) {
                      print(rating);
                      // widget.reviewModel!.rating;
                    },
                  ),
                  //Icon(Icons.star,color: Colors.amber,),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    widget.productCardModel!.salePrice!.isNotEmpty
                        ? '৳${widget.productCardModel!.salePrice}'
                        : "",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: salePriceColor, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.productCardModel!.regularPrice!.isNotEmpty
                            ? '৳${widget.productCardModel!.regularPrice}'
                            : "",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: previousPriceColor,
                            decoration: TextDecoration.lineThrough,
                            fontWeight: FontWeight.bold),
                      ),
                      //SizedBox(width: 20,),
                      Text(
                        discountPercentRound != null
                            ? '${(discountPercentRound)}% off'
                            : "",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: discountPriceColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
