import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/constants/links.dart';

import 'package:mart_up/models/category_model.dart';
import 'package:mart_up/screens/search_result_screen.dart';
//import 'package:mart_up/screens/search_screen.dart';

class CategoryCard extends StatefulWidget {
  final CategoryModel? cateoryModel;

  CategoryCard({this.cateoryModel});

  @override
  State<CategoryCard> createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SearchResultPage(
                          id: widget.cateoryModel!.id,
                          categoryName: widget.cateoryModel!.name,
                        )));
          },
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80),
              border: Border.all(
                color: borderColor,
              ),
            ),
            child: Center(
              child: Image.network(
              widget.cateoryModel!.image!.src !=null? '${widget.cateoryModel!.image!.src}':emptyImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          '${widget.cateoryModel!.name}',
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.grey, fontSize: 14),
        )
      ],
    );
  }
}
