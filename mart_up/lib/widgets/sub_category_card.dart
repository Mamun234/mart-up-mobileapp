import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/sub_category_card_model.dart';

// ignore: must_be_immutable
class SubCategoryCard extends StatefulWidget {
  SubCateoryCardModel? subCateoryCardModel;
  SubCategoryCard({this.subCateoryCardModel});

  @override
  State<SubCategoryCard> createState() => _SubCategoryCardState();
}

class _SubCategoryCardState extends State<SubCategoryCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
      child: Column(
        children: [
          Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80),
              border: Border.all(
                color: borderColor,
              ),
            ),
            child: Center(
              child: Image.asset(
                '${widget.subCateoryCardModel!.image}',
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            '${widget.subCateoryCardModel!.name}',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.grey, fontSize: 14),
          )
        ],
      ),
    );
  }
}
