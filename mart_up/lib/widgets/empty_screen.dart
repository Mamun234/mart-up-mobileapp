import 'package:flutter/material.dart';

// ignore: must_be_immutable
class EmptyScreen extends StatefulWidget {
  String? title;
  EmptyScreen({this.title});

  @override
  State<EmptyScreen> createState() => _EmptyScreenState();
}

class _EmptyScreenState extends State<EmptyScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 400,
      // color: Colors.amberAccent,
      child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 200,
            ),
            Container(
              height: 100,
              width: 200,
              child: Image.asset(
                'assets/images/empty.png',
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              widget.title!,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
