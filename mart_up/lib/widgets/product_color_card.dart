import 'package:flutter/material.dart';

class ProductColorCard extends StatefulWidget {
  const ProductColorCard({Key? key}) : super(key: key);

  @override
  _ProductColorCardState createState() => _ProductColorCardState();
}

class _ProductColorCardState extends State<ProductColorCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        color: Colors.amber,
        borderRadius: BorderRadius.circular(40),
      ),
    );
  }
}
