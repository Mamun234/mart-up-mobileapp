import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/order_details_model.dart';
//import 'package:mart_up/models/order_get_model.dart';
import 'package:mart_up/widgets/dash_line.dart';

// ignore: must_be_immutable
class PaymentDetailsCard extends StatefulWidget {
  OrderDetailsModel? paymenyDetails;
  PaymentDetailsCard({this.paymenyDetails});

  @override
  _PaymentDetailsCardState createState() => _PaymentDetailsCardState();
}

class _PaymentDetailsCardState extends State<PaymentDetailsCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      child: Padding(
        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        'item: ',
                        style: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  Text(
                    '${widget.paymenyDetails?.lineItems!.length}',
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Shiping',
                    style: TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.w700),
                  ),
                  Text(
                    '${widget.paymenyDetails?.shippingTotal}',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 20,
              ),
              DashLine(
                color: Colors.grey,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w700),
                  ),
                  Text(
                    double.parse('${widget.paymenyDetails?.total}').toString(),
                    style: TextStyle(color: tabSelectColor),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
