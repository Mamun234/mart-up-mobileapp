import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:mart_up/constants/colors.dart';

import 'package:mart_up/models/product.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class CompareProduct extends StatefulWidget {
  Product? compare;
  List<Product>? compareList;
  int? index;
  CompareProduct({this.compare, this.index, this.compareList});

  @override
  _CompareProductState createState() => _CompareProductState();
}

class _CompareProductState extends State<CompareProduct> {
  late ViewModelProvider viewModelProvider;
  @override
  void initState() {
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);

    super.initState();
    viewModelProvider.getCompare();
    //getList();
  }

  // getList()async{
  //   viewModelProvider=  Provider.of<ViewModelProvider>(context,listen: false);
  // }
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Container(
          width: ScreenUtil().setWidth(150),
          child: Column(
            children: [
              Container(
                child: Center(
                  child: Stack(children: [
                    Image.network(widget.compare!.images![0].src!),
                    Positioned(
                        right: 1,
                        top: 5,
                        child: IconButton(
                            onPressed: () async {
                              //await widget.compare!='';
                              await viewModelProvider.compareProviderList!
                                  .removeAt(widget.index!);
                              //await widget.compareList!.removeAt(widget.index!);

                              await viewModelProvider.getCompare();

                              print(viewModelProvider
                                  .compareProviderList!.length);
                              print('index:${widget.index!}');
                            },
                            icon: Icon(
                              Icons.delete_outline_outlined,
                              color: Colors.grey,
                            )))
                  ]),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                widget.compare!.name!,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                '৳${widget.compare!.salePrice!}',
                // maxLines: 2,
                // overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: tabSelectColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                '৳${widget.compare!.regularPrice!}',
                // maxLines: 2,
                // overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.lineThrough,
                    fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                '24%Off',
                // maxLines: 2,
                // overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Nike',
                // maxLines: 2,
                // overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              HtmlWidget(
                widget.compare!.shortDescription!,
                textStyle: TextStyle(color: Colors.grey),
              ),
              // Text(widget.compare!.shortDescription!,
              // maxLines: 10,
              // overflow: TextOverflow.ellipsis,
              // style: TextStyle(color: Colors.grey,),
              // ),
              SizedBox(
                height: 20,
              ),
              HtmlWidget(
                widget.compare!.description!,
                textStyle: TextStyle(color: Colors.grey),
              ),
              // Text(widget.compare!.description!,
              // maxLines: 10,
              // overflow: TextOverflow.ellipsis,
              // style: TextStyle(color: Colors.grey,),
              // ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
