import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderCardScreen extends StatefulWidget {
  const LoaderCardScreen({Key? key}) : super(key: key);

  @override
  _LoaderCardScreenState createState() => _LoaderCardScreenState();
}

class _LoaderCardScreenState extends State<LoaderCardScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 120,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.grey.shade400,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            children: [
              SkeletonAvatar(style: SkeletonAvatarStyle(width: 70, height: 80)),
              SizedBox(
                width: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 90,
                            // alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        width: 90,
                      ),
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(width: 30, height: 25)),
                      SizedBox(
                        width: 15,
                      ),
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(width: 30, height: 25)),
                    ],
                  ),
                  Row(
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 70,
                            //alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        width: 50,
                      ),
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(
                        width: 140,
                        height: 30,
                      )),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
