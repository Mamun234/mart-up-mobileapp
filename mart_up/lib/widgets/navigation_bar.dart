import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/product.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/account_screen.dart';
import 'package:mart_up/screens/cart_screen.dart';
import 'package:mart_up/screens/category_screen.dart';
import 'package:mart_up/screens/comapre_screen.dart';
import 'package:mart_up/screens/home_screen.dart';
import 'package:mart_up/screens/offer_screen.dart';
import 'package:mart_up/screens/search_result_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomNavigationBar extends StatefulWidget {
  const CustomNavigationBar({Key? key}) : super(key: key);

  @override
  _CustomNavigationBar createState() => _CustomNavigationBar();
}

class _CustomNavigationBar extends State<CustomNavigationBar> {
  int _tabIndex = 0;
  var dbHelper;
  Future<int>? cartItemCount;
  // final double? _iconHeight = 25;
  // final double? _iconWidth = 25;
  int? itemCount;
  ViewModelProvider? viewModelProvider;
  List<Widget> tabPages = [
    HomePage(),
    //CategoryPage(),
    SearchResultPage(),
    CartPage(),
    ComparePage(),
    OfferPage(),
    AccountPage(),
  ];

  @override
  void initState() {
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    viewModelProvider!.getCartModel();
    viewModelProvider!.getCompare();
    //getCom();
    dbHelper = DBHelper();
    // setState(() {
    //   getQuantity();
    // });
    // setState(() {
    //   itemCount=viewModelProvider!.cartListN!.length;
    // });
  }
    getCom()async{
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
List<String>? items = sharedPreferences.getStringList('com');
//List<Product> decoded=items.map((e)=>jsonDecode(e.fromJson));
 
List<Product> decoded=items!.map((e) => Product.fromJson(jsonDecode(e))).toList();

//List<Product> b = items.map((string) => Product()).toList();
if(decoded.isNotEmpty){

  
    viewModelProvider!.compareProviderList=decoded;


}


setState(()async{
  //selectedProduct = await viewModelProvider.compareProviderList![0];
});



 print('getcom: ${decoded.length}');
}
     //    getQuantity()async{
//     setState(() {
//       cartItemCount = DBHelper().getCartListProductsCount();
//     });
//   }

// _buildQuentityNumber() {
//     return FutureBuilder(
//         future: cartItemCount,
//         builder: (context, snapshot) {
//           if (snapshot.hasData) {
//             //totalQuantity = snapshot.data;
//             return Text('${snapshot.data.toString()}',
//                 style: TextStyle(fontSize: 12,color: Colors.white));
//           }

//           if (null == snapshot.data) {
//             return Text("0", style: TextStyle(fontSize: 12));
//           }
//           return CircularProgressIndicator();
//         });

// }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: primaryBGColor,
      body: SafeArea(child: tabPages[_tabIndex]),
      bottomNavigationBar: SizedBox(
        height: 80,
        // color: Colors.amber,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          //backgroundColor: Colors.amber,
          currentIndex: _tabIndex,
          onTap: (int index) {
            setState(() {
              _tabIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Icon(
                  Icons.home_outlined,
                  color:
                      _tabIndex == 0 ? tabSelectColor : const Color(0xffCCCDCE),
                ),
              ),
              label: 'Home',
              backgroundColor:
                  _tabIndex == 0 ? tabSelectColor : const Color(0xffCCCDCE),
            ),
            // BottomNavigationBarItem(
            //   icon: Padding(
            //     padding: const EdgeInsets.only(bottom: 5),
            //     child: Icon(
            //       Icons.search_outlined,
            //       color:
            //           _tabIndex == 1 ? tabSelectColor : const Color(0xffCCCDCE),
            //     ),
            //   ),
            //   label: 'Explore',
            //   backgroundColor:
            //       _tabIndex == 1 ? tabSelectColor : const Color(0xffCCCDCE),
            // ),
             BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Icon(
                  Icons.search_outlined,
                  color:
                      _tabIndex == 1 ? tabSelectColor : const Color(0xffCCCDCE),
                ),
              ),
              label: 'Search',
              backgroundColor:
                  _tabIndex == 1 ? tabSelectColor : const Color(0xffCCCDCE),
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Stack(
                  children: [
                    Container(
                      height: 35,
                      //color: Colors.amber,
                      child: Icon(
                        Icons.shopping_cart_outlined,
                        color: _tabIndex == 2
                            ? tabSelectColor
                            : const Color(0xffCCCDCE),
                      ),
                    ),
                    Positioned(
                      left: 8,
                      bottom: 20,
                      child: Container(
                          height: 15,
                          width: 15,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Consumer<ViewModelProvider>(
                              builder: (context, viewModel, child) {
                            return Text(
                              '${viewModelProvider!.cartListN!.length}',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            );
                          })),
                    )
                  ],
                ),
              ),
              label: 'Cart',
            ),
             BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Stack(
                  children: [
                    Container(
                      height: 35,
                      //color: Colors.amber,
                      child: Icon(
                        Icons.compare_arrows_outlined,
                        color: _tabIndex == 3
                            ? tabSelectColor
                            : const Color(0xffCCCDCE),
                      ),
                    ),
                    Positioned(
                      left: 8,
                      bottom: 20,
                      child: Container(
                          height: 15,
                          width: 15,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Consumer<ViewModelProvider>(
                              builder: (context, viewModel, child) {
                            return Text(
                              '${viewModelProvider!.compareProviderList!.length}',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            );
                          })),
                    )
                  ],
                ),
              ),
              label: 'Compare',
            ),
           
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Icon(
                  Icons.local_offer_outlined,
                  color:
                      _tabIndex == 4 ? tabSelectColor : const Color(0xffCCCDCE),
                ),
              ),
              label: 'Offer',
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Icon(
                  Icons.account_box_outlined,
                  color:
                      _tabIndex == 5 ? tabSelectColor : const Color(0xffCCCDCE),
                ),
              ),
              label: 'Account',
            ),
          ],
        ),
      ),
    );
  }
}
