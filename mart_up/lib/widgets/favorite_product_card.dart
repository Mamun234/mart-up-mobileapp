// import 'package:flutter/material.dart';
// import 'package:mart_up/constants/colors.dart';
// import 'package:mart_up/models/favorite_model.dart';
// import 'package:mart_up/models/product_card_model.dart';
// import 'package:mart_up/models/product_list.dart';
// import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'package:mart_up/screens/product_details_screen.dart';
// import 'package:mart_up/widgets/rating.dart';

// class FavoritProductCard extends StatefulWidget {
//   //final double? height,width;
//   final FavoriteModel? fm;

//   FavoritProductCard({this.fm});

//   @override
//   State<FavoritProductCard> createState() => _FavoritProductCardState();
// }

// class _FavoritProductCardState extends State<FavoritProductCard> {
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         Navigator.push(context,
//             MaterialPageRoute(builder: (context) => ProductDetailPage()));
//       },
//       child: Card(
//         elevation: 2.0,
//         shadowColor: Colors.black38,
//         child: Container(
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(100),
//           ),
//           height: 280,
//           width: 140,
//           child: Padding(
//             padding: EdgeInsets.all(12.0),
//             child: Container(
//               child: Column(
//                 //mainAxisAlignment: MainAxisAlignment.start,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     height: 100,
//                     //width: 80,

//                     child: Center(
//                       child: Image.asset(
//                         '${widget.productCardModel!.image}',
//                         fit: BoxFit.cover,
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Text(
//                     '${widget.productCardModel!.name}',
//                     overflow: TextOverflow.ellipsis,
//                     maxLines: 2,
//                     style: TextStyle(
//                         color: productNameColor, fontWeight: FontWeight.bold),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Rating(),
//                   //Icon(Icons.star,color: Colors.amber,),
//                   SizedBox(
//                     height: 20,
//                   ),
//                   Text(
//                     '\$299,34',
//                     maxLines: 1,
//                     overflow: TextOverflow.ellipsis,
//                     style: TextStyle(
//                         color: salePriceColor, fontWeight: FontWeight.bold),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),

//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     //crossAxisAlignment: CrossAxisAlignment.,
//                     children: [
//                       Text(
//                         '\$545,98',
//                         maxLines: 1,
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                             color: previousPriceColor,
//                             decoration: TextDecoration.lineThrough,
//                             fontWeight: FontWeight.bold),
//                       ),
//                       //SizedBox(width: 20,),
//                       Text(
//                         '24% off',
//                         maxLines: 1,
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                             color: discountPriceColor,
//                             fontWeight: FontWeight.bold),
//                       ),
//                       Icon(
//                         Icons.delete_outlined,
//                         size: 30,
//                         color: Colors.grey,
//                       ),
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
