import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mart_up/models/order_details_model.dart';
//import 'package:mart_up/models/order_get_model.dart';

// ignore: must_be_immutable
class ShipingDetailsCard extends StatefulWidget {
  OrderDetailsModel? shipingDetails;
  ShipingDetailsCard({this.shipingDetails});

  @override
  _ShipingDetailsCardState createState() =>
      _ShipingDetailsCardState(this.shipingDetails);
}

class _ShipingDetailsCardState extends State<ShipingDetailsCard> {
  OrderDetailsModel? shipingDetails;
  _ShipingDetailsCardState(this.shipingDetails);

  // var value = shipingDetails;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3.0,
      shadowColor: Colors.black54,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Container(
          height: 130,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Shiping date",
                    style: TextStyle(fontSize: 18, color: Colors.grey),
                  ),
                  Text(
                    DateFormat('dd MMM yyyy').format(DateTime.parse(
                        '${widget.shipingDetails?.dateCreated}')),
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Order Number",
                    style: TextStyle(fontSize: 18, color: Colors.grey),
                  ),
                  Text(
                    '${widget.shipingDetails?.id}',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      "Address",
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      '${widget.shipingDetails?.shipping!.address2}',
                      textAlign: TextAlign.end,
                      maxLines: 2,
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
