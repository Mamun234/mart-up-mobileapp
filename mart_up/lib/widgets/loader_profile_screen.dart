import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderProfile extends StatefulWidget {
  const LoaderProfile({Key? key}) : super(key: key);

  @override
  _LoaderProfileState createState() => _LoaderProfileState();
}

class _LoaderProfileState extends State<LoaderProfile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: double.infinity,
                height: 110,
                child: Row(
                  children: [
                    SkeletonAvatar(
                      style: SkeletonAvatarStyle(
                          shape: BoxShape.circle, width: 100, height: 100),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Column(
                        children: [
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 6,
                                width: 90,
                                alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          SkeletonLine(
                            style: SkeletonLineStyle(
                                height: 6,
                                width: 70,
                                // alignment: Alignment.center,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 22,
            ),

            //email
            Padding(
              padding: const EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
              ),
              child: Container(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 6,
                          width: 300,
                          //alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
            ),

            //Number
            Padding(
              padding: const EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
              ),
              child: Container(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SkeletonLine(
                      style: SkeletonLineStyle(
                          height: 6,
                          width: 300,
                          alignment: Alignment.center,
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
