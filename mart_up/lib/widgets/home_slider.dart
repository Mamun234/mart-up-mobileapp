import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:mart_up/models/slider_model.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:mart_up/constants/colors.dart';
// import 'package:mart_up/models/product.dart';
// import 'package:mart_up/screens/home_screen.dart';

// ignore: must_be_immutable
class HomeCarouselWithDotsPage extends StatefulWidget {
  List<SliderModel>? imgList1;

  HomeCarouselWithDotsPage({this.imgList1});

  @override
  _HomeCarouselWithDotsPageState createState() =>
      _HomeCarouselWithDotsPageState();
}

class _HomeCarouselWithDotsPageState extends State<HomeCarouselWithDotsPage> {
  final CarouselController _controller = CarouselController();
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = widget.imgList1!
        .map((item) => Container(
              child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
                child: Stack(
                  children: [
                    Image.network(
                      item.thumbnailImg!,
                      fit: BoxFit.cover,
                      width: 1000,
                    ),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                          horizontal: 20,
                          vertical: 10,
                        ),
                        // child: Text(
                        //   'No. ${widget.imgList1!.indexOf(item)} image',
                        //   style: TextStyle(
                        //     color: Colors.white,
                        //     fontSize: 20.0,
                        //     fontWeight: FontWeight.bold,
                        //   ),
                        // ),
                      ),
                    ),
                  ],
                ),
              ),
            ))
        .toList();

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
        ),
        CarouselSlider(
          items: imageSliders,
          carouselController: _controller,
          options: CarouselOptions(
              viewportFraction: 1,
              autoPlay: true,
              enlargeCenterPage: true,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget.imgList1!.map((url) {
            int index = widget.imgList1!.indexOf(url);
            return Container(
              width: 5,
              height: 5,
              margin: EdgeInsets.symmetric(
                vertical: 6,
                horizontal: 5,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index ? Colors.blueAccent : Colors.grey,
              ),
            );
          }).toList(),
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            // Flexible(
            //   child: ElevatedButton(
            //     onPressed: () => _controller.previousPage(),
            //     child: Text('←'),
            //   ),
            // ),
            // Flexible(
            //   child: ElevatedButton(
            //     onPressed: () => _controller.nextPage(),
            //     child: Text('→'),
            //   ),
            // ),
            // ...Iterable<int>.generate(imgList1.length).map(
            //   (int pageIndex) =>
            //     //  ElevatedButton(
            //     //   onPressed: () => _controller.animateToPage(pageIndex),
            //     //   child: Text("$pageIndex"),
            //     // ),
            //     Container(
            //        height: 80,
            //        width: 80,
            //        decoration: BoxDecoration(
            //          //color: Colors.amber,
            //          borderRadius: BorderRadius.circular(10),
            //          border: Border.all(
            //            color: tabSelectColor,
            //          )
            //        ),
            //        child: Center(
            //         //  child:  Image.asset('assets/images/shoe.png')
            //          child:  Image.network(imgList1[pageIndex].)
            //        ),
            //      ),
            // ),
// widget.imgList1[index].src

            //   Container(
            //     // height: ScreenUtil().setHeight(70),
            //     height: ScreenUtil().setHeight(90),
            //     //width: ScreenUtil().setWidth(90),
            //     color: Colors.amber,

            //     child: ListView.builder(

            //       scrollDirection: Axis.horizontal,
            //       itemCount: widget.imgList1.length,
            //       shrinkWrap: true,

            //       itemBuilder: (context,index){
            //         return GestureDetector(
            //  onTap:(){
            //    //_controller.animateToPage(pageIndex);
            //  },
            //           child: Padding(
            //             padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(8)),
            //             child: Container(
            //               height: ScreenUtil().setHeight(30),
            //               width: ScreenUtil().setWidth(50),
            //                decoration: BoxDecoration(
            //                //color: Colors.amber,
            //                borderRadius: BorderRadius.circular(10),
            //                border: Border.all(
            //                  color: tabSelectColor,
            //                )
            //              ),
            //               child: Center(child: Image.network('${widget.imgList1[index].src}',fit: BoxFit.cover,))
            //               ),
            //           ),
            //         );
            //        // return Text('${widget.imgList1[index].src}');
            //     }
            //     ),
            //
            //               ),

            //     Container(
            //   //color: Colors.amber,
            //   height: ScreenUtil().setHeight(80),
            //   child: ListView.builder(
            //       scrollDirection: Axis.horizontal,
            //       shrinkWrap: true,

            //       physics: BouncingScrollPhysics(),
            //       itemCount: widget.imgList1.length,

            //       itemBuilder: (BuildContext context, index) {

            //         return

            //          GestureDetector(
            //            onTap: ()=> _controller.animateToPage(_current),
            //            child: Padding(
            //             padding:  EdgeInsets.symmetric(horizontal: 8),
            //             child: Container(
            //               height: 40,
            //               width: 40,

            //               child: Center(child: Image.network('${widget.imgList1[index].src}',fit: BoxFit.cover,)),
            //             ),
            //                                  ),
            //          );
            //       }),
            // ),
          ],
        )
      ],
    );
  }
}
