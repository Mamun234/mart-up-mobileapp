import 'package:flutter/cupertino.dart';
import 'package:skeletons/skeletons.dart';

class LoaderBrand extends StatefulWidget {
  const LoaderBrand({Key? key}) : super(key: key);

  @override
  _LoaderBrandState createState() => _LoaderBrandState();
}

class _LoaderBrandState extends State<LoaderBrand> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisExtent: 120,
          ),
          itemCount: 9,
          itemBuilder: (BuildContext context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SkeletonAvatar(
                    style: SkeletonAvatarStyle(width: 60, height: 60)),
                SizedBox(
                  height: 5,
                ),
                SkeletonLine(
                  style: SkeletonLineStyle(
                      height: 10,
                      width: 60,
                      alignment: Alignment.center,
                      borderRadius: BorderRadius.circular(6)),
                )
              ],
            );
          }),
    );
  }
}
