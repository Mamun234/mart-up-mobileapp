import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoaderCategory extends StatefulWidget {
  const LoaderCategory({ Key? key }) : super(key: key);

  @override
  _LoaderCategoryState createState() => _LoaderCategoryState();
}

class _LoaderCategoryState extends State<LoaderCategory> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:  GridView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisExtent: 120,
                ),
                itemCount: 6,
                itemBuilder: (BuildContext context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            shape: BoxShape.circle, width: 60, height: 60),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 6,
                            width: 60,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      )
                    ],
                  );
                }),
      
    );
  }
}