import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class LoderReview extends StatefulWidget {
  const LoderReview({Key? key}) : super(key: key);

  @override
  _LoderReviewState createState() => _LoderReviewState();
}

class _LoderReviewState extends State<LoderReview> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          mainAxisExtent: 200,
        ),
        itemCount: 6,
        itemBuilder: (BuildContext context, index) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: (Container(
              decoration: BoxDecoration(
                color: Colors.grey.shade400,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 110,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 12, top: 12),
                          child: SkeletonAvatar(
                            style: SkeletonAvatarStyle(
                                shape: BoxShape.circle, width: 90, height: 90),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 60, left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SkeletonLine(
                                style: SkeletonLineStyle(
                                    height: 6,
                                    width: 70,
                                    // alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(6)),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SkeletonLine(
                                style: SkeletonLineStyle(
                                    height: 6,
                                    width: 100,
                                    // alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(6)),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),

                  //email
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 300,
                            //alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),

                  //Number
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 300,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),

                  //Number
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 300,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),

                  //Number
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 300,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),

                  //Number
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 300,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),

                  //Number
                  Padding(
                    padding: const EdgeInsets.only(left: 35),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 6,
                              width: 90,
                              alignment: Alignment.center,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
          );
        });
  }
}
