import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';

import 'package:mart_up/models/order_details_model.dart';
import 'package:mart_up/screens/product_details_screen.dart';

// ignore: must_be_immutable
class OrderItemCard extends StatefulWidget {
  OrderDetailsModel? orderDetailsModel;
  LineItem? products;
  OrderItemCard({this.orderDetailsModel, this.products});

  @override
  _OrderItemCardState createState() => _OrderItemCardState();
}

class _OrderItemCardState extends State<OrderItemCard> {
  bool btnShowstatus = true;
  int orderDetailsToWriteReview=1;
  @override
  void initState() {
     
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetailPage(
                      id: widget.products?.productId,
                      btnShowstatus: btnShowstatus,orderDetailsToWriteReview: orderDetailsToWriteReview,
                    )));
      },
      child: Card(
        elevation: 1,
        child: Container(
          height: 100,
          child: Padding(
            padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text(
                      '${widget.products?.name}',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: ScreenUtil().setSp(16.0)),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'x${widget.products!.quantity}',
                      style: TextStyle(
                          color: Colors.grey,
                          // fontWeight: FontWeight.bold,
                          fontSize: ScreenUtil().setSp(16.0)),
                    ),
                  ],
                ),
                Text(
                  double.parse('${widget.products!.subtotal}').toString(),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: tabSelectColor,
                      fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(16)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
