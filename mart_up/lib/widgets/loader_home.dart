import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class Loader extends StatefulWidget {
  Loader();

  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            SkeletonAvatar(
                style:
                    SkeletonAvatarStyle(width: double.infinity, height: 160)),
            SizedBox(
              height: 14,
            ),
            GridView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisExtent: 120,
                ),
                itemCount: 6,
                itemBuilder: (BuildContext context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            shape: BoxShape.circle, width: 60, height: 60),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 60,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      )
                    ],
                  );
                }),
            GridView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisExtent: 120,
                ),
                itemCount: 6,
                itemBuilder: (BuildContext context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(width: 60, height: 60)),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 60,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      )
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }
}
