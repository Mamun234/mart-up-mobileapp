import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:skeletons/skeletons.dart';

class LoaderCompare extends StatefulWidget {
  const LoaderCompare({Key? key}) : super(key: key);

  @override
  _LoaderCompareState createState() => _LoaderCompareState();
}

class _LoaderCompareState extends State<LoaderCompare> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Container(
            width: ScreenUtil().setWidth(150),
            height: ScreenUtil().setHeight(double.infinity),
            decoration: BoxDecoration(
              color: Colors.grey.shade400,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Column(
                    children: [
                      SkeletonAvatar(
                          style: SkeletonAvatarStyle(
                              width: double.infinity, height: 140)),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 70,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 110,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 50,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 110,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 50,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 70,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 120,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      //dec
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),

                      //dec
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 10,
                            width: 120,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(
                        style: SkeletonLineStyle(
                            height: 3,
                            width: 140,
                            alignment: Alignment.center,
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            child: GridView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                  mainAxisExtent: 210,
                ),
                itemCount: 3,
                itemBuilder: (BuildContext context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: (Container(
                      decoration: BoxDecoration(
                        color: Colors.grey.shade400,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: [
                            Column(
                              children: [
                                SkeletonAvatar(
                                    style: SkeletonAvatarStyle(
                                        width: double.infinity, height: 140)),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 70,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 110,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 50,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 110,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 50,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 70,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 120,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                //dec
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),

                                //dec
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 10,
                                      width: 120,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  style: SkeletonLineStyle(
                                      height: 3,
                                      width: 140,
                                      alignment: Alignment.center,
                                      borderRadius: BorderRadius.circular(6)),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )),
                  );
                }),
          ),
        )
      ],
    );
  }
}
