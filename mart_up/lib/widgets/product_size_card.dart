import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';

class ProductSizeCard extends StatefulWidget {
  const ProductSizeCard({Key? key}) : super(key: key);

  @override
  _ProductSizeCardState createState() => _ProductSizeCardState();
}

class _ProductSizeCardState extends State<ProductSizeCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          border: Border.all(
            color: borderColor,
          )),
      child: Center(
        child: Text(
          '7',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
