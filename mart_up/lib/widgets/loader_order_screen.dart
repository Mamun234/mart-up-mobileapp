import 'package:flutter/material.dart';
import 'package:mart_up/widgets/dash_line.dart';
import 'package:skeletons/skeletons.dart';

class LoaderOrderScreen extends StatefulWidget {
  const LoaderOrderScreen({Key? key}) : super(key: key);

  @override
  _LoaderOrderScreenState createState() => _LoaderOrderScreenState();
}

class _LoaderOrderScreenState extends State<LoaderOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: GridView.builder(
            shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              mainAxisExtent: 200,
            ),
            itemCount: 6,
            itemBuilder: (BuildContext context, index) {
              return (Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  height: 190,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SkeletonLine(
                          style: SkeletonLineStyle(
                              height: 10,
                              width: 50,
                              borderRadius: BorderRadius.circular(6)),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 90,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 90,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        DashLine(color: Colors.grey.withOpacity(0.3)),
                        SizedBox(
                          height: 18,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 80,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 80,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 70,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 60,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 50,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            SkeletonLine(
                              style: SkeletonLineStyle(
                                  height: 10,
                                  width: 50,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ));
            }));
  }
}
