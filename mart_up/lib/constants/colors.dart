import 'package:flutter/material.dart';

var lebelColor= Color(0xff0F0F10);
var themeColor=Color(0xff40BFFF);
var tabSelectColor=Color(0xff40BFFF);
var brandNameColor=Color(0xff000000);
var productNameColor=Color(0xff223263);
var salePriceColor=Color(0xff40BFFF);
var previousPriceColor=Color(0xff9098B1);
var discountPriceColor=Color(0xffFB7181);
var borderColor=Color(0xffEBF0FF);