import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mart_up/models/user_model.dart';
import 'package:mart_up/services/user_service.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'LoginScreen.dart';
//import 'package:mart_up/widgets/roundedButton.dart' as eos;

class RegisterScreen extends StatefulWidget {
  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final formKey = GlobalKey<FormState>(); //key for form
  String name = "";
  bool _obscureText = true;
  bool _isLoading = false;
  TextEditingController fullName = TextEditingController();

  TextEditingController email = TextEditingController();

  TextEditingController password = TextEditingController();

  TextEditingController confirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(builder: (context) {
        return Scaffold(
          body: SafeArea(
            child: Form(
              key: formKey,
              child: Container(
                margin: EdgeInsets.all(25),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Image.asset(
                        "assets/images/enlight.png",
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      const Text(
                        "Let's go started",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Create an new account",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          //       decoration: BoxDecoration(
                          // border: Border.all(color: Colors.black26, width: 1.5),
                          // borderRadius: BorderRadius.circular(16),
                          //       ),
                          child: TextFormField(
                            controller: fullName,
                            decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.symmetric(vertical: 16),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              hintText: "Full Name",
                              prefixIcon: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Icon(
                                  Icons.person,
                                  size: 30,
                                ),
                              ),
                              //hintStyle: kBodyText,
                            ),
                            validator: (value) {
                              if (value!.isEmpty ||
                                  !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                                return "Enter correct Name";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      // TextInput(
                      //   controller: fullName,
                      //   hint: "Full Name",
                      //   icon: Icons.person,

                      // ),
                      // TextInput(
                      //   controller: email,
                      //   hint: "Email",
                      //   icon: Icons.email,
                      // ),

                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          //       decoration: BoxDecoration(
                          // border: Border.all(color: Colors.black26, width: 1.5),
                          // borderRadius: BorderRadius.circular(16),
                          //       ),
                          child: TextFormField(
                            controller: email,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.symmetric(vertical: 16),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              hintText: "Email",
                              prefixIcon: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Icon(
                                  Icons.email,
                                  size: 30,
                                ),
                              ),
                              //hintStyle: kBodyText,
                            ),
                            validator: (value) {
                              if (value!.isEmpty ||
                                  !RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}')
                                      .hasMatch(value)) {
                                return "Enter correct Email";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      // PasswordInput(
                      //   pController: password,
                      //   icon: Icons.lock,
                      //   hint: "Password",
                      // ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          //       decoration: BoxDecoration(
                          // border: Border.all(color: Colors.black26, width: 1.5),
                          // borderRadius: BorderRadius.circular(16),
                          //       ),
                          child: TextFormField(
                            controller: password,
                            decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: "Password",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(Icons.lock),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            obscureText: _obscureText,
                            validator: (value) {
                              if (value!.isEmpty ||
                                  value != confirmPassword.text) {
                                return "Enter correct Password";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      // PasswordInput(
                      //   pController: confirmPassword,
                      //   icon: Icons.lock,
                      //   hint: "Confirm password",
                      // ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          //       decoration: BoxDecoration(
                          // border: Border.all(color: Colors.black26, width: 1.5),
                          // borderRadius: BorderRadius.circular(16),
                          //       ),
                          child: TextFormField(
                            controller: confirmPassword,
                            decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: "Confirm Password",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(Icons.lock),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            obscureText: _obscureText,
                            validator: (value) {
                              if (value!.isEmpty || value != password.text) {
                                return "Enter correct Password";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      _isLoading
                          ? BuildLoading()
                          : RoundedButton(
                              buttonText: "Sign up",
                              func: () async {
                                // userModel!.firstName=fullName.text;
                                // userModel!.email=email.text;
                                // userModel!.password=password.text;

                                UserModel userModel = UserModel(
                                    firstName: fullName.text,
                                    email: email.text,
                                    password: password.text);
                                if (formKey.currentState!.validate()) {
                                  setState(() {
                                    _isLoading = true;
                                  });
                                  Response? data =
                                      await UserService().createUser(userModel);
                                  if (data!.statusCode < 300) {
                                    await CustomSnackbar.snackbar(context,
                                        "Registration Succesfull, Please Login");
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                LoginScreen()));
                                  } else {
                                    setState(() {
                                      _isLoading = false;
                                    });
                                    CustomSnackbar.snackbar(
                                        context, "Registration Failed");
                                  }
                                }
                              }),
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Have a account?",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            SizedBox(width: 10,),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()));
                              },
                              child: Text(
                                "Sign In",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}

// class TextInput extends StatefulWidget {
//   final IconData icon;
//   final String hint;
//   TextEditingController? controller;

//    TextInput({ required this.icon, required this.hint,this.controller});

//   @override
//   State<TextInput> createState() => _TextInputState();
// }

// class _TextInputState extends State<TextInput> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 7.0),
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(color: Colors.black26, width: 1.5),
//           borderRadius: BorderRadius.circular(16),
//         ),
//         child: TextField(
//           decoration: InputDecoration(
//             contentPadding: const EdgeInsets.symmetric(vertical: 16),
//             border: InputBorder.none,
//             hintText: widget.hint,
//             prefixIcon: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16),
//               child: Icon(
//                 widget.icon,
//                 size: 30,
//               ),
//             ),
//             //hintStyle: kBodyText,
//           ),
//         ),
//       ),
//     );
//   }
// }

// class PasswordInput extends StatefulWidget {
//   final IconData icon;
//   final String hint;
//   TextEditingController? pController;

//    PasswordInput({Key? key, required this.icon, required this.hint,this.pController})
//       : super(key: key);

//   @override
//   State<PasswordInput> createState() => _PasswordInputState();
// }

// class _PasswordInputState extends State<PasswordInput> {
//   TextEditingController? pcontroller;

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 7.0),
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(color: Colors.black26, width: 1.5),
//           borderRadius: BorderRadius.circular(16),
//         ),
//         child: TextField(
//           decoration: InputDecoration(
//             contentPadding: const EdgeInsets.symmetric(vertical: 16),
//             border: InputBorder.none,
//             hintText: widget.hint,
//             prefixIcon: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16),
//               child: Icon(widget.icon),
//             ),
//           ),
//           obscureText: true,
//         ),
//       ),
//     );
//   }
// }
