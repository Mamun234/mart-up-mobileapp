import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:mart_up/constants/colors.dart';

import 'package:mart_up/models/product.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/product_details_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
//import 'package:mart_up/widgets/compare_product.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loader_compare_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class ComparePage extends StatefulWidget {
  List<Product>? compare;
  //int? discountPercentRound;
  ComparePage({this.compare});

  @override
  _ComparePageState createState() => _ComparePageState();
}

class _ComparePageState extends State<ComparePage> {
  late ViewModelProvider viewModelProvider;
  Product? selectedProduct;
  double? difference;
  double? discountPercent;
  int? discountPercentRound;
  //List<int>? discountList;
  int? selectedDiscount;
  bool _isLoading = true;

  @override
  void initState() {
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);

    super.initState();
    viewModelProvider.getCompare();
    viewModelProvider.compareDeleteCount;
    getCom();
    getDiscount();
    // _saveCompare();
    // saveDiscount();

    //getLists();
    //getLists();

    // getCompare();
    // _saveCompare();
  }

  setfirstProduct() async {
    selectedProduct = await viewModelProvider.compareProviderList![0];

    setState(() {});

    ///DBHelper().saveCompare(viewModelProvider.compareProviderList);
  }

  setFirstDiscount() async {
    selectedDiscount = await viewModelProvider.discountProvider![0];
    setState(() {});
  }

  void _saveCompare() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.remove('com');
    List<String> Encoded = viewModelProvider.compareProviderList!
        .map((e) => jsonEncode(e.toJson()))
        .toList();
    await sharedPreferences.setStringList('com', Encoded);

    print("save com: ${Encoded.length}");
  }

  getCom() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String>? items = sharedPreferences.getStringList('com');
//List<Product> decoded=items.map((e)=>jsonDecode(e.fromJson));
    if (items == null ||
        viewModelProvider.compareDeleteCount != 0 ||
        viewModelProvider.compareAddCount != 0) {
      setfirstProduct();
      _saveCompare();
      setState(() async {
        _isLoading = false;
      });
    } else {
      List<Product> decoded =
          items.map((e) => Product.fromJson(jsonDecode(e))).toList();

//List<Product> b = items.map((string) => Product()).toList();

      viewModelProvider.compareProviderList = decoded;

      setfirstProduct();
      _saveCompare();
      setState(() {
        //selectedProduct = await viewModelProvider.compareProviderList![0];
        _isLoading = false;
      });
      print('getcom: ${decoded.length}');
    }
  }

  saveDiscount() async {
    List<String> strList =
        viewModelProvider.discountProvider!.map((i) => i.toString()).toList();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    //sharedPreferences.remove('dis');
    await sharedPreferences.remove('dis');
    // List<String> Encoded = viewModelProvider.discountProvider!.map((e) => jsonEncode(e.toInt())).toList();
    await sharedPreferences.setStringList('dis', strList);

    print("save dis: ${strList.length}");
  }

  getDiscount() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String>? savedStrList = sharedPreferences.getStringList('dis');
    if (savedStrList == null ||
        viewModelProvider.compareDeleteCount != 0 ||
        viewModelProvider.compareAddCount != 0) {
      setFirstDiscount();
      saveDiscount();
      setState(() async {
        _isLoading = false;
      });
    } else {
      List<int> intProductList = savedStrList.map((i) => int.parse(i)).toList();

      viewModelProvider.discountProvider = intProductList;

      setFirstDiscount();
      saveDiscount();
      setState(() async {
        _isLoading = false;
      });
      print('dis len ${intProductList.length}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CustomNavigationBar()));
              //Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios_new_rounded),
            color: Colors.grey,
          ),
          title: Text(
            'Compare Product',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          // actions: [
          //   IconButton(
          //   onPressed: () {

          //     _saveCompare();
          //      saveDiscount();
          //   },
          //   icon: Icon(Icons.save_alt_outlined),
          //   color: Colors.grey,
          // ),
          //  IconButton(
          //   onPressed: () async{

          //      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          //      sharedPreferences.remove("com");
          //      sharedPreferences.remove("dis");
          //      setState(() {

          //      });
          //   },
          //   icon: Icon(Icons.remove),
          //   color: Colors.grey,
          // )
          // ],
        ),
        // body: SafeArea(
        //   child: Column(

        //     children: [
        // Container(
        //   height: 3000,
        //   color: Colors.grey,
        //   child: ListView.builder(
        //                   //reverse: true,
        //                    primary: false,
        //                  //physics: NeverScrollableScrollPhysics(),
        //                   scrollDirection: Axis.horizontal,
        //                   shrinkWrap: true,
        //                   itemCount: viewModelProvider.compareProviderList!.length,

        //                   itemBuilder: (BuildContext context, index) {
        //                     // var productCardModel =
        //                     //     _categories[index];

        //                     return Padding(
        //                       padding: const EdgeInsets.all(8.0),
        //                       child: (CompareProduct(compare:viewModelProvider.compareProviderList![index] ,)),
        //                     );
        //                   }),
        // ),
        //       Container(
        //         color: Colors.indigo,
        //         width: 100,
        //       ),
        //       Container(
        //         width: 100,
        //         color: Colors.red,
        //       )
        //     ],
        //   ),
        //   ),
        body: (_isLoading)
            ? LoaderCompare()
            : viewModelProvider.compareProviderList!.isEmpty
                ? Center(
                    child: Column(
                      children: [
                        SizedBox(height: 200),
                        Container(
                          height: 100,
                          width: 200,
                          child: Image.asset(
                            'assets/images/empty.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "No Product Added",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  )
                : Builder(builder: (context) {
                    return SafeArea(
                        child: SingleChildScrollView(
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: selectedProduct != null
                                ? Card(
                                    elevation: 1,
                                    child: Padding(
                                      padding: const EdgeInsets.all(6.0),
                                      child: Container(
                                        width: ScreenUtil().setWidth(140),
                                        height: ScreenUtil().setHeight(7000),
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Center(
                                                child: Stack(
                                                  children: [
                                                    Image.network(
                                                        selectedProduct!
                                                            .images![0].src!),
                                                    Positioned(
                                                        left: 6,
                                                        top: 18,
                                                        child: Icon(
                                                          Icons.lock_outline,
                                                          color: themeColor,
                                                        ))
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Text(
                                              "Name:",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              viewModelProvider
                                                          .compareProviderList!
                                                          .length >
                                                      0
                                                  ? selectedProduct!.name!
                                                  : '',
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Text(
                                              "Sale Price:",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            // Text(
                                            //   '৳${viewModelProvider
                                            //       .compareProviderList![
                                            //           index + 1]
                                            //       .salePrice!}',
                                            //   // maxLines: 2,
                                            //   // overflow: TextOverflow.ellipsis,
                                            //   style: TextStyle(
                                            //       color: tabSelectColor,
                                            //       fontWeight: FontWeight.bold,

                                            //       fontSize: 16),
                                            // ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Text(
                                              "Regular Price:",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            // Text(
                                            //   '৳${viewModelProvider
                                            //       .compareProviderList![
                                            //           index + 1]
                                            //       .regularPrice!}',
                                            //   // maxLines: 2,
                                            //   // overflow: TextOverflow.ellipsis,
                                            //   style: TextStyle(
                                            //       color: Colors.grey,
                                            //       fontWeight: FontWeight.bold,
                                            //       decoration: TextDecoration.lineThrough,
                                            //       fontSize: 16),
                                            // ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Text(
                                              "Discount:",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              selectedDiscount != null
                                                  ? '${selectedDiscount}%Off'
                                                  : '',
                                              // maxLines: 2,
                                              // overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.red,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                            ),
                                            // SizedBox(
                                            //   height: 20,
                                            // ),
                                            // Text(
                                            //   "Nike",
                                            //   // maxLines: 2,
                                            //   // overflow: TextOverflow.ellipsis,
                                            //   style: TextStyle(
                                            //       color: Colors.black,
                                            //       fontWeight: FontWeight.bold,
                                            //       fontSize: 18),
                                            // ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Text(
                                              "Short Description:",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            HtmlWidget(
                                              viewModelProvider
                                                          .compareProviderList!
                                                          .length >
                                                      0
                                                  ? selectedProduct!
                                                      .shortDescription!
                                                  : '',
                                              textStyle:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                            // Text(viewModelProvider.compareProviderList!.shortDescription!,
                                            // maxLines: 10,
                                            // overflow: TextOverflow.ellipsis,
                                            // style: TextStyle(color: Colors.grey,),
                                            // ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Text(
                                              "Description:",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            HtmlWidget(
                                              viewModelProvider
                                                          .compareProviderList!
                                                          .length >
                                                      0
                                                  ? selectedProduct!
                                                      .description!
                                                  : '',
                                              textStyle:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                            // Text(viewModelProvider.compareProviderList!.description!,
                                            // maxLines: 10,
                                            // overflow: TextOverflow.ellipsis,
                                            // style: TextStyle(color: Colors.grey,),
                                            // ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : Text("add product"),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(7018),
                            width: ScreenUtil().setWidth(200),
                            //color: Colors.grey,
                            child: viewModelProvider
                                        .compareProviderList!.length >
                                    1
                                ? ListView.builder(
                                    //reverse: true,
                                    // primary: false,
                                    physics: BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    itemCount: viewModelProvider
                                            .compareProviderList!.length -
                                        1,
                                    itemBuilder: (BuildContext context, index) {
                                      // var productCardModel =
                                      //     _categories[index];
                                      //int ind = index + 1;

                                      //  return GestureDetector(
                                      //    onTap: (){
                                      //      viewModelProvider.compareProviderList!.add(selectedProduct!);
                                      //      selectedProduct=viewModelProvider.compareProviderList![index+1];
                                      //      viewModelProvider.compareProviderList!.removeAt(index+1);

                                      //      setState(() {

                                      //      });
                                      //    },

                                      //    child: (CompareProduct(compare:viewModelProvider.compareProviderList![index+1] ,index: ind,)));
                                      return GestureDetector(
                                        onTap: () async {
                                          viewModelProvider.compareProviderList!
                                              .add(selectedProduct!);
                                          viewModelProvider.discountProvider!
                                              .add(selectedDiscount!);
                                          selectedProduct = viewModelProvider
                                              .compareProviderList![index + 1];
                                          selectedDiscount = viewModelProvider
                                              .discountProvider![index + 1];
                                          viewModelProvider.compareProviderList!
                                              .removeAt(index + 1);
                                          viewModelProvider.discountProvider!
                                              .removeAt(index + 1);

                                          setState(() {});
                                        },
                                        child: Card(
                                          elevation: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.all(6.0),
                                            child: Container(
                                              width: ScreenUtil().setWidth(150),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    child: Center(
                                                      child: Stack(children: [
                                                        Image.network(
                                                            viewModelProvider
                                                                .compareProviderList![
                                                                    index + 1]
                                                                .images![0]
                                                                .src!),
                                                        Positioned(
                                                            right: 1,
                                                            top: 5,
                                                            child: IconButton(
                                                                onPressed:
                                                                    () async {
                                                                  //await viewModelProvider.compareProviderList!='';
                                                                  viewModelProvider
                                                                      .compareProviderList!
                                                                      .removeAt(
                                                                          index +
                                                                              1);
                                                                  viewModelProvider
                                                                      .compareDeleteCount++;
                                                                  viewModelProvider
                                                                      .discountProvider!
                                                                      .removeAt(
                                                                          index +
                                                                              1);
                                                                  //                              SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

                                                                  //   await sharedPreferences.remove('com');
                                                                  //   //await sharedPreferences.remove(key)
                                                                  // await sharedPreferences.remove('dis');
                                                                  getCom();
                                                                  getDiscount();
                                                                  await viewModelProvider
                                                                      .getCompare();
                                                                  CustomSnackbar
                                                                      .snackbar(
                                                                          context,
                                                                          "product remove");
                                                                  //await viewModelProvider.compareProviderListList!.removeAt(widget.index!);
                                                                  setState(
                                                                      () {});
                                                                  //await viewModelProvider.getCompare();

                                                                  print(viewModelProvider
                                                                      .compareProviderList!
                                                                      .length);
                                                                  print(
                                                                      'index:${index}');
                                                                },
                                                                icon: Icon(
                                                                  Icons
                                                                      .delete_outline_outlined,
                                                                  color: Colors
                                                                      .grey,
                                                                ))),
                                                        Positioned(
                                                            left: 6,
                                                            top: 18,
                                                            child: Icon(
                                                              Icons
                                                                  .lock_open_outlined,
                                                              color: themeColor,
                                                            ))
                                                      ]),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    "Name:",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    viewModelProvider
                                                        .compareProviderList![
                                                            index + 1]
                                                        .name!,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    "Sale Price:",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    viewModelProvider
                                                        .compareProviderList![
                                                            index + 1]
                                                        .salePrice!,
                                                    // maxLines: 2,
                                                    // overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: tabSelectColor,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    "Regular Price:",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    viewModelProvider
                                                        .compareProviderList![
                                                            index + 1]
                                                        .regularPrice!,
                                                    // maxLines: 2,
                                                    // overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    "Discount:",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    viewModelProvider
                                                                .discountProvider !=
                                                            null
                                                        ? '${viewModelProvider.discountProvider![index + 1]}%Off'
                                                        : '',
                                                    // maxLines: 2,
                                                    // overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Colors.red,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  // SizedBox(
                                                  //   height: 20,
                                                  // ),
                                                  // Text(
                                                  //   'Nike',
                                                  //   // maxLines: 2,
                                                  //   // overflow: TextOverflow.ellipsis,
                                                  //   style: TextStyle(
                                                  //       color: Colors.black,
                                                  //       fontWeight: FontWeight.bold,
                                                  //       fontSize: 18),
                                                  // ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    "Short Description:",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  HtmlWidget(
                                                    viewModelProvider
                                                        .compareProviderList![
                                                            index + 1]
                                                        .shortDescription!,
                                                    textStyle: TextStyle(
                                                        color: Colors.grey),
                                                  ),
                                                  // Text(viewModelProvider.compareProviderList!.shortDescription!,
                                                  // maxLines: 10,
                                                  // overflow: TextOverflow.ellipsis,
                                                  // style: TextStyle(color: Colors.grey,),
                                                  // ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    "Description:",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  HtmlWidget(
                                                    viewModelProvider
                                                        .compareProviderList![
                                                            index + 1]
                                                        .description!,
                                                    textStyle: TextStyle(
                                                        color: Colors.grey),
                                                  ),
                                                  // Text(viewModelProvider.compareProviderList!.description!,
                                                  // maxLines: 10,
                                                  // overflow: TextOverflow.ellipsis,
                                                  // style: TextStyle(color: Colors.grey,),
                                                  // ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    })
                                : Center(child: Text("Add to compare")),
                          )
                        ],
                      ),
                    ));
                  })

        // : Center(
        //     child: Column(
        //       children: [
        //         SizedBox(height: 200),
        //         Container(
        //           height: 100,
        //           width: 200,
        //           child: Image.asset(
        //             'assets/images/empty.png',
        //             fit: BoxFit.cover,
        //           ),
        //         ),
        //         SizedBox(
        //           height: 20,
        //         ),
        //         Text(
        //           "No Product Added",
        //           style: TextStyle(
        //               color: Colors.black,
        //               fontSize: 20,
        //               fontWeight: FontWeight.bold),
        //         ),
        //       ],
        //     ),
        //   ),

        );
  }
}
