import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/models/product.dart';
import 'package:mart_up/models/profileInfoModel.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/comapre_screen.dart';
import 'package:mart_up/screens/loginScreen.dart';
import 'package:connectivity/connectivity.dart';
import 'package:mart_up/services/get_user_service.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/roundedButton.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  bool _isConnection = false;

  LoginResponseModel info = LoginResponseModel();
  ViewModelProvider? viewModelProvider;
  ProfileInfoModel? _users;

  @override
  void initState() {
    super.initState();
    navigateTo();
    getInfo();
    getCom();
    getDiscount();
    //getUser();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
  }

// :CustomSnackbar.snackbar(context, "No Internet")
//  await checkNetConnectivity();
  // _isConnection ?
  getInfo() async {
    var a = await DBHelper().getLoginUserData();
    setState(() {
      info = a!;
      //  if(info.data == null){
      //    viewModelProvider!.checkLogin=0;
      //  }
      print('info: ${info.data!.email}');
    });
    getUser();
  }

  getCom() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String>? items = sharedPreferences.getStringList('com');
//List<Product> decoded=items.map((e)=>jsonDecode(e.fromJson));

    List<Product> decoded =
        items!.map((e) => Product.fromJson(jsonDecode(e))).toList();

//List<Product> b = items.map((string) => Product()).toList();
    if (decoded.isNotEmpty) {
      viewModelProvider!.compareProviderList = decoded;
    }

    setState(() async {
      //selectedProduct = await viewModelProvider.compareProviderList![0];
    });

    print('getcom: ${decoded.length}');
  }

  getDiscount() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String>? savedStrList = sharedPreferences.getStringList('dis');

    List<int> intProductList = savedStrList!.map((i) => int.parse(i)).toList();
    if (intProductList.isNotEmpty) {
      viewModelProvider!.discountProvider = intProductList;
    }

    setState(() {});

    print('dis len ${intProductList.length}');
  }

  navigateTo() async {
    //await Future.delayed(Duration(seconds: 2),(){});
    await checkNetConnectivity();
    await viewModelProvider!.getCartModel();
    await viewModelProvider!.getFavoriteModel();
    //await getCom();
    await viewModelProvider!.compareProviderList;
    //await ComparePage.getCompare();
    _isConnection
        ? await Future.delayed(Duration(seconds: 2), () {
            if (info.data != null) {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CustomNavigationBar()));
            }
            //  else if((info.data==null) && (_users== null)){
            //     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>CustomNavigationBar()));
            //  }
            else {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        title: Text(
                          'SIGNIN',
                          textScaleFactor: 1,
                        ),
                        content: Text('Do you want to sign-in ?',
                            textScaleFactor: 1),
                        actions: <Widget>[
                          TextButton(
                              child: Text('Sign In'),
                              onPressed: () {
                                Navigator.of(context).pop(true);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()));
                              }),
                          TextButton(
                              child: Text('Not Now'),
                              onPressed: () {
                                Navigator.of(context).pop(true);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            CustomNavigationBar()));
                              }),
                        ],
                      ));

              //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
            }
          })
        : CustomSnackbar.snackbar(
            context,
            "No Internet. Check Connection",
          );
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }

  Future<ProfileInfoModel>? getUser() {
    print("call1");
    GetUserService.fetchUserInfo(info.data!.id!).then((value) {
      // setState({
      //   _categories
      // })

      setState(() {
        _users = value;
      });
      print(_users!.email);

      //print("products: ");
      //print(value);
      // ignore: unused_local_variable
      var val = jsonEncode(value);
      return true;
      //print(val);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _isConnection
            ? Column(
                children: [
                  SizedBox(
                    height: ScreenUtil().setHeight(300),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(50),
                    width: ScreenUtil().setWidth(300),
                    child: Image.asset(
                      'assets/images/enlight.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    'Welcome To Mart-Up',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil().setSp(24)),
                  ),
                  // IconButton(
                  //                   onPressed: (){
                  //                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                  //                   },
                  //                   icon: Icon(Icons.arrow_forward,color: Colors.grey,size: 30,),),
                ],
              )
            : Column(
                children: [
                  SizedBox(
                    height: ScreenUtil().setHeight(300),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(50),
                    width: ScreenUtil().setWidth(300),
                    child: Image.asset(
                      'assets/images/enlight.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    'Welcome To Mart-Up',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil().setSp(24)),
                  ),
                  IconButton(
                    onPressed: () async {
                      await checkNetConnectivity();
                      navigateTo();

                      //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                    },
                    icon: Icon(
                      Icons.refresh_outlined,
                      color: Colors.grey,
                      size: 40,
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
