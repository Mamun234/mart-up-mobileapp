//import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
//import 'package:mart_up/models/add_address_model.dart';
import 'package:mart_up/models/favorite_model.dart';
import 'package:mart_up/models/product.dart';
//import 'package:mart_up/models/product_card_model.dart';
//import 'package:mart_up/models/product_list.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/product_details_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
//import 'package:mart_up/services/product-service.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
//import 'package:mart_up/widgets/empty_screen.dart';
//import 'package:mart_up/widgets/favorite_product_card.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/product_card.dart';
import 'package:mart_up/widgets/rating.dart';
import 'package:provider/provider.dart';

class FavoritProductScreen extends StatefulWidget {
  //Product? fav;
  // FavoritProductScreen({this.fav});

  @override
  State<FavoritProductScreen> createState() => _FavoritProductScreenState();
}

class _FavoritProductScreenState extends State<FavoritProductScreen> {
  List<FavoriteModel> favoriteProducts = [];
  var dbHelper;
  late ViewModelProvider viewModelProvider;
  Product? favoriteProductDetails;
  int? favoriteProductId;

  @override
  void initState() {
    
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    dbHelper = DBHelper();
    getFavorite();

    //getProduct();

    //_favorites=widget.fav;
  }

  getFavorite() async {
    var a = await DBHelper().getFavoriteListProducts();
    setState(() {
      favoriteProducts = a;
    });
    return a;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context,
                MaterialPageRoute(builder: (context) => CustomNavigationBar()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Favorite products",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
        child: Container(
          child: FutureBuilder(
            future: getFavorite(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return _buildItems(favoriteProducts, viewModelProvider);
                //print(snapshot.data);
              } else {
                return BuildLoading();
              }

              // if (null == snapshot.data) {
              //   return  EmptyScreen(title: "you have not add any product",);
              // }
            },
          ),
        ),
      ),
    ));
  }
}

_buildItems(
  List<FavoriteModel> fs,
  ViewModelProvider view,
) {
  return fs.length > 0
      ? GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisExtent: 294,
          ),

          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductDetailPage(
                              id: fs[index].id!,
                            )));
              },
              child: Card(
                elevation: 2.0,
                shadowColor: Colors.black38,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  height: 300,
                  width: 140,
                  child: Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Container(
                      child: Column(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 100,
                            //width: 80,

                            child: Center(
                              child: Image.network(
                                '${fs[index].img}',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            '${fs[index].name}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                                color: productNameColor,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Rating(),
                          //Icon(Icons.star,color: Colors.amber,),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            '৳${fs[index].salePrice}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: salePriceColor,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //crossAxisAlignment: CrossAxisAlignment.,
                            children: [
                              Text(
                                '৳${fs[index].regularPrice}',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: previousPriceColor,
                                    decoration: TextDecoration.lineThrough,
                                    fontWeight: FontWeight.bold),
                              ),
                              //SizedBox(width: 20,),
                              Text(
                                '24% off',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: discountPriceColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              IconButton(
                                  onPressed: () {
                                    // CustomSnackbar.snackbar(context, 'Product deleted');
                                    // DBHelper().deleteFavoriteProduct(fs[index].id!);
                                    //getAdd();

                                    showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                              title: Text('Delete',
                                                  textScaleFactor: 1),
                                              content: Text(
                                                  'Are you sure delete',
                                                  textScaleFactor: 1),
                                              actions: <Widget>[
                                                TextButton(
                                                    child: Text('Cancle'),
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop(false);
                                                    }),
                                                TextButton(
                                                    child: Text('Delete'),
                                                    onPressed: () async {
                                                      Navigator.of(context)
                                                          .pop(true);
                                                      CustomSnackbar.snackbar(
                                                          context,
                                                          "Item deleted");
                                                      DBHelper()
                                                          .deleteFavoriteProduct(
                                                              fs[index].id!);
                                                      await view
                                                          .getFavoriteModel();
                                                    }),
                                              ],
                                            ));
                                  },
                                  icon: Icon(
                                    Icons.delete_outlined,
                                    size: 30,
                                    color: Colors.grey,
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
          itemCount: fs.length,
          //itemBuilder: itemBuilder
        )
      : Center(
          child: Column(
            children: [
              SizedBox(height: 200),
              Container(
                height: 100,
                width: 200,
                child: Image.asset(
                  'assets/images/empty.png',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "No Product Added",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        );
}
