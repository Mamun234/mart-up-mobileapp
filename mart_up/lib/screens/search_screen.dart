import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.white,
          title: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Container(
              height: 45,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 1.5),
                  borderRadius: BorderRadius.circular(8)),
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.search_outlined),
                      color: tabSelectColor,
                    ),
                    hintText: 'Nike Air Max',
                    hintStyle: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    suffixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.clear),
                      color: Colors.grey,
                    )),
              ),
            ),
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.mic),
              color: Colors.grey,
            )
          ],
        ),
      ),
      body: SafeArea(
          child: ListView(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 12.0),
            child: Text(
              'Nike Air Max',
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 12.0),
            child: Text(
              'Nike Air Max',
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 12.0),
            child: Text(
              'Nike Air Max',
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
        ],
      )),
    );
  }
}
