import 'package:flutter/material.dart';

import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/add_address.dart';
import 'package:mart_up/screens/loginScreen.dart';
import 'package:mart_up/screens/order_screen.dart';
import 'package:mart_up/screens/profileScreen.dart';
import 'package:mart_up/screens/splash_screen.dart';

import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:provider/provider.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  // Color color = Colors.white38;
  LoginResponseModel info = LoginResponseModel();
// int? loginCheck;
  ViewModelProvider? viewModelProvider;
  int? loginCheck;
  @override
  void initState() {
    super.initState();
    //viewModelProvider=Provider.of<ViewModelProvider>(context,listen: false);
    getInfo();
    //print('log: ${viewModelProvider!.checkLogin}');
  }

  getInfo() async {
    var a = await DBHelper().getLoginUserData();
    setState(() {
      info = a!;
      if (info.data == null) {
        loginCheck = 0;
      } else {
        loginCheck = 1;
      }
      print('info: ${info.data!.email}');
      print('logincheck: ${loginCheck}');
    });
    //getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CustomNavigationBar()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Account",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Builder(
          builder: (context) => Container(
                width: double.infinity,
                height: double.infinity,
                margin: EdgeInsets.all(5.0),
                child: Column(
                  children: [
                    loginCheck == 1
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                //color = Colors.amber;
                              });
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProfileScreen()));
                            },
                            child: Container(
                              //color: color,
                              padding: EdgeInsets.all(16.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Icon(
                                      Icons.account_circle_outlined,
                                      color: themeColor,
                                      size: 36.0,
                                    ),
                                  ),
                                  Expanded(
                                      flex: 4,
                                      child: Text(
                                        "Profile",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ))
                                ],
                              ),
                            ),
                          )
                        : Text(''),
                    loginCheck == 1
                        ? GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OrderScreen()));
                            },
                            child: Container(
                              padding: EdgeInsets.all(16.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Icon(
                                      Icons.local_mall_outlined,
                                      color: themeColor,
                                      size: 36.0,
                                    ),
                                  ),
                                  Expanded(
                                      flex: 4,
                                      child: Text("Orders",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold)))
                                ],
                              ),
                            ),
                          )
                        : Text(''),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddAddressPage()));
                      },
                      child: Container(
                        padding: EdgeInsets.all(16.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Icon(
                                Icons.location_on_outlined,
                                color: themeColor,
                                size: 36.0,
                              ),
                            ),
                            Expanded(
                                flex: 4,
                                child: Text("Address",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)))
                          ],
                        ),
                      ),
                    ),
                    // GestureDetector(
                    //   onTap: () {
                    //     Navigator.push(
                    //         context,
                    //         MaterialPageRoute(
                    //             builder: (context) => PaymentScreen()));
                    //   },
                    //   child: Container(
                    //     padding: EdgeInsets.all(16.0),
                    //     child: Row(
                    //       children: [
                    //         Expanded(
                    //           flex: 1,
                    //           child: Icon(
                    //             Icons.payment_outlined,
                    //             color: themeColor,
                    //             size: 36.0,
                    //           ),
                    //         ),
                    //         Expanded(
                    //           flex: 4,
                    //           child: Text("Payment",
                    //               style: TextStyle(
                    //                   fontSize: 18, fontWeight: FontWeight.bold)),
                    //         )
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    loginCheck == 1
                        ? GestureDetector(
                            onTap: () async {
                              await CustomSnackbar.snackbar(
                                  context, "you Logged Out");
                              await DBHelper().removeLoginUserData();
                              //viewModelProvider!.checkLogin=null;

                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SplashScreen()));
                            },
                            child: Container(
                              padding: EdgeInsets.all(16.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Icon(
                                      Icons.logout_outlined,
                                      color: tabSelectColor,
                                      size: 26,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Text("Log Out",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold)),
                                  )
                                ],
                              ),
                            ),
                          )
                        : GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            },
                            child: Container(
                              padding: EdgeInsets.all(16.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Icon(
                                      Icons.login_outlined,
                                      color: themeColor,
                                      size: 36.0,
                                    ),
                                  ),
                                  Expanded(
                                      flex: 4,
                                      child: Text("Log In",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold)))
                                ],
                              ),
                            ),
                          ),
                  ],
                ),
              )),
    );
  }
}
