//import 'dart:convert';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//import 'package:mart_up/constants/colors.dart';
//import 'package:mart_up/models/profileInfoModel.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/models/update_name_model.dart';
import 'package:mart_up/screens/profileScreen.dart';
//import 'package:mart_up/services/get_user_service.dart';
//import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/services/update_user_service.dart';
import 'package:mart_up/widgets/roundedButton.dart';

// ignore: must_be_immutable
class ChangeNameScreen extends StatefulWidget {
  String? name;
  int? id;
  ChangeNameScreen({this.name,this.id});

  @override
  State<ChangeNameScreen> createState() => _ChangeNameScreenState();
}

class _ChangeNameScreenState extends State<ChangeNameScreen> {
  TextEditingController name = TextEditingController();
  TextEditingController userName = TextEditingController();
  //ProfileInfoModel? _users;

  int? id;
  LoginResponseModel info = LoginResponseModel();
  @override
  void initState() {
    //print("call");
    super.initState();
    name.text = widget.name!;
    //getInfo();

    // final productProvider= Provider.of<ProductProviderApi>(context,listen: false);
    //productProvider.getProductModelList();
  }
//    getInfo()async{
//    var a = await DBHelper().getLoginUserData();
//    setState(() {
//      info=a!;
//        print('info: ${info.data!.id}');
//    });
//   getUser();
// }
//     Future<GetUserModel>? getUser() {
//     // print("call1");
//     GetUserService.fetchUser(info.data!.id!)
//         .then((value) {
//       // setState({
//       //   _categories
//       // })

//       setState(() {
//         _users = value;

//       });
//     //name.text=_users!.firstName!;
//       //print("products: ");
//       //print(value);
//       var val = jsonEncode(value);
//       return true;
//       //print(val);
//     });
//   }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context,
                  MaterialPageRoute(builder: (context) => ProfileScreen()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Change Name",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 12, right: 12),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Name",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  //TextInput(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 7.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black26, width: 1.5),
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: TextField(
                        controller: name,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.only(left: 15),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  //               Text(
                  //                 "User Name",
                  //                 style: TextStyle(
                  //                     color: Colors.black,
                  //                     fontSize: 20,
                  //                     fontWeight: FontWeight.bold),
                  //               ),
                  //               //TextInput(),
                  //               Padding(
                  //   padding: const EdgeInsets.symmetric(vertical: 7.0),
                  //   child: Container(
                  //     decoration: BoxDecoration(
                  //       border: Border.all(color: Colors.black26, width: 1.5),
                  //       borderRadius: BorderRadius.circular(16),
                  //     ),
                  //     child: TextField(
                  //       controller: userName,
                  //       decoration: InputDecoration(
                  //         contentPadding: const EdgeInsets.symmetric(vertical: 16),
                  //         border: InputBorder.none,
                  //         //hintText: "Frist",
                  //         prefixIcon: Padding(
                  //           padding: const EdgeInsets.symmetric(horizontal: 16),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // )
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(12.0),
          child: RoundedButton(
            buttonText: "Change Name",
            func: () async {
              UpdateNameModel nameUpdate =
                  UpdateNameModel(firstName: name.text);
              UpdateUserService().updateUserName(nameUpdate,widget.id!, context);
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => const ProfileScreen()));
            },
          ),
        ),
      ),
    );
  }
}

class TextInput extends StatefulWidget {
  @override
  State<TextInput> createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black26, width: 1.5),
          borderRadius: BorderRadius.circular(16),
        ),
        child: TextField(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical: 16),
            border: InputBorder.none,
            //hintText: "Frist",
            prefixIcon: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
            ),
          ),
        ),
      ),
    );
  }
}
