import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/brand_model.dart';

import 'package:mart_up/models/category_model.dart';
import 'package:mart_up/screens/search_result_screen.dart';
import 'package:mart_up/services/brand_service.dart';
import 'package:mart_up/services/category_service.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loader_brane_filter_screen.dart';
import 'package:mart_up/widgets/loader_category_filter_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/no_internet.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'package:connectivity/connectivity.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  //bool _hasBeenPressed = false;
  List<CategoryModel> _ccategories = [];
  List<BrandModel> _brands = [];
  int _selectedIndex = -1;
  int _selectedIndexForBrand = -1;

  var min_price;
  var max_price;
  var stock_status;
  var category_id;
  var brand_id;
  bool _isConnection = false;
  TextEditingController minPriceCnt = TextEditingController();
  TextEditingController maxPriceCnt = TextEditingController();

  @override
  void initState() {
    getCategorys();
    getBrands();
    checkNetConnectivity();
    //new add
    super.initState();
  }

  void getCategorys() {
    print("call category");
    CategoryService.fetchCategory().then((value) {
      // setState({
      //   _categories
      // })
      setState(() {
        _ccategories = value;
      });

      print("category: ");
      //print(value);
      var val = jsonEncode(value);
      print(val);
    });
  }

  void getBrands() {
    print("call category");
    BrandService.fetchBrand().then((value) {
      // setState({
      //   _categories
      // })
      setState(() {
        _brands = value;
      });

      var val = jsonEncode(value);
      print(val);
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    print(result);

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  _onSelectedForBrand(int index) {
    setState(() => _selectedIndexForBrand = index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.clear),
            color: Colors.grey,
          ),
          title: Text(
            'Filter Search',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: ScreenUtil().setWidth(14.0),
                  horizontal: ScreenUtil().setWidth(14.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Price Range',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil().setSp(18)),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: ScreenUtil().setHeight(60),
                        width: ScreenUtil().setWidth(160),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1.5),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16),
                          ),
                          child: Center(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: minPriceCnt,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '৳100',
                                  hintStyle: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: ScreenUtil().setHeight(60),
                        width: ScreenUtil().setWidth(160),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1.5),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16),
                          ),
                          child: Center(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: maxPriceCnt,
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '৳1000',
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),

                  // Text('Buying Format',
                  // style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: ScreenUtil().setSp(18)),),
                  // SizedBox(height: 20,),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     GestureDetector(
                  //       onTap: (){
                  //          setState(() {
                  //            stock_status="instock";
                  //            _hasBeenPressed = !_hasBeenPressed;
                  //     });
                  //       },
                  //       child: Container(
                  //         height: ScreenUtil().setHeight(60),
                  //         width: ScreenUtil().setWidth(70),
                  //         decoration: BoxDecoration(
                  //           color: _hasBeenPressed ? borderColor : Colors.white,
                  //           border: Border.all(
                  //             color: Colors.grey,width: 1.5,
                  //           ),borderRadius: BorderRadius.circular(5),
                  //         ),
                  //         child: Center(
                  //           child: Text('In Stock',style: TextStyle(
                  //             color:_hasBeenPressed ? tabSelectColor : Colors.grey,
                  //             ),),
                  //         ),
                  //       ),
                  //     ),
                  //     GestureDetector(
                  //       onTap: (){
                  //         setState(() {
                  //           stock_status="outofstock";
                  //         });
                  //       },
                  //       child: Container(
                  //         height: ScreenUtil().setHeight(60),
                  //         width: ScreenUtil().setWidth(110),
                  //         decoration: BoxDecoration(
                  //           border: Border.all(
                  //             color: Colors.grey,width: 1.5,
                  //           ),borderRadius: BorderRadius.circular(5),
                  //         ),
                  //         child: Center(
                  //           child: Text('Out Of Stock',style: TextStyle(color: Colors.grey),),
                  //         ),
                  //       ),
                  //     ),
                  //     GestureDetector(
                  //       onTap: (){
                  //         setState(() {
                  //           stock_status="onbackorder";
                  //         });
                  //       },
                  //       child: Container(
                  //         height: ScreenUtil().setHeight(60),
                  //         width: ScreenUtil().setWidth(140),
                  //         decoration: BoxDecoration(
                  //           border: Border.all(
                  //             color: Colors.grey,width: 1.5,
                  //           ),borderRadius: BorderRadius.circular(5),
                  //         ),
                  //         child: Center(
                  //           child: Text('On Back Order',style: TextStyle(color: Colors.grey),),
                  //         ),
                  //       ),
                  //     )
                  //   ],
                  // ),
                  SizedBox(
                    height: 14,
                  ),

                  //SizedBox(height: 20,),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                'Category',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(18)),
              ),
            ),
            _isConnection
                ? Container(
                    child: _ccategories.length > 0
                        ? GridView.builder(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              mainAxisExtent: ScreenUtil().setWidth(80),
                            ),
                            itemCount: _ccategories.length,
                            itemBuilder: (BuildContext context, index) {
                              //CateoryCardModel categoryCardModel =categoryList[index];
                              //BrandCardModel brandCardModel = brandList[index];
                              //ProductCardModel productCardModel=productList[index];
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setWidth(8),
                                  horizontal: ScreenUtil().setWidth(12),
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    _onSelected(index);
                                    setState(() {
                                      category_id = _ccategories[index].id!;

                                      //_hasBeenPressed = !_hasBeenPressed;
                                    });
                                  },
                                  child: (

                                      //BrandCard(brandCardModel: brandCardModel,)
                                      //ProductCard(productCardModel: productCardModel,)
                                      //CategoryCard(cateoryCardModel: categoryCardModel,)
                                      Container(
                                    //height: 60,
                                    width: ScreenUtil().setWidth(80),
                                    decoration: BoxDecoration(
                                      color: _selectedIndex != null &&
                                              _selectedIndex == index
                                          ? borderColor
                                          : Colors.white,
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 1.5,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Center(
                                      child: Text(
                                        _ccategories[index].name!,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  )),
                                ),
                              );
                            })
                        : LoadetFilterCat(),
                  )
                : NoInternet(
                    pressed: () async {
                      await checkNetConnectivity();
                      _isConnection == false
                          ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                          : checkNetConnectivity();

                      getCategorys();
                    },
                  ),
            SizedBox(
              height: 14,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                'Brand',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(18)),
              ),
            ),
            _isConnection
                ? Container(
                    child: _brands.length > 0
                        ? GridView.builder(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisExtent: ScreenUtil().setWidth(80),
                            ),
                            itemCount: _brands.length,
                            itemBuilder: (BuildContext context, index) {
                              //CateoryCardModel categoryCardModel =categoryList[index];
                              //BrandCardModel brandCardModel = brandList[index];
                              //ProductCardModel productCardModel=productList[index];
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setWidth(8),
                                  horizontal: ScreenUtil().setWidth(12),
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    _onSelectedForBrand(index);
                                    setState(() {
                                      brand_id = _brands[index].id!;

                                      //_hasBeenPressed = !_hasBeenPressed;
                                    });
                                  },
                                  child: (

                                      //BrandCard(brandCardModel: brandCardModel,)
                                      //ProductCard(productCardModel: productCardModel,)
                                      //CategoryCard(cateoryCardModel: categoryCardModel,)
                                      Container(
                                    //height: 60,
                                    width: ScreenUtil().setWidth(80),
                                    decoration: BoxDecoration(
                                      color: _selectedIndexForBrand != null &&
                                              _selectedIndexForBrand == index
                                          ? borderColor
                                          : Colors.white,
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 1.5,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Center(
                                      child: Text(
                                        _brands[index].name!,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  )),
                                ),
                              );
                            })
                        : LoaderFilterBrand(),
                  )
                : NoInternet(
                    pressed: () async {
                      await checkNetConnectivity();
                      _isConnection == false
                          ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                          : checkNetConnectivity();

                      getBrands();
                    },
                  ),
          ],
        ),
      )),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
        child: RoundedButton(
            buttonText: "Apply",
            func: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SearchResultPage(
                            id: category_id,
                            brandId: brand_id,
                            minPrice: minPriceCnt.text,
                            maxPrice: maxPriceCnt.text,
                            // stockStatus: stock_status,
                            categoryName: _ccategories[0].name,
                            brandName: _brands[0].name,
                          )));
            }),
      ),
    );
  }
}
