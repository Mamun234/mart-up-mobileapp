import 'package:flutter/material.dart';
import 'package:mart_up/models/product.dart';
import 'package:mart_up/screens/product_details_screen.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
class ProductImageGallery extends StatefulWidget {
  List<Images>? imgList1;
   ProductImageGallery({this.imgList1});

  @override
  _ProductImageGalleryState createState() => _ProductImageGalleryState();
}

class _ProductImageGalleryState extends State<ProductImageGallery> {
  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: (){
              Navigator.pop(context,MaterialPageRoute(builder: (context)=>ProductDetailPage()));
          },
           icon: Icon(Icons.close_outlined,color: Colors.redAccent,size: 24,)),
        ),
        body: Container(
          child: PhotoViewGallery.builder(
        scrollPhysics: const BouncingScrollPhysics(),
        builder: (BuildContext context, int index) {
          return PhotoViewGalleryPageOptions(
            imageProvider: NetworkImage(widget.imgList1![index].src!),
            initialScale: PhotoViewComputedScale.contained * 0.8,
            //heroAttributes: PhotoViewHeroAttributes(tag: widget.imgList1[index].id),
          );
        },
        itemCount: widget.imgList1!.length,
        // loadingBuilder: (context, event) => Center(
        //   child: Container(
        //     width: 20.0,
        //     height: 20.0,
        //     child: CircularProgressIndicator(
        //       value: event == null
        //           ? 0
        //           : event.cumulativeBytesLoaded / event.expectedTotalBytes,
        //     ),
        //   ),
        // ),
        // backgroundDecoration: widget.backgroundDecoration,
        // pageController: widget.pageController,
        // onPageChanged: onPageChanged,
          )
        ),
      );
  }
}