//import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/models/order_get_model.dart';
import 'package:mart_up/screens/account_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/services/order_service.dart';
import 'package:mart_up/widgets/loader_order_screen.dart';
import 'package:mart_up/widgets/orders_card.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  LoginResponseModel info = LoginResponseModel();
  int? id;
  int page = 1, limit = 5;
  bool _isloading = true;
  List<OrderGetModel> allOrders = [];

  final RefreshController refreshController =
      RefreshController(initialRefresh: false);

  void initState() {
    super.initState();
    getInfo();
  }

  getInfo() async {
    var a = await DBHelper().getLoginUserData();
    setState(() {
      info = a!;
      print('info: ${info.data!.id}');
    });

    getOrders(info.data!.id);
  }

  getOrders(id) {
    print("call1");
    OrderService.fetchOrder(customerId: info.data!.id, page: page, limit: limit)
        .then((value) {
      setState(() {
        allOrders = value;
        _isloading = false;

        page++;
      });

      // var val = jsonEncode(OrderGetModel);
      // print(val);
    });
  }

  void _onLoading() async {
    page++;
    var list = await OrderService.fetchOrder(
        customerId: info.data!.id, page: page, limit: limit);
    if (list != null) {
      allOrders.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AccountPage()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Orders",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
          child: SmartRefresher(
            controller: refreshController,
            // scrollDirection: Axis.vertical,
            // physics: BouncingScrollPhysics(),

            enablePullUp: true,
            onLoading: _onLoading,
            onRefresh: _onRefresh,
            footer: ClassicFooter(
              loadStyle: LoadStyle.ShowWhenLoading,
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Container(
                  child: _isloading
                      ? Center(child: LoaderOrderScreen())
                      : _isloading == false && allOrders.isEmpty
                          ? Center(
                              child: Column(
                                children: [
                                  SizedBox(height: 200),
                                  Container(
                                    height: 100,
                                    width: 200,
                                    child: Image.asset(
                                      'assets/images/empty.png',
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    "No Product Added",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            )
                          : GridView.builder(
                              shrinkWrap: true,
                              physics: BouncingScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 1,
                                mainAxisExtent: 200,
                              ),
                              itemCount: allOrders.length,
                              itemBuilder: (BuildContext context, index) {
                                return (OrdersCard(
                                    orderGetModel: allOrders[index]));
                              })),
            ),
          ),
        ),
      ),
    );
  }
}
