import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mart_up/models/user_login_model.dart';
import 'package:mart_up/services/user_login_service.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'package:mart_up/screens/registerScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController email = TextEditingController();

  TextEditingController password = TextEditingController();
  final formKey = GlobalKey<FormState>(); //key for form
  //String name = "";
  bool _obscureText = true;
  bool _isLoading = false;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Builder(
        builder: (context) => Scaffold(
          body: SafeArea(
            child: Form(
              key: formKey,
              child: Container(
                margin: EdgeInsets.all(25),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Image.asset(
                        "assets/images/enlight.png",
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      const Text(
                        "Welcome to Mart-Up",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Sign in to continue",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          // decoration: BoxDecoration(
                          //   border: Border.all(color: Colors.black26, width: 1.5),
                          //   borderRadius: BorderRadius.circular(16),
                          // ),
                          child: TextFormField(
                            controller: email,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.symmetric(vertical: 16),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              hintText: "Email",
                              prefixIcon: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Icon(
                                  Icons.email,
                                  size: 30,
                                ),
                              ),
                              //hintStyle: kBodyText,
                            ),
                            validator: (value) {
                              if (value!.isEmpty ||
                                  !RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}')
                                      .hasMatch(value)) {
                                return "Enter correct Email";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      // PasswordInput(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          // decoration: BoxDecoration(
                          //   border: Border.all(color: Colors.black26, width: 1.5),
                          //   borderRadius: BorderRadius.circular(16),
                          // ),
                          child: TextFormField(
                            controller: password,
                            decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                hintText: "Password",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(Icons.lock),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            obscureText: _obscureText,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Enter correct Password";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      _isLoading
                          ? BuildLoading()
                          : RoundedButton(
                              buttonText: "Sign in",
                              func: () async {
                                if (formKey.currentState!.validate()) {
                                  //_isLoading=true;
                                  UserLoginModel lmr = UserLoginModel(
                                      username: email.text,
                                      password: password.text);
                                  setState(() {
                                    _isLoading = true;
                                  });

                                  Response? response =
                                      await UserLoginService().loginUser(lmr);
                                  //  if(response!.body==null){
                                  //     compute(UserLoginService.parseLoginBadCode, response!.body);
                                  //  }
                                  if (response!.statusCode == 200) {
                                    compute(UserLoginService.parseLogin,
                                        response.body);
                                    await CustomSnackbar.snackbar(
                                        context, "sign in succesful");
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                CustomNavigationBar()));
                                  } else if (response.statusCode == 403) {
                                    setState(() {
                                      _isLoading = false;
                                    });

                                    await CustomSnackbar.snackbar(context,
                                        "Login Faild check email and password");
                                  }
                                }

                                // Navigator.push(
                                //     context,
                                //     MaterialPageRoute(
                                //         builder: (context) =>
                                //             const CustomNavigationBar()));
                              }),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "OR",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        //height: 60,
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                                width: 1, color: Colors.grey.withOpacity(0.2))),
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 30,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset(
                                "assets/images/google.png",
                                height: 32,
                                width: 32,
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Center(
                                child: Text(
                                  "Login with Google",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        //height: 60,
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                                width: 1, color: Colors.grey.withOpacity(0.2))),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                child: Image.asset(
                                  "assets/images/facebook.png",
                                  height: 32,
                                  width: 32,
                                ),
                              ),
                              SizedBox(
                                width: 30,
                              ),
                              Center(
                                child: Text(
                                  "Login with Facebook",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Text(
                        "Forgot password?",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Don't have a account?",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            SizedBox(width: 10,),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            RegisterScreen()));
                              },
                              child: Container(
                                child: Text(
                                  "Register",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue),
                                ),
                              ),
                            ),
                            // SizedBox(width: 5,),
                            // Text(
                            //   "Or",
                            //   style: TextStyle(
                            //       fontSize: 16,
                            //       fontWeight: FontWeight.bold,
                            //       color: Colors.grey),
                            // ),
                            //  SizedBox(width: 5,),
                            //  GestureDetector(
                            //   onTap: () {
                            //     Navigator.push(
                            //         context,
                            //         MaterialPageRoute(
                            //             builder: (context) => CustomNavigationBar()));
                            //   },
                            //   child: Container(
                            //     child: Text(
                            //       "Not Now",
                            //       style: TextStyle(
                            //           fontSize: 16,
                            //           fontWeight: FontWeight.bold,
                            //           color: Colors.blue),
                            //     ),
                            //   ),

                            // ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// class TextInput extends StatefulWidget {
//   @override
//   State<TextInput> createState() => _TextInputState();
// }

// class _TextInputState extends State<TextInput> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 7.0),
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(color: Colors.black26, width: 1.5),
//           borderRadius: BorderRadius.circular(16),
//         ),
//         child: TextField(
//           decoration: InputDecoration(
//             contentPadding: const EdgeInsets.symmetric(vertical: 16),
//             border: InputBorder.none,
//             hintText: "Your Email",
//             prefixIcon: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16),
//               child: Icon(
//                 Icons.email,
//                 size: 30,
//               ),
//             ),
//             //hintStyle: kBodyText,
//           ),
//         ),
//       ),
//     );
//   }
// }

// class PasswordInput extends StatefulWidget {
//   @override
//   State<PasswordInput> createState() => _PasswordInputState();
// }

// class _PasswordInputState extends State<PasswordInput> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 7.0),
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(color: Colors.black26, width: 1.5),
//           borderRadius: BorderRadius.circular(16),
//         ),
//         child: TextField(
//           decoration: InputDecoration(
//             contentPadding: const EdgeInsets.symmetric(vertical: 16),
//             border: InputBorder.none,
//             hintText: "Password",
//             prefixIcon: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16),
//               child: Icon(Icons.lock),
//             ),
//           ),
//           obscureText: true,
//         ),
//       ),
//     );
//   }
// }
