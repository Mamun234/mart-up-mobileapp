import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:mart_up/models/profileInfoModel.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/models/write_review_model.dart';
import 'package:mart_up/screens/review_screen.dart';
import 'package:mart_up/services/get_user_service.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/services/review_service.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';

// ignore: must_be_immutable
class WriteReviewScreen extends StatefulWidget {
  int? productId;
  WriteReviewScreen({this.productId});

  @override
  _WriteReviewScreenState createState() => _WriteReviewScreenState();
}

class _WriteReviewScreenState extends State<WriteReviewScreen> {
  int? ratingValue;
  List<Asset> invoiceimages = [];
  LoginResponseModel info = LoginResponseModel();
  TextEditingController writeReviewCtr = new TextEditingController();

  @override
  void initState() {
    
    super.initState();
    getInfo();
  }

  getInfo() async {
    var a = await DBHelper().getLoginUserData();
    setState(() {
      info = a!;
      print('info: ${info.data!.email}');
    });
    getUser();
  }

  //fatch user info
  ProfileInfoModel? users;
  Future<ProfileInfoModel>? getUser() {
    // print("call1");
    GetUserService.fetchUserInfo(info.data!.id!).then((value) {
      // setState({
      //   _categories
      // })

      setState(() {
        users = value;
      });

      //print("products: ");
      //print(value);
      // ignore: unused_local_variable
      var val = jsonEncode(value);
      return true;
      //print(val);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context,
                MaterialPageRoute(builder: (context) => ReviewScreen()));
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Write Review",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Container(
            child: ListView(
              //crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Please write Overall level of satisfaction with your shipping / Delivery Service",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 14,
                ),
                //Rating
                RatingBar.builder(
                  itemSize: 32,
                  initialRating: 0,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: false,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                    ratingValue = rating.toInt();
                  },
                ),
                SizedBox(
                  height: 14,
                ),
                Padding(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    height: 180,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black26, width: 1.5),
                    ),
                    child: TextField(
                      maxLines: 3,
                      controller: writeReviewCtr,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(5),
                        border: InputBorder.none,
                        hintText: "Write your review here",
                      ),
                      keyboardType: TextInputType.multiline,
                    ),
                  ),
                ),
                // SizedBox(
                //   height: 10,
                // ),
                // Text(
                //   "Add Photo",
                //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                // ),
                // SizedBox(
                //   height: 10,
                // ),
                // GestureDetector(
                //   onTap: () {
                //    // pickInvoiceImages();
                //   },
                //   child: Container(
                //     height: 90,
                //     width: 90,
                //     decoration:
                //         BoxDecoration(border: Border.all(color: Colors.grey)),
                //     child: Icon(
                //       Icons.add_a_photo_outlined,
                //       size: 32,
                //     ),
                //   ),
                // ),
                // Visibility(
                //     visible: invoiceimages.length > 0,
                //     child: Container(
                //       height: 100,
                //       child: Container(
                //         child: ListView.builder(
                //             scrollDirection: Axis.horizontal,
                //             itemCount: invoiceimages.length,
                //             itemBuilder: (context, index) {
                //               Asset asset = invoiceimages[index];

                //               return Container(
                //                 padding: EdgeInsets.all(5),
                //                 child: AssetThumb(
                //                   asset: asset,
                //                   width: 90,
                //                   height: 90,
                //                 ),
                //               );
                //             }),
                //       ),
                //     )),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(12.0),
        child: RoundedButton(
            buttonText: "Add Review",
            func: () async {
              ReviewModel reviewModel = ReviewModel(
                  review: writeReviewCtr.text,
                  reviewer: users!.username,
                  productId: widget.productId,
                  rating: ratingValue);

              reviewModel.reviewerEmail = "mamun@gmail.com";

              ReviewService().createReview(reviewCreate: reviewModel);

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ReviewScreen(
                            productId: widget.productId,
                          )));
            }),
      ),
    ));
  }

//add pic
  // Future<void> pickInvoiceImages() async {
  //   List<Asset> resultList = [];
  //   try {
  //     resultList = await MultiImagePicker.pickImages(
  //       maxImages: 5,
  //       enableCamera: true,
  //       selectedAssets: invoiceimages,
  //       materialOptions: MaterialOptions(
  //         statusBarColor: "#4CAF50",
  //         actionBarColor: "#4CAF50",
  //         actionBarTitle: "All Photos",
  //         useDetailsView: false,
  //         selectCircleStrokeColor: "#4CAF50",
  //       ),
  //     );
  //   } on Exception catch (e) {
  //     print(e);
  //   }
  //   setState(() {
  //     invoiceimages = resultList;
  //   });
  // }
}
