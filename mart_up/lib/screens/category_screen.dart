import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/constants/links.dart';

import 'package:mart_up/models/category_model.dart';
import 'package:mart_up/providers/view_model_provider.dart';

import 'package:mart_up/services/category_service.dart';
import 'package:mart_up/widgets/category_card.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loader_category_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:mart_up/widgets/no_internet.dart';
import 'package:connectivity/connectivity.dart';
import 'package:provider/provider.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  bool _isConnection = false;
  late ViewModelProvider viewModelProvider;
  //int? itemCount;
  @override
  void initState() {
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);

    getCategorys();
    checkNetConnectivity();
    // setState(() {
    //   itemCount = viewModelProvider.cartListN!.length;
    // });
  }

  List<CategoryModel> _ccategories = [];

  void getCategorys() {
    print("call1");
    CategoryService.fetchCategory().then((value) {
      // setState({
      //   _categories
      // })
      setState(() {
        _ccategories = value;
      });

      print("category: ");
      //print(value);
      var val = jsonEncode(value);
      print(val);
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    print(result);
    await viewModelProvider.getCartModel();
    setState(() {});

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CustomNavigationBar()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Center(
            child: Container(
                width: 150,
                //color: Colors.yellow,

                // child: Image.asset(
                //   'assets/images/enlight.png',
                //   fit: BoxFit.cover,
                // ),
                child: Image.network(logoLink)),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Stack(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.grey,
                      size: 30,
                    ),
                  ),
                  Positioned(
                    left: 25,
                    bottom: 20,
                    child: Container(
                        height: 20,
                        width: 20,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Consumer<ViewModelProvider>(
                            builder: (context, viewModel, child) {
                          return Text(
                            '${viewModelProvider.cartListN!.length}',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          );
                        })),
                  )
                ],
              ),
            )
          ]),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Category',
                    style: TextStyle(
                        color: lebelColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18)),
                SizedBox(
                  height: 20,
                ),
                _isConnection
                    ? Container(
                        child: _ccategories.length > 0
                            ? GridView.builder(
                                shrinkWrap: true,
                                physics: BouncingScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  mainAxisExtent: 200,
                                ),
                                itemCount: _ccategories.length,
                                itemBuilder: (BuildContext context, index) {
                                  return (

                                      //BrandCard(brandCardModel: brandCardModel,)
                                      //ProductCard(productCardModel: productCardModel,)
                                      CategoryCard(
                                          cateoryModel: _ccategories[index]
                                          //  productCardModel: _categories[index],
                                          ));
                                })
                            : LoaderCategory(),
                      )
                    : NoInternet(
                        pressed: () async {
                          await checkNetConnectivity();
                          _isConnection == false
                              ? CustomSnackbar.snackbar(
                                  context, "Turn Internet ON")
                              : checkNetConnectivity();

                          getCategorys();
                        },
                      ),
                SizedBox(
                  height: 20,
                ),
                // Text('Sub Category',
                //     style: TextStyle(
                //         color: lebelColor,
                //         fontWeight: FontWeight.bold,
                //         fontSize: 18)),
                // SizedBox(
                //   height: 20,
                // ),
                // Container(
                //   child: GridView.builder(
                //       shrinkWrap: true,
                //       physics: BouncingScrollPhysics(),
                //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                //         crossAxisCount: 4,
                //         mainAxisExtent: 124,
                //       ),
                //       itemCount: SubCategoryList.length,
                //       itemBuilder: (BuildContext context, index) {
                //         SubCateoryCardModel subCategoryCardModel =
                //             SubCategoryList[index];
                //         //BrandCardModel brandCardModel = brandList[index];
                //         //ProductCardModel productCardModel=productList[index];
                //         return (

                //             //BrandCard(brandCardModel: brandCardModel,)
                //             //ProductCard(productCardModel: productCardModel,)
                //             SubCategoryCard(
                //           subCateoryCardModel: subCategoryCardModel,
                //         ));
                //       }),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
