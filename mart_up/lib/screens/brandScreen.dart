//import 'dart:convert';

import 'package:connectivity/connectivity.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mart_up/models/brand_model.dart';
import 'package:mart_up/services/brand_service.dart';
import 'package:mart_up/widgets/brand_card.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loader_brand_screen.dart';
import 'package:mart_up/widgets/loader_home.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:mart_up/widgets/no_internet.dart';
//import 'package:mart_up/widgets/product_card.dart';

//import 'package:mart_up/models/product_card_model.dart';
//import 'package:mart_up/models/product_list.dart';

class BrandScreen extends StatefulWidget {
  const BrandScreen({Key? key}) : super(key: key);

  @override
  _BrandScreenState createState() => _BrandScreenState();
}

class _BrandScreenState extends State<BrandScreen> {
  bool _isConnection = false;
  List<BrandModel> _brands = [];
  @override
  void initState() {
    super.initState();
    checkNetConnectivity();
    getBrands();
  }

  void getBrands() {
    print("call1");
    BrandService.fetchBrand().then((value) {
      setState(() {
        _brands = value;
      });

      print("Brand: ");

      //var val = jsonEncode(value);
      //print(val);
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    print(result);

    setState(() {});

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CustomNavigationBar()));
              },
              icon: Icon(
                Icons.arrow_back_ios_new_rounded,
                color: Colors.grey,
                size: 22,
              ),
            ),
            title: Center(
              child: Container(
                width: 150,
                //color: Colors.yellow,

                child: Image.asset(
                  'assets/images/enlight.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.grey,
                        size: 30,
                      ),
                    ),
                    Positioned(
                      left: 25,
                      bottom: 20,
                      child: Container(
                        height: 20,
                        width: 20,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Text(
                          '2',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ]),
        body: SafeArea(
          child: SingleChildScrollView(
              child: Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // SizedBox(
                //   height: 10,
                // ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: const Text(
                    "All Brands",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                _isConnection
                    ? Container(
                        child: _brands.length > 0
                            ? GridView.builder(
                                shrinkWrap: true,
                                physics: BouncingScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  mainAxisExtent: 120,
                                ),
                                itemCount: _brands.length,
                                itemBuilder: (BuildContext context, index) {
                                  return (

                                      //BrandCard(brandCardModel: brandCardModel,)
                                      //ProductCard(productCardModel: productCardModel,)
                                      BrandCard(
                                    brandModel: _brands[index],
                                  ));
                                })
                            : LoaderBrand(),
                      )
                    : NoInternet(
                        pressed: () async {
                          await checkNetConnectivity();
                          _isConnection == false
                              ? CustomSnackbar.snackbar(
                                  context, "Turn Internet ON")
                              : checkNetConnectivity();

                          getBrands();
                        },
                      ),
              ],
            ),
          )),
        ),
      ),
    );
  }
}
