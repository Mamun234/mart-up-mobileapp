//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/update_email_model.dart';
import 'package:mart_up/screens/profileScreen.dart';
import 'package:mart_up/services/update_user_service.dart';
import 'package:mart_up/widgets/roundedButton.dart';

// ignore: must_be_immutable
class ChangeEmailScreen extends StatefulWidget {
  String? email;
  int? id;
  ChangeEmailScreen({this.email,this.id});

  @override
  State<ChangeEmailScreen> createState() => _ChangeEmailScreenState();
}

class _ChangeEmailScreenState extends State<ChangeEmailScreen> {
  TextEditingController email = TextEditingController();
  @override
  void initState() {
  
    super.initState();
    email.text = widget.email!;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(44), // here the desired height
            child: AppBar(
              backgroundColor: Colors.white,
              elevation: 1,
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (context) => ProfileScreen()));
                },
                icon: Icon(
                  Icons.arrow_back_ios_new_rounded,
                  color: Colors.grey,
                  size: 22,
                ),
              ),
              title: Text(
                "Email",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            )),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 12, right: 12),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Change Email",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  //TextInput(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 7.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black26, width: 1.5),
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: TextField(
                        controller: email,
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.symmetric(vertical: 16),
                          border: InputBorder.none,
                          hintText: "mamun@gmail.com",
                          prefixIcon: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Icon(
                              Icons.email_outlined,
                              size: 30,
                            ),
                          ),
                          //hintStyle: kBodyText,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // Text(
                  //   "We will send verification to your new mail",
                  //   style: TextStyle(
                  //     color: themeColor,
                  //     fontSize: 18,
                  //   ),
                  // ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(12.0),
          child: RoundedButton(
            buttonText: "Change Email",
            func: () async {
              UpdateEmailModel updateEmail =
                  UpdateEmailModel(email: email.text);
              UpdateUserService().updateUserEmail(updateEmail,widget.id!, context);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ProfileScreen()));
            },
          ),
        ),
      ),
    );
  }
}

// class TextInput extends StatefulWidget {
//   @override
//   State<TextInput> createState() => _TextInputState();
// }

// class _TextInputState extends State<TextInput> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 7.0),
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(color: Colors.black26, width: 1.5),
//           borderRadius: BorderRadius.circular(16),
//         ),
//         child: TextField(
//           decoration: InputDecoration(
//             contentPadding: const EdgeInsets.symmetric(vertical: 16),
//             border: InputBorder.none,
//             hintText: "mamun@gmail.com",
//             prefixIcon: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16),
//               child: Icon(
//                 Icons.email_outlined,
//                 size: 30,
//               ),
//             ),
//             //hintStyle: kBodyText,
//           ),
//         ),
//       ),
//     );
//   }
// }
