import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/models/product.dart';
import 'package:mart_up/services/offer_service.dart';
import 'package:mart_up/widgets/loader_search_result_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:mart_up/widgets/productCard.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OfferPage extends StatefulWidget {
  const OfferPage({Key? key}) : super(key: key);

  @override
  _OfferPageState createState() => _OfferPageState();
}

class _OfferPageState extends State<OfferPage> {
  List<Product> offerProduct = [];
  int page = 1;
  int limit = 5;
  bool _isloading = true;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  //double? discountPercent;

  @override
  void initState() {
    super.initState();
    getOffer();
  }

  getOffer() async {
    var a = await OfferService.fetchOffer(page, limit);
    setState(() {
      offerProduct = a;
      _isloading = false;

      page++;

      // for (int i = 0; i < offerProduct.length; i++) {
      //   if (offerProduct[i].salePrice != null ||
      //       offerProduct[i].regularPrice != null) {
      // discountPercent = double.parse(offerProduct[i].salePrice! * 100) /
      //     double.parse(offerProduct[i].regularPrice!);
      //   }
      //   print('discount: $discountPercent');
      // }
    });
  }

  void _onLoading() async {
    page++;
    var list = await OfferService.fetchOffer(page, limit);
    if (list != null) {
      offerProduct.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CustomNavigationBar()));
            //Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          "Hot Offers",
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
        child: SmartRefresher(
          controller: refreshController,
          // scrollDirection: Axis.vertical,
          // physics: BouncingScrollPhysics(),

          enablePullUp: true,
          onLoading: _onLoading,
          onRefresh: _onRefresh,
          footer: ClassicFooter(
            loadStyle: LoadStyle.ShowWhenLoading,
          ),
          child: _isloading
              ? LoaderSearchResultScreen()
              : GridView.builder(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisExtent: ScreenUtil().setHeight(260)),
                  itemCount: offerProduct.length,
                  itemBuilder: (BuildContext context, index) {
                    //ProductCardModel productCardModel=productList[index];
                    return (ProductCardNew(
                      productCardModel: offerProduct[index],
                      // discountPercent: discountPercent,
                    ));
                  }),
        ),
      ),
    );
  }
}
