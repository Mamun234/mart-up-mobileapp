import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/order_details_model.dart';
import 'package:mart_up/screens/order_screen.dart';
import 'package:mart_up/services/order_service.dart';
import 'package:mart_up/widgets/loader_order_details_screen.dart';

import 'package:mart_up/widgets/order_item_card.dart';
import 'package:mart_up/widgets/payment_details_card.dart';
import 'package:mart_up/widgets/shiping_details_card.dart';

// ignore: must_be_immutable
class OrderDetailsScreen extends StatefulWidget {
  //OrderGetModel? orderDetails;
  //OrderDetailsModel? orderDetailsModel;
  int? orderId;

  OrderDetailsScreen({this.orderId});

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  void initState() {
    getOrders();
    super.initState();
  }

  OrderDetailsModel orderDetails = OrderDetailsModel();
  bool isTrue = true;

  void getOrders() {
    print("call1");
    OrderService().fetchOrderDetails(widget.orderId).then((value) {
      setState(() {
        orderDetails = value;
      });

      var val = jsonEncode(orderDetails);
      print(val);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => OrderScreen()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Order details",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: orderDetails != null && orderDetails.lineItems != null
                  ? Container(
                      child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            height: 90,
                            width: double.infinity,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      height: 30,
                                      width: 30,
                                      child: Icon(
                                        Icons.done_outlined,
                                        color: Colors.white,
                                        size: 22.0,
                                      ),
                                      // : Icon(
                                      //     Icons.done_outlined,
                                      //     color: Colors.white,
                                      //     size: 24.0,
                                      //   ),
                                      decoration: BoxDecoration(
                                        color:
                                            orderDetails.status == "pending" ||
                                                    orderDetails.status ==
                                                        "processing" ||
                                                    orderDetails.status ==
                                                        "completed"
                                                ? themeColor
                                                : Colors.grey.withOpacity(.25),
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                    Container(
                                      height: 3,
                                      width: ScreenUtil().setWidth(122),
                                      decoration: BoxDecoration(
                                        color: orderDetails.status ==
                                                    "processing" ||
                                                orderDetails.status ==
                                                    "completed"
                                            ? themeColor
                                            : Colors.grey.withOpacity(.25),
                                      ),
                                    ),
                                    Container(
                                      height: 30,
                                      width: 30,
                                      child: Icon(
                                        Icons.done_outlined,
                                        color: Colors.white,
                                        size: 22.0,
                                      ),
                                      // : Icon(
                                      //     Icons.done_outlined,
                                      //     color: Colors.white,
                                      //     size: 24.0,
                                      //   ),
                                      decoration: BoxDecoration(
                                        color: orderDetails.status ==
                                                    "processing" ||
                                                orderDetails.status ==
                                                    "completed"
                                            ? themeColor
                                            : Colors.grey.withOpacity(.25),
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                    Container(
                                      height: 3,
                                      width: ScreenUtil().setWidth(123),
                                      decoration: BoxDecoration(
                                        color:
                                            orderDetails.status == "completed"
                                                ? themeColor
                                                : Colors.grey.withOpacity(.25),
                                      ),
                                    ),
                                    Container(
                                      height: 30,
                                      width: 30,
                                      child:
                                          // orderDetails.status == "completed"
                                          Icon(
                                        Icons.done_outlined,
                                        color: Colors.white,
                                        size: 22.0,
                                      ),
                                      // : Icon(
                                      //     Icons.done_outlined,
                                      //     color: Colors.white,
                                      //     size: 24.0,
                                      //   ),
                                      decoration: BoxDecoration(
                                        color:
                                            orderDetails.status == "completed"
                                                ? themeColor
                                                : Colors.grey.withOpacity(.25),
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                  ],
                                ),
                                Stack(children: [
                                  Container(
                                    width: double.infinity,
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 2, top: 10),
                                    child: Text(
                                      "Pending",
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      left: 130,
                                      top: 10,
                                      child: Text(
                                        "Processing",
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                      )),
                                  Positioned(
                                      right: 2,
                                      top: 10,
                                      child: Text(
                                        "Completed",
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                      ))
                                ])
                              ],
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Products",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        // OrderItemCard(),
                        Container(
                            //height: 100,
                            //child: orderDetails != null && orderDetails.lineItems != null
                            child: ListView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemCount: orderDetails.lineItems!.length,
                          itemBuilder: (context, index) {
                            return OrderItemCard(
                                products: orderDetails.lineItems![index]);
                            // return ListTile(
                            //     title: Text(
                            //         "item${orderDetails.lineItems![index].name}"));
                          },
                        )),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Shiping details",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        ShipingDetailsCard(
                          shipingDetails: orderDetails,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Payment details",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        PaymentDetailsCard(
                          paymenyDetails: orderDetails,
                        ),
                      ],
                    ))
                  : LoaderOrderDetail(),
            ),
          ),
        ),
      ),
    );
  }
}
