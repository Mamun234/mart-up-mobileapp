import 'dart:convert';
//import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/product.dart';
import 'package:connectivity/connectivity.dart';
import 'package:mart_up/screens/filter_screen.dart';
import 'package:mart_up/services/product-service.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loader_search_result_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:mart_up/widgets/no_internet.dart';
import 'package:mart_up/widgets/productCard.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

// ignore: must_be_immutable
class SearchResultPage extends StatefulWidget {
  int? id;
  int? brandId;
  String? categoryName;
  String? brandName;

  var minPrice, maxPrice, stockStatus;
  SearchResultPage(
      {this.id,
      this.brandId,
      this.minPrice,
      this.maxPrice,
      this.stockStatus,
      this.categoryName,
      this.brandName});

  @override
  _SearchResultPageState createState() => _SearchResultPageState();
}

class _SearchResultPageState extends State<SearchResultPage> {
  int _page = 1;
  int _limit = 5;

  final RefreshController refreshController =
      RefreshController(initialRefresh: false);

  List<Product> _products = [];
  List<Product> searchedProducts = [];
  bool _isConnection = false;
  bool _isloading = true;

  // GlobalKey _contentKey = GlobalKey();
  // GlobalKey _refresherKey = GlobalKey();
  TextEditingController _searchKeyController = TextEditingController();
  String? searchKey;
  String hintText = 'Search Text Here';
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    //print("call");
    super.initState();
    getProducts(
      _page,
      _limit,
    );
    checkNetConnectivity();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = 'Search Text Here';
      }
      setState(() {});
    });
    // if (widget.brandId == null) {
    //   widget.slug = null;
    // }

    // if (widget.brandId != null) {
    //   widget.slug != null;
    // }

    // final productProvider= Provider.of<ProductProviderApi>(context,listen: false);
    //productProvider.getProductModelList();
  }

  Future<Product>? getProducts(int p, int l) {
    // print("call1");
    ProductService.fetchProducts(
      _page, _limit,
      categoryId: widget.id,
      minPrice: widget.minPrice,
      maxPrice: widget.maxPrice,
      brandId: widget.brandId,

      //stock_status: widget.stockStatus
    ).then((value) {
      setState(() {
        _products = value;
        _isloading = false;
        _page++;
      });

      //print("products: ");
      //print(value);
      // ignore: unused_local_variable
      var val = jsonEncode(value);
      return true;
      //print(val);
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    print(result);

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: AppBar(
            elevation: 1.0,
            backgroundColor: Colors.white,
            leading: IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CustomNavigationBar()));
              },
              icon: Icon(Icons.arrow_back_ios_new_rounded),
              color: Colors.grey,
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Container(
                height: ScreenUtil().setHeight(40),
                width: ScreenUtil().setWidth(200),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1.5),
                    borderRadius: BorderRadius.circular(5)),
                child: TextField(
                  focusNode: focusNode,
                  onChanged: (value) {
                    searchProduct();
                  },
                  controller: _searchKeyController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: IconButton(
                      onPressed: () {
                        searchProduct();
                      },
                      icon: Icon(Icons.search_outlined),
                      color: tabSelectColor,
                    ),
                    hintText: hintText,
                    hintStyle: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            actions: [
              // IconButton(
              //   onPressed: () {
              //     searchProduct();
              //   },
              //   icon: Icon(Icons.search_outlined),
              //   color: tabSelectColor,
              // ),
              IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FilterPage()));
                },
                icon: Icon(
                  Icons.filter_alt_outlined,
                  size: 32,
                ),
                color: tabSelectColor,
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: _isConnection
                ? SizedBox(
                    height: double.infinity,
                    width: double.infinity,
                    child: SmartRefresher(
                      controller: refreshController,
                      enablePullUp: true,
                      onLoading: _onLoading,
                      onRefresh: _onRefresh,
                      footer: ClassicFooter(
                        loadStyle: LoadStyle.ShowWhenLoading,
                      ),

                      // onLoading: ()async{
                      //  await getProducts(_page, _limit);
                      //  //print(_loaded);
                      //  if(_categories.length>0){
                      //    refreshController.loadComplete();
                      //  }
                      //  else{
                      //    refreshController.loadFailed();
                      //  }
                      // },
                      // child: _isloading ? LoadingBuild() : _buildBrandList(),
                      child: _isloading
                          ? LoaderSearchResultScreen()
                          : _isloading == false && _products.isEmpty
                              ? Center(
                                  child: Column(
                                    children: [
                                      SizedBox(height: 200),
                                      Container(
                                        height: 100,
                                        width: 200,
                                        child: Image.asset(
                                          'assets/images/empty.png',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        "No Product Found",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                )
                              :

                              //                   :
                              //                    _isloading?
                              //                   Center(
                              //    child: Column(
                              //      children: [
                              //        SizedBox(height: 200),
                              //        Container(
                              //         height: 100,
                              //         width: 200,
                              //         child: Image.asset('assets/images/empty.png',fit: BoxFit.cover,),
                              //       ),
                              //       SizedBox(height: 20,),
                              //       Text("No Product Added",
                              //       style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),
                              //       ),
                              //      ],
                              //    ),
                              //  )
                              SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12, right: 12),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            widget.brandId == null &&
                                                    widget.categoryName == null
                                                ? Center(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets
                                                            .all(8.0),
                                                    child: Text(
                                                      "All Products",
                                                      style: TextStyle(
                                                          color: themeColor,
                                                          fontWeight:
                                                              FontWeight
                                                                  .w700,
                                                          fontSize: 13),
                                                    ),
                                                  ),
                                                )
                                                : Center(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets
                                                            .all(8.0),
                                                    child: Text(
                                                      widget.brandId == null
                                                          ? "Category>${widget.categoryName}"
                                                          : "Brand>${widget.brandName}",
                                                      style: TextStyle(
                                                          color: themeColor,
                                                          fontWeight:
                                                              FontWeight
                                                                  .w700,
                                                          fontSize: 14),
                                                    ),
                                                  ),
                                                )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: GridView.builder(
                                            shrinkWrap: true,
                                            physics: BouncingScrollPhysics(),
                                            gridDelegate:
                                                SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              mainAxisExtent:
                                                  ScreenUtil().setHeight(275),
                                            ),
                                            itemCount: _products.length,
                                            itemBuilder:
                                                (BuildContext context, index) {
                                              //ProductCardModel productCardModel=productList[index];
                                              return (ProductCardNew(
                                                productCardModel:
                                                    _products[index],
                                              ));
                                            }),
                                      )
                                    ],
                                  ),
                                ),
                    ),
                  )
                : NoInternet(
                    pressed: () async {
                      await checkNetConnectivity();
                      _isConnection == false
                          ? CustomSnackbar.snackbar(context, "Turn Internet ON")
                          : checkNetConnectivity();

                      getProducts(_page, _limit);
                    },
                  ),
          ),
        ));
  }

  void _onLoading() async {
    _page++;

    var list = await ProductService.fetchProducts(
      _page,
      _limit,
      searchKey: _searchKeyController.text,
      categoryId: widget.id,
      brandId: widget.brandId,
    );
    if (list != null) {
      _products.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  Future searchProduct() async {
    int _page = 1;
    _products.clear();
    setState(() {
      _isloading = true;
    });
    var list = await ProductService.fetchProducts(
      _page,
      _limit,
      searchKey: _searchKeyController.text,
      categoryId: widget.id,
      brandId: widget.brandId,
    );
    //page = 1;
    if (list != null) {
      _products.addAll(list);
    }
    setState(() {
      //searchKey=_searchKeyController.text;
      _isloading = false;
    });
  }
}
