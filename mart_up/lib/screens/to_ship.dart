//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/add_address_model.dart';
import 'package:mart_up/models/cart_model.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/models/order_model.dart';
import 'package:mart_up/screens/add_address.dart';
import 'package:mart_up/screens/loginScreen.dart';
import 'package:mart_up/screens/profileScreen.dart';
import 'package:mart_up/screens/success_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/services/order_service.dart';
//import 'package:mart_up/services/product-service.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
//import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'package:mart_up/widgets/ship_card.dart';

// ignore: must_be_immutable
class ToShip extends StatefulWidget {
  var id;
  var pay;
  bool? showBtn;
  String? copuneCtr;

  ToShip({this.id, this.pay, this.showBtn, this.copuneCtr});

  @override
  State<ToShip> createState() => _ToShipState();
}

class _ToShipState extends State<ToShip> {
  List<AddAddressModel> addresses = [];
  AddAddressModel selectedAddresses = AddAddressModel();
  late OrderModel orderModel;
  //bool _hasBeenPressed = false;
  bool _isLoading = false;
  //AddAddressModel addAddressModel;
  var dbHelper;
  LoginResponseModel info = LoginResponseModel();

  List<CartModel> cartProducts = [];
  int _selectedIndex = -1;
  int selecteAddressCheck = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  getCartItem() async {
    var a = await DBHelper().getCartListProducts();
    setState(() {
      cartProducts = a;
    });
    // _totalSum();
    return a;
  }

  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    getAddresse();
    getCartItem();
    orderModel = OrderModel();
    //_favorites=widget.fav;
    getInfo();
  }

  getAddresse() async {
    var ad = await DBHelper().getAddresses();
    setState(() {
      addresses = ad;
    });
    return ad;
  }

  getInfo() async {
    var a = await DBHelper().getLoginUserData();
    setState(() {
      if (a != null) {
        info = a;
      }

      //   if(info.data == null){
      //    info.data!.id=0;
      //  }
      //print('info: ${info.data!.id}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 1,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (context) => ProfileScreen()));
              },
              icon: Icon(
                Icons.arrow_back_ios_new_rounded,
                color: Colors.grey,
                size: 22,
              ),
            ),
            title: Text(
              "To Ship",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddAddressPage()));
                  },
                  icon: Icon(
                    Icons.add_outlined,
                    color: themeColor,
                    size: 28,
                  ),
                ),
              ),
            ],
          ),
          body: addresses.isNotEmpty
              ? Builder(
                  builder: (context) => SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Container(
                        height: double.infinity,
                        width: double.infinity,
                        child: FutureBuilder(
                          future: getAddresse(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData && snapshot.data != []) {
                              return _buildItems(addresses);
                            } else if (!snapshot.hasData ||
                                snapshot.data == null) {
                              return Text("not data");
                            }
                            return Text("data");
                          },
                        ),
                      ),
                    ),
                  ),
                )
              : Center(
                  child: Column(
                    children: [
                      SizedBox(height: 200),
                      Container(
                        height: 100,
                        width: 200,
                        child: Image.asset(
                          'assets/images/empty.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "No Address Added",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
          bottomNavigationBar: addresses.isNotEmpty
              ? Builder(
                  builder: (context) => widget.showBtn == false
                      ? Text('')
                      : Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: _isLoading
                              ? Container(height: 100, child: BuildLoading())
                              : RoundedButton(
                                  buttonText: "Confirm Order",
                                  func: () async {
                                    //  final  snackBar= SnackBar(content: Text("Select Address"));
                                    //  Scaffold.of(context).showSnackBar(snackBar);
                                    // print('Confirm order btn tap');
                                    setState(() {
                                      orderModel.paymentMethod = widget.pay;
                                      orderModel.paymentMethodTitle = "cash";
                                      orderModel.setPaid = false;
                                      //orderModel.status="pending";
                                      Shipping shipAddress = Shipping();
                                      // shipAddress.firstName="s";
                                      // shipAddress.lastName="h";
                                      // shipAddress.company="en";
                                      // shipAddress.city="r";
                                      // shipAddress.address1="4";
                                      // shipAddress.address2="23";
                                      // shipAddress.phone="0923809238";
                                      // shipAddress.country="bd";
                                      // shipAddress.postcode="222";
                                      // shipAddress.state="arizona";
                                      shipAddress.firstName =
                                          selectedAddresses.fname;
                                      shipAddress.lastName =
                                          selectedAddresses.lname;
                                      //shipAddress.company="default";
                                      shipAddress.city = selectedAddresses.city;
                                      shipAddress.address1 = "none";
                                      shipAddress.address2 =
                                          selectedAddresses.houseNo;
                                      shipAddress.phone =
                                          selectedAddresses.mobileNo;
                                      shipAddress.country = "Bangladesh";
                                      //doc not provide phone
                                      shipAddress.postcode =
                                          selectedAddresses.mobileNo;
                                      shipAddress.state = "none";
                                      orderModel.shipping = shipAddress;
                                      List<LineItem> items = [];
                                      List<CouponLine> coupones = [];
                                      CouponLine coupon = CouponLine();
                                      coupon.code = widget.copuneCtr;
                                      coupones.add(coupon);
                                      // coupones[0].code = widget.copuneCtr;
                                      if (widget.copuneCtr != null) {
                                        orderModel.couponLines = coupones;
                                      } else {}
                                      //        cartProducts.map((e)  {
                                      //     // var item={
                                      //     //           'id':e.id,
                                      //     //            'id': e.quantity};
                                      //     LineItem item= LineItem();

                                      //     item.productId=e.id;
                                      //     item.quantity=e.quantity;
                                      //     item.variation_id=0;
                                      //       items.add(item);
                                      //   });
                                      // orderModel.lineItems=items;
                                      for (var i = 0;
                                          i < cartProducts.length;
                                          i++) {
                                        LineItem item = LineItem();
                                        item.productId = cartProducts[i].id;
                                        item.quantity =
                                            cartProducts[i].quantity;
                                        item.variation_id = 0;
                                        items.add(item);
                                      }
                                      orderModel.lineItems = items;
                                      orderModel.customerId =
                                          info.data != null ? info.data!.id : 0;
                                      // change login user
                                      // LineItem litem=LineItem();
                                      // // orderModel.lineItems![0].productId=2;
                                      // // orderModel.lineItems![0].quantity=1;
                                      // // orderModel.lineItems![0].variation_id=3;
                                      // litem.productId=2;
                                      // litem.quantity=1;
                                      // litem.variation_id=3;
                                      // orderModel.lineItems=litem;
                                    });

                                    // OrderModel or=OrderModel(paymentMethod: widget.pay,paymentMethodTitle: "cash",
                                    //             );
                                    //or.lineItems=cartProducts;
                                    // List<LineItem> items=[];
                                    // //  Ing? ships;
                                    // //or.paymentMethod=widget.pay;
                                    // //  or.lineItems![0].productId=cartProducts[0].id;
                                    // //  or.lineItems![0].quantity=cartProducts[0].quantity;
                                    // var orderItem=cartProducts.map((e)  {
                                    //   // var item={
                                    //   //           'id':e.id,
                                    //   //            'id': e.quantity};
                                    //   LineItem item= LineItem();

                                    //   item.productId=e.id;
                                    //   item.quantity=e.quantity;
                                    //     items.add(item);
                                    // });
                                    // or.lineItems=items;
                                    // // print(items);
                                    // // var shipItem=selectedAddresses.map((e) {
                                    // //   Ing ship=Ing();
                                    // //   ship.firstName=e.fname;
                                    // //   ship.lastName=e.lname;
                                    // //   ship.city=e.city;
                                    // //   ship.address1=e.roadNo;
                                    // //   ship.address2=e.houseNo;
                                    // //   ship.phone=e.mobileNo;
                                    // //   ships;
                                    // // });
                                    // // or.lineItems=items;
                                    // // or.shipping=ships;
                                    // // print(ships);

                                    //or.paymentMethodTitle="djjkf";
                                    // or.setPaid=false;
                                    //  or.shipping!.firstName="s";
                                    //  or.shipping!.lastName="h";
                                    //  or.shipping!.city="d";
                                    //  or.shipping!.address1="f";
                                    //  or.shipping!.address2="t";
                                    //  or.shipping!.phone="32423543";
                                    //  or.shipping!.email="d@d.com";
                                    //  or.shipping!.country="bd";
                                    //  or.shipping!.postcode="333";
                                    //  or.shipping!.state="delware";
                                    print(selecteAddressCheck);
                                    if (selecteAddressCheck == 0) {
                                      CustomSnackbar.snackbar(
                                          context, "please select Address");
                                    } else {
                                      if (info.data != null &&
                                          selecteAddressCheck > 0) {
                                        _isLoading = true;
                                        Response data = await OrderService()
                                            .createOrder(
                                                orderModel, info.data!.id!);

                                        if (data.statusCode < 300) {
                                          //  var d= await compute(ProductService.parseProduct, data.body);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SuccessScreen()));
                                        } else {
                                          CustomSnackbar.snackbar(context,
                                              "Your order not comfirm, Please try agian.");
                                          _isLoading = false;
                                        }
                                      } else {
                                        await CustomSnackbar.snackbar(
                                            context, "Please log In");
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    LoginScreen()));
                                      }
                                    }
                                  },
                                )),
                )
              : Text(''),
        ));
  }

  Widget _buildItems(List<AddAddressModel> data) {
    return ListView.builder(
        // scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (BuildContext context, index) {
          // return Text("data");
          // return ShipCard(
          //   addAddressModel: addresses[index],
          // );

          return GestureDetector(
              onTap: () {
                selectedAddresses = data[index];

                selecteAddressCheck++;

                _onSelected(index);
                print('tapped');
                print(selecteAddressCheck);
                print(selectedAddresses.fname);
              },
              child: ShipCard(
                addAddressModel: addresses[index],
                tab: _selectedIndex,
                index: index,
              ));
        });
  }
}
