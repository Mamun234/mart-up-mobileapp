import 'package:flutter/material.dart';
import 'package:mart_up/constants/colors.dart';
//import 'package:mart_up/models/category_model.dart';
import 'package:mart_up/screens/account_screen.dart';
import 'package:mart_up/screens/to_ship.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';

// ignore: must_be_immutable
class PaymentScreen extends StatelessWidget {
  var paymentType;
  String? couponCtr;

  PaymentScreen({this.couponCtr});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context,
                  MaterialPageRoute(builder: (context) => AccountPage()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Payment",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: Builder(builder: (context) {
          return SafeArea(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => ProfileScreen()));
                    },
                    child: Container(
                      //color: color,
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Icon(
                              Icons.payment_outlined,
                              color: themeColor,
                              size: 36.0,
                            ),
                          ),
                          Expanded(
                              flex: 4,
                              child: Text(
                                "Debit or credit card  ",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ))
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => OrderScreen()));
                    },
                    child: Container(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Image.asset(
                              "assets/images/paypal.png",
                              height: 32,
                              width: 32,
                            ),
                          ),
                          Expanded(
                              flex: 4,
                              child: Text("Mobile Banking",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)))
                        ],
                      ),
                    ),
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     Navigator.push(context,
                  //         MaterialPageRoute(builder: (context) => ToShip()));
                  //   },
                  //   child: Container(
                  //     padding: EdgeInsets.all(16.0),
                  //     child: Row(
                  //       children: [
                  //         Expanded(
                  //             flex: 1,
                  //             child: Image.asset(
                  //               "assets/images/bankn.png",
                  //               height: 36,
                  //               width: 36,
                  //               color: salePriceColor,
                  //             )),
                  //         Expanded(
                  //             flex: 4,
                  //             child: GestureDetector(
                  //               onTap: () {
                  //                 paymentType = "cash on delivery";
                  //                 CustomSnackbar.snackbar(
                  //                     context, "cash on delivery selected");
                  //                 Navigator.push(
                  //                     context,
                  //                     MaterialPageRoute(
                  //                         builder: (context) => ToShip(
                  //                               pay: paymentType,
                  //                               copuneCtr: couponCtr,
                  //                             )));
                  //               },
                  //               child: Text("Cash on delivery",
                  //                   style: TextStyle(
                  //                       fontSize: 18,
                  //                       fontWeight: FontWeight.bold)),
                  //             )),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  Container(
                    padding: EdgeInsets.all(16.0),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Image.asset(
                              "assets/images/bankn.png",
                              color: themeColor,
                              width: 36,
                              height: 36,
                            )),
                        Expanded(
                            flex: 4,
                            child: GestureDetector(
                              onTap: () async {
                                paymentType = "cash on delivery";
                                await CustomSnackbar.snackbar(
                                    context, "cash on delivery selected");
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ToShip(
                                              pay: paymentType,
                                              copuneCtr: couponCtr,
                                            )));
                              },
                              child: Text("Cash on delivery",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
