import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/cart_model.dart';
import 'package:mart_up/models/coupon_model.dart';
import 'package:mart_up/models/favorite_model.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/payment_screen.dart';
import 'package:mart_up/screens/product_details_screen.dart';
import 'package:mart_up/services/copon_service.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/dash_line.dart';
import 'package:mart_up/widgets/loader_product_card_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:mart_up/widgets/navigation_bar.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'package:provider/provider.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List<CartModel> cartProducts = [];
  var dbHelper;
  Future<int>? cartItemCount;
  Future<double>? totalAmount;
  Future<double>? totalAmountDiscount;
  // final GlobalKey<AnimatedListState> _key = GlobalKey();
  double shiping = 60;
  var totalQuantity;
  late ViewModelProvider viewModelProvider;
  TextEditingController couponCtr = TextEditingController();
  final formKey = GlobalKey<FormState>();
  //bool _isLoading = false;
  String? couponCode;
  List<CouponModel> couponInfo = [];

  @override
  void initState() {
    super.initState();
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    //dbHelper = DBHelper();
    getCart();
    //_favorites=widget.fav;
  }

  @override
  void dispose() {
    super.dispose();
  }

  getCoupon(code) {
    CouponService.fetchCoupon(couponCode: code).then((value) {
      if (value.isNotEmpty) {
        setState(() {
          couponCode = code;
          couponInfo = value;
          totalAmountDiscount = DBHelper().getCartTotalAmount();
        });
      } else {
        CustomSnackbar.snackbar(context, "Coupon code is invalid");
      }
      // setState(() {
      //   couponInfo = value;
      //   //  couponInfo.length > 0
      //   //     ? couponInfo[0].amount
      //   //     : CustomSnackbar.snackbar(
      //   //         context, "Coupon code is invalid");

      // });
      grandTotoal();
    });
  }

  getCart() async {
    var a = await DBHelper().getCartListProducts();
    setState(() {
      cartProducts = a;
      cartItemCount = DBHelper().getCartListProductsCount();
      totalAmount = DBHelper().getCartTotalAmount();
      totalAmountDiscount = DBHelper().getCartTotalAmount();
    });
    // _totalSum();
    return a;
  }

// _totalSum(){
//    var totalSum=cartProducts
//       .map((e) => double.parse(e.salePrice!))
//       .reduce((value, element) => value + element);
//       setState(() {
//         totalPrice=totalSum;
//       });
// }
  _buildQuentityNumber() {
    return FutureBuilder(
        future: cartItemCount,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            //totalQuantity = snapshot.data;
            return Text('${snapshot.data.toString()}',
                style: TextStyle(fontSize: 12));
          }

          if (null == snapshot.data) {
            return Text("0 items", style: TextStyle(fontSize: 12));
          }
          return CircularProgressIndicator();
        });
  }

  grandTotoal() {
    // Future<double>? amount;
    // if (couponInfo != null && couponInfo[0].amount != null) {
    //   Future<double>? discount = double.parse(couponInfo[0].amount.toString()) as Future<double>?;
    //   amount = totalAmount! - discount;
    // } else {}
    // return FutureBuilder(
    //     future: totalAmountDiscount,
    //     builder: (context, snapshot) {
    //       if (snapshot.hasData) {
    //         double amount = double.parse(snapshot.data.toString());
    //         if (couponInfo != null &&
    //             couponInfo.length > 0 &&
    //             couponInfo[0].amount != null) {
    //           amount = amount - double.parse(couponInfo[0].amount.toString());
    //         }
    //         return Text(
    //           '৳${amount.toString()}',
    //           style: TextStyle(
    //               color: Colors.black87,
    //               fontSize: 16,
    //               fontWeight: FontWeight.w500),
    //         );
    //       }

    //       if (null == snapshot.data || snapshot.data == 0) {
    //         return Text(
    //           "0 TAKA",
    //           style: TextStyle(
    //               color: Colors.white70,
    //               fontSize: 16,
    //               fontWeight: FontWeight.w500),
    //         );
    //       }

    //       return BuildLoading();
    //     });

    return FutureBuilder(
        future: totalAmount,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            double amount = double.parse(snapshot.data.toString());
            if (couponInfo != null &&
                couponInfo.length > 0 &&
                couponInfo[0].amount != null) {
              amount = amount - double.parse(couponInfo[0].amount.toString());
            }
            return Text(
              '৳${amount.toString()}',
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            );
          }

          if (null == snapshot.data || snapshot.data == 0) {
            return Text(
              "0 TAKA",
              style: TextStyle(
                  color: Colors.white70,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            );
          }

          return BuildLoading();
        });
  }

  _buildTotalAmount() {
    // var a = await totalAmount;

    return FutureBuilder(
        future: totalAmount,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Text(
              '৳${snapshot.data.toString()}',
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            );
          }

          if (null == snapshot.data || snapshot.data == 0) {
            return Text(
              "0 TAKA",
              style: TextStyle(
                  color: Colors.white70,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            );
          }

          return BuildLoading();
        });
  }

//  var totalSum = cartProducts
//       .map((e) => int.parse(e["price"]))
//       .reduce((value, element) => value + element);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        // leading: IconButton(
        //   onPressed: () {
        //      Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => CustomNavigationBar()));
        //   },
        //   icon: Icon(Icons.arrow_back_ios_new_rounded),
        //   color: Colors.grey,
        // ),
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CustomNavigationBar()));
            //Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            color: Colors.grey,
            size: 22,
          ),
        ),
        title: Text(
          'Your Cart',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
          child: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(ScreenUtil().setWidth(2.0)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                // Container(
                //   child: ListView.builder(

                //     itemCount: cartList.length,
                //    shrinkWrap: true,
                //     physics: BouncingScrollPhysics(),
                //     itemBuilder: (context,index){
                //       CartCardModel cartCardModel=cartList[index];
                //       return CartProductCard(cartCardModel: cartCardModel,);

                //   }),
                // ),
                Container(
                  child: FutureBuilder(
                    future: getCart(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return _buildItems(cartProducts, viewModelProvider);
                        //print(snapshot.data);
                      } else {
                        return LoaderCardScreen();
                      }
                    },
                  ),
                ),

                // SizedBox(height: 20,),
                // Container(
                //   child: ListView.builder(

                //     itemCount: viewModel.cartListN.length,
                //    shrinkWrap: true,
                //     physics: BouncingScrollPhysics(),
                //     itemBuilder: (context,index){
                //       CartCardModel cartCardModel=viewModel.cartListN[index];
                //       return CartProductCard(cartCardModel: cartCardModel,);

                //   }),
                // ),
                SizedBox(
                  height: 20,
                ),
                cartProducts.isNotEmpty
                    ? Row(
                        children: [
                          Container(
                            //color: Colors.amberAccent,
                            height: ScreenUtil().setHeight(60),
                            width: ScreenUtil().setWidth(275),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey)),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(16)),
                              child: Center(
                                child: TextFormField(
                                  controller: couponCtr,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Enter Coupone Code',
                                    hintStyle: TextStyle(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "Enter your coupon";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (formKey.currentState!.validate()) {
                                getCoupon(couponCtr.text);
                                // getCoupon(couponCtr.text);
                                // setState(() {
                                //   //couponCode = couponCtr.text;
                                //   getCoupon(couponCtr.text);
                                //   couponInfo.length > 0
                                //       ? couponInfo[0].amount
                                //       : CustomSnackbar.snackbar(
                                //           context, "Coupon code is invalid");
                                // });
                              } else {
                                print("vsldjufh");
                              }

                              print("tap applay btn");
                            },
                            child: Container(
                              height: ScreenUtil().setHeight(60),
                              width: ScreenUtil().setWidth(80),
                              color: tabSelectColor,
                              child: Center(
                                child: Text(
                                  'Apply ',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    : Center(
                        child: Column(
                          children: [
                            SizedBox(height: 200),
                            Container(
                              height: 100,
                              width: 200,
                              child: Image.asset(
                                'assets/images/empty.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "No Product Added",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),

                SizedBox(
                  height: 20,
                ),

                cartProducts.isNotEmpty
                    ? Card(
                        elevation: 1,
                        child: Padding(
                          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          'item: ',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w700),
                                        ),
                                        _buildQuentityNumber(),
                                        //  Text(totalQuantity.toString(),
                                        //  style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w700),
                                        //  ),
                                      ],
                                    ),
                                    //  Text('\$599.25',
                                    //  style: TextStyle(color: Colors.black),
                                    //  )
                                    _buildTotalAmount(),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    //  Text('Shiping',
                                    //  style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w700),
                                    //  ),
                                    //  Text('\$${shiping}',
                                    //  style: TextStyle(color: Colors.black),
                                    //  ),
                                    //  Text('\$${cartProducts[0].id}',
                                    //  style: TextStyle(color: Colors.black),
                                    //  )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Discount',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Text(
                                      // "dsk",
                                      '৳${couponInfo.length > 0 ? couponInfo[0].amount : "0"}',
                                      style: TextStyle(color: tabSelectColor),
                                    ),
                                  ],
                                ),
                                //  Row(
                                //    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //    children: [
                                //      Text('Import Charge',
                                //      style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w700),
                                //      ),
                                //      Text('\$25',
                                //      style: TextStyle(color: Colors.black),
                                //      )
                                //    ],
                                //  ),
                                SizedBox(
                                  height: 20,
                                ),
                                DashLine(
                                  color: Colors.grey,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Total',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    //  totalPrice==null ?  Text('0'):Text(totalPrice.toString(),
                                    //  style: TextStyle(color: tabSelectColor),
                                    //  )
                                    Text(
                                      '',
                                      style: TextStyle(color: tabSelectColor),
                                    ),
                                    // _buildTotalAmount(),
                                    grandTotoal(),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Text(''),
              ],
            ),
          ),
        ),
      )),
      bottomNavigationBar: cartProducts.isNotEmpty
          ? Padding(
              padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
              child: RoundedButton(
                  buttonText: "Check Out",
                  func: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PaymentScreen(
                                  couponCtr: couponCode,
                                )));
                  }),
            )
          : Text(''),
    );
  }
}

_buildItems(List<CartModel> fs, ViewModelProvider view) {
  return fs.length > 0
      ? ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductDetailPage(
                              id: fs[index].id!,
                            )));
              },
              child: Card(
                  elevation: 1,
                  child: Container(
                      child: Padding(
                    padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
                    child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                right: ScreenUtil().setWidth(16.0)),
                            child: Container(
                                height: ScreenUtil().setHeight(100.0),
                                width: ScreenUtil().setWidth(100.0),
                                //color: Colors.amber,
                                child: Image.network(
                                  '${fs[index].img}',
                                  fit: BoxFit.cover,
                                )),
                          ),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height: ScreenUtil().setHeight(50.0),
                                      width: ScreenUtil().setWidth(130.0),
                                      //color: Colors.amber,
                                      child: Text(
                                        '${fs[index].name}',
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: ScreenUtil().setSp(16.0)),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Stack(
                                          children: [
                                            Container(
                                              width:
                                                  ScreenUtil().setWidth(45.0),
                                              height:
                                                  ScreenUtil().setHeight(50.0),
                                              //color: Colors.red,
                                            ),
                                            Positioned(
                                              left: ScreenUtil().setWidth(8.0),
                                              bottom:
                                                  ScreenUtil().setWidth(16.0),
                                              child: IconButton(
                                                  onPressed: () async {
                                                    CustomSnackbar.snackbar(
                                                        context,
                                                        'favorite product Added');
                                                    FavoriteModel fv =
                                                        FavoriteModel(
                                                            id: fs[index].id,
                                                            name:
                                                                fs[index].name,
                                                            salePrice: fs[index]
                                                                .salePrice,
                                                            regularPrice: fs[
                                                                    index]
                                                                .regularPrice,
                                                            quantity: fs[index]
                                                                .quantity,
                                                            img: fs[index].img);
                                                    DBHelper()
                                                        .saveToFavoriteList(fv);
                                                    await view
                                                        .getFavoriteModel();
                                                  },
                                                  icon: Icon(
                                                    Icons.favorite_outline,
                                                    color: Colors.grey,
                                                  )),
                                            )
                                          ],
                                        ),
                                        Stack(
                                          children: [
                                            Container(
                                              width:
                                                  ScreenUtil().setWidth(33.0),
                                              height:
                                                  ScreenUtil().setHeight(50),
                                              //color: Colors.amber,
                                            ),
                                            Positioned(
                                              //left: 4,
                                              bottom:
                                                  ScreenUtil().setWidth(16.0),
                                              //right: 2,
                                              child: IconButton(
                                                  onPressed: () {
                                                    // CustomSnackbar.snackbar(
                                                    //     context, 'Item deleted');
                                                    // DBHelper()
                                                    //     .deleteCartItem(fs[index].id!);
                                                    //     await view.getCartModel();
                                                    showDialog(
                                                        context: context,
                                                        builder:
                                                            (context) =>
                                                                AlertDialog(
                                                                  title: Text(
                                                                      'Delete',
                                                                      textScaleFactor:
                                                                          1),
                                                                  content: Text(
                                                                      'Are you sure delete',
                                                                      textScaleFactor:
                                                                          1),
                                                                  actions: <
                                                                      Widget>[
                                                                    TextButton(
                                                                        child: Text(
                                                                            'Cancle'),
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.of(context)
                                                                              .pop(false);
                                                                        }),
                                                                    TextButton(
                                                                        child: Text(
                                                                            'Delete'),
                                                                        onPressed:
                                                                            () async {
                                                                          Navigator.of(context)
                                                                              .pop(true);
                                                                          CustomSnackbar.snackbar(
                                                                              context,
                                                                              'Item deleted');
                                                                          DBHelper()
                                                                              .deleteCartItem(fs[index].id!);
                                                                          await view
                                                                              .getCartModel();
                                                                        }),
                                                                  ],
                                                                ));
                                                  },
                                                  icon: Icon(
                                                    Icons
                                                        .delete_outline_outlined,
                                                    color: Colors.grey,
                                                  )),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: ScreenUtil().setWidth(90.0),
                                      //color: Colors.amber,
                                      child: Text(
                                        '৳${fs[index].salePrice}',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: tabSelectColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: ScreenUtil().setSp(16)),
                                      ),
                                    ),
                                    Container(
                                      //color: Colors.amber,
                                      child: Row(
                                        children: [
                                          Container(
                                            height: ScreenUtil().setHeight(30),
                                            width: ScreenUtil().setWidth(42.0),
                                            // child: IconButton(
                                            //   onPressed: () {
                                            //     if (fs[index].quantity! > 1) {
                                            //       CartModel cq = CartModel(
                                            //           id: fs[index].id,
                                            //           name: fs[index].name,
                                            //           salePrice:
                                            //               fs[index].salePrice,
                                            //           regularPrice: fs[index]
                                            //               .regularPrice,
                                            //           quantity: fs[index]
                                            //                   .quantity! -
                                            //               1,
                                            //           img: fs[index].img);
                                            //       DBHelper().update(cq);
                                            //     }
                                            //   },
                                            //   icon: Icon(
                                            //     Icons.remove_outlined,
                                            //     color: Colors.grey,
                                            //   ),
                                            // ),
                                            child: GestureDetector(
                                                onTap: () {
                                                  if (fs[index].quantity! > 1) {
                                                    CartModel cq = CartModel(
                                                        id: fs[index].id,
                                                        name: fs[index].name,
                                                        salePrice:
                                                            fs[index].salePrice,
                                                        regularPrice: fs[index]
                                                            .regularPrice,
                                                        quantity: fs[index]
                                                                .quantity! -
                                                            1,
                                                        img: fs[index].img);
                                                    DBHelper().update(cq);
                                                  }
                                                },
                                                child: Icon(
                                                  Icons.remove_outlined,
                                                  color: Colors.grey,
                                                )),
                                            decoration: BoxDecoration(
                                              //color: Colors.redAccent,

                                              border: Border.all(
                                                color: Colors.grey,
                                              ),
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(5),
                                                  bottomLeft:
                                                      Radius.circular(5)),
                                            ),
                                          ),
                                          Container(
                                            height: ScreenUtil().setHeight(30),
                                            width: ScreenUtil().setWidth(42.0),
                                            child: Center(
                                              child: Text(
                                                '${fs[index].quantity}',
                                                style: TextStyle(
                                                  color: Colors.grey,
                                                ),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                              color: borderColor,
                                              border: Border.all(
                                                color: Colors.grey,
                                              ),
                                              //borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5)),
                                            ),
                                          ),
                                          Container(
                                            height: ScreenUtil().setHeight(30),
                                            width: ScreenUtil().setWidth(42.0),
                                            // child: Center(
                                            //   child: IconButton(
                                            //     icon: Icon(
                                            //       Icons.add,
                                            //       color: Colors.grey,
                                            //     ),
                                            //     onPressed: () {
                                            //       CartModel cq = CartModel(
                                            //           id: fs[index].id,
                                            //           name: fs[index].name,
                                            //           salePrice:
                                            //               fs[index].salePrice,
                                            //           regularPrice: fs[index]
                                            //               .regularPrice,
                                            //           quantity:
                                            //               fs[index].quantity! +
                                            //                   1,
                                            //           img: fs[index].img);
                                            //       DBHelper().update(cq);
                                            //     },
                                            //   ),
                                            // ),
                                            child: GestureDetector(
                                                onTap: () {
                                                  CartModel cq = CartModel(
                                                      id: fs[index].id,
                                                      name: fs[index].name,
                                                      salePrice:
                                                          fs[index].salePrice,
                                                      regularPrice: fs[index]
                                                          .regularPrice,
                                                      quantity:
                                                          fs[index].quantity! +
                                                              1,
                                                      img: fs[index].img);
                                                  DBHelper().update(cq);
                                                },
                                                child: Icon(
                                                  Icons.add,
                                                  color: Colors.grey,
                                                )),
                                            decoration: BoxDecoration(
                                              //color: Colors.redAccent,
                                              border: Border.all(
                                                color: Colors.grey,
                                              ),
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(5),
                                                  bottomRight:
                                                      Radius.circular(5)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ]),
                        ]),
                  ))
                  ),
            );
          },
          itemCount: fs.length,
          //itemBuilder: itemBuilder
        )
      : Text('');
}
