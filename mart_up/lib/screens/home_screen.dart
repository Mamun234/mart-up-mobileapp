import 'dart:convert';

import 'package:flutter/material.dart' hide Action;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mart_up/constants/links.dart';
import 'package:mart_up/models/brand_model.dart';
import 'package:mart_up/models/category_model.dart';
import 'package:mart_up/models/product.dart';
import 'package:mart_up/models/slider_model.dart';
// import 'package:mart_up/models/product_card_model.dart';
// import 'package:mart_up/models/product_list.dart';
// import 'package:mart_up/providers/product_provider_api.dart';
import 'package:mart_up/providers/view_model_provider.dart';
import 'package:mart_up/screens/cart_screen.dart';
import 'package:mart_up/screens/category_screen.dart';
import 'package:mart_up/screens/favorite_product_screen.dart';
import 'package:mart_up/screens/search_result_screen.dart';
import 'package:mart_up/services/brand_service.dart';
import 'package:mart_up/services/category_service.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/services/product-service.dart';
import 'package:mart_up/services/slider_service.dart';
import 'package:mart_up/widgets/brand_card.dart';
//import 'package:mart_up/widgets/carousel_slider.dart';
import 'package:mart_up/widgets/category_card.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';
import 'package:mart_up/widgets/home_slider.dart';
import 'package:mart_up/widgets/loader_brand_screen.dart';
import 'package:mart_up/widgets/loader_category_screen.dart';
import 'package:mart_up/widgets/loader_home.dart';
import 'package:mart_up/widgets/loader_search_result_screen.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/no_internet.dart';
import 'package:mart_up/widgets/productCard.dart';
import 'package:mart_up/constants/colors.dart';

import 'package:connectivity/connectivity.dart';
import 'package:mart_up/screens/brandScreen.dart';
//import 'package:mart_up/widgets/roundedButton.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

// final List<String> imgList = [
//   'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
//   'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
//   'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
// ];

// @override
//   void initState() {
//     super.initState();
//     isConnection();

//     APIService.fetchCategories().then((dataFromServer) => setState(() {
//           _allProducts = dataFromServer;
//           _isloading = false;
//         }));
//   }

class _HomePageState extends State<HomePage> {
  int _page = 1;
  int _limit = 5;
  bool _isConnection = false;
  bool _isloading = true;
  bool _isloading2 = true;
  var dbHelper;
  Future<int>? cartItemCount;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  final RefreshController refreshController2 =
      RefreshController(initialRefresh: false);
  //GlobalKey _contentKey = GlobalKey();
  //GlobalKey _refresherKey = GlobalKey();
  List<Product> _allProducts = [];
  List<CategoryModel> _ccategories = [];
  double? discountPercent;
  double? difference;
  double? regular;
  double? sale;
  int? itemCount;
  ViewModelProvider? viewModelProvider;
  List<BrandModel> brands = [];
   List<SliderModel> Slider = [];

  @override
  void initState() {
    //print("call");
    viewModelProvider = Provider.of<ViewModelProvider>(context, listen: false);
    getQuantity();
    getProducts();
    // final productProvider= Provider.of<ProductProviderApi>(context,listen: false);
    //productProvider.getProductModelList();
    dbHelper = DBHelper();
    getCategorys();
    //getQuantity();
    checkNetConnectivity();

    //difference=double.parse(_allProducts)
    //getDiscount();

    getBrands();

    getSlider();
    super.initState();
  }

  getQuantity() async {
    await viewModelProvider!.getFavoriteModel();
    await viewModelProvider!.getCartModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getSlider() {
    print("call1");
    SliderService.fetchSlider().then((value) {
      setState(() {
        Slider = value;
      });

      print("Slider: ");

      //var val = jsonEncode(value);
      //print(val);
    });
  }

  void getBrands() {
    print("call1");
    BrandService.fetchBrand().then((value) {
      setState(() {
        brands = value;
      });

      print("Brand: ");

      //var val = jsonEncode(value);
      //print(val);
    });
  }

  void getProducts() {
    print("call1");
    ProductService.fetchProducts(_page, _limit).then((value) {
      // setState({
      //   _allProducts
      // })
      setState(() {
        _allProducts = value;

        _isloading = false;
        _isloading2 = false;
        // for(int i=0;i<_allProducts.length; i++){
        //   if(_allProducts[i].salePrice !=null || _allProducts[i].regularPrice !=null){
        //     difference= double.parse(_allProducts[i].regularPrice!) - double.parse(_allProducts[i].salePrice!);
        //     discountPercent=difference! - double.parse(_allProducts[i].regularPrice!);
        //   }
        //   print('discount: $discountPercent');
        // }

        _page++;
      });
      //    for(int i=0;i<_allProducts.length; i++){
      //      if(_allProducts[i].regularPrice == null){

      //        setState(() {
      //          regular=double.parse(_allProducts[i].regularPrice!);
      //        regular =1000.0;
      //        });
      //      }
      //       if(_allProducts[i].salePrice == null){

      //       setState(() {
      //          sale=double.parse(_allProducts[i].salePrice!);
      //        sale =500.0;
      //       });
      //      }
      //   setState(() {
      //   //   difference=regular! -sale!;
      //   // discountPercent=(difference! / regular!)*100;
      //   });
      // }
      // print(discountPercent);

      //print(discountPercent);

      print("products: ");
      //print(value);
      var val = jsonEncode(value);
      print(val);
    });
  }

  void getCategorys() {
    print("call1");
    CategoryService.fetchCategory().then((value) {
      // setState({
      //   _allProducts
      // })
      setState(() {
        _ccategories = value;
      });

      print("category: ");
      //print(value);
      //var val = jsonEncode(value);
      //print(val);
    });
  }

  void _onLoading() async {
    _page++;
    var list = await ProductService.fetchProducts(_page, _limit);
    if (list != null) {
      _allProducts.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  void _onLoading2() async {
    _page++;
    var list = await ProductService.fetchProducts(_page, _limit);
    if (list != null) {
      _allProducts.addAll(list);
    }
    setState(() {
      refreshController2.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  _onRefresh2() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  checkNetConnectivity() async {
    var result = await (Connectivity().checkConnectivity());
    viewModelProvider!.compareProviderList!.length;
    print(result);
    await viewModelProvider!.getFavoriteModel();

    await viewModelProvider!.getCartModel();
    setState(() {});

    if (result == ConnectivityResult.mobile) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.wifi) {
      setState(() {
        _isConnection = true;
      });
    } else if (result == ConnectivityResult.none) {
      setState(() {
        _isConnection = false;
      });
    }
  }
  //  getDiscount(){

  //   print(discountPercent);
  // }

//   getQuantity()async{
//     setState(() {
//       cartItemCount = DBHelper().getCartListProductsCount();
//     });
//   }

// _buildQuentityNumber() {
//     return FutureBuilder(
//         future: cartItemCount,
//         builder: (context, snapshot) {
//           if (snapshot.hasData) {
//             //totalQuantity = snapshot.data;
//             return Text('${snapshot.data.toString()}',
//                 style: TextStyle(fontSize: 12));
//           }

//           if (null == snapshot.data) {
//             return Text("0", style: TextStyle(fontSize: 12));
//           }
//           return CircularProgressIndicator();
//         });
//   }

  @override
  Widget build(BuildContext context) {
    // final productProvider =
    //     Provider.of<ProductProviderApi>(context, listen: false);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          // leading: IconButton(
          //   onPressed: () {
          //     Navigator.push(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => FavoritProductScreen()));
          //   },
          //   icon: Icon(
          //     Icons.favorite_border_outlined,
          //     color: Colors.grey,
          //     size: 30,
          //   ),
          // ),
          leading: Stack(
            children: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FavoritProductScreen()));
                },
                icon: Icon(
                  Icons.favorite_border_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              ),
              Positioned(
                left: 28,
                bottom: 34,
                child: Container(
                    height: 20,
                    width: 20,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: viewModelProvider!.favoriteListLength != null
                        ? Consumer<ViewModelProvider>(
                            builder: (context, viewModel, child) {
                            return Text(
                              '${viewModelProvider!.favoriteListLength!.length}',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            );
                          })
                        : Text('')
                    //child: _buildQuentityNumber(),
                    // child: Text(
                    //   '2',
                    //   style: TextStyle(
                    //     color: Colors.white,
                    //   ),
                    // ),
                    ),
              )
            ],
          ),
          title: Center(
            child: Container(
                width: 150,
                //color: Colors.yellow,
                // child: Image.asset(
                //   'assets/images/enlight.png',
                //   fit: BoxFit.cover,
                // ),
                child: Image.network(logoLink)),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Stack(
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => CartPage()));
                    },
                    icon: Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.grey,
                      size: 30,
                    ),
                  ),
                  Positioned(
                    left: 25,
                    bottom: 20,
                    child: Container(
                        height: 20,
                        width: 20,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Consumer<ViewModelProvider>(
                            builder: (context, viewModel, child) {
                          return Text(
                            '${viewModelProvider!.cartListN!.length}',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          );
                        })
                        //child: _buildQuentityNumber(),
                        // child: Text(
                        //   '2',
                        //   style: TextStyle(
                        //     color: Colors.white,
                        //   ),
                        // ),
                        ),
                  )
                ],
              ),
            )
          ],
        ),
        body: SafeArea(
          child: Container(
            child: SmartRefresher(
              controller: refreshController,
              // scrollDirection: Axis.vertical,
              // physics: BouncingScrollPhysics(),

              enablePullUp: true,
              onLoading: _onLoading,
              onRefresh: _onRefresh,
              footer: ClassicFooter(
                loadStyle: LoadStyle.ShowWhenLoading,
              ),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: HomeCarouselWithDotsPage(imgList1: Slider),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Category',
                              style: TextStyle(
                                  color: lebelColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CategoryPage()));
                            },
                            child: Text('See More',
                                style: TextStyle(
                                    color: themeColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),

                      //Category

                      _isConnection
                          ? Container(
                              child: _ccategories.length > 0
                                  ? GridView.builder(
                                      shrinkWrap: true,
                                      physics: BouncingScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        mainAxisExtent: 200,
                                      ),
                                      itemCount: _ccategories.length,
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        return (

                                            //BrandCard(brandCardModel: brandCardModel,)
                                            //ProductCard(productCardModel: productCardModel,)
                                            CategoryCard(
                                                cateoryModel:
                                                    _ccategories[index]
                                                //  productCardModel: _allProducts[index],
                                                ));
                                      })
                                  : LoaderCategory(),
                            )
                          : NoInternet(
                              pressed: () async {
                                await checkNetConnectivity();
                                _isConnection == false
                                    ? CustomSnackbar.snackbar(
                                        context, "Turn Internet ON")
                                    : checkNetConnectivity();
                                getCategorys();
                                getProducts();
                                getBrands();
                              },
                            ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Brand',
                              style: TextStyle(
                                  color: lebelColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => BrandScreen(),
                                  ));
                            },
                            child: Text('See More',
                                style: TextStyle(
                                    color: themeColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      //brand
                      _isConnection
                          ? Container(
                              child: brands.length > 0
                                  ? GridView.builder(
                                      shrinkWrap: true,
                                      physics: BouncingScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        mainAxisExtent: 120,
                                      ),
                                      itemCount: 6,
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        return (

                                            //BrandCard(brandCardModel: brandCardModel,)
                                            //ProductCard(productCardModel: productCardModel,)
                                            BrandCard(
                                          brandModel: brands[index],
                                        ));
                                      })
                                  : LoaderBrand(),
                            )
                          : NoInternet(
                              pressed: () async {
                                await checkNetConnectivity();
                                _isConnection == false
                                    ? CustomSnackbar.snackbar(
                                        context, "Turn Internet ON")
                                    : checkNetConnectivity();
                                getCategorys();
                                getProducts();
                                getBrands();
                              },
                            ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      // Container(
                      //   child: GridView.builder(
                      //       shrinkWrap: true,
                      //       physics: BouncingScrollPhysics(),
                      //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      //         crossAxisCount: 3,
                      //         mainAxisExtent: 140,
                      //       ),
                      //       itemCount: 6,
                      //       itemBuilder: (BuildContext context, index) {
                      //         BrandCardModel brandCardModel = brandList[index];
                      //         //ProductCardModel productCardModel=productList[index];
                      //         return (BrandCard(
                      //           brandCardModel: brandCardModel,
                      //         )
                      //             //ProductCard(productCardModel: productCardModel,)

                      //             );
                      //       }),
                      // ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Flash Sale',
                              style: TextStyle(
                                  color: lebelColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)),
                          // Text('See More',
                          //     style: TextStyle(
                          //         color: themeColor,
                          //         fontWeight: FontWeight.bold,
                          //         fontSize: 16)),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),

                      //product _isloading?BuildLoading():

                      SafeArea(
                        child: _isConnection
                            ? Container(
                                //color: Colors.amber,
                                height: ScreenUtil().setHeight(260),
                                child: SmartRefresher(
                                  controller: refreshController2,
                                  // scrollDirection: Axis.vertical,
                                  // physics: BouncingScrollPhysics(),

                                  enablePullUp: true,
                                  enablePullDown: false,
                                  onLoading: _onLoading2,
                                  onRefresh: _onRefresh2,
                                  footer: ClassicFooter(
                                    loadStyle: LoadStyle.ShowWhenLoading,
                                    canLoadingText: "Pull Left to Refresh",
                                    textStyle: TextStyle(),
                                  ),
                                  child: _isloading2
                                      //_allProducts.length>0
                                      ? LoaderSearchResultScreen()
                                      : ListView.builder(
                                          //reverse: true,
                                          physics: BouncingScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          shrinkWrap: true,
                                          itemCount: _allProducts.length,
                                          itemBuilder:
                                              (BuildContext context, index) {
                                            // var productCardModel =
                                            //     _allProducts[index];

                                            return (ProductCardNew(
                                              productCardModel:
                                                  _allProducts[index],
                                              //discount: discountPercent,
                                            ));
                                          }),
                                ))
                            : NoInternet(
                                pressed: () async {
                                  await checkNetConnectivity();
                                  _isConnection == false
                                      ? CustomSnackbar.snackbar(
                                          context, "Turn Internet ON")
                                      : checkNetConnectivity();

                                  getProducts();
                                  getCategorys();
                                },
                              ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('All Products',
                              style: TextStyle(
                                  color: lebelColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SearchResultPage()));
                            },
                            child: Text('See More',
                                style: TextStyle(
                                    color: themeColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Container(
                      //   child: GridView.builder(
                      //       shrinkWrap: true,
                      //       physics: BouncingScrollPhysics(),
                      //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      //         crossAxisCount: 2,
                      //         mainAxisExtent: 280,
                      //       ),
                      //       itemCount: productProvider.productModelList.length,
                      //       itemBuilder: (BuildContext context, index) {

                      //         return Text('${productProvider.productModelList[index].name}');
                      //       }),
                      // )
                      _isConnection
                          ? Container(
                              child: _isloading
                                  ? LoaderSearchResultScreen()
                                  : GridView.builder(
                                      shrinkWrap: true,
                                      physics: BouncingScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        mainAxisExtent:
                                            ScreenUtil().setHeight(260),
                                      ),
                                      itemCount: _allProducts.length,
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //ProductCardModel productCardModel=productList[index];
                                        return (ProductCardNew(
                                          productCardModel: _allProducts[index],
                                          //discount: discountPercent,
                                        ));
                                      }))
                          : NoInternet(
                              pressed: () async {
                                await checkNetConnectivity();
                                _isConnection == false
                                    ? CustomSnackbar.snackbar(
                                        context, "Turn Internet ON")
                                    : checkNetConnectivity();

                                getProducts();
                                getCategorys();
                              },
                            ),

                      /*SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        children: [
                                          Row(
                                            
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              //ProductCard(height: 280,width: 170,),
                                              
                                               //ProductCard(height: 280,width: 170,),
                                            ],
                                          ),
                                          SizedBox(height: 20,),
                                           Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              // ProductCard(height: 280,width: 170,),
                                               //ProductCard(height: 280,width: 170,),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),*/
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
