import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:mart_up/models/write_review_model.dart';
import 'package:mart_up/screens/product_details_screen.dart';
import 'package:mart_up/screens/write_review_screen.dart';
import 'package:mart_up/services/review_service.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/loder_review_screen.dart';
import 'package:mart_up/widgets/review_card.dart';
import 'package:mart_up/widgets/roundedButton.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

// ignore: must_be_immutable
class ReviewScreen extends StatefulWidget {
  int? productId;
  //bool? btnShowstatus;
  //bool? productToReview;
  int? orderDetailsToWriteReview;
  ReviewScreen({this.productId, this.orderDetailsToWriteReview});

  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  int _page = 1;
  int _limit = 3;
  int? showWriteReviewButton;

  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  bool _isloading = true;

  @override
  void initState() {
    getreview(_page, _limit);

    // isReviewLoading = false;

    super.initState();
    if (widget.orderDetailsToWriteReview == 1) {
      showWriteReviewButton = widget.orderDetailsToWriteReview!;
    } else {
      showWriteReviewButton = 0;
    }
  }

  List<ReviewModel> review = [];

  void getreview(int p, int l) {
    ReviewService.fetchRivewbyProductId(
            productId: widget.productId, limit: _limit, page: _page)
        .then((value) {
      setState(() {
        review = value;
        _isloading = false;
        _page++;
        //isReviewLoading = false;
      });

      var val = jsonEncode(review);
      print(val);
    });
  }

  void _onLoading() async {
    _page++;
    var list = await ReviewService.fetchRivewbyProductId(
        productId: widget.productId, limit: _limit, page: _page);
    if (list != null) {
      review.addAll(list);
    }
    setState(() {
      refreshController.loadComplete();
    });
  }

  _onRefresh() {
    setState(() {
      refreshController.refreshCompleted();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context,
                  MaterialPageRoute(builder: (context) => ProductDetailPage()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Reviews ",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
          child: SmartRefresher(
            controller: refreshController,
            // scrollDirection: Axis.vertical,
            // physics: BouncingScrollPhysics(),

            enablePullUp: true,
            onLoading: _onLoading,
            onRefresh: _onRefresh,
            footer: ClassicFooter(
              loadStyle: LoadStyle.ShowWhenLoading,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _isloading
                            ? LoderReview()
                            : _isloading == false && review.isEmpty
                                ? Center(
                                    child: Column(
                                      children: [
                                        SizedBox(height: 200),
                                        Container(
                                          height: 100,
                                          width: 200,
                                          child: Image.asset(
                                            'assets/images/empty.png',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          "No Review Added",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container(
                                    child: ListView.builder(
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    //scrollDirection: Axis.vertical,
                                    itemCount: review.length,
                                    itemBuilder: (context, index) {
                                      return ReviewCard(
                                          reviewModel: review[index]);
                                    },
                                  )
                                  ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: showWriteReviewButton == 0
            ? Text('')
            : Padding(
                padding: const EdgeInsets.all(12.0),
                child: RoundedButton(
                    buttonText: "Write Review",
                    func: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WriteReviewScreen(
                                    productId: widget.productId,
                                  )));
                    }),
              ),
      ),
    );
  }
}
