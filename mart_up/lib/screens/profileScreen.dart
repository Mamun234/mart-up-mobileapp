import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mart_up/constants/colors.dart';
//import 'package:mart_up/models/PhotoUploadeModel.dart';
import 'package:mart_up/models/profileInfoModel.dart';
import 'package:mart_up/models/login_response_model.dart';
//import 'package:mart_up/models/update_picture_model.dart';
import 'package:mart_up/screens/change_email_screen.dart';
import 'package:mart_up/screens/change_name_screen.dart';
import 'package:mart_up/screens/change_password_screen.dart';
import 'package:mart_up/services/get_user_service.dart';
//import 'package:mart_up/services/image_upload_service.dart';
import 'package:mart_up/services/local_db_helper.dart';
import 'package:mart_up/widgets/loader_profile_screen.dart';
//import 'package:mart_up/services/update_user_service.dart';
import 'package:mart_up/widgets/loading_indicator.dart';
import 'package:mart_up/widgets/navigation_bar.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  XFile? image;
  //final ImagePicker _picker = ImagePicker();
  String imagePath = '';
  ProfileInfoModel? _users;
  //PhotoUploadeModel model = PhotoUploadeModel();
  int? id;
  LoginResponseModel info = LoginResponseModel();
  // UpdatePhotoModel? photoUrl;
  // UpdatePhotoModel img = UpdatePhotoModel();
  @override
  void initState() {
    //print("call");
    super.initState();
    getInfo();
    // imageUpload(model);

    // final productProvider= Provider.of<ProductProviderApi>(context,listen: false);
    //productProvider.getProductModelList();
  }

  // void getImageUrl() {
  //   UpdateUserService.updateUserImage().then((value) {
  //     // setState({
  //     //
  //     // })
  //     setState(() {
  //       photoUrl = value;
  //     });

  //     var val = jsonEncode(value);
  //     print(val);
  //   });
  // }

  // imageUpload(PhotoUploadeModel model) async {
  //   ImageUplodeServicr().imageupload(model).then((value) {
  //     setState(() {
  //       // abc = value;
  //     });

  //     var val = jsonEncode(value);
  //     //print(val);
  //   });
  // }

  getInfo() async {
    var a = await DBHelper().getLoginUserData();
    setState(() {
      info = a!;
      print('info: ${info.data!.id}');
    });
    getUser();
  }

  Future<ProfileInfoModel>? getUser() {
    GetUserService.fetchUserInfo(info.data!.id!).then((value) {
      setState(() {
        _users = value;
      });

      // ignore: unused_local_variable
      var val = jsonEncode(value);
      return true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CustomNavigationBar()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Profile",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
          child: _users != null
              ? Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: double.infinity,
                            height: 110,
                            child: Row(
                              children: [
                                GestureDetector(
                                  // onTap: () {
                                  //   _showImagePicker(context);
                                  // },
                                  child: imagePath == ''
                                      ? CircleAvatar(
                                          radius: 50,
                                          // backgroundImage: AssetImage(
                                          //   "assets/images/avater.jpeg",
                                          // ),
                                          backgroundImage: _users!.avatarUrl! !=
                                                  ''
                                              ? NetworkImage(_users!.avatarUrl!)
                                              : AssetImage(
                                                      "assets/images/avater.jpeg")
                                                  as ImageProvider,
                                          // Image.file(file)
                                        )
                                      : ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          child: Image.file(
                                            File(
                                              imagePath,
                                            ),
                                            width: 100,
                                            height: 100,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                ),
                                SizedBox(
                                  width: 22,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(25.0),
                                  child: Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ChangeNameScreen(
                                                        name:
                                                            _users!.firstName!,
                                                        id: info.data!.id!,
                                                      )));
                                        },
                                        child: Text(
                                          _users!.firstName!,
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Text(
                                        _users!.username!,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),

                        SizedBox(
                          height: 22,
                        ),

                        //email
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 14.0,
                            bottom: 14.0,
                          ),
                          child: Container(
                            width: double.infinity,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Icon(
                                    Icons.mail_outlined,
                                    color: themeColor,
                                    size: 30.0,
                                  ),
                                ),
                                Expanded(
                                    flex: 2,
                                    child: Text(
                                      "Email",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    )),
                                Expanded(
                                    flex: 3,
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ChangeEmailScreen(
                                                      email: _users!.email,
                                                      id: info.data!.id!,
                                                    )));
                                      },
                                      child: Text(
                                        _users!.email!,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    )),
                                Icon(
                                  Icons.chevron_right_outlined,
                                  color: Colors.grey,
                                  size: 22,
                                ),
                              ],
                            ),
                          ),
                        ),

                        //Number
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 14.0,
                            bottom: 14.0,
                          ),
                          child: Container(
                            width: double.infinity,
                            child: Row(
                              children: [
                                // Expanded(
                                //   flex: 1,
                                //   child: Icon(
                                //     Icons.phone_iphone_outlined,
                                //     color: themeColor,
                                //     size: 30.0,
                                //   ),
                                // ),
                                // Expanded(
                                //     flex: 2,
                                //     // ignore: prefer_const_constructors
                                //     child: Text(
                                //       "Phone Number",
                                //       style: TextStyle(
                                //           fontSize: 16, fontWeight: FontWeight.bold),
                                //     )),
                                // Expanded(
                                //     flex: 3,
                                //     child: GestureDetector(
                                //       onTap: () {
                                //         Navigator.push(
                                //             context,
                                //             MaterialPageRoute(
                                //                 builder: (context) =>
                                //                     ChangePhoneNumScreen()));
                                //       },
                                //       child: Text(
                                //         "(+880)1738672234",
                                //         textAlign: TextAlign.right,
                                //         style: TextStyle(
                                //             fontSize: 14,
                                //             color: Colors.grey,
                                //             fontWeight: FontWeight.bold),
                                //       ),
                                //     )),
                                // Icon(
                                //   Icons.chevron_right_outlined,
                                //   color: Colors.grey,
                                //   size: 22,
                                // ),
                              ],
                            ),
                          ),
                        ),

                        //password
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 14.0,
                            bottom: 14.0,
                          ),
                          child: Container(
                            width: double.infinity,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Icon(
                                    Icons.lock_outlined,
                                    color: themeColor,
                                    size: 30.0,
                                  ),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Text(
                                      "Password",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    )),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                    flex: 2,
                                    child: Center(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ChangePasswordScreen(
                                                        id: info.data!.id!,
                                                      )));
                                        },
                                        child: Text(
                                          "***********",
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    )),
                                Icon(
                                  Icons.chevron_right_outlined,
                                  color: Colors.grey,
                                  size: 22,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : LoaderProfile(),
        ),
      ),
    );
  }

  // _imgFromCamera() async {
  //   try {
  //     final XFile? photo = await _picker.pickImage(source: ImageSource.camera);

  //     // PhotoUploadeModel photoUplodeModel = PhotoUploadeModel(image: photo);
  //     // ImageUplodeServicr().imageupload(
  //     //   photoUplodeModel,
  //     // );
  //     setState(() {
  //       imagePath = photo!.path;
  //     });
  //     // UpdatePhotoModel cameraImage = UpdatePhotoModel(sourceUrl: imagePath);
  //     // UpdateUserService().updateUserImage(cameraImage, context);
  //   } catch (error) {
  //     print(error.toString());
  //   }
  // }

  // _imgFromGallery() async {
  //   try {
  //     final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

  //     print(image);
  //     // PhotoUploadeModel photoUplodeModel = PhotoUploadeModel(image: image);
  //     // ImageUplodeServicr().imageupload(
  //     //   photoUplodeModel,
  //     // );

  //     setState(() {
  //       imagePath = image!.path;
  //     });
  //     // UpdatePhotoModel galleryImage = UpdatePhotoModel(sourceUrl: imagePath);
  //     // UpdateUserService().updateUserImage(galleryImage, context);
  //   } catch (error) {
  //     print(error.toString());
  //   }
  // }

  // void _showImagePicker(context) {
  //   showModalBottomSheet(
  //       context: context,
  //       builder: (BuildContext bc) {
  //         return SafeArea(
  //           child: Container(
  //             child: new Wrap(
  //               children: <Widget>[
  //                 new ListTile(
  //                     leading: new Icon(Icons.photo_library),
  //                     title: new Text('Photo Library'),
  //                     onTap: () {
  //                       _imgFromGallery();
  //                       Navigator.of(context).pop();
  //                     }),
  //                 new ListTile(
  //                   leading: new Icon(Icons.photo_camera),
  //                   title: new Text('Camera'),
  //                   onTap: () {
  //                     _imgFromCamera();
  //                     Navigator.of(context).pop();
  //                   },
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
}
