//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/models/update_password_model.dart';
import 'package:mart_up/screens/profileScreen.dart';
import 'package:mart_up/services/update_user_service.dart';
import 'package:mart_up/widgets/roundedButton.dart';

// ignore: must_be_immutable
class ChangePasswordScreen extends StatefulWidget {
  int? id;
  ChangePasswordScreen({this.id});

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final formKey = GlobalKey<FormState>(); //key for form
  String name = "";
  bool _obscureText = true;
  //TextEditingController fullName=TextEditingController();

  TextEditingController oldPassword = TextEditingController();

  TextEditingController newPassword = TextEditingController();

  TextEditingController confirmNewPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context,
                  MaterialPageRoute(builder: (context) => ProfileScreen()));
            },
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.grey,
              size: 22,
            ),
          ),
          title: Text(
            "Change Password",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 12, right: 12),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Old Password",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      //PasswordInput(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.black26, width: 1.5),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: TextFormField(
                            controller: oldPassword,
                            decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: InputBorder.none,
                                hintText: "Confirm Password",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(Icons.lock),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            obscureText: _obscureText,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Current Password",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                      // PasswordInput(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.black26, width: 1.5),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: TextFormField(
                            controller: newPassword,
                            decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: InputBorder.none,
                                hintText: "Confirm Password",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(Icons.lock),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            obscureText: _obscureText,
                            validator: (value) {
                              if (value!.isEmpty ||
                                  value != confirmNewPassword.text) {
                                return "Enter correct Password";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Confirm Current Password",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      //PasswordInput(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.black26, width: 1.5),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: TextFormField(
                            controller: confirmNewPassword,
                            decoration: InputDecoration(
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                border: InputBorder.none,
                                hintText: "Confirm New Password",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Icon(Icons.lock),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(_obscureText
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            obscureText: _obscureText,
                            validator: (value) {
                              if (value!.isEmpty || value != newPassword.text) {
                                return "Enter correct Password";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(12.0),
          child: RoundedButton(
            buttonText: "Save",
            func: () async {
              UpdatePasswordModel updatePass =
                  UpdatePasswordModel(password: confirmNewPassword.text);
              if (formKey.currentState!.validate()) {
                UpdateUserService().updateUserPassword(updatePass,widget.id!, context);
              }
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => const ProfileScreen()));
            },
          ),
        ),
      ),
    );
  }
}

// class PasswordInput extends StatefulWidget {
//   @override
//   State<PasswordInput> createState() => _PasswordInputState();
// }

// class _PasswordInputState extends State<PasswordInput> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 7.0),
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(color: Colors.black26, width: 1.5),
//           borderRadius: BorderRadius.circular(16),
//         ),
//         child: TextField(
//           decoration: InputDecoration(
//             contentPadding: const EdgeInsets.symmetric(vertical: 16),
//             border: InputBorder.none,
//             //hintText: "Password",
//             prefixIcon: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 16),
//               child: Icon(Icons.lock),
//             ),
//           ),
//           obscureText: true,
//         ),
//       ),
//     );
//   }
// }
