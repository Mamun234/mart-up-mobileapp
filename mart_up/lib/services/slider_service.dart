import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/brand_model.dart';
import 'package:mart_up/models/slider_model.dart';

class SliderService {
  static var client = http.Client();

  static List<SliderModel> parseSlider(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<SliderModel> slider =
        list.map((e) => SliderModel.fromJson(e)).toList();
    return slider;
  }

  static Future<List<SliderModel>> fetchSlider() async {
    var client = http.Client();
    var url = Uri.parse(sliderApi);
    final sliderresponse = await client.get(url);

    print(sliderresponse.body);

    return compute(parseSlider, sliderresponse.body);
  }
}
