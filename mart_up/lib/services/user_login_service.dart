import 'dart:convert';

//import 'package:flutter/material.dart';
import 'package:http/http.dart';
//import 'package:mart_up/constants/links.dart';
import 'package:mart_up/environment/environment.dart';
//import 'package:mart_up/models/profileInfoModel.dart';
import 'package:mart_up/models/login_response_model.dart';
//import 'package:mart_up/models/order_model.dart';
//import 'package:mart_up/models/product.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up/models/user_login_model.dart';
//import 'package:mart_up/screens/success_screen.dart';
import 'package:mart_up/services/local_db_helper.dart';
//import 'package:mart_up/widgets/custom_snackbar.dart';
//import 'package:mart_up/widgets/navigation_bar.dart';
//import 'package:shared_preferences/shared_preferences.dart';

class UserLoginService {
  static var client = http.Client();

  static LoginResponseModel parseLogin(String responseBody) {
    //var data = jsonDecode(responseBody) as dynamic;
    var data = jsonDecode(responseBody);
    LoginResponseModel log = LoginResponseModel.fromJson(data);
    // List<GetUserModel> user =
    //     list.map((model) => GetUserModel.fromJson(model)).toList();

    print(log.data!.nicename);
    return log;
  }
//  static  parseLoginBadCode(String responseBody) {
//     //var data = jsonDecode(responseBody) as dynamic;
//     var data= jsonDecode(responseBody);
//     //LoginResponseModel log= LoginResponseModel.fromJson(data);
//     // List<GetUserModel> user =
//     //     list.map((model) => GetUserModel.fromJson(model)).toList();

//     //print(log.data!.email);
//     return data;
//   }

  Future<Response?> loginUser(UserLoginModel login) async {
    print(jsonEncode(login.toJson()));
    // LoginResponseModel login;
    var apiUrl = Uri.parse(loginUrl);
//final response = await client.get(apiUrl);
    try {
      final response = await client.post(
        apiUrl,
        body: login.toJson(),
        // body:{
        //   "email":email,
        //   "password":password,
        // },
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
      );
      final responseString = response.body;
      print(responseString);
      //final encodedResponse=jsonEncode(responseString);

      //login=LoginResponseModel.fromJson(response.body);
      if (response.statusCode == 200) {
        //LoginResponseModel loginResponseModel=LoginResponseModel.fromJson(encodedResponse);
        DBHelper().removeLoginUserData();
        DBHelper().saveLoginUserData(response.body);

        //return compute(parseLogin, response.body);

      }
      return response;
    } catch (e) {
      print(e);
    }
  }

//    Future<LoginResponseModel> loginU(String  username,password, context) async {

//      //print(jsonEncode(login.toJson()));
//    LoginResponseModel login;
//     var apiUrl = Uri.parse(
//        loginUrl);
// //final response = await client.get(apiUrl);
//     try {
//       final response = await client.post(
//         apiUrl,
//        // body: login.toJson(),
//         body:{
//           "username":username,
//           "password":password,
//         },
//         headers: {"Content-Type": "application/x-www-form-urlencoded"},

//       );
//       final responseString = response.body;
//       print(responseString);
//       final encodedResponse=jsonEncode(responseString);

//       //login=LoginResponseModel.fromJson(response.body);
//       if(response.statusCode==200){
//      //LoginResponseModel loginResponseModel=LoginResponseModel.fromJson(encodedResponse);
//      login=LoginResponseModel.fromJson(response);
//          Navigator.push(
//           context, MaterialPageRoute(builder: (context) => CustomNavigationBar()));
//       }
//       else{
//         CustomSnackbar.snackbar(context, "Login Failed");
//       }

//     } catch (e) {
//       print(e);
//     }

//   }
// static Future<List<OrderModel>> createOrder(var payment,List<dynamic> listItem,List<dynamic> shiping)async{

}
