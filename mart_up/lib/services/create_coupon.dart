import 'package:flutter/material.dart';

class CreateCoupon extends StatefulWidget {
  const CreateCoupon({ Key? key }) : super(key: key);

  @override
  _CreateCouponState createState() => _CreateCouponState();
}

class _CreateCouponState extends State<CreateCoupon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(onPressed: (){}, icon: Icon(Icons.arrow_back,color: Colors.grey,)),
        title: Text("Add Coupon",
        style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}