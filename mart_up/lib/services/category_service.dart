import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/category_model.dart';
import 'package:http/http.dart' as http;

class CategoryService {
  static var client = http.Client();

  static List<CategoryModel> parseCategory(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<CategoryModel> categories =
        list.map((e) => CategoryModel.fromJson(e)).toList();
    return categories;
  }

  static Future<List<CategoryModel>> fetchCategory() async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products/categories?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    final cresponse = await client.get(url);

    print(cresponse);

    return compute(parseCategory, cresponse.body);
    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }
}
