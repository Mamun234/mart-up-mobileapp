import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/product.dart';
import 'package:http/http.dart' as http;

class OfferService {
  static var client = http.Client();

  static List<Product> parseOffer(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;
    List<Product> offers =
        list.map((model) => Product.fromJson(model)).toList();
    return offers;
  }
  

  static Future<List<Product>> fetchOffer(int page, int limit,{bool onSale=true}
    ) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit}&page=${page}&on_sale=${onSale}");
    final response = await client.get(url);

    print(response);

    return compute(parseOffer, response.body);
  
    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }

 

// static Future<List<OrderModel>> createOrder(var payment,List<dynamic> listItem,List<dynamic> shiping)async{
}
