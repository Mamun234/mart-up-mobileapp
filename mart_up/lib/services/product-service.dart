import 'dart:convert';
// import 'dart:ffi';

import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:mart_up/constants/links.dart';
import 'package:mart_up/environment/environment.dart';
// import 'package:mart_up/models/order_model.dart';
import 'package:mart_up/models/product.dart';
import 'package:http/http.dart' as http;
// import 'package:mart_up/screens/success_screen.dart';

class ProductService {
  static var client = http.Client();

  static List<Product> parseProduct(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;
    List<Product> categories =
        list.map((model) => Product.fromJson(model)).toList();
    return categories;
  }

  static Product parseProductById(String responseBody) {
    //var data = jsonDecode(responseBody) as dynamic;
    var data = jsonDecode(responseBody);
    Product user = Product.fromJson(data);
    // List<GetUserModel> user =
    //     list.map((model) => GetUserModel.fromJson(model)).toList();
    return user;
  }

  static Future<List<Product>> fetchProducts(
    int page,
    int limit, {
    String? searchKey,
    int? categoryId,
    String? minPrice,
    String? maxPrice,
    int? brandId,

    //String? stock_status
  }) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit}&page=${page}&search=${searchKey == null ? ' ' : searchKey}&category=${categoryId == null ? '' : categoryId}&min_price=${minPrice == null ? ' ' : minPrice}&max_price=${maxPrice == null ? ' ' : maxPrice}&attribute=pa_brand&attribute_term=${brandId == null ? '' : brandId}");
    final response = await client.get(url);

    print(response);

    return compute(parseProduct, response.body);

    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }

  static Future<Product> fetchProductsById(int id) async {
    var client = http.Client();

    // var url = Uri.parse(
    //     "https://demo.baburhut.com/wp-json/wc/v3/products/$id?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");

    var url = Uri.parse(rootApi +
        "products/" +
        id.toString() +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    final response = await client.get(url);

    print('product by id: ${response.body}');

    Future<Product> product = compute(parseProductById, response.body);
    return product;

    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }

  static Future<List<Product>> fetchRelatedProducts(
      int page, int limit, List<int>? relatedId) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "products?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit}&page=${page}&include=${relatedId}");
    final response = await client.get(url);

    print(response);

    return compute(parseProduct, response.body);

    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }

// static Future<List<OrderModel>> createOrder(var payment,List<dynamic> listItem,List<dynamic> shiping)async{
}
