import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:mart_up/environment/environment.dart';
// import 'package:flutter/material.dart';
// import 'package:mart_up/constants/links.dart';
// import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/profileInfoModel.dart';
// import 'package:mart_up/models/order_model.dart';
// import 'package:mart_up/models/product.dart';
import 'package:http/http.dart' as http;
// import 'package:mart_up/screens/success_screen.dart';

class GetUserService {
  static var client = http.Client();

  static ProfileInfoModel parseUser(String responseBody) {
    //var data = jsonDecode(responseBody) as dynamic;
    var data = jsonDecode(responseBody);
    ProfileInfoModel user = ProfileInfoModel.fromJson(data);
    // List<GetUserModel> user =
    //     list.map((model) => GetUserModel.fromJson(model)).toList();
    return user;
  }

  static Future<ProfileInfoModel> fetchUserInfo(int? customerId) async {
    var client = http.Client();
    // var url = Uri.parse("https://demo.baburhut.com/wp-json/wc/v3/customers/${customerId == null ? '' : customerId}?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f");
    var url = Uri.parse(rootApi +
        "customers/" +
        customerId.toString() +
        "?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    final response = await client.get(url);

    print(response);

    return compute(parseUser, response.body);
    // if (response.statusCode == 200) {
    //   return compute(parseCategory, response.body);
    // } else if (response.statusCode == 404) {
    //   // throw Exception("Not Found");
    // } else {
    //    throw Exception("Can\'t get brands");
    // }
  }

// static Future<List<OrderModel>> createOrder(var payment,List<dynamic> listItem,List<dynamic> shiping)async{

}
