import 'dart:convert';
// import 'dart:io';
// import 'package:flutter/material.dart';
import 'package:mart_up/models/add_address_model.dart';
import 'package:mart_up/models/cart_model.dart';
import 'package:mart_up/models/favorite_model.dart';
import 'package:mart_up/models/login_response_model.dart';
import 'package:mart_up/models/product.dart';
// import 'package:mart_up/models/product.dart';
// import 'package:mart_up/services/user_login_service.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
// import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DBHelper {
  static Database? _db;
  static const String ID = 'id';
  static const String PROID = 'pid';
  //static const String NAME = 'fname';
  static const String PNAME = 'name';
  static const String CITY = 'city';
  static const String FNAME = 'fname';
  static const String LNAME = 'lname';
  static const String ROADNO = 'roadNo';
  static const String HOUSENO = 'houseNo';
  static const String MOBILENO = 'mobileNo';
  static const String IMAGE = 'image';
  static const String PRICE = 'price';
  static const String REGULARPRICE = 'regularprice';
  static const String SHOTDES = 'shotdescription';
  static const String QUANTITY = 'quantity';
  static const String CATEGORIES = 'categories';
  static const String ATTRIBUTES = 'attribute';
  static const String FAVORITELIST_TABLE = 'Favoritelist';

  static const String CARTLIST_TABLE = 'Cartlist';
  static const String ADDRESS_TABLE = 'Addresslist';
  static const String DB_NAME = 'martup.db';

  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    }
    _db = await initDb();
    return _db!;
  }

  static initDb() async {
    String path = join(await getDatabasesPath(), DB_NAME);
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  static _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $FAVORITELIST_TABLE (id INTEGER PRIMARY KEY,name TEXT,salePrice TEXT,regularPrice TEXT,quantity INTEGER,img TEXT)');

    await db.execute(
        'CREATE TABLE $CARTLIST_TABLE (id INTEGER PRIMARY KEY,name TEXT,salePrice TEXT,regularPrice TEXT,quantity INTEGER,img TEXT)');

    await db.execute(
        "CREATE TABLE $ADDRESS_TABLE ($ID INTEGER PRIMARY KEY,$CITY TEXT, $FNAME TEXT, $LNAME TEXT, $ROADNO TEXT,$HOUSENO TEXT,$MOBILENO TEXT)");
  }

  Future<FavoriteModel> saveToFavoriteList(FavoriteModel product) async {
    var dbClient = await db;
    product.id = await dbClient.insert(FAVORITELIST_TABLE, product.toJson());
    print(product);
    return product;
  }
  // Future<int> insertTransaction(
  //     FavoriteModel transactionModel, context) async {
  //       print(transactionModel);
  //   Database db = await this.db;
  //   final int result = await db.insert(FAVORITELIST_TABLE, transactionModel.toJson());
  //    print('Data added: $result');

  //   return result;
  // }

  Future<List<FavoriteModel>> getFavoriteListProducts() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(FAVORITELIST_TABLE, columns: [
      'id',
      'name',
      'salePrice',
      'regularPrice',
      'quantity',
      'img'
    ]);
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
    List<FavoriteModel> products = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        products.add(FavoriteModel.fromJson(Map.from(maps[i])));
      }
    }
    // ignore: unused_local_variable
    var v = jsonEncode(products);
    //print(v);
    return products;
  }

  Future<int> deleteFavoriteProduct(int id) async {
    var dbClient = await db;
    return await dbClient
        .delete(FAVORITELIST_TABLE, where: '$ID = ?', whereArgs: [id]);
  }

  Future<CartModel> saveToCartList(CartModel productCart) async {
    var dbClient = await db;
    productCart.id =
        await dbClient.insert(CARTLIST_TABLE, productCart.toJson());
    print(productCart);
    return productCart;
  }

  Future<List<CartModel>> getCartListProducts() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(CARTLIST_TABLE, columns: [
      'id',
      'name',
      'salePrice',
      'regularPrice',
      'quantity',
      'img'
    ]);
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
    List<CartModel> products = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        products.add(CartModel.fromJson(Map.from(maps[i])));
      }
    }
    // ignore: unused_local_variable
    var v = jsonEncode(products);
    //print(v);
    return products;
  }

  Future<double> getCartTotalAmount() async {
    var dbClient = await db;
    var maps = await dbClient.query(CARTLIST_TABLE, columns: [
      'id',
      'name',
      'salePrice',
      'regularPrice',
      'quantity',
      'img'
    ]);
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");

    double? amount;
    List<CartModel> products = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        products.add(CartModel.fromJson(Map.from(maps[i])));
      }
      amount = products
          .map((e) => double.parse(e.salePrice!) * e.quantity!)
          .reduce((value, element) => value + element);
    }
    amount ??= 0;
    return amount;
  }

  Future<int> getCartListProductsCount() async {
    var dbClient = await db;
    var maps = await dbClient.query(CARTLIST_TABLE, columns: [
      'id',
      'name',
      'salePrice',
      'regularPrice',
      'quantity',
      'img'
    ]);
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");

    int count = maps.length;
    return count;
  }

  Future<int> deleteCartItem(int id) async {
    var dbClient = await db;
    return await dbClient
        .delete(CARTLIST_TABLE, where: '$ID = ?', whereArgs: [id]);
  }

  Future<int> update(CartModel product) async {
    var dbClient = await db;
    return await dbClient.update(CARTLIST_TABLE, product.toJson(),
        where: '$ID = ?', whereArgs: [product.id]);
  }

  Future<AddAddressModel> saveToAddressList(
      AddAddressModel addAddressModel) async {
    var dbClient = await db;
    addAddressModel.id =
        await dbClient.insert(ADDRESS_TABLE, addAddressModel.toJson());
    //print(addAddressModel);
    return addAddressModel;
    /*
    await dbClient.transaction((txn) async {
      var query = "INSERT INTO $TABLE ($NAME) VALUES ('" + employee.name + "')";
      return await txn.rawInsert(query);
    });
    */
  }

  Future<List<AddAddressModel>> getAddresses() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(ADDRESS_TABLE,
        columns: [ID, CITY, FNAME, LNAME, ROADNO, HOUSENO, MOBILENO]);
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
    List<AddAddressModel> addresses = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        addresses.add(AddAddressModel.fromJson(Map.from(maps[i])));
      }
    }
    // ignore: unused_local_variable
    var val = jsonEncode(addresses);
    //print(val);
    return addresses;
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }

  static Future<AddAddressModel> getAddressById(int id) async {
    final db = await initDb();
    final List<Map<String, dynamic>> adrs =
        await db.query(ADDRESS_TABLE, where: '$ID = ?', whereArgs: [id]);
    // if (adrs.length > 0) {
    return AddAddressModel.fromJson(adrs.first);
    // }
    // return null;
  }

  Future<int> updateAddres(AddAddressModel addAddressModel) async {
    final db = await initDb();
    var re = await db.update(ADDRESS_TABLE, addAddressModel.toJson(),
        where: '$ID    = ?', whereArgs: [addAddressModel.id]);
    return re;
  }

  static Future<void> deleteAddress(int id) async {
    final db = await initDb();
    await db.delete(ADDRESS_TABLE, where: '$ID = ?', whereArgs: [id]);
  }
  //SharedPreference

  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  //  void saveLoginData(){
  //    LoginResponseModel info1=LoginResponseModel();
  //    //UserLoginService()
  //  }

  Future<bool> saveLoginUserData(String data) async {
    bool result = await prefs.then((value) => value.setString("user", data));
    return result;
  }

  Future<LoginResponseModel?> getLoginUserData() async {
    var result = await prefs.then((value) => value.getString("user"));
    if (result != null && result != "") {
      var data = parseProfileInfo(result);
      return data;
    } else {
      return null;
    }
  }

  removeLoginUserData() {
    prefs.then((value) => value.remove("user"));
  }

  static LoginResponseModel parseProfileInfo(String data) {
    var resultdata = jsonDecode(data) as dynamic;
    LoginResponseModel profileInfo = LoginResponseModel.fromJson(resultdata);
    return profileInfo;
  }
  // Future<bool> saveCompare(String data) async {
  //   bool result = await prefs.then((value) => value.setString("com", data));
  //   return result;
  // }

  // Future <List<Product?>> getComapre() async {
  //   var result = await prefs.then((value) => value.getString("com"));
  //   if (result != null && result != "") {
  //     var data = parseCompare(result);
  //     return data;
  //   } else {
  //     return null!;
  //   }
  // }

  // removeCompare() {
  //   prefs.then((value) => value.remove("com"));
  // }

  // static List<Product> parseCompare(String data) {
  //   var resultdata = jsonDecode(data) as dynamic;
  //  List <Product> compare = List<Product>.from(resultdata);
  //   return compare;
  // }
  

  // Future _onCreate(Database db, int version) async {
  //   await db.execute('''
  //     CREATE TABLE address(
  //         id INTEGER PRIMARY KEY,
  //         city TEXT,
  //         fname TEXT,
  //         lname TEXT,
  //         roadNo TEXT,
  //         houseNo TEXT,
  //         mobileNo TEXT

  //     )
  //     ''');
}
