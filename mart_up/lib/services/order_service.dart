import 'dart:convert';
import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/order_get_model.dart';
import 'package:mart_up/models/order_model.dart';
// import 'package:mart_up/screens/success_screen.dart';
import 'package:mart_up/models/order_details_model.dart';
import 'package:http/http.dart' as http;
// import 'package:mart_up/widgets/loading_indicator.dart';

class OrderService {
  static var client = http.Client();
  Future<Response> createOrder(OrderModel orderCreate,int id) async {
    print('amar data:');
    var orderBody;
    try {
      if (orderCreate.couponLines == null) {
        orderBody = jsonEncode(orderCreate.toJsonWithoutCoupon());
      } else {
        orderBody = jsonEncode(orderCreate.toJson());
      }
      // print();
    } catch (e) {
      print(e);
    }
    // print(jsonEncode(orderCreate.toJson()));
    // var apiUrl = Uri.parse(
    //     "https://demo.baburhut.com/wp-json/wc/v3/orders?consumer_key=ck_6cf35a586966d90c7bdb4f25f5c65844a5fdff2e&consumer_secret=cs_bdf7be4ca4283deb41a2811d2a41c1bc4902ee1f&customer=2");
    var apiUrl=Uri.parse(rootApi+"orders"+"?consumer_key="+ consumerKey+ "&consumer_secret=" +
        consumerSecret+"&customer="+ id.toString());
//final response = await client.get(apiUrl);
    // try {
    final response = await client.post(
      apiUrl,
      body: orderBody,
      headers: {"Content-Type": "application/json"},
    );
    final responseString = response.body;
    // if(response.body.isNotEmpty){
    //   _isLoading=true;
    // }
    print(responseString);

    return response;
  }

  //get order

  static List<OrderGetModel> parseOrder(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<OrderGetModel> orders =
        list.map((e) => OrderGetModel.fromJson(e)).toList();
    return orders;
  }

  static Future<List<OrderGetModel>> fetchOrder(
      {int? customerId, int? page, int? limit}) async {
    print(customerId);
    var url = Uri.parse(rootApi +
        "orders?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&customer=${customerId == null ? '' : customerId}&per_page=${limit}&page=${page}");
    // "&customer=${customerId == null ? '' : customerId = 2}");
    final orderResponse = await client.get(url);

    print(orderResponse);

    return compute(parseOrder, orderResponse.body);
    //compute(parseOrder, oresponse.body);
  }
  //order details

  OrderDetailsModel parseOrderItem(String responseBody) {
    var order = jsonDecode(responseBody);

    OrderDetailsModel orderItem = OrderDetailsModel.fromJson(order);
    return orderItem;
  }

//order details
  Future<OrderDetailsModel> fetchOrderDetails(int? orderId) async {
    var url = Uri.parse(rootApi +
        "orders/${orderId}?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    // "&customer=${customerId == null ? '' : customerId = 2}");
    final orderItemResponse = await client.get(url);

    print(orderItemResponse);

    return compute(parseOrderItem, orderItemResponse.body);
    //compute(parseOrder, oresponse.body);
  }
}
