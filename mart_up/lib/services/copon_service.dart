import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/coupon_model.dart';

class CouponService {
  static var client = http.Client();

  static List<CouponModel> parseCopun(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<CouponModel> coupon =
        list.map((e) => CouponModel.fromJson(e)).toList();
    return coupon;
  }

  static Future<List<CouponModel>> fetchCoupon({String? couponCode}) async {
    var client = http.Client();
    var url = Uri.parse(rootApi +
        "coupons?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&code=${couponCode}");
    final couponresponse = await client.get(url);

    print(couponresponse);

    return compute(parseCopun, couponresponse.body);
  }
}
