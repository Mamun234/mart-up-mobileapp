import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/write_review_model.dart';

class ReviewService {
  //String status="approved";
  static var client = http.Client();
  Future createReview({ReviewModel? reviewCreate}) async {
    print('amar data:');
    print(jsonEncode(reviewCreate!.toJson()));

    var apiUrl = Uri.parse(rootApi +
        "products/reviews?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    try {
      final createReviewResponse = await client.post(
        apiUrl,
        body: jsonEncode(reviewCreate.toJson()),
        headers: {"Content-Type": "application/json"},
      );
      final responseString = createReviewResponse.body;
      print(responseString);
    } catch (e) {
      print(e);
    }
  }

  //get rivew
  static List<ReviewModel> parseReview(String responseBody) {
    var list = jsonDecode(responseBody) as List<dynamic>;

    List<ReviewModel> orders =
        list.map((e) => ReviewModel.fromJson(e)).toList();
    return orders;
  }

  static Future<List<ReviewModel>> fetchRivewbyProductId(
      {int? limit, int? page, int? productId, int? one}) async {
    var url = Uri.parse(rootApi +
        "products/reviews?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret +
        "&per_page=${limit}&page=${page}&product=${productId}&status=approved");

    final reviewResponse = await client.get(url);

    print(reviewResponse);

    return compute(parseReview, reviewResponse.body);
    //compute(parseReview, oresponse.body);
  }
}
