import 'dart:convert';

// import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// import 'package:mart_up/constants/links.dart';
// import 'package:mart_up/environment/environment.dart';
// import 'package:mart_up/models/profileInfoModel.dart';
// import 'package:mart_up/models/order_model.dart';
// import 'package:mart_up/models/product.dart';
import 'package:http/http.dart' as http;
import 'package:mart_up/environment/environment.dart';
import 'package:mart_up/models/update_email_model.dart';
import 'package:mart_up/models/update_name_model.dart';
import 'package:mart_up/models/update_password_model.dart';
//import 'package:mart_up/models/update_picture_model.dart';
import 'package:mart_up/screens/profileScreen.dart';
//import 'package:mart_up/screens/success_screen.dart';
import 'package:mart_up/widgets/custom_snackbar.dart';

class UpdateUserService {
  static var client = http.Client();

  static Future updateUserImage() async {
    var client = http.Client();

    var apiUrl = Uri.parse("https://demo.baburhut.com/wp-json/wp/v2/media");
    try {
      final response = await client.get(apiUrl,
          //headers: {"Content-Type": "application/json",},
          //responseType: ResponseType.PLAIN
          headers: {"content-type": 'image/jpeg'});

      final responseString = response.body;
      print(responseString);
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => ProfileScreen()));
    } catch (e) {
      print(e);
    }
  }

  Future updateUserName(UpdateNameModel name,int id, context) async {
    var client = http.Client();
    var apiUrl = Uri.parse(
        rootApi+"customers/"+ id.toString() +"?consumer_key="+ consumerKey +
        "&consumer_secret=" +
        consumerSecret);
    try {
      final response = await client.put(
        apiUrl,
        body: jsonEncode(name),
        headers: {"Content-Type": "application/json"},
      );
      final responseString = response.body;
      print(responseString);
      CustomSnackbar.snackbar(context, "Name succesfully changed");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ProfileScreen()));
    } catch (e) {
      print(e);
    }
  }

  Future updateUserEmail(UpdateEmailModel email, int id, context) async {
    var client = http.Client();
    var apiUrl = Uri.parse(
      rootApi+"customers/"+ id.toString() +"?consumer_key="+ consumerKey +
        "&consumer_secret=" +
        consumerSecret
        );
    try {
      final response = await client.put(
        apiUrl,
        body: jsonEncode(email),
        headers: {"Content-Type": "application/json"},
      );
      final responseString = response.body;
      print(responseString);
      CustomSnackbar.snackbar(context, "Email succesfully changed");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ProfileScreen()));
    } catch (e) {
      print(e);
    }
  }

  Future updateUserPassword(UpdatePasswordModel password,int id, context) async {
    var client = http.Client();
    var apiUrl = Uri.parse(
      rootApi+"customers/"+ id.toString() +"?consumer_key="+ consumerKey +
        "&consumer_secret=" +
        consumerSecret
       );
    try {
      final response = await client.put(
        apiUrl,
        body: jsonEncode(password),
        headers: {"Content-Type": "application/json"},
      );
      final responseString = response.body;
      print(responseString);
      CustomSnackbar.snackbar(context, "password succesfully changed");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ProfileScreen()));
    } catch (e) {
      print(e);
    }
  }

// static Future<List<OrderModel>> createOrder(var payment,List<dynamic> listItem,List<dynamic> shiping)async{

}
