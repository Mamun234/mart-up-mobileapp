import 'dart:convert';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mart_up/environment/environment.dart';
//import 'package:mart_up/models/order_model.dart';
import 'package:mart_up/models/user_model.dart';
//import 'package:mart_up/screens/loginScreen.dart';
//import 'package:mart_up/screens/success_screen.dart';
import 'package:http/http.dart' as http;
//import 'package:mart_up/widgets/navigation_bar.dart';

class UserService {
  // static var http;
 static var client = http.Client();
  Future<Response?> createUser(UserModel userCreate) async {
    print('amar data:');
    print(jsonEncode(userCreate.toJson()));

    var apiUrl = Uri.parse(rootApi +
        "customers?consumer_key=" +
        consumerKey +
        "&consumer_secret=" +
        consumerSecret);
//final response = await client.get(apiUrl);

    try {
     
      final response = await client.post(
        apiUrl,
        body: jsonEncode(userCreate.toJson()),
        headers: {"Content-Type": "application/json"},
      );
      final responseString = response.body;
      print(responseString);
      return response;
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => LoginScreen()));
    } catch (e) {
      print(e);
    }
  }


}
