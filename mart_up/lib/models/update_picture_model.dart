// To parse this JSON data, do
//
//     final updatePhotoModel = updatePhotoModelFromJson(jsonString);

import 'dart:convert';

List<UpdatePhotoModel> updatePhotoModelFromJson(String str) => List<UpdatePhotoModel>.from(json.decode(str).map((x) => UpdatePhotoModel.fromJson(x)));

String updatePhotoModelToJson(List<UpdatePhotoModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UpdatePhotoModel {
    UpdatePhotoModel({
        this.sourceUrl,
    });

    String? sourceUrl;

    factory UpdatePhotoModel.fromJson(Map<String, dynamic> json) => UpdatePhotoModel(
        sourceUrl: json["source_url"],
    );

    Map<String, dynamic> toJson() => {
        "source_url": sourceUrl,
    };
}
