// To parse this JSON data, do
//
//     final profileInfoModel = profileInfoModelFromJson(jsonString);

import 'dart:convert';

List<ProfileInfoModel> profileInfoModelFromJson(String str) => List<ProfileInfoModel>.from(json.decode(str).map((x) => ProfileInfoModel.fromJson(x)));

String profileInfoModelToJson(List<ProfileInfoModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProfileInfoModel {
    ProfileInfoModel({
        this.id,
        this.email,
        this.firstName,
        this.username,
        this.avatarUrl,
    });

    int? id;
    String? email;
    String? firstName;
    String? username;
    String? avatarUrl;

    factory ProfileInfoModel.fromJson(Map<String, dynamic> json) => ProfileInfoModel(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        username: json["username"],
        avatarUrl: json["avatar_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "username": username,
        "avatar_url": avatarUrl,
    };
}
