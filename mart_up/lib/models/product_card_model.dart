// To parse this JSON data, do
//
//     final productCardModel = productCardModelFromJson(jsonString);

import 'dart:convert';

List<ProductCardModel> productCardModelFromJson(String str) => List<ProductCardModel>.from(json.decode(str).map((x) => ProductCardModel.fromJson(x)));

String productCardModelToJson(List<ProductCardModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductCardModel {
    ProductCardModel({
        this.id,
        this.name,
        this.price,
        this.image,
    });

    int? id;
    String? name;
    int? price;
    String? image;

    factory ProductCardModel.fromJson(Map<String, dynamic> json) => ProductCardModel(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "image": image,
    };
}
