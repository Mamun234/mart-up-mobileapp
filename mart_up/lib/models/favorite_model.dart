// To parse this JSON data, do
//
//     final favoriteModel = favoriteModelFromJson(jsonString);

import 'dart:convert';

List<FavoriteModel> favoriteModelFromJson(String str) => List<FavoriteModel>.from(json.decode(str).map((x) => FavoriteModel.fromJson(x)));

String favoriteModelToJson(List<FavoriteModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FavoriteModel {
    FavoriteModel({
        this.id,
        this.name,
        this.salePrice,
        this.regularPrice,
        this.quantity,
        this.img,
    });

    int? id;
    String? name;
    String? salePrice;
    String? regularPrice;
    int? quantity;
    String? img;

    factory FavoriteModel.fromJson(Map<String, dynamic> json) => FavoriteModel(
        id: json["id"],
        name: json["name"],
        salePrice: json["salePrice"],
        regularPrice: json['regularPrice'],
        quantity: json["quantity"],
        img: json["img"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "salePrice": salePrice,
        "regularPrice":regularPrice,
        "quantity": quantity,
        "img": img,
    };
}
