// To parse this JSON data, do
//
//     final orderModel = orderModelFromJson(jsonString);
import 'dart:convert';

List<OrderModel> orderModelFromJson(String str) =>
    List<OrderModel>.from(json.decode(str).map((x) => OrderModel.fromJson(x)));
String orderModelToJson(List<OrderModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderModel {
  OrderModel(
      {this.paymentMethod,
      this.paymentMethodTitle,
      this.setPaid,
      this.shipping,
      this.lineItems,
      this.couponLines,
      this.customerId});
  String? paymentMethod;
  String? paymentMethodTitle;
  bool? setPaid;
  Shipping? shipping;
  List<LineItem>? lineItems;
  List<CouponLine>? couponLines;
  int? customerId;
  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        paymentMethod: json["payment_method"],
        paymentMethodTitle: json["payment_method_title"],
        setPaid: json["set_paid"],
        shipping: Shipping.fromJson(json["shipping"]),
        lineItems: List<LineItem>.from(
            json["line_items"].map((x) => LineItem.fromJson(x))),
        couponLines: List<CouponLine>.from(
            json["coupon_lines"].map((x) => CouponLine.fromJson(x))),
        customerId: json["customer_id"],
      );
  Map<String, dynamic> toJson() => {
        "payment_method": paymentMethod,
        "payment_method_title": paymentMethodTitle,
        "set_paid": setPaid,
        "shipping": shipping!.toJson(),
        "line_items": List<dynamic>.from(lineItems!.map((x) => x.toJson())),
        "coupon_lines": List<dynamic>.from(couponLines!.map((x) => x.toJson())),
        "customer_id": customerId,
      };
//extra add coupone null value
      Map<String, dynamic> toJsonWithoutCoupon() => {
        "payment_method": paymentMethod,
        "payment_method_title": paymentMethodTitle,
        "set_paid": setPaid,
        "shipping": shipping!.toJson(),
        "line_items": List<dynamic>.from(lineItems!.map((x) => x.toJson())),
        "customer_id": customerId,
      };
}

class LineItem {
  LineItem({this.productId, this.quantity, this.variation_id});
  int? productId;
  int? quantity;
  int? variation_id;
  factory LineItem.fromJson(Map<String, dynamic> json) => LineItem(
      productId: json["product_id"],
      quantity: json["quantity"],
      variation_id: json["variation_id"]);
  Map<String, dynamic> toJson() => {
        "product_id": productId,
        "quantity": quantity,
        "variation_id": variation_id
      };
}

class CouponLine {
  CouponLine({
    this.code,
  });

  String? code;

  factory CouponLine.fromJson(Map<String, dynamic> json) => CouponLine(
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
      };
}

class Shipping {
  Shipping(
      {this.firstName,
      this.lastName,
      this.address1,
      this.address2,
      this.city,
      this.state,
      this.postcode,
      this.country,
      this.phone});
  String? firstName;
  String? lastName;
  String? address1;
  String? address2;
  String? city;
  String? state;
  String? postcode;
  String? country;
  String? phone;
  factory Shipping.fromJson(Map<String, dynamic> json) => Shipping(
        firstName: json["first_name"],
        lastName: json["last_name"],
        address1: json["address_1"],
        address2: json["address_2"],
        city: json["city"],
        state: json["state"],
        postcode: json["postcode"],
        country: json["country"],
      );
  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "address_1": address1,
        "address_2": address2,
        "city": city,
        "state": state,
        "postcode": postcode,
        "country": country,
      };
}
