// To parse this JSON data, do
//
//     final subCateoryCardModel = subCateoryCardModelFromJson(jsonString);

import 'dart:convert';

List<SubCateoryCardModel> subCateoryCardModelFromJson(String str) => List<SubCateoryCardModel>.from(json.decode(str).map((x) => SubCateoryCardModel.fromJson(x)));

String subCateoryCardModelToJson(List<SubCateoryCardModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SubCateoryCardModel {
    SubCateoryCardModel({
       required this.id,
       required this.name,
       required this.image,
    });

    int id;
    String name;
    String image;

    factory SubCateoryCardModel.fromJson(Map<String, dynamic> json) => SubCateoryCardModel(
        id: json["id"],
        name: json["name"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
    };
}
