// To parse this JSON data, do
//
//     final updateNameModel = updateNameModelFromJson(jsonString);

import 'dart:convert';

List<UpdateNameModel> updateNameModelFromJson(String str) => List<UpdateNameModel>.from(json.decode(str).map((x) => UpdateNameModel.fromJson(x)));

String updateNameModelToJson(List<UpdateNameModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UpdateNameModel {
    UpdateNameModel({
        //this.id,
        this.firstName,
        //this.username,
    });

    //int? id;
    String? firstName;
    //String? username;

    factory UpdateNameModel.fromJson(Map<String, dynamic> json) => UpdateNameModel(
        //id: json["id"],
        firstName: json["first_name"],
        //username: json["username"],
    );

    Map<String, dynamic> toJson() => {
       // "id": id,
        "first_name": firstName,
        //"username": username,
    };
}
