// // To parse this JSON data, do
// //
// //     final profileUpdateModel = profileUpdateModelFromJson(jsonString);

// import 'dart:convert';

// import 'dart:io';

// import 'package:image_picker/image_picker.dart';

// List<PhotoUploadeModel> profileUpdateModelFromJson(String str) =>
//     List<PhotoUploadeModel>.from(
//         json.decode(str).map((x) => PhotoUploadeModel.fromJson(x)));

// String profileUpdateModelToJson(List<PhotoUploadeModel> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class PhotoUploadeModel {
//   PhotoUploadeModel({this.image});

//   XFile? image;

//   factory PhotoUploadeModel.fromJson(Map<String, dynamic> json) =>
//       PhotoUploadeModel(
//         image: json["image"],
//       );

//   Map<String, dynamic> toJson() => {
//         "image": image,
//       };
// }
