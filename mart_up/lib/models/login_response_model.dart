// To parse this JSON data, do
//
//     final loginResponseModel = loginResponseModelFromJson(jsonString);

import 'dart:convert';

List<LoginResponseModel> loginResponseModelFromJson(String str) => List<LoginResponseModel>.from(json.decode(str).map((x) => LoginResponseModel.fromJson(x)));

String loginResponseModelToJson(List<LoginResponseModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LoginResponseModel {
    LoginResponseModel({
        this.email,
        this.password,
        this.success,
        this.statusCode,
        this.code,
        this.message,
        this.data,
    });
    String? email;
    String? password;
    bool? success;
    int? statusCode;
    String? code;
    String? message;
    Data? data;

    factory LoginResponseModel.fromJson(Map<String, dynamic> json) => LoginResponseModel(
        email: json["email"],
        password: json["password"],
        success: json["success"],
        statusCode: json["statusCode"],
        code: json["code"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "email":email,
        "password":password,
        "success": success,
        "statusCode": statusCode,
        "code": code,
        "message": message,
        "data": data!.toJson(),
        
    };
   
    
}

class Data {
    Data({
        this.token,
        this.id,
        this.email,
        this.nicename,
        this.firstName,
        this.lastName,
        this.displayName,
    });

    String? token;
    int? id;
    String? email;
    String? nicename;
    String? firstName;
    String? lastName;
    String? displayName;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        token: json["token"],
        id: json["id"],
        email: json["email"],
        nicename: json["nicename"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        displayName: json["displayName"],
    );

    Map<String, dynamic> toJson() => {
        "token": token,
        "id": id,
        "email": email,
        "nicename": nicename,
        "firstName": firstName,
        "lastName": lastName,
        "displayName": displayName,
    };
    
    
}
