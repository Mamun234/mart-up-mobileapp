// // To parse this JSON data, do
// //
// //     final loginCommonModel = loginCommonModelFromJson(jsonString);

// import 'dart:convert';

// List<LoginCommonModel> loginCommonModelFromJson(String str) => List<LoginCommonModel>.from(json.decode(str).map((x) => LoginCommonModel.fromJson(x)));

// String loginCommonModelToJson(List<LoginCommonModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class LoginCommonModel {
//     LoginCommonModel({
//         this.statusCode,
//         this.message,
//         this.data,
//     });

//     int? statusCode;
//     String? message;
//     Data? data;

//     factory LoginCommonModel.fromJson(Map<String, dynamic> json) => LoginCommonModel(
//         statusCode: json["statusCode"],
//         message: json["message"],
//         data: Data.fromJson(json["data"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "statusCode": statusCode,
//         "message": message,
//         "data": data!.toJson(),
//     };
// }

// class Data {
//     Data({
//         this.token,
//         this.id,
//         this.email,
//         this.nicename,
//         this.firstName,
//         this.lastName,
//         this.displayName,
//     });

//     String? token;
//     int? id;
//     String? email;
//     String? nicename;
//     String? firstName;
//     String? lastName;
//     String? displayName;

//     factory Data.fromJson(Map<String, dynamic> json) => Data(
//         token: json["token"],
//         id: json["id"],
//         email: json["email"],
//         nicename: json["nicename"],
//         firstName: json["firstName"],
//         lastName: json["lastName"],
//         displayName: json["displayName"],
//     );

//     Map<String, dynamic> toJson() => {
//         "token": token,
//         "id": id,
//         "email": email,
//         "nicename": nicename,
//         "firstName": firstName,
//         "lastName": lastName,
//         "displayName": displayName,
//     };
// }
