// To parse this JSON data, do
//
//     final reviewModel = reviewModelFromJson(jsonString);

import 'dart:convert';

List<ReviewModel> reviewModelFromJson(String str) => List<ReviewModel>.from(
    json.decode(str).map((x) => ReviewModel.fromJson(x)));

String reviewModelToJson(List<ReviewModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ReviewModel {
  ReviewModel({
    // this.id,
    this.dateCreated,
    this.productId,
    this.reviewer,
    this.review,
    this.reviewerEmail,
    this.rating,
    
    //this.reviewerAvatarUrls,
  });

  // int? id;
  DateTime? dateCreated;
  int? productId;
  String? reviewer;
  String? review;
  String? reviewerEmail;
  int? rating;
  // Map<String, String>? reviewerAvatarUrls;

  factory ReviewModel.fromJson(Map<String, dynamic> json) => ReviewModel(
        // id: json["id"],
        dateCreated: DateTime.parse(json["date_created"]),
        productId: json["product_id"],
        reviewer: json["reviewer"],
        reviewerEmail: json["reviewer_email"],
        review: json["review"],
        rating: json["rating"],
        // reviewerAvatarUrls: Map.from(json["reviewer_avatar_urls"])
        //     .map((k, v) => MapEntry<String, String>(k, v)),
      );

  Map<String, dynamic> toJson() => {
        // "id": id,
        "date_created": dateCreated?.toIso8601String(),
        "product_id": productId,
        "reviewer": reviewer,
        "reviewer_email": reviewerEmail,
        "review": review,
        "rating": rating,
        // "reviewer_avatar_urls": Map.from(reviewerAvatarUrls!)
        //     .map((k, v) => MapEntry<String, dynamic>(k, v)),
      };
}
