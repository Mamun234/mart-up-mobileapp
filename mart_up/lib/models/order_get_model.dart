
import 'dart:convert';

List<OrderGetModel> orderGetModelFromJson(String str) =>
    List<OrderGetModel>.from(
        json.decode(str).map((x) => OrderGetModel.fromJson(x)));

String orderModelGetToJson(List<OrderGetModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderGetModel {
  OrderGetModel({
    this.id,
    this.status,
    this.total,
    this.dateCreated,
    this.paymentMethod,
    this.paymentMethodTitle,
    this.setPaid,
    this.shipping,
    this.lineItems,
    this.couponLines,
  });
  int? id;
  String? status;
  String? total;
  String? dateCreated;
  String? paymentMethod;
  String? paymentMethodTitle;
  bool? setPaid;
  Shipping? shipping;
  List<LineItem>? lineItems;
  List<CouponLine>? couponLines;

  factory OrderGetModel.fromJson(Map<String, dynamic> json) => OrderGetModel(
        id: json["id"],
        status: json["status"],
        total: json["total"],
        dateCreated: json['date_created'],
        paymentMethod: json["payment_method"],
        paymentMethodTitle: json["payment_method_title"],
        setPaid: json["set_paid"],
        shipping: Shipping.fromJson(json["shipping"]),
        lineItems: List<LineItem>.from(
            json["line_items"].map((x) => LineItem.fromJson(x))),
        couponLines: List<CouponLine>.from(
            json["coupon_lines"].map((x) => CouponLine.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "status": status,
        "total": total,
        "date_created": dateCreated,
        "payment_method": paymentMethod,
        "payment_method_title": paymentMethodTitle,
        "set_paid": setPaid,
        "shipping": shipping!.toJson(),
        "line_items": List<dynamic>.from(lineItems!.map((x) => x.toJson())),
        "coupon_lines": List<dynamic>.from(couponLines!.map((x) => x.toJson())),
      };
}

class LineItem {
  LineItem({this.productId, this.quantity, this.variation_id});

  int? productId;
  int? quantity;
  int? variation_id;

  factory LineItem.fromJson(Map<String, dynamic> json) => LineItem(
      productId: json["product_id"],
      quantity: json["quantity"],
      variation_id: json["variation_id"]);

  Map<String, dynamic> toJson() => {
        "product_id": productId,
        "quantity": quantity,
        "variation_id": variation_id
      };
}

class CouponLine {
  CouponLine({
    this.code,
    this.discount,
  });

  String? code;
  String? discount;

  factory CouponLine.fromJson(Map<String, dynamic> json) => CouponLine(
        code: json["code"],
        discount: json["discount"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "discount": discount,
      };
}

class Shipping {
  Shipping(
      {this.firstName,
      this.lastName,
      this.address1,
      this.address2,
      this.city,
      this.state,
      this.postcode,
      this.country,
      this.phone});

  String? firstName;
  String? lastName;
  String? address1;
  String? address2;
  String? city;
  String? state;
  String? postcode;
  String? country;
  String? phone;

  factory Shipping.fromJson(Map<String, dynamic> json) => Shipping(
        firstName: json["first_name"],
        lastName: json["last_name"],
        address1: json["address_1"],
        address2: json["address_2"],
        city: json["city"],
        state: json["state"],
        postcode: json["postcode"],
        country: json["country"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "address_1": address1,
        "address_2": address2,
        "city": city,
        "state": state,
        "postcode": postcode,
        "country": country,
      };
}
