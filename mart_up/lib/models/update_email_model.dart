// To parse this JSON data, do
//
//     final updateEmailModel = updateEmailModelFromJson(jsonString);

import 'dart:convert';

List<UpdateEmailModel> updateEmailModelFromJson(String str) => List<UpdateEmailModel>.from(json.decode(str).map((x) => UpdateEmailModel.fromJson(x)));

String updateEmailModelToJson(List<UpdateEmailModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UpdateEmailModel {
    UpdateEmailModel({
        this.email,
    });

    String? email;

    factory UpdateEmailModel.fromJson(Map<String, dynamic> json) => UpdateEmailModel(
        email: json["email"],
    );

    Map<String, dynamic> toJson() => {
        "email": email,
    };
}
