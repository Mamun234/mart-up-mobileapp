// To parse this JSON data, do
//
//     final updatePasswordModel = updatePasswordModelFromJson(jsonString);

import 'dart:convert';

List<UpdatePasswordModel> updatePasswordModelFromJson(String str) => List<UpdatePasswordModel>.from(json.decode(str).map((x) => UpdatePasswordModel.fromJson(x)));

String updatePasswordModelToJson(List<UpdatePasswordModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UpdatePasswordModel {
    UpdatePasswordModel({
        this.password,
    });

    String? password;

    factory UpdatePasswordModel.fromJson(Map<String, dynamic> json) => UpdatePasswordModel(
        password: json["password"],
    );

    Map<String, dynamic> toJson() => {
        "password": password,
    };
}
