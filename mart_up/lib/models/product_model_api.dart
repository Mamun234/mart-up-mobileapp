import 'dart:convert';

List< ProductModelApi> productFromJson(String str) => List< ProductModelApi>.from(json.decode(str).map((x) =>  ProductModelApi.fromJson(x)));

String productToJson(List< ProductModelApi> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductModelApi {
     ProductModelApi({
        this.id,
        this.name,
        this.description,
    });

    int? id;
    String? name;
    String? description;

    factory  ProductModelApi.fromJson(Map<String, dynamic> json) =>  ProductModelApi(
        id: json["id"],
        name: json["name"],
        description: json["description"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
    };
}