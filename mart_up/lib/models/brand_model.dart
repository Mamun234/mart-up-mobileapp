// To parse this JSON data, do
//
//     final brandModel = brandModelFromJson(jsonString);

import 'dart:convert';

List<BrandModel> brandModelFromJson(String str) => List<BrandModel>.from(json.decode(str).map((x) => BrandModel.fromJson(x)));

String brandModelToJson(List<BrandModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BrandModel {
    BrandModel({
        this.id,
        this.name,
        this.termThumbnailImage,
    });

    int? id;
    String? name;
    TermThumbnailImage? termThumbnailImage;

    factory BrandModel.fromJson(Map<String, dynamic> json) => BrandModel(
        id: json["id"],
        name: json["name"],
        termThumbnailImage: TermThumbnailImage.fromJson(json["term_thumbnail_image"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "term_thumbnail_image": termThumbnailImage?.toJson(),
    };
}

class TermThumbnailImage {
    TermThumbnailImage({
        this.url,
       
    });

    String? url;
   

    factory TermThumbnailImage.fromJson(Map<String, dynamic> json) => TermThumbnailImage(
        url: json["url"],
      
    );

    Map<String, dynamic> toJson() => {
        "url": url,
       
    };
}
