// To parse this JSON data, do
//
//     final couponModel = couponModelFromJson(jsonString);

import 'dart:convert';

List<CouponModel> couponModelFromJson(String str) => List<CouponModel>.from(json.decode(str).map((x) => CouponModel.fromJson(x)));

String couponModelToJson(List<CouponModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CouponModel {
    CouponModel({
        this.id,
        this.code,
        this.amount,
    });

    int? id;
    String? code;
    String? amount;

    factory CouponModel.fromJson(Map<String, dynamic> json) => CouponModel(
        id: json["id"],
        code: json["code"],
        amount: json["amount"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "amount": amount,
    };
}
