// To parse this JSON data, do
//
//     final cartModel = cartModelFromJson(jsonString);

import 'dart:convert';

List<CartModel> cartModelFromJson(String str) => List<CartModel>.from(json.decode(str).map((x) => CartModel.fromJson(x)));

String cartModelToJson(List<CartModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CartModel {
    CartModel({
        this.id,
        this.name,
        this.salePrice,
        this.regularPrice,
        this.quantity,
        this.img,
        // this.size,
        // this.color,
    });

    int? id;
    String? name;
    String? salePrice;
    String? regularPrice;
    int? quantity;
    String? img;
    // String? size;
    // String? color;

    factory CartModel.fromJson(Map<String, dynamic> json) => CartModel(
        id: json["id"],
        name: json["name"],
        salePrice: json["salePrice"],
        regularPrice: json["regularPrice"],
        quantity: json["quantity"],
        img: json["img"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "salePrice": salePrice,
        "regularPrice": regularPrice,
        "quantity": quantity,
        "img": img,
    };
}
