import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mart_up/constants/colors.dart';
import 'package:mart_up/providers/product_provider_api.dart';
import 'package:mart_up/providers/view_model_provider.dart';
// import 'package:mart_up/screens/add_address.dart';
// import 'package:mart_up/screens/brandScreen.dart';
// import 'package:mart_up/screens/cart_screen.dart';
// import 'package:mart_up/screens/category_screen.dart';
// import 'package:mart_up/screens/comapre_screen.dart';
// import 'package:mart_up/screens/filter_screen.dart';
// import 'package:mart_up/screens/loginScreen.dart';
// import 'package:mart_up/screens/payment_screen.dart';
// import 'package:mart_up/screens/product_details_screen.dart';
// import 'package:mart_up/screens/registerScreen.dart';
// import 'package:mart_up/screens/home_screen.dart';
// import 'package:mart_up/screens/search_result_screen.dart';
// import 'package:mart_up/screens/search_screen.dart';
// import 'package:mart_up/screens/success_screen.dart';
import 'package:mart_up/screens/splash_screen.dart';
// import 'package:mart_up/screens/to_ship.dart';
// import 'package:mart_up/widgets/Alart_dialog.dart';
import 'package:provider/provider.dart';
// import 'widgets/navigation_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      //systemNavigationBarColor: seeMoreColor,
      statusBarColor: tabSelectColor,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
      splitScreenMode: true,
     
      builder: () => MultiProvider(
        providers: [
          ChangeNotifierProvider<ViewModelProvider>.value(
            value: ViewModelProvider(),
          ),
          ChangeNotifierProvider<ProductProviderApi>.value(
            value: ProductProviderApi(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Mart-up',
          theme: ThemeData(
           
            primarySwatch: Colors.blue,
          ),
          home: SplashScreen(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
